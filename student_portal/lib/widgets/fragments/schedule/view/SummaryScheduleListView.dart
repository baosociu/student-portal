import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/SummarySchedule.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/TextView.dart';

// ignore: must_be_immutable
class SummaryScheduleListView extends StatelessWidget {
  final GroupSummaryScheduleByDayList scheduleList;
  BuildContext context;

  SummaryScheduleListView(this.scheduleList, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return new Flexible(
        child: new ListView.builder(
            shrinkWrap: true,
            itemCount: this.scheduleList.items.length,
            itemBuilder: (context, index) {
              GroupSummaryScheduleByDay item = this.scheduleList.items[index];

              return new Column(
                children: <Widget>[
                  new Container(
                    child: new ListTile(
                      title: buildTitle(index, item),
                      subtitle: buildDetailSchedules(item.items),
                    ),
                  ),
                  index < this.scheduleList.items.length - 1
                      ? new Divider(color: Singleton.setting.primaryColor)
                      : new SizedBox()
                ],
              );
            }));
  }

  Widget buildTitle(int index, GroupSummaryScheduleByDay item) {
    Color color = Singleton.setting.primaryColor;
    Color color20 = Singleton.setting.primary20OpacityColor;

    return new Container(
      child: new Row(
        children: <Widget>[
          new Padding(
            padding: new EdgeInsets.all(4.0),
            child: new CircleAvatar(
              child: new TextView.leading(item.labelDayOfWeekShortest),
              backgroundColor: color,
            ),
          ),
          new Flexible(
              child: new ListView.builder(
                  itemCount: 2,
                  shrinkWrap: true,
                  primary: false,
                  itemBuilder: (context, index) {
                    return index == 0
                        ? buildHeaderSchedule(item)
                        : buildCountScheduleDay(
                            Singleton.setting.secondColor, item.items.length);
                  })),
        ],
      ),
      decoration: new BoxDecoration(
          gradient: new LinearGradient(
              tileMode: TileMode.clamp,
              colors: <Color>[Colors.transparent, color20, color20])),
    );
  }

  Widget buildHeaderSchedule(GroupSummaryScheduleByDay item) {
    return new TextView.title(
      item.labelDayOfWeek,
      overflow: TextOverflow.ellipsis,
    );
  }

  Widget buildCountScheduleDay(Color color, int count) {
    int countDayOfWeek = 7;
    double height = 5.0;
    List<Widget> widgets = new List();
    for (int i = 0; i < countDayOfWeek; i++)
      widgets.add(new Flexible(
        child: new SizedBox(
          height: height,
          child: new Container(
            decoration: new BoxDecoration(
                gradient: new LinearGradient(colors: <Color>[
              i < count ? color : Colors.transparent,
              Colors.transparent
            ])),
          ),
        ),
      ));
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: widgets,
    );
  }

  Widget buildDetailSchedules(List<SummarySchedule> items) {
    return new ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: items.length,
        itemBuilder: (context, index) {
          SummarySchedule item = items[index];
          return buildItemBySummarySchedule(item);
        });
  }

  Widget buildItemBySummarySchedule(SummarySchedule summarySchedule) {
    return new Container(
      padding: new EdgeInsets.all(4.0),
      child: new ListTile(
          onTap: () {
            ToastUtils.showToastSummarySchedule(context, summarySchedule);
          },
          title: new TextView.text(
            summarySchedule.nameSubject.toString(),
            fontWeight: FontWeight.bold,
          ),
          subtitle: buildDetailNormalSchedule(summarySchedule)),
    );
  }

  Widget buildDetailNormalSchedule(SummarySchedule summarySchedule) {
    return new TableView(
      <Widget>[
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.period + ": ",
            summarySchedule.time.toString() +
                " -> " +
                (summarySchedule.time + summarySchedule.countTime - 1)
                    .toString()),
        ViewUtils.buildItemTextView(Singleton.setting.localization.week + ": ",
            summarySchedule.week.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.room + ": ",
            summarySchedule.nameRoom.toString() +
                " - " +
                summarySchedule.nameBranch.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.credit + ": ",
            summarySchedule.weight.toString()),
      ],
      margin: 0.0,
    );
  }
}
