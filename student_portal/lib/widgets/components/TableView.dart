import 'package:flutter/material.dart';

class TableView extends StatelessWidget {
  final List<Widget> values;
  final double margin;
  final double marginLeft;
  final double marginRight;
  final double marginTop;
  final double marginBottom;

  TableView(
    this.values, {
    Key key,
    this.margin: -1.0,
    this.marginLeft: -1.0,
    this.marginRight: -1.0,
    this.marginTop: -1.0,
    this.marginBottom: -1.0,
  });

  final double defaultMargin = 4.0;
  final double zero = 0.0;

  @override
  Widget build(BuildContext context) {
    return new Flexible(
      child: new Container(
          margin: this.margin == -1.0
              ? (this.marginLeft == -1.0 &&
                      this.marginRight == -1.0 &&
                      this.marginTop == -1.0 &&
                      this.marginBottom == -1.0
                  ? new EdgeInsets.all(defaultMargin)
                  : new EdgeInsets.only(
                      top: marginTop == -1.0 ? zero : marginTop,
                      bottom: marginBottom == -1.0 ? zero : marginBottom,
                      left: marginLeft == -1.0 ? zero : marginLeft,
                      right: marginRight == -1.0 ? zero : marginRight))
              : new EdgeInsets.all(this.margin),
          child: new Column(children: this.values)),
    );
  }
}
