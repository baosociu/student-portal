import 'package:student_portal/global/Singleton.dart';

class Receipt {
  String dateReceipt;
  dynamic idStudent;
  String note;
  dynamic idSemester;
  String nameSemester;
  String dateCreate;
  String code;
  int type;
  dynamic id;
  int totalPrice;
  String typeString;

  Receipt.fromJson(Map json) {
    this.dateReceipt = json['ngayNop'];
    this.idStudent = json['hocVienId'];
    this.note = json['ghiChu'];
    this.idSemester = json['hocKyId'];
    this.nameSemester = json['tenHocKy'];
    this.dateCreate = json['ngayTao'];
    this.code = json['ma'];
    this.type = json['loaiPT'] == null ? 1 : json['loaiPT'];
    this.id = json['id'];
    this.totalPrice = json['soTien'];

    this.typeString = this.type == 1 ? Singleton.setting.localization.pay : Singleton.setting.localization.must_pay;
  }
}

class ReceiptList {
  List<Receipt> list = new List<Receipt>();

  ReceiptList.fromJson(dynamic data) {
    list = new List<Receipt>();

    data.forEach((json) {
      Receipt value = new Receipt.fromJson(json);
      list.add(value);
    });
  }
}

class ReceiptSemesterItem {
  String dateReceipt;
  dynamic idStudent;
  String note;
  String dateCreate;
  String code;
  int type;
  dynamic id;
  int totalPrice;
  String typeString;

  ReceiptSemesterItem.from(Receipt item) {
    this.dateReceipt = item.dateReceipt;
    this.idStudent = item.idStudent;
    this.note = item.note;
    this.dateCreate = item.dateCreate;
    this.code = item.code;
    this.type = item.type;
    this.id = item.id;
    this.totalPrice = item.totalPrice;
    this.typeString = item.typeString;
  }
}

class ReceiptSemester {
  dynamic idSemester;
  String nameSemester;
  int totalPriceReceipt;
  int totalPriceSemesterReceipt;
  int totalPriceRemainReceipt;

  List<ReceiptSemesterItem> list = new List<ReceiptSemesterItem>();

  ReceiptSemester.from(Receipt item) {
    list = new List<ReceiptSemesterItem>();

    this.idSemester = item.idSemester;
    this.nameSemester = item.nameSemester;
    this.totalPriceReceipt = 0; //tiền đã đóng
    this.totalPriceSemesterReceipt = 0; //tiền phải đóng
    this.totalPriceRemainReceipt = 0; //tiền còn lại phải đóng
  }

  void add(ReceiptSemesterItem item) {
    //todo chỉ add phiếu thanh toán của học viên
    if (item.type == 1) {
      list.add(item);
      totalPriceReceipt += item.totalPrice;
    } else
      totalPriceSemesterReceipt += item.totalPrice;
    this.totalPriceRemainReceipt += item.totalPrice;
  }
}

class ReceiptSemesterList {
  List<ReceiptSemester> list = new List<ReceiptSemester>();

  ReceiptSemesterList.fromJson(dynamic data) {
    list = new List<ReceiptSemester>();

    ReceiptList receiptList = new ReceiptList.fromJson(data);

    receiptList.list.forEach((receipt) {
      int i = 0;
      for (i = 0; i < list.length; i++)
        if (list[i].idSemester == receipt.idSemester) break;
      if (i == list.length) list.add(new ReceiptSemester.from(receipt));
      list[i].add(new ReceiptSemesterItem.from(receipt));
    });
  }
}
