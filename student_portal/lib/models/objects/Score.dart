

class Score {
  double reTest1;
  double reTest2;
  dynamic idStudent;
  dynamic idClassSubject;
  double middleScore;
  double totalScore;
  double middleRate;
  double weightPractice;
  double weight;
  dynamic idSemester;
  double finalScore;
  double finalRate;
  String codeSubject;
  double processScore;
  double processRate;
  String nameSubject;
  dynamic idSubject;
  double weightTheory;


  Score.from(Map json){
    this.reTest1 = json['diemThiLai1'];
    this.reTest2 = json['diemThiLai2'];
    this.idStudent = json['hocVienId'];
    this.idClassSubject = json['lopMonHocId'];
    this.middleScore = json['diemGK'];
    this.totalScore = json['tongdiem'];
    this.middleRate = json['phanTramGK'];
    this.weightPractice = json['soTinChiTH'];
    this.weight = json['soTinChi'];
    this.idSemester = json['hocKyId'];
    this.finalScore = json['diemCK'];
    this.finalRate = json['phanTramCK'];
    this.codeSubject = json['maMonHoc'];
    this.processScore = json['diemCC'];
    this.processRate = json['phanTramCC'];
    this.nameSubject = json['tenMonHoc'];
    this.idSubject = json['monHocId'];
    this.weightTheory = json['soTinChiLT'];
  }
}

class ScoreList {
  List<Score> list = new List<Score>();

  ScoreList.fromJson(dynamic listItems){
    list = new List<Score>();
    listItems.forEach((json){
      Score score = new Score.from(json);
      list.add(score);
    });
  }
}

