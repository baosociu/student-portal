import 'dart:async';
import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/OpacityLoading.dart';
import 'package:student_portal/widgets/components/TextView.dart';

// ignore: must_be_immutable
class RoundButton extends StatefulWidget {
  RoundButton(this.labelText,
      {Key key,
      this.opacity: 1.0,
      this.onTap,
      Color colorText,
      Color color,
      this.padding: 6.0,
      this.margin: 4.0,
      this.isLoading})
      : this.color = color ?? Singleton.textDarkColor,
        this.colorText = colorText ?? Singleton.textLightColor;

  final String labelText;
  final double opacity;
  final GestureTapCallback onTap;
  final Color color;
  final Color colorText;
  final double padding;
  final double margin;
  final bool isLoading;

  RoundButtonState state;
  @override
  RoundButtonState createState() {
    state = new RoundButtonState(isLoading == null ? false : isLoading);
    return state;
  }
}

class RoundButtonState extends State<RoundButton> {
  bool isLoading;
  List<OpacityLoading> opacities;

  RoundButtonState(this.isLoading);

  @override
  void initState() {
    super.initState();
    this.opacities = new List<OpacityLoading>();
    this.opacities.add(new OpacityLoading(0.0, true));
    this.opacities.add(new OpacityLoading(0.5, true));
    this.opacities.add(new OpacityLoading(1.0, false));
    updateOpacityLoading();
  }

  void setLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  updateOpacityLoading() async {
    if (!this.mounted) return;
    setState(() {
      opacities.forEach((f) {
        f.updateOpacity();
      });
    });
    new Timer(new Duration(microseconds: 20), () {
      updateOpacityLoading();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      child: new Container(
        padding: new EdgeInsets.all(widget.padding),
        margin: new EdgeInsets.all(widget.margin),
        child: new Opacity(
          opacity: widget.opacity,
          child: new Material(
              color: widget.color,
              borderRadius: new BorderRadius.circular(2.0),
              shadowColor: widget.color,
              elevation: 5.0,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    padding: new EdgeInsets.all(widget.padding),
                    margin: new EdgeInsets.all(widget.margin),
                    child: isLoading
                        ? new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              circleOpacity(this.opacities[0].value,
                                  9.0 * (1.0 - this.opacities[0].value)),
                              new Padding(
                                  padding: new EdgeInsets.only(
                                      left: 5.0, right: 5.0),
                                  child: new TextView.text("")),
                              circleOpacity(this.opacities[1].value,
                                  9.0 * this.opacities[1].value),
                              new Padding(
                                  padding: new EdgeInsets.only(
                                      left: 5.0, right: 5.0),
                                  child: new TextView.text("")),
                              circleOpacity(this.opacities[2].value,
                                  9.0 * (1.0 - this.opacities[2].value)),
                            ],
                          )
                        : new TextView.text(
                            widget.labelText,
                            color: widget.colorText,
                          ),
                  )
                ],
              )),
        ),
      ),
      onTap: isLoading
          ? () {}
          : () {
              FocusScope.of(context).requestFocus(new FocusNode());
              widget.onTap();
            },
    );
  }

  Widget circleOpacity(double opacity, double radius) {
    return new Center(
      child: new Opacity(
        opacity: opacity,
        child: new CircleAvatar(
          radius: radius,
          backgroundColor: widget.colorText,
        ),
      ),
    );
  }
}
