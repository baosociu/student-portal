import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/models/utils/Setting.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/components/alert/ChoiceOneFixedHeightAlert.dart';
import 'package:student_portal/widgets/fragments/setting/SettingsFragment.dart';

// ignore: must_be_immutable
class FontSettingView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new FontSettingViewState();
  }
}

class FontSettingViewState extends State<FontSettingView> {
  String navigationFont;
  String titleFont;
  String textFont;
  final List<TextView> valuesDialog = <TextView>[
    new TextView.alertText(FontConstant.FONT_FAMILY_ARIMO,
        fontFamily: FontConstant.FONT_FAMILY_ARIMO),
    new TextView.alertText(FontConstant.FONT_FAMILY_ASAP,
        fontFamily: FontConstant.FONT_FAMILY_ASAP),
    new TextView.alertText(FontConstant.FONT_FAMILY_CUPRUM,
        fontFamily: FontConstant.FONT_FAMILY_CUPRUM),
    new TextView.alertText(FontConstant.FONT_FAMILY_INCONSOLATA,
        fontFamily: FontConstant.FONT_FAMILY_INCONSOLATA),
    new TextView.alertText(FontConstant.FONT_FAMILY_MONTSERRAT,
        fontFamily: FontConstant.FONT_FAMILY_MONTSERRAT),
    new TextView.alertText(FontConstant.FONT_FAMILY_OSWALD,
        fontFamily: FontConstant.FONT_FAMILY_OSWALD),
    new TextView.alertText(FontConstant.FONT_FAMILY_PLAY,
        fontFamily: FontConstant.FONT_FAMILY_PLAY),
    new TextView.alertText(FontConstant.FONT_FAMILY_ROBOTO,
        fontFamily: FontConstant.FONT_FAMILY_ROBOTO),
    new TextView.alertText(FontConstant.FONT_FAMILY_SOURCESANSPRO,
        fontFamily: FontConstant.FONT_FAMILY_SOURCESANSPRO),
  ];
  Map<String, String> subtitles;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    navigationFont =(Singleton.currentFragment as SettingsFragment).state.setting.navigationFont;
    titleFont = (Singleton.currentFragment as SettingsFragment).state.setting.titleFont;
    textFont = (Singleton.currentFragment as SettingsFragment).state.setting.textFont;
    onUpdateSubtitles();
  }

  void onUpdateSubtitles() {
    subtitles = {
      Singleton.setting.localization.navigation_font: navigationFont,
      Singleton.setting.localization.title_font: titleFont,
      Singleton.setting.localization.text_font: textFont,
    };
  }

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: subtitles.length,
      itemBuilder: (BuildContext context, int index) {
        return new InkWell(
            onTap: () async {
              await showDialog<Null>(
                  context: context,
                  barrierDismissible: false,
                  child: new ChoiceOneAlertFixedHeight(valuesDialog,
                      height: 210.0,
                      textTitle: Singleton.setting.localization.select+" " +
                          subtitles.keys.toList()[index].toLowerCase(),
                      textMessage: Singleton.setting.localization.you_need_select_font,
                      textPositiveButton: Singleton.setting.localization.select,
                      textNegativeButton: Singleton.setting.localization.cancel,
                      iconHeader: Icons.font_download,
                      isAutoSelect: true,
                      indexSelected: valuesDialog.indexOf(
                          valuesDialog.asMap().values.firstWhere((TextView t) {
                        return t.style.fontFamily ==
                            subtitles.values.toList()[index];
                      })), onPositiveTap: (int indexSelected) {
                    //todo save font here
                    print(valuesDialog[indexSelected]);
                    setState(() {
                      if (index == 0) {
                        this.navigationFont =
                            valuesDialog[indexSelected].style.fontFamily;
                        (Singleton.currentFragment as SettingsFragment).state.setting.navigationFont = this.navigationFont;
                      } else if (index == 1) {
                        this.titleFont =
                            valuesDialog[indexSelected].style.fontFamily;
                        (Singleton.currentFragment as SettingsFragment).state.setting.titleFont = this.titleFont;
                      } else{
                        this.textFont =
                            valuesDialog[indexSelected].style.fontFamily;
                        (Singleton.currentFragment as SettingsFragment).state.setting.textFont = this.textFont;
                      }
                      onUpdateSubtitles();
                    });
                  }));
            },
            child: new Column(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(8.0),
                  child: new Row(
                    children: <Widget>[
                      new Flexible(
                        child: new Align(
                          child: new TextView.text(
                            subtitles.keys.toList()[index],
                            overflow: TextOverflow.ellipsis,
                          ),
                          alignment: Alignment.centerLeft,
                        ),
                      ),
                      new Align(
                        child: new TextView.text(
                          subtitles.values.toList()[index],
                          fontFamily: subtitles.values.toList()[index],
                          overflow: TextOverflow.ellipsis,
                        ),
                        alignment: Alignment.centerRight,
                      ),
                    ],
                  ),
                ),
                index < subtitles.length - 1 ? new Divider(height: 5.0,) : new SizedBox()
              ],
            ));
      },
    );
  }
}
