import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/models/utils/Theme.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/setting/SettingsFragment.dart';

// ignore: must_be_immutable
class BackgroundSettingView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new BackgroundSettingState();
  }
}

class BackgroundSettingState extends State<BackgroundSettingView> {
  String background;
  @override
  void initState() {
    super.initState();
    this.background = (Singleton.currentFragment as SettingsFragment)
        .state
        .setting
        .background;
  }

  @override
  Widget build(BuildContext context) {
    Map<String, String> subtitles = {
      BackgroundConstant.BACKGROUND_RED: BackgroundConstant
          .getNameBackground(BackgroundConstant.BACKGROUND_RED),
      BackgroundConstant.BACKGROUND_YELLOW: BackgroundConstant
          .getNameBackground(BackgroundConstant.BACKGROUND_YELLOW),
      BackgroundConstant.BACKGROUND_GREEN: BackgroundConstant
          .getNameBackground(BackgroundConstant.BACKGROUND_GREEN),
      BackgroundConstant.BACKGROUND_CYAN: BackgroundConstant
          .getNameBackground(BackgroundConstant.BACKGROUND_CYAN),
      BackgroundConstant.BACKGROUND_BLUE: BackgroundConstant
          .getNameBackground(BackgroundConstant.BACKGROUND_BLUE),
      BackgroundConstant.BACKGROUND_PURPLE: BackgroundConstant
          .getNameBackground(BackgroundConstant.BACKGROUND_PURPLE),
      BackgroundConstant.BACKGROUND_PINK: BackgroundConstant
          .getNameBackground(BackgroundConstant.BACKGROUND_PINK),
    };

    return new ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: subtitles.length,
      itemBuilder: (BuildContext context, int index) {
        return new InkWell(
            onTap: () {
              setState(() {
                this.background = subtitles.keys.toList()[index];
                (Singleton.currentFragment as SettingsFragment)
                    .state
                    .setting
                    .theme = new ThemeColor.byNameBackground(this.background);
              });
            },
            child: new Column(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(8.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Container(
                        margin: new EdgeInsets.only(right: 8.0),
                        child: new SizedBox(
                          height: 20.0,
                          width: 20.0,
                        ),
                        decoration: new BoxDecoration(
                            image: new DecorationImage(
                                fit: BoxFit.cover,
                                image: new AssetImage(
                                    subtitles.keys.toList()[index]))),
                      ),
                      new Flexible(
                        child: new Align(
                          child: new TextView.text(
                            subtitles.values.toList()[index],
                            overflow: TextOverflow.ellipsis,
                          ),
                          alignment: Alignment.centerLeft,
                        ),
                      ),
                      new Align(
                        child: new Icon(
                          BackgroundConstant.getIndexBackground(background) ==
                                  index
                              ? Icons.radio_button_checked
                              : Icons.radio_button_unchecked,
                          color: Singleton.setting.primaryColor,
                        ),
                      )
                    ],
                  ),
                ),
                index < subtitles.length - 1
                    ? new Divider(
                        height: 5.0, color: Singleton.setting.primaryColor)
                    : new SizedBox()
              ],
            ));
      },
    );
  }
}
