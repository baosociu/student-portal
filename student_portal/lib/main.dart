import 'package:flutter/material.dart';
import 'package:student_portal/widgets/StudentPortalWidget.dart';
import 'package:student_portal/widgets/pages/LoginPage.dart';

void main() => runApp(new StudentPortalWidget(
      child: new StudentPortalApp(),
    ));

class StudentPortalApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Student Portal',
      home: new Scaffold(body: new LoginPage()),
    );
  }
}
