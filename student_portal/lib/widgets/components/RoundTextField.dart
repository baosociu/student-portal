import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';

enum TypeShape{ round, square }
// ignore: must_be_immutable
class RoundTextField extends StatefulWidget {
  RoundTextField({
    Key key,
    this.labelText,
    this.prefixIcon,
    this.obscureText,
    this.opacity,
    this.color,
    this.hintText,
    this.textColor,
    this.controller,
    this.onTextChanged,
    this.padding,
    this.typeShape,
  });

  final String hintText;
  final String labelText;
  final IconData prefixIcon;
  final bool obscureText;
  final double opacity;
  final Color color;
  final Color textColor;
  final TextEditingController controller;
  final Function(String) onTextChanged;
  final double padding;
  final TypeShape typeShape;

  RoundTextFieldState state;
  @override
  RoundTextFieldState createState() {
    state = new RoundTextFieldState(
      this.hintText == null ? '' : this.hintText,
      this.labelText,
      this.prefixIcon,
      this.obscureText == null ? false : this.obscureText,
      this.opacity == null ? 1.0 : this.opacity,
      this.color == null ? Singleton.textLightColor : this.color,
      this.textColor == null ? Singleton.textDarkColor : this.textColor,
      this.controller,
      this.onTextChanged,
      this.padding == null ? 10.0 : this.padding,
      this.typeShape == null ? TypeShape.round : this.typeShape,
    );
    return state;
  }

  String get value => state.controller.text;
}

class RoundTextFieldState extends State<RoundTextField> {
  final String hintText;
  final String labelText;
  final IconData prefixIcon;
  final bool obscureText;
  final double opacity;
  final Color color;
  final Color textColor;
  final Function(String) onTextChanged;
  final double padding;
  final TypeShape typeShape;
  TextEditingController controller;
  String text;

  RoundTextFieldState(
    this.hintText,
    this.labelText,
    this.prefixIcon,
    this.obscureText,
    this.opacity,
    this.color,
    this.textColor,
    this.controller,
    this.onTextChanged,
    this.padding,
      this.typeShape,
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (this.controller == null) this.controller = new TextEditingController();
    text = this.controller.text;
  }

  void onValueTextChanged(String text) {
    if (text.length - this.text.length > 1) {
      this.text += text.substring(text.length - 1);
      this.controller.text = this.text;
      this.controller.selection =
          new TextSelection.collapsed(offset: text.length);
    }
    this.text = this.controller.text;
    this.onTextChanged(this.text);
  }

  @override
  Widget build(BuildContext context) {
    TextView textView = new TextView.text("",theme: TypeTheme.light, textSize: TextView.MEDIUM_SIZE, color: textColor,);
    TextView hintText = new TextView.text("",theme: TypeTheme.dark, textSize: TextView.MEDIUM_SIZE, color: Singleton.hintColor,);
    double radius = typeShape == TypeShape.square ? 0.0 : textView.style.fontSize*1.8;

    return new Padding(
      padding: new EdgeInsets.all(this.padding),
      child: new Opacity(
        opacity: this.opacity,
        child: new Material(
          shadowColor: Singleton.setting.primary80OpacityColor,
          elevation: 5.0,
          borderRadius: new BorderRadius.circular(radius),
          color: this.color,
          child: new Container(
              margin: new EdgeInsets.all(this.labelText == null ? 10.0 : 0.0),
              padding:
                  new EdgeInsets.only(left: this.padding, right: this.padding),
              child: new Row(
                children: <Widget>[
                  new Flexible(
                    child: new TextField(
                      obscureText: this.obscureText,
                      style:textView.style,
                      controller: this.controller,
                      onChanged: (String text) => onValueTextChanged(text),
                      decoration: new InputDecoration(
                        labelText: this.labelText,
                        labelStyle:textView.style,
                        icon: new Icon(
                          this.prefixIcon,
                          color: this.textColor,
                        ),
                        hintText: this.hintText,
                        hintStyle:hintText.style,
                        border: InputBorder.none,
                        contentPadding:
                            new EdgeInsets.only(top: 5.0, bottom: 0.0),
                      ),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}

