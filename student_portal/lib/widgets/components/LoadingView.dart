import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/OpacityLoading.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class LoadingView extends StatefulWidget {
  LoadingView({Key key, Color color, Color background, double opacity})
      : this.color = color ?? Singleton.setting.primaryColor,
        this.background = background ?? Singleton.backgroundNothing,
        this.opacity = opacity ?? 1.0;

  final Color color;
  final Color background;
  final double opacity;

  @override
  State<StatefulWidget> createState() {
    return new LoadingViewState();
  }
}

class LoadingViewState extends State<LoadingView> {
  List<OpacityLoading> opacities;
  @override
  void initState() {
    super.initState();
    this.opacities = new List<OpacityLoading>();
    this.opacities.add(new OpacityLoading(0.0, true));
    this.opacities.add(new OpacityLoading(0.5, true));
    this.opacities.add(new OpacityLoading(1.0, false));
    updateOpacityLoading();
  }

  updateOpacityLoading() async {
    if (!this.mounted) return;
    setState(() {
      opacities.forEach((f) {
        f.updateOpacity();
      });
    });
    new Timer(new Duration(microseconds: 20), () {
      updateOpacityLoading();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Flexible(
      child: new Opacity(
        opacity: widget.opacity,
        child: new Container(
          color: widget.background,
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
//            new CircularProgressIndicator(
//              backgroundColor: this.color,
//            ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    circleOpacity(this.opacities[0].value,
                        12.0 * (1.0 - this.opacities[0].value)),
                    new Padding(
                        padding: new EdgeInsets.only(left: 5.0, right: 5.0),
                        child: new TextView.text("")),
                    circleOpacity(this.opacities[1].value,
                        12.0 * this.opacities[1].value),
                    new Padding(
                        padding: new EdgeInsets.only(left: 5.0, right: 5.0),
                        child: new TextView.text("")),
                    circleOpacity(this.opacities[2].value,
                        12.0 * (1.0 - this.opacities[2].value)),
                  ],
                )
              ]),
        ),
      ),
    );
  }

  Widget circleOpacity(double opacity, double radius) {
    return new Center(
      child: new Opacity(
        opacity: opacity,
        child: new CircleAvatar(
          radius: radius,
          backgroundColor: widget.color,
        ),
      ),
    );
  }
}
