
class Localization {
  static const String KEY_LOCALE_CODE = "KEY_LOCALE_CODE";
  static const String CODE_VI = "vi";
  static const String CODE_EN  = "en";

  final String locale;
  Localization(this.locale);

  factory Localization.vi() {
    return new Localization(CODE_VI);
  }

  factory Localization.en() {
    return new Localization(CODE_EN);
  }

  factory Localization.fromJson(dynamic json) {
    return new Localization(json[KEY_LOCALE_CODE]);
  }

  Map toJson() {
    return {KEY_LOCALE_CODE: this.locale};
  }

  bool get isVietnamese => this.locale == CODE_VI;
  bool get isEnglish => this.locale == CODE_EN;

  //todo multi language
  String get schedule=>
      isVietnamese ? "Lịch học" : "Schedule";
  String get scheduling =>
      isVietnamese ? "Xếp lịch" : "Scheduling";
  String get login =>
      isVietnamese ? "Đăng nhập" : "Login";
  String get user_name =>
      isVietnamese ? "Tên tài khoản" : "Username";
  String get password =>
      isVietnamese ? "Mật khẩu" : "Password";
  String get you_have_new_notification =>
      isVietnamese ? "Bạn có một thông báo mới" : "A new notification";
  String get user_name_and_password_not_notthing => isVietnamese ? "Username và password không được để trống." : "Username and password is require.";
  String get inform_account_not_exist => isVietnamese ? "Thông tin tài khoản không tồn tại." : "Inform of account not exist.";
  String get inform_login_wrong => isVietnamese ? "Thông tin đăng nhập không chính xác." : "Inform login is wrong.";
  String get error => isVietnamese ? "Lỗi" : "Error";
  String get error_in_process => isVietnamese ? "Có lỗi trong quá trình xử lý." : "Something wrong...";
  String get error_empty => isVietnamese ? "Không có dữ liệu để hiển thị" : "Nothing to show";

  //main
  String get require_try_login  => isVietnamese ? "Yêu cầu đăng nhập lại." : "Require login again.";
  String get home_fragment_title => isVietnamese ? "Trang chủ": "Home";
  String get schedule_fragment_title => isVietnamese ?"Lịch học/thi": "Schedule";
  String get score_fragment_title => isVietnamese ? "Kết quả học tập":"Score";
  String get course_category_fragment_title => isVietnamese ? "Chương trình khung":"Course Category";
  String get notification_fragment_title => isVietnamese ?"Thông báo": "Notification";
  String get fee_fragment_title => isVietnamese ?"Công nợ": "Debt";
  String get register_fragment_title => isVietnamese ?"Đăng ký học phần": "Course Register";
  String get settings_fragment_title => isVietnamese ?"Cài đặt": "Settings";
  String get result_register_fragment_title => isVietnamese ?"Kết quả đăng ký": "Result register";
  String get suggest_fragment_title => isVietnamese ? "Gợi ý lịch học": "Suggested scheduling";

  String get logout => isVietnamese ?"Đăng xuất": "Logout";
  String get do_you_want_log_out => isVietnamese
      ? "Đăng xuất tài khoản khỏi ứng dụng ?"
      : "Do you want log out, now ?";
  String get restart => isVietnamese ?"Khởi động lại": "Restart";
  String get do_you_want_restart => isVietnamese
      ? "Khởi động lại ứng dụng ngay bây giờ ?"
      : "Do you want restart ?";
  String get subject_registered => isVietnamese ?"Môn học đã đăng ký": "Registered course";
  String get you_registered_this_subject => isVietnamese ?"Bạn đã đăng ký môn học này": "You have registered for this course";

  //view utils
  String get nothing_schedule  => isVietnamese ? "Không có lịch học để hiển thị" : "Nothing schedule to show";
  String get nothing_semester  => isVietnamese ? "Không có học kỳ để hiển thị" : "Nothing semester to show";
  String get nothing_subject  => isVietnamese ? "Không có môn học để hiển thị" : "Nothing subject to show";
  String get nothing_class_subject  => isVietnamese ? "Không có lớp môn học để hiển thị" : "Nothing class subject to show";
  String get nothing_detail  => isVietnamese ? "Không có chi tiết để hiển thị" : "Nothing detail to show";
  String get nothing_score  => isVietnamese ? "Không có điểm để hiển thị" : "Nothing score to show";
  String get refresh  => isVietnamese ? "Cập nhật lại" : "Refresh";
  String get load_more  => isVietnamese ? "Cập nhật thêm" : "Load more";


  String get invalid_date =>
      isVietnamese ? "Ngày không hợp lệ" : "Invalid date";
  String get year => isVietnamese ? "Năm" : "Year";
  String get month => isVietnamese ? "Tháng" : "Month";
  String get day => isVietnamese ? "Ngày" : "Day";
  String get date => isVietnamese ? "Ngày" : "Date";
  String get to_day => isVietnamese ? "Ngày hôm nay" : "To day";

  String get cancel => isVietnamese ? "Hủy" : "Cancel";
  String get confirm => isVietnamese ? "Xác nhận" : "Confirm";
  String get select => isVietnamese ? "Chọn" : "Select";
  String get save => isVietnamese ? "Lưu" : "Save";
  String get search => isVietnamese ? "Tìm" : "Search";
  String get yes => isVietnamese ? "Đồng ý" : "Yes";
  String get no => isVietnamese ? "Không" : "No";
  String get back => isVietnamese ? "Quay lại" : "Back";

  String get weekly_schedule =>
      isVietnamese ? "Xem lịch học theo tuần" : "Weekly schedule";
  String get select_week => isVietnamese ? "Chọn tuần" : "Select week";
  String get you_need_select_week =>
      isVietnamese ? "Bạn cần chọn một tuần" : "You need select one week";
  String get daily_schedule =>
      isVietnamese ? "Xem lịch học theo ngày" : "Daily schedule";
  String get select_date => isVietnamese ? "Chọn ngày" : "Select date";
  String get you_need_select_date =>
      isVietnamese ? "Bạn cần chọn một ngày" : "You need select one date";
  String get schedule_by_semester =>
      isVietnamese ? "Xem lịch học theo học kỳ" : "Schedule by semester";
  String get select_semester => isVietnamese ? "Chọn học kỳ" : "Select semester";
  String get you_need_select_semester =>
      isVietnamese ? "Bạn cần chọn một học kỳ" : "You need select one semester";
  String get schedule_by_subject =>
      isVietnamese ? "Xem lịch học theo môn học" : "Schedule by subject";
  String get select_schedule_mode =>
      isVietnamese ? "Chọn chế độ" : "Select schedule mode";
  String get you_need_select_schedule_mode => isVietnamese
      ? "Bạn cần chọn chế độ xem"
      : "You need select one schedule mode";

  String get attendance_score => isVietnamese ? "Chuyên cần" : "Attendance";
  String get middle_exam_score => isVietnamese ? "Giữa kỳ" : "Middle";
  String get final_exam_score => isVietnamese ? "Cuối kỳ" : "Final";
  String get total_score => isVietnamese ? "Tổng điểm" : "Total";

  String get monday => isVietnamese ? "Thứ hai" : "Monday";
  String get tuesday => isVietnamese ? "Thứ ba" : "Tuesday";
  String get wednesday => isVietnamese ? "Thứ tư" : "Wednesday";
  String get thursday => isVietnamese ? "Thứ năm" : "Thursday";
  String get friday => isVietnamese ? "Thứ sáu" : "Friday";
  String get saturday => isVietnamese ? "Thứ bảy" : "Saturday";
  String get sunday => isVietnamese ? "Chủ nhật" : "Sunday";
  String get monday_shortest => isVietnamese ? "T2" : "Mo";
  String get tuesday_shortest => isVietnamese ? "T3" : "Tu";
  String get wednesday_shortest => isVietnamese ? "T4" : "We";
  String get thursday_shortest => isVietnamese ? "T5" : "Th";
  String get friday_shortest => isVietnamese ? "T6" : "Fr";
  String get saturday_shortest => isVietnamese ? "T7" : "Sa";
  String get sunday_shortest => isVietnamese ? "CN" : "Su";

  String get theory => isVietnamese ? "Lý thuyết" : "Theory";
  String get practice => isVietnamese ? "Thực hành" : "Practice";
  String get progress_rate => isVietnamese ? "Quá trình" : "Progress";
  String get middle_rate => isVietnamese ? "Giữa kỳ" : "Middle";
  String get final_rate => isVietnamese ? "Cuối kỳ" : "Final";
  String get total_price => isVietnamese ? "Tổng tiền" : "Total";
  String get read_more => isVietnamese ? "Xem chi tiết" : "Read more";
  String get price => isVietnamese ? "Số tiền" : "Price";
  String get paid => isVietnamese ? "Đã nộp" : "Paid";
  String get debt => isVietnamese ? "Công nợ" : "Debt";

  //subject - fee
  String get code_class_subject => isVietnamese ? "Mã LHP" : "Class code";
  String get code_subject => isVietnamese ? "Mã môn" : "Code";
  String get name_subject => isVietnamese ? "Tên môn" : "Name";
  String get name_group_major => isVietnamese ? "Khối ngành" : "Sector";
  String get room => isVietnamese ? "Phòng" : "Room";
  String get teacher => isVietnamese ? "Giảng viên" : "Teacher";
  String get time => isVietnamese ? "Thời gian" : "Time";
  String get period => isVietnamese ? "Tiết" : "Period";
  String get exam => isVietnamese ? "Thi" : "Exam";
  String get exam_type => isVietnamese ? "Loại thi" : "Exam form";
  String get exam_time => isVietnamese ? "Giờ thi" : "Exam time";
  String get exam_date => isVietnamese ? "Ngày thi" : "Date";
  String get exam_period => isVietnamese ? "Tiết thi" : "Exam period";
  String get supervisor_1 => isVietnamese ? "Giám thị 1" : "Teacher 1";
  String get supervisor_2 => isVietnamese ? "Giám thị 2" : "Teacher 2";
  String get exam_total_time => isVietnamese ? "Thời lượng" : "Total time";
  String get credit => isVietnamese ? "Tín chỉ" : "Credit";
  String get credit_short => isVietnamese ? "STC" : "Credit";
  String get week => isVietnamese ? "Tuần" : "Week";
  String get pay => isVietnamese ? "Thanh toán" : "Pay";
  String get must_pay => isVietnamese ? "Phải thanh toán" : "Must pay";
  String get name_subject_parallel => isVietnamese ? "Môn học kèm" : "Parallel";
  String get name_subject_first => isVietnamese ? "Môn tiên quyết" : "Request";
  String get count_credit => isVietnamese ? "Số tín chỉ" : "Credit";


  //inform
  String get code_student => isVietnamese ? "MSSV" : "Student ID";
  String get full_name => isVietnamese ? "Họ tên" : "Full name";
  String get class_student => isVietnamese ? "Lớp" : "Class";
  String get major => isVietnamese ? "Ngành" : "Major";
  String get profile => isVietnamese ? "Thông tin cá nhân" : "Profile";
  String get change_password =>
      isVietnamese ? "Đổi mật khẩu" : "Change password";
  String get you_need_change_password => isVietnamese
      ? "Thay đổi mật khẩu theo mẫu bên dưới."
      : "Change your password in the form below.";
  String get update_profile =>
      isVietnamese ? "Cập nhật thông tin" : "Update proflie";
  String get old_password => isVietnamese ? "Mật khẩu cũ" : "Password";
  String get new_password => isVietnamese ? "Mật khẩu mới" : "New password";
  String get confirm_password => isVietnamese ? "Xác nhận" : "Confirm";
  String get password_new =>
      isVietnamese ? "Mật khẩu mới" : "New password";
  String get password_confirm =>
      isVietnamese ? "Xác nhận mật khẩu" : "Confirm password";
  String get password_current =>
      isVietnamese ? "Mật khẩu hiện tại" : "Current password";

  //register
  String get filter_by_progress =>
      isVietnamese ? "Lớp học phần theo tiến độ" : "Filter by plan";
  String get filter_by_all => isVietnamese ? "Tất cả lớp học phần" : "Show all";
  String get by_progress => isVietnamese ? "Theo tiến độ" : "By plan";
  String get by_all => isVietnamese ? "Tất cả" : "Show all";
  String get exist_parallel_subject =>
      isVietnamese ? "Tồn tại môn học kèm" : "Exist parallel subject";
  String get register_course =>
      isVietnamese ? "Đăng ký lớp học" : "Register course";
  String get register_n_course =>
      isVietnamese ? "Đăng ký lớp học" : "Register courses";
  String get do_you_want_register_course => isVietnamese
      ? "Bạn muốn đăng ký lớp học này ?"
      : "Do you want register this course ?";
  String get do_you_want_n_register_course => isVietnamese
      ? "Bạn muốn đăng ký các lớp học này ?"
      : "Do you want register these courses ?";
  String get delete_course =>
      isVietnamese ? "Hủy lớp học phần" : "Remove course";
  String get delete_n_course =>
      isVietnamese ? "Hủy lớp học phần" : "Remove courses";
  String get do_you_want_delete_course => isVietnamese
      ? "Bạn muốn hủy lớp học phần này ?"
      : "Do you want remove this course ?";
  String get do_you_want_delete_n_course => isVietnamese
      ? "Bạn muốn hủy các lớp học phần này ?"
      : "Do you want remove these courses ?";
  String get course_selected =>
      isVietnamese ? "Học phần đã chọn" : "Course selected";
  String get require_select_parallel_subject => isVietnamese
      ? "Yêu cầu chọn môn học kèm để tiếp tục đăng ký"
      : "You must select one subject to continue";
  String get date_register => isVietnamese ? "Đăng ký" : "Register";
  String get number_of_student => isVietnamese ? "Sỉ số" : "Student";
  String get status => isVietnamese ? "Tình trạng" : "Status";
  String get result_register =>
      isVietnamese ? "Kết quả đăng ký" : "Result register";
  String get result_process =>
      isVietnamese ? "Kết quả xử lý" : "Result process";
  String get min  => isVietnamese ? "Tối thiểu" : "Min";
  String get max  => isVietnamese ? "Tối đa" : "Max";

  //setting
  String get color => isVietnamese
      ? "Màu sắc"
      : "Color";
  String get theme => isVietnamese
      ? "Chủ đề"
      : "Theme";
  String get language => isVietnamese
      ? "Ngôn ngữ"
      : "Language";
  String get font => isVietnamese
      ? "Kiểu chữ"
      : "Font";
  String get background_login =>
      isVietnamese ? "Hình nền đăng nhập" : "Background login";
  String get background_header =>
      isVietnamese ? "Hình nền thông tin" : "Background header";
  String get navigation_font =>
      isVietnamese ? "Kiểu chữ điều hướng" : "Navigation font";
  String get title_font => isVietnamese ? "Kiểu chữ tiều đề " : "Title font";
  String get text_font => isVietnamese ? "Kiểu chữ văn bản" : "Content font";
  String get you_need_select_font =>
      isVietnamese ? "Bạn cần chọn một kiểu chữ" : "You need select one font";
  String get vietnames => isVietnamese ? "Tiếng Việt" : "Vietnamese";
  String get english => isVietnamese ? "Tiếng Anh" : "English";
  String get save_setting_sucessfully => isVietnamese
      ? "Lưu cài đặt thành công. Khởi động lại chương trình để cài đặt có hiệu lực."
      : "Settings successfully saved. These settings will take effect after the application is restarted.";
  String get save_setting_failure => isVietnamese
      ? "Lưu cài đặt thất bại."
      : "Settings failure saved.";
  String get save_setting_by_default => isVietnamese
      ? "Cài đặt theo mặc định"
      : "Save setting by default";
  String get default_ => isVietnamese
      ? "Mặc định"
      : "Default";

  String get subject_list => isVietnamese
      ? "Danh sách môn học"
      : "Subject list";
  String get conditions_option => isVietnamese
      ? "Tùy chọn điều kiện"
      : "Conditions option";
  String get no_conditions => isVietnamese
      ? "Không có điều kiện được chọn"
      : "No conditions option";
  String get select_day_of_week_condition => isVietnamese
      ? "Chọn điều kiện theo thứ"
      : "Select day of week condition";
  String get you_need_select_day_of_week_condition =>
      isVietnamese ? "Bạn cần chọn điều kiện theo thứ" : "You need select day of week condition";
  String get select_period_condition => isVietnamese
      ? "Chọn điều kiện theo tiết"
      : "Select period condition";
  String get you_need_select_period_condition =>
      isVietnamese ? "Bạn cần chọn điều kiện theo tiết" : "You need select period condition";

  String get scheduling_complete =>
      isVietnamese ? "Xếp lịch hoàn tất" : "Scheduling complete";
  String get scheduling_failure =>
      isVietnamese ? "Không thể xếp lịch" : "Failure scheduling";
  String get scheduling_nothing =>
      isVietnamese ? "Chưa có lịch học nào được tạo" : "Nothing schedule created";
  String get scheduling_result =>
      isVietnamese ? "Lịch được gợi ý" : "Suggest scheduling";

  String get on => isVietnamese ? "Bật" : "On";
  String get off => isVietnamese ? "Tắt" : "Off";
  String get hint => isVietnamese ? "Hướng dẫn" : "Guide";
  String get hint_1 => isVietnamese ? "- Trang chủ: Học viên xem thông tin cá nhân và lịch học ngày hôm nay.\n"
      "- Thông báo: Học viên xem các thông báo từ nhà trường.\n"
      "- Công nợ: Học viên xem thông tin công nợ, phiếu thu và học phí các môn học theo học kỳ." :
  "- Home: Display the student's personal information.\n"
      "- Notification: Display a notice from the school.\n"
      "- Debt: Display debt information, receipts and course fees for the semester." ;
  String get hint_2 => isVietnamese ?  "- Lịch học/thi: Học viên xem lịch học/thi theo nhiều chế độ khác nhau.\n"
      "- Điểm: Học viên xem kết quả học tập của học kỳ và của từng môn học.\n"
      "- Chương trình khung: Học viên xem chương trình đào tạo theo khối ngành mình học." :
  "- Schedule: Display scheduling information in different modes.\n"
      "- Score: Display the results of the semester and each subject.\n"
      "- Course Category: Display the course category of student." ;
  String get hint_3 => isVietnamese ?   "- Đăng ký học phần: Học viên đăng ký học phần, xem kết quả đăng ký theo học kỳ và xếp lịch đăng ký theo nhu cầu.\n"
      "- Cài đặt: Học viên tùy chỉnh giao diện theo ý muốn.\n"
      "- Đăng xuất: Đăng xuất tài khoản khỏi ứng dụng." :
  "- Course Register: Students register for the course, see the results of the semester registration and scheduling on demand.\n"
      "- Settings: Students customize the interface as desired.\n"
      "- Log out: Log out of the app." ;
  String get gotIt => isVietnamese ?  "Tôi đã hiểu" : "I got it";

  String get register_new => isVietnamese ? "Học mới" : "Register new";
  String get register_improve => isVietnamese ? "Học cải thiện" : "Improve";
  String get register_again => isVietnamese ? "Học lại" : "Try again";
  String get register_equivalent => isVietnamese ? "Học thay thế" : "Equivalent";

  String get level => isVietnamese ? "Hệ đào tạo" : "Training";
  String get semester => isVietnamese ? "Học kỳ" : "Semester";
  String get course  => isVietnamese ? "Khóa" : "Course";
  String get major_group => isVietnamese ? "Khối ngành" : "Group";
  String get type_register => isVietnamese ? "Loại" : "Type";
  String get register => isVietnamese ? "Đăng ký" : "Register";
  String get type_subject => isVietnamese ? "Loại" : "Type";
  String get avg_semester => isVietnamese ? "ĐTB học kỳ" : "Semester GPA";
  String get subject_requirement => isVietnamese ? "Tiên quyết" : "Requirement";
  String get subject_second => isVietnamese ? "Học kèm" : "Second";
  String get elective_subject => isVietnamese ? "Tự chọn" : "Electives";
  String get compulsory_subject => isVietnamese ? "Bắt buộc" : "Compulsory";

  String get select_options_register => isVietnamese
      ? "Chọn danh sách học phần"
      : "Select options";

  String get option_register_not_available => isVietnamese ? "Tùy chọn không có hiệu lực":
      "This option is not available";

  String get require_text => isVietnamese ? "Trường này là bắt buộc" : "This field is required";


  //todo images
  String get image_today => isVietnamese ? "resources/images/hom_nay.png" : "resources/images/hom_nay_en.png";

  String get image_thetest => isVietnamese ? "resources/images/ngay_thi.png" : "resources/images/ngay_thi_en.png";

  String get image_passed => isVietnamese ? "resources/images/qua_mon.png" : "resources/images/qua_mon_en.png";
  String get image_requirement=> isVietnamese ? "resources/images/mon_tien_quyet.png" : "resources/images/mon_tien_quyet_en.png";
  String get image_second=> isVietnamese ? "resources/images/mon_hoc_kem.png" : "resources/images/mon_hoc_kem_en.png";
  String get image_warning=> isVietnamese ? "resources/images/canh_bao.png" : "resources/images/canh_bao_en.png";

  //todo change pass
  String get password_not_match => isVietnamese ? "Mật khẩu xác nhận không khớp với mật khẩu mới" : "Password confirmed not match";

  String get replaced_by => isVietnamese ? "Được thay thế" : "Replaced by";

}
