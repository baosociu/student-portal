import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/service/schedule/ScheduleResult.dart';

class ScheduleState {
  final ScheduleResult result;
  final LoadingType loadingType;

  ScheduleState({
    this.result,
    this.loadingType = LoadingType.loading,
  });

  factory ScheduleState.init() =>
      new ScheduleState(result: new ScheduleResult.init());

  factory ScheduleState.loading() =>
      new ScheduleState(loadingType: LoadingType.loading);

  factory ScheduleState.error(String message) =>
      new ScheduleState(result: ScheduleResult.hasError(message));
}
