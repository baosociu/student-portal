import 'dart:async';

import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/service/schedule/ScheduleState.dart';
import 'package:student_portal/global/service/schedule/ScheduleAPI.dart';
import 'package:student_portal/global/service/schedule/ScheduleRequest.dart';
import 'package:student_portal/global/service/schedule/ScheduleResult.dart';
import 'package:rxdart/rxdart.dart';

class ScheduleStream {
  final Sink<ScheduleRequest> onExecute;
  final Stream<ScheduleState> state;

  ScheduleStream._(this.onExecute, this.state);

  factory ScheduleStream(ScheduleAPI api) {
    // ignore: close_sinks
    final onExecute = new StreamController<ScheduleRequest>();
    final state = new Observable<ScheduleRequest>(onExecute.stream)
        .debounce(const Duration(milliseconds: 250))
        .switchMap(buildExecute(api));

    return new ScheduleStream._(onExecute, state);
  }

  static Stream<ScheduleState> Function(ScheduleRequest) buildExecute(
      ScheduleAPI api) {
    return (ScheduleRequest request) {
      return new Observable<ScheduleResult>.fromFuture(api.execute(request))
          .map((ScheduleResult result) {
        return  new ScheduleState(
                result: result,
                loadingType: LoadingType.loading,
              );
      })
          // .onErrorReturn(new ScheduleState.error(error))
          .startWith(new ScheduleState.loading());
    };
  }
}
