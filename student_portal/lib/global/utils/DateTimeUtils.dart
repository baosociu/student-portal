import 'package:student_portal/global/utils/StringUtils.dart';

class DateTimeUtils {
  static DateTime convertStringToDateTime(String source, String sourceFormat,
      [String sourcePatten = "-"]) {
    String strFormat= StringUtils.convertDateStringFormat(
        source, sourceFormat, 'yyyyMMdd', sourcePatten);
    return strFormat == null ? null : DateTime.parse(strFormat);
  }

  static bool isEqualsDMY(DateTime date1, DateTime date2) {
    return (date1.year == date2.year &&
        date1.month == date2.month &&
        date1.day == date2.day);
  }
}
