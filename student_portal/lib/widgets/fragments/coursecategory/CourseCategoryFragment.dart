import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/CourseCategory.dart';
import 'package:student_portal/widgets/components/CardView.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';

// ignore: must_be_immutable
class CourseCategoryFragment extends BaseFragment {
  CourseCategoryFragment(mainPage)
      : super(mainPage, Singleton.COURSE_CATEGORY_FRAGMENT_ID,
            Singleton.setting.localization.course_category_fragment_title,"resources/icons/course_category.png",
  );
  CourseCategoryFragmentState myState;
  @override
  CourseCategoryFragmentState createState() {
    super.createState();
    myState = new CourseCategoryFragmentState();
    return myState;
  }

  void refresh(){
    myState.refresh();
  }
}

class CourseCategoryFragmentState extends BaseFragmentState {
  ServerResponse<CourseCategoryGroupSemesterList> response;
  String idGroupMajor;

  bool isRefresh;

  @override
  void initState() {
    super.initState();
    idGroupMajor = Singleton.student.groupMajor.id.toString();

    isRefresh = false;

    getCourseCategoryList(idGroupMajor);
  }

  void refresh(){
    if(isRefresh)
      return;
    setState(()=> isRefresh = true);
    getCourseCategoryList(idGroupMajor);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            isRefresh
                ? ViewUtils.buildRefreshView(LoadingType.refresh)
                : new SizedBox(),
            response == null
                ? ViewUtils.buildLoadingCardView()
                : buildCourseCategoryList(context),
          ],
        ),
      ),
    );
  }

  Widget buildCourseCategoryList(BuildContext context) {
    return response.buildViewByResponse<CourseCategoryGroupSemester>(
        context: context,
        list: response.data.list,
        callback: () => getCourseCategoryList(idGroupMajor),
        emptyView: ViewUtils.buildNothingSemesterCardView,
        item: (index, item) {
          return new CardView(
              child: buildCourseCategoryDetail(item),
              header:
                  (item.nameSemester.toString().toUpperCase().contains("HỌC KỲ")
                          ? ""
                          : "Học Kỳ ") +
                      item.nameSemester.toString());
        });
  }

  Widget buildCourseCategoryDetail(CourseCategoryGroupSemester _item) {
    return new ListView.builder(
        primary: false,
        itemCount: _item.list.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          CourseCategoryGroupSemesterItem item = _item.list[index];
          return new Column(children: <Widget>[
            new ListTile(
              title: buildTitle(item),
              subtitle: buildSubtitle(item),
            ),
            index < _item.list.length - 1 ? new Divider(color: Singleton.setting.primaryColor) : new SizedBox()
          ]);
        });
  }

  Widget buildTitle(CourseCategoryGroupSemesterItem item) {
    Color color = Singleton.setting.primaryColor;
    Color color20 = Singleton.setting.primary20OpacityColor;

    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Flexible(
            child: new Row(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(4.0),
                  child: new CircleAvatar(
                    child: new Icon(Icons.library_books,
                        color: Singleton.backgroundNothing),
                    backgroundColor: color,
                  ),
                ),
                new Flexible(
                    child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new TextView.title(
                      item.nameSubject.toString(),
                      overflow: TextOverflow.ellipsis,
                    ),
                    new TextView.text(
                      Singleton.setting.localization.code_subject +
                          ": " +
                          item.codeSubject.toString(),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )),
              ],
            ),
          ),
          new Icon(
            Icons.chevron_right,
            color: color,
          ),
        ],
      ),
      decoration: new BoxDecoration(
          gradient: new LinearGradient(
              tileMode: TileMode.clamp,
              colors: <Color>[
            Colors.transparent,
            color20,
            color20,
            Colors.transparent
          ])),
    );
  }

  Widget buildSubtitle(CourseCategoryGroupSemesterItem item) {
    return new Container(
      margin: new EdgeInsets.only(left: 24.0),
      decoration: new BoxDecoration(
          gradient: new RadialGradient(
              radius: 1.0,
              center: Alignment.bottomLeft,
              colors: <Color>[
            Singleton.setting.secondColor,
            Singleton.backgroundNothing
          ])),
      child: new Container(
        margin: new EdgeInsets.only(left: 4.0, bottom: 4.0),
        color: Singleton.backgroundNothing,
        child: new Padding(
            padding: new EdgeInsets.only(left: 8.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ViewUtils.buildItemTextView(
                    Singleton.setting.localization.credit + ": ",
                    item.weight.toString()),
                ViewUtils.buildItemTextView(
                    Singleton.setting.localization.theory + ": ",
                    item.weightTheory.toString()+" ("+Singleton.setting.localization.credit_short+")"),
                ViewUtils.buildItemTextView(
                    Singleton.setting.localization.practice + ": ",
                    item.weightPractice.toString()+" ("+Singleton.setting.localization.credit_short+")"),
                ViewUtils.buildItemTextView(
                    Singleton.setting.localization.name_group_major + ": ",
                    item.nameBlockMajor.toString()),
              ],
            )),
      ),
    );
  }

  getCourseCategoryList(String idGroupMajor) async {
    if (!isRefresh)
      setState(() {
        this.response = null;
      });

    final ServerResponse<CourseCategoryGroupSemesterList> response = await API
        .infoCourseCategory<CourseCategoryGroupSemesterList>(idGroupMajor,
            (data) {
      return new CourseCategoryGroupSemesterList.fromJson(data);
    }, () {
      return new CourseCategoryGroupSemesterList.fromJson(new List<Map>());
    });
    if (!this.mounted) return;

    setState(() {
      this.response = response;
      this.isRefresh = false;
    });
  }
}
