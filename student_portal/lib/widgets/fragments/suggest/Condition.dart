import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/StringUtils.dart';


class Condition{
  Map<int,bool> dayOfWeeks; //start: 1 (monday)
  Map<int,bool> periods;

  Condition(bool defaultValue){
    dayOfWeeks = new Map();
    periods = new Map();

    for(int i = 1 ; i <= 7; i++)
      dayOfWeeks[i] = defaultValue;
    for(int i = 1 ; i <= 18; i++ )
      periods[i] = defaultValue;
  }


  String getValueDayOfWeek(){
    String text = "";
    for(int i = 1 ; i <= 7 ; i++)
      if(dayOfWeeks[i])
        text += (StringUtils.getLabelOfWeekShortestByInt(i)+", ");
    if(text.endsWith(", "))
      text = text.substring(0, text.length-2);
    else
      text = Singleton.setting.localization.no_conditions;
    print(text);
    return text;
  }

  String getValuePeriod(){
    String text = "";
    for(int i = 1 ; i <= 18 ; i++)
      if(periods[i])
        text += (i.toString()+", ");
    if(text.endsWith(", "))
      text = text.substring(0, text.length-2);
    else
      text = Singleton.setting.localization.no_conditions;
    print(text);
    return text;
  }

  void updateDayOfWeek(List<bool> dayOfWeeks){
    if(dayOfWeeks.length != 7)
      return;
    for(int i = 1 ; i <= 7; i++)
      this.dayOfWeeks[i] = dayOfWeeks[i-1];
  }

  void updatePeriods(List<bool> periods){
    if(periods.length != 18)
      return;
    for(int i = 1 ; i <= 18; i++)
      this.periods[i] = periods[i-1];
  }
}