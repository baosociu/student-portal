import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/register/RegisterRequest.dart';
import 'package:student_portal/global/service/register/RegisterState.dart';
import 'package:student_portal/global/service/register/SubjectRegister.dart';
import 'package:student_portal/global/utils/FadeRoute.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/components/alert/PrimaryAlert.dart';
import 'package:student_portal/widgets/fragments/register_class_subject/RegisterClassSubjectFragment.dart';

class RegisterContentListView extends StatefulWidget {
  final SubjectRegisterList subjectRegisterList;
  final RegisterState registerState;
  final RegisterRequest registerRequest;

  RegisterContentListView(
      this.subjectRegisterList, this.registerState, this.registerRequest);
  @override
  State<StatefulWidget> createState() {
    return new RegisterContentListViewState();
  }
}

class RegisterContentListViewState extends State<RegisterContentListView> {
  BuildContext _context;
  List<String> subjectRegistered = new List<String>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;

    if (widget.registerState.loadingType == LoadingType.refresh)
      setState(() => subjectRegistered = new List<String>());

    return new ListView.builder(
        shrinkWrap: true,
        itemCount: widget.subjectRegisterList.list.length,
        itemBuilder: (context, index) {
          BaseSubject item = widget.subjectRegisterList.list[index];

          return new InkWell(
              child: new Container(
                  child: new Column(
                children: <Widget>[
                  buildSubjectItem(item),
                  index < widget.subjectRegisterList.list.length - 1
                      ? new Divider(color: Singleton.setting.primaryColor)
                      : new SizedBox()
                ],
              )),
              onTap: () => onTapItemListView(item, index));
        });
  }

  Widget buildSubjectItem(BaseSubject item) {
    List<Widget> widgets = new List<Widget>();
    widgets.add(buildDetailSubjectItem(item));

    if (item.namePlusSubject != null)
      widgets.add(buildPlusSubjectMark(widgets.length));
    if (item.nameFirstSubject != null)
      widgets.add(buildFirstSubjectMark(widgets.length));

    return new Stack(
      children: widgets,
    );
  }

  Widget buildPlusSubjectMark(int index) {
    double width = MediaQuery.of(context).size.width;
    double widthImage = width;
    double heightImage = width / 5;
    return new Column(
      children: <Widget>[
        new SizedBox(height: (index - 1) * 50.0),
        new Container(
          child: new SizedBox(height: heightImage, width: widthImage),
          decoration: new BoxDecoration(
            image: new DecorationImage(
                image:
                    new AssetImage(Singleton.setting.localization.image_second),
                fit: BoxFit.contain,
                alignment: Alignment.topRight),
          ),
        )
      ],
    );
  }

  Widget buildFirstSubjectMark(int index) {
    double width = MediaQuery.of(context).size.width;
    double widthImage = width;
    double heightImage = width / 5;
    return new Column(
      children: <Widget>[
        new SizedBox(height: (index - 1) * 50.0),
        new Container(
          child: new SizedBox(height: heightImage, width: widthImage),
          decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage(
                    Singleton.setting.localization.image_requirement),
                fit: BoxFit.contain,
                alignment: Alignment.topRight),
          ),
        )
      ],
    );
  }

  Widget buildDetailSubjectItem(BaseSubject item) {
    return new ListTile(
        title: new Row(
          children: <Widget>[
            new Flexible(
                child: new TextView.title(
              item.nameSubject.toString(),
              overflow: TextOverflow.ellipsis,
              textSize: TextView.LAGRE_SIZE,
              color: Singleton.setting.primaryColor,
              fontWeight: FontWeight.bold,
            )),
            subjectRegistered.contains(item.idSubject.toString())
                ? new Icon(
                    Icons.check,
                    color: Singleton.setting.primaryColor,
                  )
                : new SizedBox()
          ],
        ),
        subtitle: new TableView(
          <Widget>[
            ViewUtils.buildItemTextView(
                Singleton.setting.localization.code_subject + ": ",
                item.codeSubject.toString()),
            ViewUtils.buildItemTextView(
                Singleton.setting.localization.credit + ": ",
                item.credit.toString()),
            item.namePlusSubject == null
                ? new SizedBox()
                : ViewUtils.buildItemTextView(
                    Singleton.setting.localization.subject_second + ": ",
                    item.namePlusSubject.toString()),
            item.nameFirstSubject == null
                ? new SizedBox()
                : ViewUtils.buildItemTextView(
                    Singleton.setting.localization.subject_requirement + ": ",
                    item.nameFirstSubject.toString()),
            //todo thông tin riêng
          ]..addAll(buildDetailSubjectByType(item)),
          margin: 0.0,
        ));
  }

  List<Widget> buildDetailSubjectByType(BaseSubject item) {
    List<Widget> widgets = new List();
    if (item.isOldSubject) {
      SubjectOld _item = (item as SubjectOld);
      widgets.add(ViewUtils.buildItemTextView(
          Singleton.setting.localization.total_score + ": ",
          _item.totalScore.toString()));
      widgets.add(ViewUtils.buildItemTextView(
          Singleton.setting.localization.semester + ": ",
          _item.nameSemester.toString()));
    } else if (item.isNewSubject) {
      SubjectNew _item = (item as SubjectNew);
      widgets.add(
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.type_subject + ": ",
            _item.typeSubjectText.toString()),
      );
    }
    return widgets;
  }

  Future onTapItemListView(BaseSubject item, int index) async {
    if (widget.registerState.loadingType != LoadingType.loading) return;

    if (subjectRegistered.contains(item.idSubject.toString()))
      showAlertRegistered(_context);
    else
      chooseClassSubject(item);
  }

  //todo dialog thông báo học phần đã đăng ksy rồi
  Future<Null> showAlertRegistered(BuildContext context) async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new PrimaryAlert(
          textTitle: Singleton.setting.localization.subject_registered,
          textMessage:
              Singleton.setting.localization.you_registered_this_subject,
          textNegativeButton: Singleton.setting.localization.back,
          color: Singleton.setting.primaryColor,
          onWillPop: () {},
          onNegativeTap: () {},
          onPositiveTap: () {},
          iconHeader: Icons.check,
        ));
  }

  Future chooseClassSubject(BaseSubject baseSubject) async {
    final Map<String, dynamic> result = await Navigator.of(context).push(
        new FadeRoute(
            builder: (context) => new RegisterClassSubjectFragment(
                Singleton.setting.localization.register_fragment_title,
                Singleton.student.id.toString(),
                widget.registerRequest.semester.id.toString(),
                baseSubject.idSubject.toString())));

    if (result != null) {
      if (result.containsKey('result_flag') &&
          result['result_flag'].toString() == 'true') {
        if (result['result_id_subject'] is List &&
            result['result_id_subject'].length != 0)
          setState(() {
            subjectRegistered.addAll(result['result_id_subject']);
          });
      }
      if (result.containsKey('result_message'))
        ToastUtils.showToast(_context, result['result_message'].toString());
    }
  }
}
