import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Register.dart';
import 'package:student_portal/models/objects/Result.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class RegisterViewUtils {
  static Widget buildInfoResult(ResultSemester item) {
    return new TableView(
      <Widget>[
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.name_subject + ": ",
            item.nameSubject.toString()),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.code_subject + ": ",
            item.codeSubject.toString()),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.teacher + ": ",
            item.nameTeacher.toString()),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.time + ": ",
            item.dateStart.toString() + " - " + item.dateEnd.toString()),
      ],
      margin: 0.0,
    );
  }

  static Widget buildClassSubjectItem(RegisterSubjectItem _item) {
    return new ListTile(
        subtitle: new TableView(
      <Widget>[
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.teacher + ":",
            _item.nameTeacher.toString()),
        buildScheduleInformation(_item.schedules),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.room + ":", _item.room),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.name_group_major + ":",
            _item.groups[0].toString()),
        buildGroupsInformation(
            new List<String>.from(_item.groups)..removeAt(0)),
        ViewUtils.buildItemTextView(Singleton.setting.localization.time + ":",
            _item.dateStart.toString() + " - " + _item.dateEnd.toString()),
        new Row(
          children: <Widget>[
            new Row(
              children: <Widget>[
                new TextView.text(
                  _item.numberStudent.toString(),
                  color: Singleton.numberStudentColor,
                  textSize: TextView.LAGRE_SIZE,
                ),
                new TextView.text("/", textSize: TextView.LAGRE_SIZE),
                new TextView.text(_item.maxTotal.toString(),
                    color: Singleton.numberStudentMaxColor,
                    textSize: TextView.LAGRE_SIZE),
                new Icon(Icons.people)
              ],
            )
          ],
          mainAxisAlignment: MainAxisAlignment.end,
        )
      ],
      margin: 0.0,
    ));
  }

  static Column buildScheduleInformation(List<RegisterSchedule> schedules) {
    List<Widget> widgets = new List<Widget>();
    for (int i = 0; i < schedules.length; i++) {
      RegisterSchedule item = schedules[i];
      widgets.add(ViewUtils.buildItemTextView(
          i == 0 ? (Singleton.setting.localization.schedule + ":") : "",
          StringUtils.getLabelOfWeekByInt(item.idDate) +
              " - " +
              Singleton.setting.localization.period +
              ": " +
              item.time.toString()));
    }
    return new Column(children: widgets);
  }

  static Column buildSmallScheduleInformation(
      List<RegisterSchedule> schedules) {
    List<Widget> widgets = new List<Widget>();
    for (int i = 0; i < schedules.length; i++) {
      RegisterSchedule item = schedules[i];
      widgets.add(ViewUtils.buildItemSmallTextView(
          i == 0 ? (Singleton.setting.localization.schedule + ":") : "",
          StringUtils.getLabelOfWeekByInt(item.idDate) +
              " - " +
              Singleton.setting.localization.period +
              ": " +
              item.time.toString(),
          rightFlex: 1,
          leftFlex: 1));
    }
    return new Column(children: widgets);
  }

  static Column buildGroupsInformation(List<String> groups) {
    List<Widget> widgets = new List<Widget>();
    groups.forEach((item) {
      widgets.add(ViewUtils.buildItemTextView("", item.toString()));
    });
    return new Column(children: widgets);
  }
}
