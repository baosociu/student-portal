import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';

// ignore: must_be_immutable
class ChoiceOneAlert extends StatefulWidget {
  ChoiceOneAlert(this.values,
      {Key key,
      Color color,
      this.iconHeader,
      this.textTitle: '',
      this.textMessage: '',
      this.textPositiveButton: '',
      this.textNegativeButton: '',
      this.onPositiveTap,
      this.padding: 16.0,
      this.indexSelected: 0,
      this.isAutoSelect: false})
      : this.color = color ??Singleton.setting.primaryColor;

  final List<String> values;
  final Color color;
  final IconData iconHeader;
  final String textTitle;
  final String textMessage;
  final String textPositiveButton;
  final String textNegativeButton;
  final double padding;
  int indexSelected;
  ValueChanged<int> onPositiveTap;
  final bool isAutoSelect;

  @override
  State<StatefulWidget> createState() {
    return new ChoiceOneAlertState();
  }
}

class ChoiceOneAlertState extends State<ChoiceOneAlert> {
  static FlatButton positiveButton;
  static FlatButton negativeButton;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    positiveButton = new FlatButton(
        child: new TextView.text(widget.textPositiveButton,color: widget.color,),
        onPressed: () {
          Navigator.of(context).pop();
          widget.onPositiveTap(widget.indexSelected);
        });

    negativeButton = new FlatButton(
      child: new TextView.text(widget.textNegativeButton),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new AlertDialog(
      titlePadding: new EdgeInsets.all(0.0),
      contentPadding: new EdgeInsets.only(
          top: widget.padding, right: widget.padding, left: widget.padding),
      title: new Container(
        padding: new EdgeInsets.all(widget.padding / 2),
        decoration: new BoxDecoration(color: widget.color),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Icon(
              widget.iconHeader,
              color: Singleton.textLightColor,
              size: 30.0,
            )
          ],
        ),
      ),
      content: new SingleChildScrollView(
        child: new ListBody(
          children: <Widget>[
            new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new TextView.alertTitle (
                  widget.textTitle
                ),
                new SizedBox(height: 10.0),
                new TextView.alertText(
                  widget.textMessage
                ),
                new SizedBox(height: 10.0),
                new SingleChildScrollView(
                  child: new ListView.builder(
                      shrinkWrap: true,
                      itemCount: widget.values.length,
                      itemBuilder: (context, index) {
                        return new Column(
                          children: <Widget>[
                            new InkWell(
                              child: new Container(
                                child: new Row(
                                  children: <Widget>[
                                    new Icon(
                                        index == widget.indexSelected
                                            ? Icons.radio_button_checked
                                            : Icons.radio_button_unchecked,
                                        color: widget.color),
                                    new SizedBox(width: 10.0),
                                    new TextView.alertText(
                                      widget.values[index],
                                    ),
                                  ],
                                ),
                                margin:
                                    new EdgeInsets.only(top: 2.0, bottom: 2.0),
                              ),
                              onTap: () {
                                setState(() {
                                  widget.indexSelected = index;
                                });
                                if (widget.isAutoSelect)
                                  positiveButton.onPressed();
                              },
                            ),
                          ],
                        );
                      }),
                ),
              ],
            ),
            new Container(
              margin: new EdgeInsets.only(top: widget.padding),
              padding: new EdgeInsets.only(
                  right: widget.padding, left: widget.padding),
              decoration: new BoxDecoration(color: widget.color),
              child: new Row(
                children: <Widget>[
                  new SizedBox(
                    height: 0.5,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      actions: widget.isAutoSelect
          ? <Widget>[negativeButton]
          : <Widget>[negativeButton, positiveButton],
    );
  }
}
