import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/service/register/BaseRegister.dart';
import 'package:student_portal/global/service/register/SubjectRegister.dart';

class RegisterResult {
  final ResultType resultType;
  final TypeRegister typeRegister;
  final SubjectRegisterList result;
  final String message;

  RegisterResult(this.resultType, this.result,
      {TypeRegister typeRegister, String message})
      : this.message = message ?? "",
        this.typeRegister = typeRegister ?? new TypeRegister.nothing();

  factory RegisterResult.init() {
    return new RegisterResult(ResultType.resultEmpty, null);
  }

  factory RegisterResult.hasError(String message) {
    return new RegisterResult(ResultType.hasError, null, message: message);
  }

  factory RegisterResult.loginAgain(String message) {
    return new RegisterResult(ResultType.loginAgain, null, message: message);
  }

  bool get isAvailable => resultType == ResultType.availableItems;

  bool get isEmpty => resultType == ResultType.resultEmpty;

  factory RegisterResult.fromJson(dynamic json, TypeRegister type) {
    SubjectRegisterList list;
    if (type.isReplaceRegister) {
      list = new SubjectRegisterList.replaceSubject(json);
    } else {
      if (type.isNewRegister)
       list = new SubjectRegisterList.newSubject(json);
      else
        list = new SubjectRegisterList.oldSubject(json);
    }
    return new RegisterResult(
        list.list.length == 0
            ? ResultType.resultEmpty
            : ResultType.availableItems,
        list,
        typeRegister: type);
  }

  factory RegisterResult.fromItems(List<dynamic> items, TypeRegister type) {
    SubjectRegisterList list;
    if (type.isReplaceRegister)
      list = new SubjectRegisterList.fromItems(items);
    else
      list = new SubjectRegisterList.fromItems(items);
    return new RegisterResult(
        list.list.length == 0
            ? ResultType.resultEmpty
            : ResultType.availableItems,
        list,
        typeRegister: type);
  }
}
