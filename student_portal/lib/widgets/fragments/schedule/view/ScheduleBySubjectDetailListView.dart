import 'package:flutter/material.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Schedule.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class ScheduleBySubjectDetailListView extends StatefulWidget {
  final String idStudent;
  final String idClassSubject;

  ScheduleBySubjectDetailListView(this.idStudent, this.idClassSubject,
      {Key key});

  @override
  State<StatefulWidget> createState() {
    return new ScheduleBySubjectDetailListViewState();
  }
}

class ScheduleBySubjectDetailListViewState
    extends State<ScheduleBySubjectDetailListView> {
  ServerResponse<BaseScheduleList> response;
  String idStudent;
  String idClassSubject;
  BuildContext _context;

  @override
  void initState() {
    super.initState();
    this.idClassSubject = widget.idClassSubject;
    this.idStudent = widget.idStudent;
    getScheduleBySubject(idStudent, idClassSubject);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(body: new Builder(builder: (BuildContext context) {
      _context = context;
      return new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            response == null
                ? ViewUtils.buildLoadingCardView()
                : buildScheduleBySubjectList(),
          ],
        ),
      );
    }));
  }

  Widget buildScheduleBySubjectList() {
    return response.buildCardViewByResponse<BaseSchedule>(
        context: context,
        list: response.data.list,
        callback: () => getScheduleBySubject(idStudent, idClassSubject),
        emptyView: ViewUtils.buildNothingSubjectCardView,
        item: (index, item) {
          return new Column(children: <Widget>[
            new Stack(children: <Widget>[
              new Container(
                child: new ListTile(
                    title: buildTitle(index, item),
                    subtitle: item is NormalSchedule
                        ? buildItemByNormalSchedule(item)
                        : buildItemByTestSchedule(item)),
              ),
              item is TestSchedule ? buildTestMark() : new SizedBox()
            ],),
            index < response.data.list.length - 1 ? new Divider(color: Singleton.setting.primaryColor) : new SizedBox()
          ],);
        });
  }

  Widget buildTitle(int index, BaseSchedule item) {
    Color color = Singleton.setting.primaryColor;
    Color color20 = Singleton.setting.primary20OpacityColor;

    return new Container(
        child: new Row(
          children: <Widget>[
            new Flexible(
                child: new ListView.builder(
                    itemCount: 2,
                    shrinkWrap: true,
                    primary: false,
                    itemBuilder: (context, index) {
                      return index == 0
                          ? buildHeaderSchedule(item)
                          : buildHeaderScheduleDate(item);
                    })),
          ],
        ),
        decoration: new BoxDecoration(
            gradient: new LinearGradient(
                tileMode: TileMode.clamp,
                colors: <Color>[Colors.transparent, color20, color20])),
      );
  }

  Widget buildHeaderSchedule(BaseSchedule item) {
    return new TextView.title(
      item.labelDayOfWeek,
      overflow: TextOverflow.ellipsis,
    );
  }

  Widget buildHeaderScheduleDate(BaseSchedule item) {
    return new TextView.title(
      StringUtils.convertDateTimeToString(item.date, "ddMMyyyy"),
      overflow: TextOverflow.ellipsis,
    );
  }

  getScheduleBySubject(String idStudent, String idClassSubject) async {
    setState(() {
      this.response = null;
    });

    final ServerResponse<BaseScheduleList> response = await API
        .infoScheduleByClassSubject<BaseScheduleList>(idStudent, idClassSubject,
            (data) {
      return new BaseScheduleList.fromJson(data);
    }, () {
      return new BaseScheduleList.fromJson(new List<Map>());
    });
    if (!this.mounted) return;

    setState(() {
      this.response = response;
    });
  }

  //todo normal
  Widget buildItemByNormalSchedule(NormalSchedule normalSchedule) {
    return new Container(
      padding: new EdgeInsets.all(4.0),
      child: new InkWell(
          onTap: () {
            ToastUtils.showToastNormalSchedule(_context, normalSchedule);
          },
          child: new Row(
            children: <Widget>[
              new TableView(
                <Widget>[
                  ViewUtils.buildItemTextView(
                      Singleton.setting.localization.period + ": ",
                      normalSchedule.time.toString() +
                          " -> " +
                          (normalSchedule.time + normalSchedule.countTime - 1)
                              .toString()),
                  ViewUtils.buildItemTextView(
                      Singleton.setting.localization.room + ": ",
                      normalSchedule.nameRoom.toString() +
                          " - " +
                          normalSchedule.nameBranch.toString()),
                  ViewUtils.buildItemTextView(
                      Singleton.setting.localization.teacher + ": ",
                      normalSchedule.teacher.toString()),
                ],
                margin: 0.0,
              ),
            ],
          )),
    );
  }

//todo test
  Widget buildItemByTestSchedule(TestSchedule testSchedule) {
    return
      new Container(
        padding: new EdgeInsets.all(4.0),
        decoration: new BoxDecoration(color: Singleton.yellow10OpacityColor),
        child: new InkWell(
            onTap: () {
              ToastUtils.showToastTestSchedule(_context, testSchedule);
            },
            child: new Row(
              children: <Widget>[
                new TableView(
                  <Widget>[
                    ViewUtils.buildItemTextView(
                        Singleton.setting.localization.exam_type + ":",
                        testSchedule.typeTest.toString()),
                    ViewUtils.buildItemTextView(
                        Singleton.setting.localization.room + ":",
                        testSchedule.nameRoom.toString() +
                            " - " +
                            testSchedule.nameBranch.toString()),
                    ViewUtils.buildItemTextView(
                        Singleton.setting.localization.exam_time + ":",
                        testSchedule.startTime.toString()),
                    ViewUtils.buildItemTextView(
                        Singleton.setting.localization.exam_period + ":",
                        testSchedule.timeTest.toString()),
                    ViewUtils.buildItemTextView(
                        Singleton.setting.localization.exam_total_time + ":",
                        testSchedule.time.toString()),
                    ViewUtils.buildItemTextView(
                        Singleton.setting.localization.supervisor_1 + ":",
                        testSchedule.teacher1.toString()),
                    ViewUtils.buildItemTextView(
                        Singleton.setting.localization.supervisor_2 + ":",
                        testSchedule.teacher2 == null
                            ? ""
                            : testSchedule.teacher2.toString()),
                  ],
                  margin: 0.0,
                )
              ],
            )),
      );
  }

  Widget buildTestMark() {
    double width = MediaQuery.of(context).size.width;
    double widthImage = width;
    double heightImage = width / 5;
    return new Container(
      child: new SizedBox(height: heightImage, width: widthImage),
      decoration: new BoxDecoration(
        image: new DecorationImage(
            image: new AssetImage(Singleton.setting.localization.image_thetest),
            fit: BoxFit.contain,
            alignment: Alignment.centerRight),
      ),
    );
  }
}
