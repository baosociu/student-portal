import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/models/objects/Register.dart';

class API {
  static const String host =
      "http://congsv-congsv.1d35.starter-us-east-1.openshiftapps.com/";

  //todo handle error
  static Future<ServerResponse> handleError<T>(
      Future<StreamedResponse> response,
      Function fromJson,
      Function _default) async {

    StreamedResponse streamedResponse;
    try {
      streamedResponse = await response.catchError((error) async {
        throw error;
      });

      try {
        String value = await streamedResponse.stream.bytesToString();
        dynamic data = json.decode(value);

        switch (streamedResponse.statusCode) {
          case HttpStatus.OK:
            return new ServerResponse<T>.fromData(
                data, streamedResponse.reasonPhrase, fromJson, _default);
          case HttpStatus.UNAUTHORIZED:
            return new ServerResponse<T>.loginAgain(
                Singleton.setting.localization.require_try_login, _default);
          default:
            return new ServerResponse<T>.fromMessage(
                streamedResponse.reasonPhrase, _default);
        }
      } catch (e) {
        return new ServerResponse<T>.fromMessage(e.toString(), _default);
      }
    } catch (e) {
      return new ServerResponse<T>.fromMessage(
          (e is SocketException) ? e.message : e.toString(), _default);
    }
  }

  static Future<Response> login(String username, String password) {
    String url = host + "/BKEMS-1.0/oauth/token";

    return http.post(url, headers: {
      'Authorization': 'Basic Y2xpZW50YXBwOjEyMzQ1Ng=='
    }, body: {
      'username': username,
      'password': password,
      'scope': 'read write',
      'grant_type': 'password',
      'client_secret': '123456',
      'client_id': 'clientapp'
    });
  }

  static Future<Response> getInfoStudent(String token) {
    String url = host + "/BKEMS-1.0/me";
    Singleton.token = "bearer " + token;
    return http.post(url, headers: {'Authorization': Singleton.token});
  }

  static Future<Response> infoStudentById(String idStudent) {
    String url = host + "BKEMS-1.0/tbl_qldt_qlhv_hocvien/" + idStudent;
    return http.get(url);
  }

  //todo semester of score
  static Future<ServerResponse> infoSemestersById<T>(
      String codeStudent, Function fromJson, Function _default) async {
    String url =
        host + "BKEMS-1.0/common/sp/micro/sp_QLHV_BangDiemCaNhan_DiemTBHocKy";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "mshv","value1": "' + codeStudent.toString() + '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;
    return await handleError<T>(client.send(request), fromJson, _default);
  }

  static Future<ServerResponse> infoScoreBySemester<T>(String idStudent,
      String idSemester, Function fromJson, Function _default) async {
    String url =
        host + "BKEMS-1.0/common/sp/micro/sp_QLHV_BangDiemCaNhan_HocKy_ChiTiet";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},{"col": "hkid","value1": "' +
        idSemester.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  //todo semester of schedule
  static Future<ServerResponse> infoSemestersScheduleById<T>(
      String idStudent, Function fromJson, Function _default) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_QLHV_TKB_HocKy";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' + idStudent.toString() + '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  //todo schedule
  static Future<StreamedResponse> infoSummaryScheduleById(
      String idStudent, String idSemester) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_QLHV_TKB_HocKy_TongQuat";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},{"col": "hkid","value1": "' +
        idSemester.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await client.send(request);
  }

  static Future<StreamedResponse> infoSubjectsSemestersScheduleById(
      String idStudent, String idSemester) async {
    String url =
        host + "BKEMS-1.0/common/sp/micro/sp_QLHV_TKB_MonHoc_ByHocVien_ByHocKy";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},{"col": "hkid","value1": "' +
        idSemester.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await client.send(request);
  }

  static Future<StreamedResponse> infoScheduleByDate(
      String idStudent, DateTime date) async {
    if (date == null) date = new DateTime.now();
    String strDate = date.toIso8601String();
    String url =
        host + "BKEMS-1.0/common/sp/micro/sp_QLHV_TKB_ByHocVien_ByNgay";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},{"col": "ngay","value1": "' +
        strDate.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await client.send(request);
  }

  static Future<StreamedResponse> infoScheduleBy2Date(
      String idStudent, DateTime dateStart, DateTime dateEnd) async {
    if (dateStart == null) dateStart = new DateTime.now();
    String strDateStart = dateStart.toIso8601String();

    if (dateEnd == null) dateEnd = new DateTime.now();
    String strDateEnd = dateEnd.toIso8601String();

    String url =
        host + "BKEMS-1.0/common/sp/micro/sp_QLHV_TKB_ByHocVien_By2Ngay";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},{"col": "ngaybd","value1": "' +
        strDateStart.toString() +
        '"},{"col": "ngaykt","value1": "' +
        strDateEnd.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await client.send(request);
  }

  //todo schedule detail class subject
  static Future<ServerResponse> infoScheduleByClassSubject<T>(String idStudent,
      String idClassSubject, Function fromJson, Function _default) async {
    String url =
        host + "BKEMS-1.0/common/sp/micro/sp_QLHV_TKB_LopMonHoc_ByHocVien";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},{"col": "lmhid","value1": "' +
        idClassSubject.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  static Future<ServerResponse> infoCourseCategory<T>(
      String idGroupMajor, Function fromJson, Function _default) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_QLHV_KhoiNganh_MonHoc";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "knid","value1": "' + idGroupMajor.toString() + '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  static Future<ServerResponse> infoFee<T>(String idStudent, String idSemester,
      Function fromJson, Function _default) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_QLHV_HocPhi_ByHocKy";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},{"col": "hkid","value1": "' +
        idSemester.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  static Future<ServerResponse> infoReceiptFee<T>(
      String idStudent, Function fromJson, Function _default) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_QLHV_PhieuThu_ByHocVien";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' + idStudent.toString() + '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  static Future<ServerResponse> infoClassSubjectBySubject<T>(
      String idStudent,
      String idSemester,
      String idSubject,
      Function fromJson,
      Function _default) async {
    String url =
        host + "BKEMS-1.0/common/sp/micro/sp_DKTKB_LopMonHoc_ByHocVien_ByMonHoc";
    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},'
        '{"col": "mhid","value1": "' +
        idSubject.toString() +
        '"},'
        '{"col": "hkid","value1": "' +
        idSemester.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  //todo kết quả
  static Future<ServerResponse> infoResultRegister<T>(String idStudent,
      String idSemester, Function fromJson, Function _default) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_DKTKB_KQDK";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},{"col": "hkid","value1": "' +
        idSemester.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  //todo register
  static Future<ServerResponse> registerClassSubject<T>(
      RegisterSubjectList list,
      Function fromJson,
      Function _default) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_DKTKB_DangKy_LopMonHoc";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));

    String idStudent = Singleton.student.id.toString();
    String idSemester = "-1";
    String idClass1 = "-1";
    String idClass2 = "-1";

    if(list.list.length > 0){
      idClass1 = list.list[0].selected.idClassSubject.toString();
      idSemester = list.list[0].selected.idSemester.toString();
    }
    if(list.list.length > 1)
      idClass2 = list.list[1].selected.idClassSubject.toString();

    var body = '[{"col": "hvid","value1": "' + idStudent.toString() + '"},'
        '{"col": "hkid","value1": "' + idSemester.toString() + '"},'
        '{"col": "lmhid1","value1": "' + idClass1.toString() + '"},'
        '{"col": "lmhid2","value1": "' + idClass2.toString() + '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  //todo delete
  static Future<ServerResponse> deleteClassSubject<T>(
      String idResult, Function fromJson, Function _default) async {
    String url = host +
        "BKEMS-1.0/tbl_qldt_dkmh_hocvien_dangky_lopmonhoc?id1=" +
        idResult;

    var client = new http.Client();
    var request = new http.Request('DELETE', Uri.parse(url));

    print("Token: " + Singleton.token);
    request
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  //todo delete 2 items
  static Future<ServerResponse> deleteClassSubject2Items<T>(String idResult1,
      String idResult2, Function fromJson, Function _default) async {
    String url = host +
        "BKEMS-1.0/tbl_qldt_dkmh_hocvien_dangky_lopmonhoc?id1=" +
        idResult1 +
        "&id2=" +
        idResult2;
    var client = new http.Client();
    var request = new http.Request('DELETE', Uri.parse(url));
    print("Token: " + Singleton.token);
    request
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  //todo get semester register
  static Future<ServerResponse> infoSemesterRegister<T>(
      String idStudent, Function fromJson, Function _default) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_DKMH_HocKy";
    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' + idStudent.toString() + '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  //todo get options: hệ đào tạo, học kỳ, khối ngành
  static Future<ServerResponse> infoOptions<T>(Function fromJson, Function _default) async {
    String url = host + "BKEMS-1.0/tbl_qldt_dkmh_loadcombobox";
    var client = new http.Client();
    var request = new http.Request('GET', Uri.parse(url));

    return await handleError<T>(client.send(request), fromJson, _default);
  }

  //todo danh sáhc môn học nợ
  static Future<StreamedResponse>  infoSubjectAgain<T>(String codeStudent, String search) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_DKTKB_DSMonNo";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        codeStudent.toString() +
        '"},{"col": "search","value1": "' +
        search.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await client.send(request);
  }

  //todo danh sách môn tương đương
  static Future<StreamedResponse>  infoSubjectEquivalent<T>(String idCourse, String idGroup, String search) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_DKTKB_MonThayThe";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "knid","value1": "' +
        idGroup.toString() +
        '"},{"col": "khoahoc","value1": "' +
        idCourse.toString() +
        '"},{"col": "search","value1": "' +
        search.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await client.send(request);
  }

  //todo danh sáhc môn học cải thiện
  static Future<StreamedResponse>  infoSubjectImprove<T>(String codeStudent, String search) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_DKTKB_DSMonCaiThien";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        codeStudent.toString() +
        '"},{"col": "search","value1": "' +
        search.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await client.send(request);
  }

  //todo danh sáhc môn học mới
  static Future<StreamedResponse>  infoSubjectNew<T>(String idStudent, String idSemester, String idGroupMajor, String idCourse, String search) async {
    String url = host + "BKEMS-1.0/common/sp/micro/sp_DKTKB_MonHocMoi_ByHocVien";

    var client = new http.Client();
    var request = new http.Request('POST', Uri.parse(url));
    var body = '[{"col": "hvid","value1": "' +
        idStudent.toString() +
        '"},{"col": "hkid","value1": "' +
        idSemester.toString() +
        '"},{"col": "knid","value1": "' +
        idGroupMajor.toString() +
        '"},{"col": "khoahoc","value1": "' +
        idCourse.toString() +
        '"},{"col": "search","value1": "' +
        search.toString() +
        '"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await client.send(request);
  }

  //todo đổi mật khẩu
  static Future<ServerResponse> changePassword<T>(String username, String passwordOld, String passwordNew,Function fromJson, Function _default) async {
    String url = host + "BKEMS-1.0/tbl_taikhoan_doimatkhau";

    var client = new http.Client();
    var request = new http.Request('PUT', Uri.parse(url));
    var body = '[{"password": \"'+passwordOld+'\","login": \"'+username+'\"},{"password": \"'+passwordNew+'\","login": \"'+username+'\"}]';

    print("Token: " + Singleton.token);
    print("Body: " + body);
    request
      ..body = body
      ..headers[HttpHeaders.CONTENT_TYPE] = 'application/json'
      ..headers[HttpHeaders.AUTHORIZATION] = Singleton.token;

    return await handleError<T>(client.send(request), fromJson, _default);
  }
}
