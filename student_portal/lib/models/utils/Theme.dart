import 'dart:ui';

import 'package:flutter/material.dart';

class ThemeColor {
  static const String KEY_BACKGROUND = "KEY_BACKGROUND";
  static const String KEY_PRIMARY_COLOR = "KEY_PRIMARY_COLOR";
  static const String KEY_SECOND_COLOR = "KEY_SECOND_COLOR";

  String background;
  Color primaryColor;
  Color secondColor;

  ThemeColor(this.background, this.primaryColor, this.secondColor);

  factory ThemeColor.red() {
    return new ThemeColor(BackgroundConstant.BACKGROUND_RED,
        new Color(0xffcc4125), Colors.greenAccent);
  }

  factory ThemeColor.yellow() {
    return new ThemeColor(BackgroundConstant.BACKGROUND_YELLOW,
        new Color(0xffbf9000), Colors.blueAccent);
  }

  factory ThemeColor.green() {
    return new ThemeColor(BackgroundConstant.BACKGROUND_GREEN,
        new Color(0xff6aa84f), Colors.redAccent);
  }

  factory ThemeColor.cyan() {
    return new ThemeColor(BackgroundConstant.BACKGROUND_CYAN,
        new Color(0xff45818e), Colors.deepPurple);
  }

  factory ThemeColor.blue() {
    return new ThemeColor(BackgroundConstant.BACKGROUND_BLUE,
        new Color(0xff0b5394), Colors.yellowAccent);
  }

  factory ThemeColor.purple() {
    return new ThemeColor(BackgroundConstant.BACKGROUND_PURPLE,
        new Color(0xff674ea7), Colors.cyanAccent);
  }

  factory ThemeColor.pink() {
    return new ThemeColor(BackgroundConstant.BACKGROUND_PINK,
        new Color(0xff741b47), Colors.lightGreenAccent);
  }

  factory ThemeColor.fromJson(dynamic json) {
    return new ThemeColor(json[KEY_BACKGROUND],
        new Color(json[KEY_PRIMARY_COLOR]), new Color(json[KEY_SECOND_COLOR]));
  }

  factory ThemeColor.byNameBackground(String background) {
    switch (background) {
      case BackgroundConstant.BACKGROUND_RED:
        return new ThemeColor.red();
      case BackgroundConstant.BACKGROUND_YELLOW:
        return new ThemeColor.yellow();
      case BackgroundConstant.BACKGROUND_GREEN:
        return new ThemeColor.green();
      case BackgroundConstant.BACKGROUND_CYAN:
        return new ThemeColor.cyan();
      case BackgroundConstant.BACKGROUND_BLUE:
        return new ThemeColor.blue();
      case BackgroundConstant.BACKGROUND_PURPLE:
        return new ThemeColor.purple();
      default:
        return new ThemeColor.pink();
    }
  }

  Map toJson() {
    return {
      KEY_BACKGROUND: background,
      KEY_PRIMARY_COLOR: primaryColor.value,
      KEY_SECOND_COLOR: secondColor.value,
    };
  }
}

class BackgroundConstant {
  static const String BACKGROUND_RED = "resources/images/background_red.jpg";
  static const String BACKGROUND_YELLOW =
      "resources/images/background_yellow.jpg";
  static const String BACKGROUND_GREEN =
      "resources/images/background_green.jpg";
  static const String BACKGROUND_CYAN = "resources/images/background_cyan.jpg";
  static const String BACKGROUND_BLUE = "resources/images/background_blue.jpg";
  static const String BACKGROUND_PURPLE =
      "resources/images/background_purple.jpg";
  static const String BACKGROUND_PINK = "resources/images/background_pink.jpg";

  static String getNameBackground(String background) {
    switch (background) {
      case BackgroundConstant.BACKGROUND_RED:
        return "Fire background";
      case BackgroundConstant.BACKGROUND_BLUE:
        return "Water background";
      case BackgroundConstant.BACKGROUND_CYAN:
        return "Sky background";
      case BackgroundConstant.BACKGROUND_GREEN:
        return "Windy background";
      case BackgroundConstant.BACKGROUND_PINK:
        return "Cherry blossom background";
      case BackgroundConstant.BACKGROUND_PURPLE:
        return "Orchid background";
      default:
        return "Sun background";
    }
  }

  static int getIndexBackground(String background) {
    switch (background) {
      case BackgroundConstant.BACKGROUND_RED:
        return 0;
      case BackgroundConstant.BACKGROUND_YELLOW:
        return 1;
      case BackgroundConstant.BACKGROUND_GREEN:
        return 2;
      case BackgroundConstant.BACKGROUND_CYAN:
        return 3;
      case BackgroundConstant.BACKGROUND_BLUE:
        return 4;
      case BackgroundConstant.BACKGROUND_PURPLE:
        return 5;
      default:
        return 6;
    }
  }
}
