import 'package:flutter/material.dart';
import 'package:student_portal/models/objects/Student.dart';
import 'package:student_portal/models/utils/Setting.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';
import 'package:student_portal/widgets/pages/LoadingPage.dart';


class Singleton{

  static String token;
  static Student student;
  static BaseFragment currentFragment;
  static LoadingPage loadingPage;

  //todo Settings
  static Setting setting = Setting.byDefault(isShowHint: true);
  
  //todo default color
  static final Color textLightColor = Colors.white;
  static final Color textDarkColor = Colors.black87;
  static final Color backgroundNothing = Colors.white;
  static final Color dividerColor = Colors.black38;
  static final Color disableColor = Colors.black26;
  static final Color dark80OpacityColor = Colors.black.withOpacity(0.8);
  static final Color hintColor = Colors.black38;
  static final Color yellow10OpacityColor = Colors.yellowAccent.shade100;
  static final Color menuTextColor = Colors.black54;
  static final Color darkColor = Colors.black87;
  static final Color checkColor = Colors.green;

  static final Color starColor = Colors.yellowAccent;
  static final Color clearColor = Colors.redAccent;
  static final Color numberStudentColor = Colors.greenAccent;
  static final Color numberStudentMaxColor = Colors.redAccent;


  //todo *************************************CONSTANTS*************************************
  //todo id
  static const int HOME_FRAGMENT_ID = 1230;
  static const int SCHEDULE_FRAGMENT_ID = 1231;
  static const int SCORE_FRAGMENT_ID = 1232;
  static const int COURSE_CATEGORY_FRAGMENT_ID = 1233;
  static const int NOTIFICATION_FRAGMENT_ID = 1234;
  static const int FEE_FRAGMENT_ID = 1235;
  static const int REGISTER_FRAGMENT_ID = 1236;
  static const int SETTINGS_FRAGMENT_ID = 1237;

}