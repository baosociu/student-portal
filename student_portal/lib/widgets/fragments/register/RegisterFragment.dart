
import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/register/RegisterAPI.dart';
import 'package:student_portal/global/service/register/RegisterStream.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';
import 'package:student_portal/widgets/fragments/register/view/RegisterContentView.dart';

// ignore: must_be_immutable
class RegisterFragment extends BaseFragment {
  RegisterFragmentState state;

  RegisterFragment(mainPage)
      : super(mainPage, Singleton.REGISTER_FRAGMENT_ID,
            Singleton.setting.localization.register_fragment_title,"resources/icons/register.png");

  @override
  RegisterFragmentState createState() {
    super.createState();
    state = new RegisterFragmentState();
    return state;
  }

  void reload() {
    this.state.reload();
  }
}

class RegisterFragmentState extends BaseFragmentState {
  RegisterContentView content;

  @override
  void initState() {
    super.initState();
    final RegisterAPI api = new RegisterAPI();
    final RegisterStream stream = new RegisterStream(api);
    this.content = new RegisterContentView(stream.state, stream.onExecute.add);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(resizeToAvoidBottomPadding: false, body: content);
  }

  void reload() {
    content.myState.onExecute(type: LoadingType.refresh);
  }
}
