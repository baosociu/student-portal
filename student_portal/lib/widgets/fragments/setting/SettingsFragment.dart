import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/AlertUtils.dart';
import 'package:student_portal/global/utils/SharedPrefrencesUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/models/utils/Setting.dart';
import 'package:student_portal/widgets/components/CardView.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';
import 'package:student_portal/widgets/fragments/setting/SettingView.dart';

// ignore: must_be_immutable
class SettingsFragment extends BaseFragment {
  SettingsFragment(mainPage)
      : super(mainPage, Singleton.SETTINGS_FRAGMENT_ID,
            Singleton.setting.localization.settings_fragment_title,"resources/icons/settings.png");
  SettingsFragmentState state;
  @override
  SettingsFragmentState createState() {
    super.createState();
    state = new SettingsFragmentState();
    return state;
  }

  void onSave() {
    this.state.onSave();
  }
}

class SettingsFragmentState extends BaseFragmentState {
  int indexExpanded;
  Setting setting;
  BuildContext _context;
  @override
  void initState() {
    super.initState();
    this.indexExpanded = -1;
    this.setting = new Setting(
        Singleton.setting.theme,
        Singleton.setting.titleFont,
        Singleton.setting.navigationFont,
        Singleton.setting.textFont,
        Singleton.setting.localization,
        Singleton.setting.isShowHint);
  }

  void onTap(int index) {
    setState(() {
      this.indexExpanded = this.indexExpanded == index ? -1 : index;
    });
  }

  Future onSave() async {
    print(setting.toJson().toString());
    bool saved = await SharedPreferencesUtils.saveSettings(setting);
    ToastUtils.showToast(
        _context,
        (saved
            ? Singleton.setting.localization.save_setting_sucessfully
            : Singleton.setting.localization.save_setting_failure));

    // StudentPortalWidget.restartApp(context);
    AlertUtils.showAlertRestart(context);
  }

  @override
  Widget build(BuildContext context) {
    List<SettingView> items = <SettingView>[
      new SettingView.background(
        isExpanded: this.indexExpanded == 1,
        onTap: () => onTap(1),
      ),
      new SettingView.font(
        onTap: () => onTap(2),
        isExpanded: this.indexExpanded == 2,
      ),
      new SettingView.language(
        isExpanded: this.indexExpanded == 3,
        onTap: () => onTap(3),
      ),
      new SettingView.hint(
        isExpanded: this.indexExpanded == 4,
        onTap: () => onTap(4),
      )
    ];

    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        body: new Builder(builder: (BuildContext context) {
          _context = context;
          return new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Flexible(
                  child: new CardView(
                child: new Flexible(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Flexible(
                          child: new ListView.builder(
                              itemCount: items.length,
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return new Column(
                                  children: <Widget>[
                                    items[index],
                                    index < items.length - 1
                                        ? new Divider()
                                        : new SizedBox(),
                                  ],
                                );
                              })),
                      new Column(
                        children: <Widget>[
                          new Center(
                            child: new Padding(
                              padding: new EdgeInsets.all(8.0),
                              child: new InkWell(
                                child: new TextView.text(
                                  Singleton.setting.localization
                                      .save_setting_by_default,
                                  color: Singleton.setting.primaryColor,
                                  textSize: TextView.MEDIUM_SIZE,
                                ),
                                onTap: () {
                                  this.setting = new Setting.byDefault();
                                  onSave();
                                },
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                isShowTitle: false,
              )),
            ],
          );
        }));
  }
}
