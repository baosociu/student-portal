
class Subject {
  final int index;
  final String nameSubject;
  final String codeSubject;
  bool isSelected = true;
  List<ClassSubject> classSubjects = new List<ClassSubject>();

  Subject(this.index, this.codeSubject,this.nameSubject);
}

class SubjectList {
  List<Subject> list = new List<Subject>();

  SubjectList.fromListClassSubject(List<ClassSubject> data) {
    this.list = new List<Subject>();
    data.forEach((item) {
      int i = 0;
      for (i = 0; i < this.list.length; i++)
        if (this.list[i].codeSubject == item.codeSubject) break;
      if (i == this.list.length)
        this.list.add(
            new Subject(this.list.length, item.codeSubject, item.nameSubject));
      this.list[i].classSubjects.add(item);
    });
  }
}

class ClassSubject {
  final int index;
  final String codeSubject;
  final String idClassSubject;
  final String codeClassSubject = "code";
  final String teacher = "teacher";
  final String nameSubject;
  final int dayOfWeek;
  final int startPeriod;
  final int endPeriod;
  final String room;

  ClassSubject(
      this.index,
      this.codeSubject,
      this.idClassSubject,
      this.nameSubject,
      this.dayOfWeek,
      this.startPeriod,
      this.endPeriod,
      this.room);

  void printf(){
    print("Thứ "+dayOfWeek.toString()+" - Tiết "+startPeriod.toString()+" - "+endPeriod.toString()+" "+nameSubject);
  }
}
