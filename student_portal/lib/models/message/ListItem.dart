import 'package:student_portal/models/message/Item.dart';


class ListItem {
  List<Item> data = new List<Item>();

  ListItem(){
    data = new List<Item>();
  }

  Map toJson(){
    data = data.reversed.toList();
    Map r = new Map();
    data.forEach((f){
      r[f.itemId] = f.toJson();
    });
    return r;
  }

  ListItem.fromJson(Map json){
    data = new List<Item>();
    json.forEach((key,item){
      data.insert(0, new Item.fromJson(item));
    });
    //data = data.reversed.toList();
  }
}