import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/FadeRoute.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Subject.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/schedule/view/ScheduleBySubjectDetailFragment.dart';

class ScheduleBySubjectListView extends StatelessWidget {
  final SubjectScheduleList subjectScheduleList;

  ScheduleBySubjectListView(this.subjectScheduleList);

  @override
  Widget build(BuildContext context) {
    return new Flexible(
        child: new ListView.builder(
            shrinkWrap: true,
            itemCount: subjectScheduleList.list.length,
            itemBuilder: (context, index) {
              SubjectSchedule subjectSchedule = subjectScheduleList.list[index];
              return new Column(children: <Widget>[
                new ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      new FadeRoute(
                          builder: (context) =>
                              new ScheduleBySubjectDetailFragment(
                                  subjectSchedule.idClassSubject,
                                  subjectSchedule.name.toString())),
                    );
                  },
                  title: buildTitle(subjectSchedule),
                  subtitle: buildSubtitle(subjectSchedule),
                ),
                index < subjectScheduleList.list.length -1 ? new Divider(color: Singleton.setting.primaryColor): new SizedBox(),
              ]);
            }));
  }

  Widget buildTitle(SubjectSchedule item) {
    Color color = Singleton.setting.primaryColor;
    Color color20 = Singleton.setting.primary20OpacityColor;

    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Flexible(
            child: new Row(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(4.0),
                  child: new CircleAvatar(
                    child: new Icon(Icons.library_books,
                        color: Singleton.backgroundNothing),
                    backgroundColor: color,
                  ),
                ),
                new Flexible(
                    child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new TextView.title(
                      item.name.toString(),
                      overflow: TextOverflow.ellipsis,
                    ),
                    new TextView.text(
                      Singleton.setting.localization.code_subject +
                          ": " +
                          item.code.toString(),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )),
              ],
            ),
          ),
          new Icon(
            Icons.chevron_right,
            color: color,
          ),
        ],
      ),
      decoration: new BoxDecoration(
          gradient: new LinearGradient(
              tileMode: TileMode.clamp,
              colors: <Color>[
            Colors.transparent,
            color20,
            color20,
            Colors.transparent
          ])),
    );
  }

  Widget buildSubtitle(SubjectSchedule item) {
    return new Container(
      margin: new EdgeInsets.only(left: 24.0),
      decoration: new BoxDecoration(
          gradient: new RadialGradient(
              radius: 1.0,
              center: Alignment.bottomLeft,
              colors: <Color>[
            Singleton.setting.secondColor,
            Singleton.backgroundNothing
          ])),
      child: new Container(
        margin: new EdgeInsets.only(left: 4.0, bottom: 4.0),
        color: Singleton.backgroundNothing,
        child: new Padding(
          padding: new EdgeInsets.only(left: 8.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ViewUtils.buildItemTextView(
                Singleton.setting.localization.credit + ": ",
                item.weight.toString(),
              ),
              ViewUtils.buildItemTextView(
                  Singleton.setting.localization.progress_rate + ": ",
                  item.rateProgress.toString() + "%"),
              ViewUtils.buildItemTextView(
                Singleton.setting.localization.middle_rate + ": ",
                item.rateMiddle.toString() + "%",
              ),
              ViewUtils.buildItemTextView(
                Singleton.setting.localization.final_rate + ": ",
                item.rateFinal.toString() + "%",
              )
            ],
          )
        ),
      ),
    );
  }
}
