import 'package:student_portal/global/utils/DateTimeUtils.dart';
import 'package:student_portal/global/utils/StringUtils.dart';

class BaseHeaderSchedule {
  dynamic id;
  String name;
}

class BaseSchedule {
  dynamic idStudent;
  String typeRoom;
  dynamic idClassSubject;
  dynamic idBranch; //cơ sở
  String nameSubject;
  String nameRoom;
  dynamic idSubject;
  DateTime date;
  String nameBranch;
  String labelDayOfWeek;
  String labelDayOfWeekShortest;

  BaseSchedule();

  BaseSchedule.fromJson(Map json) {
    this.idStudent = json['hocVienId'];
    this.typeRoom = json['loaiPhong'];
    this.idClassSubject = json['lopMonHocId'];
    this.idBranch = json['coSoId'];
    this.nameSubject = json['tenMonHoc'];
    this.nameRoom = json['tenPhong'];
    this.idSubject = json['monHocId'];
    this.date = DateTimeUtils.convertStringToDateTime(
        json['ngayTrongTuan'], "ddMMyyyy", "-");
    this.nameBranch = json['tenCoSo'];
    this.labelDayOfWeek = StringUtils.getLabelOfWeek(this.date);
    this.labelDayOfWeekShortest = StringUtils.getLabelOfWeekShortest(date);
  }

  Map<String, dynamic> toJson() {
    return {
      'hocVienId': this.idStudent,
      'loaiPhong': this.typeRoom,
      'lopMonHocId': this.idClassSubject,
      'coSoId': this.idBranch,
      'tenMonHoc': this.nameSubject,
      'tenPhong': this.nameRoom,
      'monHocId': this.idSubject,
      'ngayTrongTuan':
          StringUtils.convertDateTimeToString(this.date, "ddMMyyyy"),
      'tenCoSo': this.nameBranch,
    };
  }
}

class TestSchedule extends BaseSchedule {
  String startTime;
  dynamic timeTest; //tiết thi
  String teacher1;
  String teacher2;
  String typeTest;
  int time;

  TestSchedule();

  TestSchedule.fromJson(Map json) : super.fromJson(json) {
    this.startTime = json['gioBatDau'];
    this.timeTest = json['tietThi'];
    this.teacher1 = json['canBoCoiThi1'];
    this.teacher2 = json['canBoCoiThi2'];
    this.typeTest = json['loaiThi'];
    this.time = json['thoiGian'];
  }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson();
    json['gioBatDau'] = this.startTime;
    json['tietThi'] = this.timeTest;
    json['canBoCoiThi1'] = this.teacher1;
    json['canBoCoiThi2'] = this.teacher2;
    json['loaiThi'] = this.typeTest;
    json['thoiGian'] = this.time;
    return json;
  }
}

class NormalSchedule extends BaseSchedule {
  int countTime; //số tiết
  int indexWeek;
  String teacher;
  int time; //tiết học

  NormalSchedule();

  NormalSchedule.fromJson(Map json) : super.fromJson(json) {
    this.countTime = json['soTiet'];
    this.indexWeek = json['tuanHoc'];
    this.teacher = json['tenGiangVien'];
    this.time = json['tietHoc'];
  }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson();
    json['soTiet'] = this.countTime;
    json['tuanHoc'] = this.indexWeek;
    json['tenGiangVien'] = this.teacher;
    json['tietHoc'] = this.time;
    return json;
  }
}

class BaseScheduleList {
  List<BaseSchedule> list = new List<BaseSchedule>();

  BaseScheduleList.fromJson(dynamic data) {
    list = new List<BaseSchedule>();

    for (Map item in data) {
      String type = item['loaiThi'];
      if (type == null) {
        NormalSchedule schedule = new NormalSchedule.fromJson(item);
        list.add(schedule);
      } else {
        TestSchedule schedule = new TestSchedule.fromJson(item);
        list.add(schedule);
      }
    }
  }
}


class GroupScheduleByDay {
  final DateTime date;
  final String labelDayOfWeek;
  final String labelDayOfWeekShortest;
  final List<BaseSchedule> items;

  GroupScheduleByDay(
      this.date, this.labelDayOfWeek, this.labelDayOfWeekShortest, this.items);

  factory GroupScheduleByDay.from(
      BaseSchedule item, List<BaseSchedule> items) {
    return new GroupScheduleByDay(
        item.date, item.labelDayOfWeek, item.labelDayOfWeekShortest, items);
  }
}

class GroupScheduleByDayList {
  final List<GroupScheduleByDay> items;

  GroupScheduleByDayList(this.items);

  factory GroupScheduleByDayList.from(List<BaseSchedule> items) {
    Map<dynamic, GroupScheduleByDay> maps = new Map();
    for (BaseSchedule item in items) {
      if (!maps.containsKey(item.date))
        maps[item.date] = new GroupScheduleByDay.from(
            item, new List<BaseSchedule>());
      maps[item.date].items.add(item);
    }

    return new GroupScheduleByDayList(maps.values.toList());
  }
}
