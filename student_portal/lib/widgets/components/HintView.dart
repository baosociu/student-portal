import 'package:flutter/material.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class HintView extends StatefulWidget {
  final String text;

  const HintView({Key key, this.text}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new HintViewState(this.text);
  }
}

class HintViewState extends State<HintView> {
  final String text;

  HintViewState(this.text);

  @override
  Widget build(BuildContext context) {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        new Container(
            margin: new EdgeInsets.all(10.0),
            padding: new EdgeInsets.all(5.0),
            decoration: new BoxDecoration(color: Colors.black.withOpacity(0.8)),
            child: new Column(
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    new Icon(
                      Icons.clear,
                      color: Colors.white,
                      size: 16.0,
                    ),
                  ],
                ),
//                new Padding(
//                  padding: new EdgeInsets.only(right: 20.0, left: 20.0),
//                  child: new Divider(
//                    color: Colors.white,
//                  ),
//                ),
                new Container(
                  padding: new EdgeInsets.all(10.0),
                  child: new Align(
                    alignment: Alignment.center,
                    child: new TextView.text(
                      text,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            )),
      ],
    );
  }
}
