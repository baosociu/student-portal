
class Fee {
  dynamic idStudent;
  double weightTheory;
  dynamic idStaff;
  double priceTheory;
  dynamic idClassSubject;
  String dateUpdate;
  dynamic idSemester;
  double weightPractice;
  double pricePractice;
  String codeSubject;
  double totalPrice;
  String nameSubject;
  dynamic idSubject;
  String nameSemester;

  Fee.fromJson(Map json){
    this.idStudent = json['hocVienId'];
    this.weightTheory = json['heSoTinChiLT'];
    this.idStaff = json['canBoId'];
    this.priceTheory = json['donGiaTinChiLT'];
    this.idClassSubject = json['lopMonHocId'];
    this.dateUpdate = json['ngayCapNhat'];
    this.idSemester = json['hocKyId'];
    this.weightPractice = json['heSoTinChiTH'];
    this.pricePractice = json['donGiaTinChiTH'];
    this.codeSubject = json['maMonHoc'];
    this.totalPrice = json['tongTien'];
    this.nameSubject = json['tenMonHoc'];
    this.idSubject = json['monHocId'];
    this.nameSemester = json['tenHocKy'];
  }
}

class FeeList{
  List<Fee> list = new List<Fee>();

  FeeList.fromJson(dynamic data){
    this.list = new List<Fee>();

    data.forEach((json){
      Fee value = new Fee.fromJson(json);
      this.list.add(value);
    });
  }
}

class FeeSemesterItem{
  double weightTheory;
  dynamic idStaff;
  double priceTheory;
  dynamic idClassSubject;
  String dateUpdate;
  double weightPractice;
  double pricePractice;
  String codeSubject;
  double totalPrice;
  String nameSubject;
  dynamic idSubject;
  
  FeeSemesterItem.fromFee(Fee f){
    this.weightTheory = f.weightTheory;
    this.idStaff = f.idStaff;
    this.priceTheory = f.priceTheory;
    this.idClassSubject = f.idClassSubject;
    this.dateUpdate = f.dateUpdate;
    this.weightPractice = f.weightPractice;
    this.pricePractice = f.pricePractice;
    this.codeSubject = f.codeSubject;
    this.totalPrice = f.totalPrice;
    this.nameSubject = f.nameSubject;
    this.idSubject = f.idSubject;
  }
}

class FeeSemester{
  dynamic idStudent;
  dynamic idSemester;
  final String nameSemester;
  List<FeeSemesterItem> list = new List<FeeSemesterItem>();

  FeeSemester(this.idStudent, this.idSemester, this.nameSemester);
}

class FeeSemesterList{
  List<FeeSemester> list = new List<FeeSemester>();
  
  FeeSemesterList.fromJson(dynamic data){
    list = new List<FeeSemester>();
    
    FeeList feeList = new FeeList.fromJson(data);
    feeList.list.forEach((Fee feeListItem){
      
      int i = 0;
      for(i = 0;i < list.length;i++)
        if(list[i].idSemester == feeListItem.idSemester)
          break;
      
      if(i == list.length)
        this.list.add(new FeeSemester(feeListItem.idStudent, feeListItem.idSemester, feeListItem.nameSemester));
      this.list[i].list.add(new FeeSemesterItem.fromFee(feeListItem));
    });
  }
}