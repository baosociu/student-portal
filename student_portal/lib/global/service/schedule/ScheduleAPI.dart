import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/global/service/schedule/ScheduleRequest.dart';
import 'package:student_portal/global/service/schedule/ScheduleResult.dart';

class ScheduleAPI {
  final Map<String, ScheduleResult> cache;
  final HttpClient client;

  ScheduleAPI({
    HttpClient client,
    Map<String, ScheduleResult> cache,
  })  : this.client = client ?? new HttpClient(),
        this.cache = cache ?? <String, ScheduleResult>{};

  Future<ScheduleResult> execute(ScheduleRequest request) async {
    String key = getKey(request);

    if (key.isNotEmpty && cache.containsKey(key) && !request.isRefresh) {
      return cache[key];
    } else {
      final result = await _fetchResults(request);

      if (key.isNotEmpty && !result.isEmpty) cache[key] = result;

      return result;
    }
  }

  Future<ScheduleResult> _fetchResults(ScheduleRequest request) async {
    return await fetchByScheduleType(request).then((response) async {
      final dynamic data = await json
          .decode(await response.stream.transform(utf8.decoder).join());
      switch (response.statusCode) {
        case HttpStatus.OK:
          switch (request.type) {
            case ScheduleType.day:
              return ScheduleResult.fromJsonDate(data);
            case ScheduleType.week:
              return ScheduleResult.fromJsonWeek(data);
            case ScheduleType.subject:
              return ScheduleResult.fromJsonSubject(data);
            case ScheduleType.semester:
              return ScheduleResult.fromJsonSemester(data);
            default:
              return ScheduleResult.hasError(Singleton.setting.localization.error_in_process);
          }
          break;
        case HttpStatus.UNAUTHORIZED:
          return ScheduleResult.loginAgain(Singleton.setting.localization.require_try_login);
        default:
          return ScheduleResult
              .hasError(Singleton.setting.localization.error+": " + response.reasonPhrase.toString());
      }
    });
  }

  Future<StreamedResponse> fetchByScheduleType(ScheduleRequest request) async {
    String idStudent = Singleton.student.id.toString();
    switch (request.type) {
      case ScheduleType.day:
        return API.infoScheduleByDate(
            idStudent, (request as ScheduleDateRequest).date);
      case ScheduleType.week:
        ScheduleWeekRequest _request = (request as ScheduleWeekRequest);
        return API.infoScheduleBy2Date(
            idStudent, _request.startDate, _request.endDate);
      case ScheduleType.subject:
        ScheduleSubjectRequest _request = (request as ScheduleSubjectRequest);
        return API.infoSubjectsSemestersScheduleById(idStudent,
            _request.listHeader[_request.indexSelected].id.toString());
      default:
        ScheduleSemesterRequest _request = (request as ScheduleSemesterRequest);
        return API.infoSummaryScheduleById(idStudent,
            _request.listHeader[_request.indexSelected].id.toString());
    }
  }

  String getKey(ScheduleRequest request) {
    switch (request.type) {
      case ScheduleType.day:
        return StringUtils.convertDateTimeToString(
            (request as ScheduleDateRequest).date, "ddMMyyyy");
      case ScheduleType.week:
        return StringUtils.convertDateTimeToString(
                (request as ScheduleWeekRequest).startDate, "ddMMyyyy") +
            " - " +
            StringUtils.convertDateTimeToString(
                (request as ScheduleWeekRequest).endDate, "ddMMyyyy");
      case ScheduleType.semester:
        return "Semester " +
            (request as ScheduleSemesterRequest)
                .listHeader[(request as ScheduleSemesterRequest).indexSelected]
                .name;
      case ScheduleType.subject:
        return "Subject " +
            (request as ScheduleSubjectRequest)
                .listHeader[(request as ScheduleSubjectRequest).indexSelected]
                .name;
      default:
        return "";
    }
  }
}
