import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/FadeRoute.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Receipt.dart';
import 'package:student_portal/widgets/components/CardView.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';
import 'package:student_portal/widgets/fragments/fee/DetailFeeFragment.dart';

// ignore: must_be_immutable
class FeeFragment extends BaseFragment {
  FeeFragment(mainPage)
      : super(mainPage, Singleton.FEE_FRAGMENT_ID,
            Singleton.setting.localization.fee_fragment_title,"resources/icons/fee.png");
  FeeFragmentState myState;
  @override
  FeeFragmentState createState() {
    super.createState();
    myState = new FeeFragmentState();
    return myState;
  }

  void refresh(){
    myState.refresh();
  }
}

class FeeFragmentState extends BaseFragmentState {
  ServerResponse<ReceiptSemesterList> response;
  String idStudent;

  bool isRefresh;
  BuildContext _context;

  @override
  void initState() {
    super.initState();
    idStudent = Singleton.student.id.toString();

    isRefresh = false;

    getInfoReceiptSemester(idStudent);
  }

  void refresh(){
    if(isRefresh)
      return;
    setState(()=> isRefresh = true);
    getInfoReceiptSemester(idStudent);
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        body: new Builder(builder: (BuildContext context) {
          _context = context;

          return new Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                isRefresh
                    ? ViewUtils.buildRefreshView(LoadingType.refresh)
                    : new SizedBox(),
                response == null
                    ? ViewUtils.buildLoadingCardView()
                    : buildFeeList(),
              ],
            ),
          );
        }));
  }

  Widget buildFeeList() {
    return response.buildViewByResponse<ReceiptSemester>(
        context: context,
        list: response.data.list,
        callback: () => getInfoReceiptSemester(idStudent),
        emptyView: ViewUtils.buildNothingSemesterCardView,
        item: (index, item) {
          return new CardView(
              isShowBottom: true,
              bottomTitle: Singleton.setting.localization.read_more,
              onTapBottomCenter: () {
                Navigator.of(context).push(
                      new FadeRoute(
                          builder: (context) => new DetailFeeFragment(
                              item.idSemester, item.nameSemester)),
                    );
              },
              child: buildFeeDetail(item),
              header: item.nameSemester.toString());
        });
  }

  Widget buildFeeDetail(ReceiptSemester _item) {
    return new ListView.builder(
        primary: false,
        itemCount: _item.list.length + 1,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return index == _item.list.length
              ? new Column(children: <Widget>[buildSummaryReceipt(_item)])
              : new Column(children: <Widget>[
                  buildFeeItem(index, _item.list[index]),
                  new Divider()
                ]);
        });
  }

  Widget buildFeeItem(int index, ReceiptSemesterItem item) {
    return new ListTile(
      onTap: () => ToastUtils.showToast(_context, item.note.toString()),
      leading: new CircleAvatar(
        child: new TextView.leading((index + 1).toString()),
        backgroundColor: Singleton.setting.primaryColor,
      ),
      subtitle: new TableView(
        <Widget>[
          ViewUtils.buildItemNumberTextView(
              Singleton.setting.localization.day + ":",
              item.dateReceipt.toString()),
          ViewUtils.buildItemNumberTextView(
              Singleton.setting.localization.price + ":",
              StringUtils.toVND(item.totalPrice)),
        ],
        margin: 0.0,
      ),
      trailing: new Icon(
        Icons.check_circle,
        color: Singleton.setting.primaryColor,
      ),
      //   dense: true,
    );
  }

  Widget buildSummaryReceipt(ReceiptSemester item) {
    return new ListTile(
      subtitle: new TableView(
        <Widget>[
          ViewUtils.buildItemNumberTextView(
              Singleton.setting.localization.total_price + ":",
              StringUtils.toVND(item.totalPriceSemesterReceipt * -1),
              fontWeight: FontWeight.bold),
          ViewUtils.buildItemNumberTextView(
              Singleton.setting.localization.paid + ":",
              StringUtils.toVND(item.totalPriceReceipt),
              fontWeight: FontWeight.bold),
          ViewUtils.buildItemNumberTextView(
              Singleton.setting.localization.debt + ":",
              StringUtils.toVND(item.totalPriceRemainReceipt * -1),
              fontWeight: FontWeight.bold),
        ],
      ),
      trailing: new Icon(
        Icons.attach_money,
        color: Singleton.setting.primaryColor,
      ),
    );
  }

  getInfoReceiptSemester(String idStudent) async {
    if (!isRefresh)
      setState(() {
        this.response = null;
      });

    final ServerResponse<ReceiptSemesterList> response =
        await API.infoReceiptFee<ReceiptSemesterList>(idStudent, (data) {
      return new ReceiptSemesterList.fromJson(data);
    }, () {
      return new ReceiptSemesterList.fromJson(new List<Map>());
    });
    if (!this.mounted) return;

    setState(() {
      this.response = response;
      this.isRefresh = false;
    });
  }
}
