import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/models/objects/Register.dart';
import 'package:student_portal/widgets/fragments/register/view/RegisterViewUtils.dart';

class ClassSubjectView extends StatefulWidget {
  final RegisterSubject item;
  final bool isExpanded;
  final Function(int index) indexChanged;

  const ClassSubjectView({Key key, this.item, this.isExpanded, this.indexChanged})
      : super(key: key);
  @override
  State<ClassSubjectView> createState() {
    return new ClassSubjectViewState(indexChanged);
  }
}

class ClassSubjectViewState extends State<ClassSubjectView> {
  int selected;
  RegisterSubject item;
  final Function(int index) indexChanged;

  ClassSubjectViewState(this.indexChanged);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.selected = 0;
    this.item = widget.item;
  }

  @override
  Widget build(BuildContext context) {
    return widget.isExpanded
        ? new ListTile(
            subtitle: new ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: item.list.length,
                itemBuilder: (context, index) {
                  RegisterSubjectItem _item = item.list[index];
                  return buildInfoChild(index, _item);
                }),
          )
        : new SizedBox();
  }

  Widget buildInfoChild(int index, RegisterSubjectItem _item) {
    Color color = item.indexSelected == index
        ? Singleton.setting.primary20OpacityColor
        : Singleton.backgroundNothing;
    return new InkWell(
        child: new Stack(
          children: <Widget>[
            new Column(
              children: <Widget>[
                new Container(
                  decoration: new BoxDecoration(
                      gradient: new LinearGradient(
                          colors: [
                    Singleton.backgroundNothing,
                    color,
                    Singleton.backgroundNothing
                  ])),
                  child: RegisterViewUtils.buildClassSubjectItem(_item),
                ),
              ],
            ),
            item.indexSelected == index
                ? new Align(
              alignment: Alignment.topRight,
              child: new Icon(
                Icons.check,
                color: Singleton.setting.primaryColor,
              ),
            )
                : new SizedBox(),
          ],
        ),
        onTap: () => setState(() {
              this.item.indexSelected = index;
              indexChanged(index);
            }));
  }
}
