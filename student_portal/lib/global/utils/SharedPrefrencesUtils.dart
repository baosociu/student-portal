import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:student_portal/models/utils/Setting.dart';

class SharedPreferencesUtils {
  static const String KEY_USERNAME = "KEY_USERNAME";
  static const String KEY_PASSWORD = "KEY_PASSWORD";
  static const String KEY_BACKGROUND_LOGIN = "KEY_BACKGROUND_LOGIN";
  static const String KEY_SETTINGS = "KEY_SETTINGS";
  static const String KEY_REGISTER_OPTIONS = "KEY_REGISTER_OPTIONS";
  static const String KEY_REGISTER_OPTIONS_DAY = "KEY_REGISTER_OPTIONS_DAY";
  static const String KEY_REGISTER_OPTIONS_DATA = "KEY_REGISTER_OPTIONS_DATA";

  static void getUsernamePassword(void todo(String username, String password)) {
    SharedPreferences.getInstance().then((prefs) {
      String username = prefs.getString(KEY_USERNAME);
      String password = prefs.getString(KEY_PASSWORD);
      if (username != null && password != null) todo(username, password);
    });
  }

  static Future<bool> setUsernamePassword(
      String username, String password) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    bool r = await prefs.setString(KEY_USERNAME, username) &&
        await prefs.setString(KEY_PASSWORD, password);
    return r;
  }

  static Future<bool> clearUsernamePassword() async {
    return await setUsernamePassword(null, null);
  }

  static Future<bool> saveSettings(Setting setting) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String _json = jsonEncode(setting.toJson());
    bool r = await prefs.setString(KEY_SETTINGS, _json);
    return r;
  }

  static Future<Setting> getSettings() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String str = prefs.getString(KEY_SETTINGS);
    print(str);
    return str == null ? null : new Setting.fromJson(jsonDecode(str));
  }
  
  static Future<bool> clearSettings()async {
    Setting setting = new Setting.byDefault();
   return await saveSettings(setting);
  }

  static Future<Map> getRegisterOptions() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String str = prefs.getString(KEY_REGISTER_OPTIONS);
    if(str == null)
      return null;
    else{
      dynamic map = jsonDecode(str);
      DateTime date = DateTime.parse(map[KEY_REGISTER_OPTIONS_DAY]);
      dynamic data = jsonDecode(map[KEY_REGISTER_OPTIONS_DATA]);
      //todo 1 tháng
      if((new DateTime.now()).difference(date).inDays > 30 || data == null || data.length == 0)
        return null;
      else return data;
    }
  }

  static Future<bool> saveRegisterOptions(dynamic data) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String,String> map = new Map();
    map[KEY_REGISTER_OPTIONS_DAY] = new DateTime.now().toIso8601String();
    map[KEY_REGISTER_OPTIONS_DATA] = jsonEncode(data);
    bool r = await prefs.setString(KEY_REGISTER_OPTIONS, jsonEncode(map));
    return r;
  }

}
