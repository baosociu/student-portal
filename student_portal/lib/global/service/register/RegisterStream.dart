import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/service/register/BaseRegister.dart';
import 'package:student_portal/global/service/register/RegisterAPI.dart';
import 'package:student_portal/global/service/register/RegisterRequest.dart';
import 'package:student_portal/global/service/register/RegisterResult.dart';
import 'package:student_portal/global/service/register/RegisterState.dart';
import 'package:student_portal/global/service/register/SubjectRegister.dart';

class RegisterStream {
  final Sink<RegisterRequest> onExecute;
  final Stream<RegisterState> state;

  RegisterStream._(this.onExecute, this.state);

  factory RegisterStream(RegisterAPI api) {
    // ignore: close_sinks
    final onExecute = new StreamController<RegisterRequest>();
    final state = new Observable<RegisterRequest>(onExecute.stream)
        .debounce(const Duration(milliseconds: 250))
        .switchMap(buildExecute(api));

    return new RegisterStream._(onExecute, state);
  }

  static Stream<RegisterState> Function(RegisterRequest) buildExecute(
      RegisterAPI api) {
    return (RegisterRequest request) {
      return new Observable<RegisterResult>.fromFuture(api.search(request))
          .map((RegisterResult result) {
            return new RegisterState(
              result: result,
              loadingType: LoadingType.loading,
            );
          })
//          .onErrorReturn(new RegisterState.error(Singleton.setting.localization.error_in_process))
          .startWith(buildStartViewState(request.loadType, request.dataPrev, request.type));
    };
  }

  static RegisterState buildStartViewState(
      LoadingType loadingType, List<BaseSubject> result, TypeRegister typeRegister) {
    if (loadingType == null)
      return new RegisterState.init();
    else {
      switch (loadingType) {
        case LoadingType.loading:
          return RegisterState.loading();
        case LoadingType.loadMore:
          return RegisterState.loadMore(result,typeRegister);
        case LoadingType.refresh:
          return RegisterState.refresh(result,typeRegister);
        case LoadingType.change:
          return RegisterState.change(result,typeRegister);
        default:
          return RegisterState.init();
      }
    }
  }
}
