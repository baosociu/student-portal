import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/DateTimeUtils.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Schedule.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/TextView.dart';

// ignore: must_be_immutable
class ScheduleByWeekListView extends StatelessWidget {
  final GroupScheduleByDayList scheduleList;
  BuildContext context;

  ScheduleByWeekListView(this.scheduleList);
  @override
  Widget build(BuildContext context) {
    this.context = context;
    return new Flexible(
        child: new ListView.builder(
            shrinkWrap: true,
            itemCount: this.scheduleList.items.length,
            itemBuilder: (context, index) {
              GroupScheduleByDay item = this.scheduleList.items[index];
              bool isToday =
                  DateTimeUtils.isEqualsDMY(item.date, new DateTime.now());
              return new Stack(
                children: <Widget>[
                  new Column(
                    children: <Widget>[
                      new Container(
                        child: new ListTile(
                          title: buildTitle(index, item),
                          subtitle: buildDetailSchedules(item.items),
                        ),
                      ),
                      index < this.scheduleList.items.length - 1
                          ? new Divider(color: Singleton.setting.primaryColor)
                          : new SizedBox()
                    ],
                  ),
                  isToday ? buildTodayMark() : new SizedBox(),
                ],
              );
            }));
  }

  Widget buildTestMark() {
    double width = MediaQuery.of(context).size.width;
    double widthImage = width;
    double heightImage = width / 5;
    return new Container(
      child: new SizedBox(height: heightImage, width: widthImage),
      decoration: new BoxDecoration(
        image: new DecorationImage(
            image: new AssetImage(Singleton.setting.localization.image_thetest),
            fit: BoxFit.contain,
            alignment: Alignment.centerRight),
      ),
    );
  }

  Widget buildTodayMark() {
    double width = MediaQuery.of(context).size.width;
    double widthImage = width;
    double heightImage = width / 5;
    return new Container(
      child: new SizedBox(height: heightImage, width: widthImage),
      decoration: new BoxDecoration(
        image: new DecorationImage(
            image: new AssetImage(Singleton.setting.localization.image_today),
            fit: BoxFit.contain,
            alignment: Alignment.centerRight),
      ),
    );
  }

  Widget buildTitle(int index, GroupScheduleByDay item) {
    Color color = Singleton.setting.primaryColor;
    Color color20 = Singleton.setting.primary20OpacityColor;

    return new Container(
      child: new Row(
        children: <Widget>[
          new Padding(
            padding: new EdgeInsets.all(4.0),
            child: new CircleAvatar(
              child: new TextView.leading(item.labelDayOfWeekShortest),
              backgroundColor: color,
            ),
          ),
          new Flexible(
              child: new ListView.builder(
                  itemCount: 2,
                  shrinkWrap: true,
                  primary: false,
                  itemBuilder: (context, index) {
                    return index == 0
                        ? buildHeaderSchedule(item)
                        : buildCountScheduleDay( Singleton.setting.secondColor, item.items.length);
                  })),
        ],
      ),
      decoration: new BoxDecoration(
          gradient:
              new LinearGradient(tileMode: TileMode.clamp, colors: <Color>[
        Colors.transparent,
        color20,
        color20
      ])),
    );
  }

  Widget buildCountScheduleDay(Color color, int count) {

    int countDayOfWeek = 7;
    double height = 5.0;
    List<Widget> widgets = new List();
    for (int i = 0; i < countDayOfWeek; i++)
      widgets.add(new Flexible(
        child: new SizedBox(
          height: height,
          child: new Container(
            decoration: new BoxDecoration(
                gradient: new LinearGradient(colors: <Color>[
              i < count ? color : Colors.transparent,
              Colors.transparent
            ])),
          ),
        ),
      ));
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: widgets,
    );
  }

  Widget buildDetailSchedules(List<BaseSchedule> items) {
    return new ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: items.length,
        itemBuilder: (context, index) {
          BaseSchedule item = items[index];
          return item is NormalSchedule
              ? buildItemByNormalSchedule(item)
              : buildItemByTestSchedule(item);
        });
  }

  //todo normal
  Widget buildItemByNormalSchedule(NormalSchedule normalSchedule) {
    return new Container(
      padding: new EdgeInsets.all(4.0),
      child: new ListTile(
        onTap: () {
          ToastUtils.showToastNormalSchedule(context, normalSchedule);
        },
        title: new TextView.text(
          normalSchedule.nameSubject.toString(),
          fontWeight: FontWeight.bold,
        ),
        subtitle: buildDetailNormalSchedule(normalSchedule),
      ),
    );
  }

  Widget buildHeaderSchedule(GroupScheduleByDay item) {
    return new TextView.title(
      item.labelDayOfWeek +
          " - " +
          StringUtils.convertDateTimeToString(item.date, "ddMMyyyy"),
      overflow: TextOverflow.ellipsis,
    );
  }

  Widget buildDetailNormalSchedule(NormalSchedule normalSchedule) {
    return new TableView(
      <Widget>[
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.period + ": ",
            normalSchedule.time.toString() +
                " -> " +
                (normalSchedule.time + normalSchedule.countTime - 1)
                    .toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.name_subject + ": ",
            normalSchedule.nameSubject.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.room + ": ",
            normalSchedule.nameRoom.toString() +
                " - " +
                normalSchedule.nameBranch.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.teacher + ": ",
            normalSchedule.teacher.toString()),
      ],
      margin: 0.0,
    );
  }

  //todo test
  Widget buildItemByTestSchedule(TestSchedule testSchedule) {
    return new Stack(
      children: <Widget>[
        new Container(
          padding: new EdgeInsets.all(4.0),
          decoration: new BoxDecoration(color: Singleton.yellow10OpacityColor),
          child: new ListTile(
            onTap: () {
              ToastUtils.showToastTestSchedule(context, testSchedule);
            },
            title: buildHeaderTestSchedule(testSchedule),
            subtitle: buildDetailTestSchedule(testSchedule),
          ),
        ),
        buildTestMark(),
      ],
    );
  }

  Widget buildDetailTestSchedule(TestSchedule testSchedule) {
    return new TableView(
      <Widget>[
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.exam_type + ":",
            testSchedule.typeTest.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.room + ":",
            testSchedule.nameRoom.toString() +
                " - " +
                testSchedule.nameBranch.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.exam_date + ":",
            StringUtils.convertDateTimeToString(testSchedule.date, "ddMMyyyy")),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.exam_time + ":",
            testSchedule.startTime.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.exam_period + ":",
            testSchedule.timeTest.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.exam_total_time + ":",
            testSchedule.time.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.supervisor_1 + ":",
            testSchedule.teacher1.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.supervisor_2 + ":",
            testSchedule.teacher2 == null
                ? ""
                : testSchedule.teacher2.toString()),
      ],
      margin: 0.0,
    );
  }

  Widget buildHeaderTestSchedule(TestSchedule testSchedule) {
    return new Row(
      children: <Widget>[
        ViewUtils.buildStartIcon(),
        new Flexible(
            child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new TextView.title(
                Singleton.setting.localization.exam.toUpperCase() +
                    " " +
                    testSchedule.typeTest.toString().toUpperCase()),
            new TextView.title(
              testSchedule.nameSubject.toString().toUpperCase(),
              overflow: TextOverflow.ellipsis,
            ),
          ],
        )),
      ],
    );
  }
}
