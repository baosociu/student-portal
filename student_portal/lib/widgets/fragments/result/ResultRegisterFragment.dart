import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/utils/AlertUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Result.dart';
import 'package:student_portal/widgets/components/LoadingView.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/register/view/RegisterViewUtils.dart';

class ResultRegisterFragment extends StatefulWidget {
  final String title =
      Singleton.setting.localization.result_register_fragment_title;

  ResultRegisterFragment({Key key, this.idSemester});

  final String idSemester;

  @override
  ResultRegisterFragmentState createState() =>
      new ResultRegisterFragmentState(this.title);
}

class ResultRegisterFragmentState extends State<ResultRegisterFragment> {
  final String title;
  String idStudent, idSemester;
  ServerResponse<ResultSemesterList> response;
  int indexSelected;
  bool enableLoading;
  bool requireReload;

  BuildContext _context;

  ResultRegisterFragmentState(this.title);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    enableLoading = false;
    requireReload = false;

    idStudent = Singleton.student.id.toString();
    idSemester = widget.idSemester;

    indexSelected = -1;

    getInfoResultRegister(idStudent);
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: enableLoading
            ? () {}
            : () async {
                Navigator.of(context).pop(this.requireReload);
                return false;
              },
        child: new Stack(
          children: <Widget>[
            buildMainPage(),
            enableLoading
                ? new Column(
                    children: <Widget>[
                      new LoadingView(
                        opacity: 0.6,
                        background: Singleton.darkColor,
                      ),
                    ],
                  )
                : new SizedBox()
          ],
        ));
  }

  Widget buildMainPage() {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: new TextView.appbar(this.title),
          backgroundColor: Singleton.setting.primaryColor,
        ),
        body: new Builder(builder: (BuildContext context) {
          this._context = context;
          return new Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                response == null
                    ? ViewUtils.buildLoadingCardView()
                    : buildResultRegisterList(),
              ],
            ),
          );
        }));
  }

// title: response.data.list[0].nameSemester.toString()
  Widget buildResultRegisterList() {
    return response.data.list.length == 1
        ? response.buildCardViewByResponse<ResultSemester>(
            context: context,
            list: response.data.list[0].list,
            callback: () => getInfoResultRegister(idStudent),
            emptyView: ViewUtils.buildNothingSubjectCardView,
            item: (index, item) {
              return new Column(
                children: <Widget>[
                  new ListTile(
                      title: buildTitle(index, item),
                      subtitle: buildSubtitle(item)),
                  index < response.data.list[0].list.length - 1
                      ? new Divider(color: Singleton.setting.primaryColor)
                      : new SizedBox()
                ],
              );
            })
        : ViewUtils.buildNothingSubjectCardView(
            () => getInfoResultRegister(idStudent));
  }

  Widget buildSubtitle(ResultSemester item) {
    return new Container(
      margin: new EdgeInsets.only(left: 24.0),
      decoration: new BoxDecoration(
          gradient: new RadialGradient(
              radius: 1.0,
              center: Alignment.bottomLeft,
              colors: <Color>[
            Singleton.setting.secondColor,
            Singleton.backgroundNothing
          ])),
      child: new Container(
        margin: new EdgeInsets.only(left: 4.0, bottom: 4.0),
        color: Singleton.backgroundNothing,
        child: new Padding(
            padding: new EdgeInsets.only(left: 8.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ViewUtils.buildItemSmallTextView(
                    Singleton.setting.localization.code_subject + ":",
                    item.codeSubject.toString(),
                    leftFlex: 1,
                    rightFlex: 1),
                RegisterViewUtils.buildSmallScheduleInformation(item.schedule),
                ViewUtils.buildItemSmallTextView(
                  Singleton.setting.localization.teacher + ": ",
                  item.nameTeacher.toString(),
                  leftFlex: 1,
                  rightFlex: 1,
                ),
                ViewUtils.buildItemSmallTextView(
                    Singleton.setting.localization.time + ":\n",
                    item.dateStart.toString() + "\n" + item.dateEnd.toString(),
                    leftFlex: 1,
                    rightFlex: 1),
                ViewUtils.buildItemSmallTextView(
                    Singleton.setting.localization.number_of_student + ": ",
                    item.minTotal.toString() +
                        " - " +
                        item.numberOfStudent.toString() +
                        " - " +
                        item.maxTotal.toString(),
                    leftFlex: 1,
                    rightFlex: 1),
                ViewUtils.buildItemSmallTextView(
                    Singleton.setting.localization.room + ": ",
                    item.room.toString(),
                    leftFlex: 1,
                    rightFlex: 1),
                ViewUtils.buildItemSmallTextView(
                    Singleton.setting.localization.status + ": ",
                    item.status.toString(),
                    leftFlex: 1,
                    rightFlex: 1),
                ViewUtils.buildItemSmallTextView(
                    Singleton.setting.localization.result_register + ": ",
                    item.resultRegister.toString(),
                    leftFlex: 1,
                    rightFlex: 1),
                ViewUtils.buildItemSmallTextView(
                    Singleton.setting.localization.result_process + ": ",
                    item.resultProcess.toString(),
                    leftFlex: 1,
                    rightFlex: 1),
              ],
            )),
      ),
    );
  }

  Widget buildTitle(int index, ResultSemester item) {
    Color color = Singleton.setting.primaryColor;
    Color color20 = Singleton.setting.primary20OpacityColor;

    return new Container(
      child: new Row(
        children: <Widget>[
          new Padding(
            padding: new EdgeInsets.all(4.0),
            child: new CircleAvatar(
              child: indexSelected == index
                  ? new Icon(Icons.clear, color: Singleton.textLightColor)
                  : new TextView.leading(
                      (index + 1).toString(),
                      color: Singleton.textLightColor,
                    ),
              backgroundColor: color,
            ),
          ),
//          new Flexible(
//              child: new ListView.builder(
//                  itemCount: 2,
//                  shrinkWrap: true,
//                  itemBuilder: (context, index) {
//                    return index == 0
//                        ? buildHeaderResult(index, item)
//                        : buildHeaderResultDate(item)
//                    ;
//                  })),
          new Flexible(
              child: new ListBody(
            children: <Widget>[
              buildHeaderResult(index, item),
              buildHeaderResultDate(item),
            ],
          ))
        ],
      ),
      decoration: new BoxDecoration(
          gradient: new LinearGradient(
              tileMode: TileMode.clamp,
              colors: <Color>[Colors.transparent, color20, color20])),
    );
  }

  Widget buildHeaderResult(int index, ResultSemester item) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        new Flexible(
            child: new TextView.title(
          item.nameSubject,
          fontWeight: FontWeight.bold,
          color: Singleton.setting.primaryColor,
          overflow: TextOverflow.ellipsis,
        )),
        new InkWell(
          child: new Icon(Icons.clear, color: Singleton.textDarkColor),
          onTap: () => showDeleteDialog(item, index),
        ),
      ],
    );
  }

  Widget buildHeaderResultDate(ResultSemester item) {
    return new TextView.text(
      Singleton.setting.localization.date_register +
          ": " +
          item.dateRegister.toString(),
      // StringUtils.convertDateTimeToString(item.date, "ddMMyyyy"),
    );
  }

  //todo action
  void setLoading(bool enable) {
    setState(() {
      this.enableLoading = enable;
    });
  }

  getInfoResultRegister(String idStudent) async {
    setState(() {
      this.response = null;
    });

    final ServerResponse<ResultSemesterList> response = await API
        .infoResultRegister<ResultSemesterList>(idStudent, idSemester, (data) {
      return new ResultSemesterList.fromJson(data);
    }, () {
      return new ResultSemesterList.fromJson(new List<Map>());
    });
    if (!this.mounted) return;

    setState(() {
      this.response = response;
    });
  }

  //todo delete 1
  Future showDeleteDialog(ResultSemester item, int index) async {
    setState(() {
      this.indexSelected = index;
    });

    if (item.idSubjectPlus == null)
      await AlertUtils.showDeleteResultItemAlert(context, item,
          funcPositive: delete, funcNegative: cancel);
    else {
      //todo tìm item học kèm
      try {
        ResultSemester item2 = response.data.list[0].list
            .firstWhere((it) => it.idSubject == item.idSubjectPlus);
        if (item2 != null) {
          ToastUtils.showToast(
              _context, Singleton.setting.localization.exist_parallel_subject);
          await AlertUtils.showDeleteResultItemsAlert(context, item, item2,
              funcPositive: deleteItems, funcNegative: cancel);
        } else
          throw new Error();
      } catch (e) {
        print(e.toString());
        await AlertUtils.showDeleteResultItemAlert(context, item,
            funcPositive: delete, funcNegative: cancel);
      }
    }
  }

  Future deleteItems(ResultSemester item1, ResultSemester item2) async {
    this.setLoading(true);

    final ServerResponse<Map> response = await API
        .deleteClassSubject2Items<Map>(item1.id.toString(), item2.id.toString(),
            (data) {
      return data;
    }, () {
      return new Map();
    });

    if (!this.mounted) return;

    switch (response.type) {
      case ResultType.availableItems:
        if (response.data['result_flag'].toString() == 'true')
          //todo thành công
          setState(() {
            this.response.data.list[0].list.removeWhere((it) =>
                it.idSubject == item1.idSubject ||
                it.idSubject == item2.idSubject);
            this.requireReload = true;
          });

        ToastUtils.showToast(
            _context, response.data['result_message'].toString());

        break;
      case ResultType.hasError:
        ToastUtils.showToast(_context, response.message.toString());
        break;
      case ResultType.loginAgain:
        ToastUtils.showToast(_context, response.message.toString());
        break;
      default:
        print(response.message);
    }

    this.setLoading(false);
    resetIndexTouch();
  }

  Future delete(ResultSemester item) async {
    this.setLoading(true);

    final ServerResponse<Map> response =
        await API.deleteClassSubject<Map>(item.id.toString(), (data) {
      return data;
    }, () {
      return new Map();
    });

    if (!this.mounted) return;

    switch (response.type) {
      case ResultType.availableItems:
        if (response.data['result_flag'].toString() == 'true')
          setState(() {
            this
                .response
                .data
                .list[0]
                .list
                .removeWhere((it) => it.idSubject == item.idSubject);
            this.requireReload = true;
          });

        ToastUtils.showToast(
            _context, response.data['result_message'].toString());

        break;
      case ResultType.hasError:
        ToastUtils.showToast(_context, response.message.toString());
        break;
      case ResultType.loginAgain:
        ToastUtils.showToast(_context, response.message.toString());
        break;
      default:
        print(response.message);
    }

    this.setLoading(false);
    resetIndexTouch();
  }

  void cancel() {
    resetIndexTouch();
  }

  void resetIndexTouch() {
    setState(() {
      this.indexSelected = -1;
    });
  }
}
