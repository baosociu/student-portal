import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/utils/FadeRoute.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Semester.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';
import 'package:student_portal/widgets/fragments/score/DetailScoreFragment.dart';

// ignore: must_be_immutable
class ScoreFragment extends BaseFragment {
  ScoreFragment(mainPage)
      : super(mainPage, Singleton.SCORE_FRAGMENT_ID,
            Singleton.setting.localization.score_fragment_title,"resources/icons/score.png");
  ScoreFragmentState myState;

  @override
  ScoreFragmentState createState() {
    super.createState();
    myState =  new ScoreFragmentState();
    return myState;
  }

  void refresh(){
    myState.refresh();
  }
}

class ScoreFragmentState extends BaseFragmentState {
  ServerResponse<SemesterScoreList> response;
  String codeStudent;

  bool isRefresh;
  @override
  void initState() {
    super.initState();
    codeStudent = Singleton.student.code;

    isRefresh = false;

    getInformationOfScore(codeStudent);
  }

  void refresh(){
    if(isRefresh)
      return;
    setState(()=> isRefresh = true);
    getInformationOfScore(codeStudent);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            isRefresh
                ? ViewUtils.buildRefreshView(LoadingType.refresh)
                : new SizedBox(),
            response == null
                ? ViewUtils.buildLoadingCardView()
                : buildScoreList(),
          ],
        ),
      ),
    );
  }

  Widget buildScoreList() {
    return response.buildCardViewByResponse<SemesterScore>(
        context: context,
        list: response.data.list,
        callback: () => getInformationOfScore(codeStudent),
        emptyView: ViewUtils.buildNothingSemesterCardView,
        item: (index, item) {
          return new Column(children: <Widget>[
            new ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    new FadeRoute(
                        builder: (context) => new DetailScoreFragment(
                            item.idSemester, item.name)),
                  );
                },
                title: buildTitle(item),
                subtitle: buildSubtitle(item)),
            index < response.data.list.length - 1
                ? new Divider(color: Singleton.setting.primaryColor)
                : new SizedBox()
          ]);
        });
  }

  Widget buildTitle(SemesterScore item) {
    Color color = Singleton.setting.primaryColor;
    Color color20 = Singleton.setting.primary20OpacityColor;

    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Flexible(
            child: new Row(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(4.0),
                  child: new CircleAvatar(
                    child: new TextView.leading(
                      item.avgSemester.toString(),
                      color: Singleton.textLightColor,
                      fontWeight: FontWeight.bold,
                    ),
                    backgroundColor: color,
                  ),
                ),
                new Flexible(
                    child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new TextView.title(
                      item.name.toString(),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )),
              ],
            ),
          ),
          new Icon(
            Icons.chevron_right,
            color: color,
          ),
        ],
      ),
      decoration: new BoxDecoration(
          gradient: new LinearGradient(
              tileMode: TileMode.clamp,
              colors: <Color>[
            Colors.transparent,
            color20,
            color20,
            Colors.transparent
          ])),
    );
  }

  Widget buildSubtitle(SemesterScore item) {
    return new Container(
      margin: new EdgeInsets.only(left: 24.0),
      decoration: new BoxDecoration(
          gradient: new RadialGradient(
              radius: 1.0,
              center: Alignment.bottomLeft,
              colors: <Color>[
            Singleton.setting.secondColor,
            Singleton.backgroundNothing
          ])),
      child: new Container(
        margin: new EdgeInsets.only(left: 4.0, bottom: 4.0),
        color: Singleton.backgroundNothing,
        child: new Padding(
            padding: new EdgeInsets.only(left: 8.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ViewUtils.buildItemNumberTextView(
                    Singleton.setting.localization.count_credit + ":",
                    item.weight.toString()),
                ViewUtils.buildItemNumberTextView(
                    Singleton.setting.localization.avg_semester + ":",
                    item.avgSemester.toString()),
                ViewUtils.buildItemNumberTextView(
                    Singleton.setting.localization.total_score + ":",
                    item.sumScore.toString(), fontWeight: FontWeight.bold),
              ],
            )),
      ),
    );
  }



  getInformationOfScore(String codeStudent) async {
    if (!isRefresh)
      setState(() {
        this.response = null;
      });

    final ServerResponse<SemesterScoreList> response =
        await API.infoSemestersById<SemesterScoreList>(codeStudent, (data) {
      return new SemesterScoreList.fromJson(data);
    }, () {
      return new SemesterScoreList.fromJson(new List<Map>());
    });
    if (!this.mounted) return;

    setState(() {
      this.response = response;
      this.isRefresh = false;
    });
  }
}
