import 'dart:async';

import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/fragments/suggest/Condition.dart';
import 'package:student_portal/widgets/fragments/suggest/Subject.dart';
import 'dart:math';

class Edge {
  final int start;
  final int end;

  Edge(this.start, this.end);
}

class SubjectScheduling {
  Subject subject;
  List<ClassSubject> adj = new List<ClassSubject>();

  SubjectScheduling.from(Subject subject) {
    this.subject =
        new Subject(subject.index, subject.codeSubject, subject.nameSubject);
    this.subject.classSubjects = new List<ClassSubject>()
      ..addAll(subject.classSubjects);
    this.subject.isSelected = subject.isSelected;
  }

  void addAdj(ClassSubject it) {
    this.adj.add(it);
  }
}

class Scheduling {
  List<ClassSubject> R = List<ClassSubject>();
  List<ClassSubject> _classSubjects = new List<ClassSubject>();
  SubjectList _subjectList;
  Condition _condition;

  Scheduling.from(List<ClassSubject> classSubjects,SubjectList subjectList, Condition condition) {
    this._classSubjects = classSubjects;
    this._subjectList = subjectList;
    this._condition = condition;
  }

  List<SubjectScheduling> groupBySubject() {
//    SubjectList subjectList =
//        new SubjectList.fromListClassSubject(classSubjects);

    List<SubjectScheduling> data = new List<SubjectScheduling>();
    for (Subject subject in _subjectList.list)
      if (!containsClassSubject(R, subject.codeSubject) && subject.isSelected)
        data.add(new SubjectScheduling.from(
            new Subject(data.length, subject.codeSubject, subject.nameSubject)
              ..classSubjects.addAll(subject.classSubjects)));

    for (int i = 0; i < data.length; i++)
      for (int j = 0; j < data.length; j++)
        for (int _i = 0; _i < data[i].subject.classSubjects.length; _i++)
          for (int _j = 0; _j < data[j].subject.classSubjects.length; _j++)
            if (!conflicts(data[i].subject.classSubjects[_i],
                data[j].subject.classSubjects[_j]))
              data[i].addAdj(data[j].subject.classSubjects[_j]);
    return data;
  }

  Future<Map<String, dynamic>> scheduling() async {
    return new Future(() async {
      String message = "";
      bool sucesss = true;
      try {
        List<SubjectScheduling> data = groupBySubject();
        List<Edge> edges = prepareEdges(data);

        await new Future(() {
          while (data.length != 0 && edges.length != 0) {
            reduce(data, edges);
            data = groupBySubject();
            edges = prepareEdges(data);

            if (data.length == 1) {
              int min = -1;
              int distance_min = -1;
              List<ClassSubject> _class_subjects = new List<ClassSubject>();
              for (ClassSubject _it in data[0].subject.classSubjects) {
                bool exists = false;
                for (ClassSubject it in R) {
                  if (conflict(it, _it)) {
                    exists = true;
                    break;
                  }
                }
                if (!exists) _class_subjects.add(_it);
              }

              for (int i = 0; i < _class_subjects.length; i++) {
                ClassSubject it = _class_subjects[i];
                for (ClassSubject _it in R) {
                  int d = distance(_it, it);
                  if (min == -1 || (distance_min > d)) {
                    min = i;
                    distance_min = d;
                  }
                }
              }

              if (min != -1) {
                R.add(_class_subjects[min]);
                print(_class_subjects[min].index);
                data = groupBySubject();
              }
            }

            if (data.length == 0) {
              for (ClassSubject item in R) item.printf();
              print("Sắp lịch xong");
              message = Singleton.setting.localization.scheduling_complete;
            } else if (edges.length == 0) {
              message = Singleton.setting.localization.scheduling_failure;
              sucesss = false;
              print("Không thể sắp xếp lịch học");
            }
          }
        });
      } catch (e) {
        sucesss = false;
        message = e.toString();
      }
      return {"result": R, "message": message, "success": sucesss};
    });
  }

  void reduce(List<SubjectScheduling> subjects, List<Edge> edges) {
    Map<Edge, Map<String, dynamic>> edges_label = new Map();
    for (Edge e in edges)
      edges_label[e] =
          distanceSubjects(subjects[e.start].subject, subjects[e.end].subject);

    int index_high = -1;
    int index_low = -1;

    for (int i = 0; i < edges_label.length; i++) {
      Map<String, dynamic> item = edges_label.values.toList()[i];
      DistanceType type_item = item['type'];
      if (type_item == DistanceType.low_distance && index_high == -1)
        index_high = i;
      else if (type_item == DistanceType.high_distance && index_low == -1)
        index_low = i;
      if (index_high != -1 && index_low != -1) break;
    }

    for (int i = 0; i < edges_label.length; i++) {
      Map<String, dynamic> item = edges_label.values.toList()[i];

      //todo high
      if (index_high != -1) {
        Map<String, dynamic> high = edges_label.values.toList()[index_high];
        double distance_item = item['distance'];
        double distance_high = high['distance'];

        //todo distance
        if (distance_item < distance_high)
          index_high = i;
        //todo distance/entropy
        else if (distance_item == distance_high) {
          DistanceType type_item = item['type'];
          double entropy_item = item['entropy'];
          double entropy_high = high['entropy'];
          //todo có nhiều liên kết có k/c thấp nhưng ko ổn định
          if (entropy_item > entropy_high &&
              type_item == DistanceType.low_distance) index_high = i;
        }
      }

      //todo low
      if (index_low != -1) {
        Map<String, dynamic> low = edges_label.values.toList()[index_low];
        double distance_item = item['distance'];
        double distance_low = low['distance'];

        //todo distance
        if (distance_item < distance_low)
          index_low = i;
        //todo distance/entropy
        else if (distance_item == distance_low) {
          DistanceType type_item = item['type'];
          double entropy_item = item['entropy'];
          double entropy_low = low['entropy'];
          //todo có nhiều liên kết có k/c thấp nhưng ko ổn định
          if (entropy_item < entropy_low &&
              type_item == DistanceType.high_distance) index_low = i;
        }
      }
    }

    int index = 0;
    if (index_high == -1)
      index = index_low;
    else if (index_low == -1)
      index = index_high;
    else {
      double distance_high =
          edges_label.values.toList()[index_high]['distance'];
      double distance_low = edges_label.values.toList()[index_low]['distance'];

      if (distance_high > distance_low)
        index = index_low;
      else if (distance_high < distance_low)
        index = index_high;
      else {
        double e_high = edges_label.values.toList()[index_high]['entropy'];
        double e_low = edges_label.values.toList()[index_low]['entropy'];
        index = (1.0 - e_high) < e_low ? index_high : index_low;
      }
    }

    Map<String, dynamic> item = edges_label.values.toList()[index];
    Edge edge = item["edges"][0];
    print("(" + edge.start.toString() + ", " + edge.end.toString() + ")");
    ClassSubject class_subject1 = this._classSubjects[edge.start - 1];
    ClassSubject class_subject2 = this._classSubjects[edge.end - 1];
    R.add(class_subject1);
    R.add(class_subject2);
  }

  Map<String, dynamic> distanceSubjects(Subject it1, Subject it2) {
    List<Map<String, dynamic>> d1 = new List<Map<String, dynamic>>();
    for (ClassSubject class_subject1 in it1.classSubjects)
      for (ClassSubject class_subject2 in it2.classSubjects)
        if (!conflicts(class_subject1, class_subject2)) {
          int _d = distance(class_subject1, class_subject2);
          d1.add({
            'class1': class_subject1.index,
            'class2': class_subject2.index,
            'distance': _d
          });
        }

    Map<String, dynamic> r = entropyByClasses(d1);
    double e = r["value"];
    //todo type: 1 => distance high, -1 => distance low
    DistanceType type = r["type"];

    int min = -1;
    for (int i = 0; i < d1.length; i++)
      if (min == -1 || (d1[min]['distance'] > d1[i]['distance'])) min = i;

    List<Edge> edges = List<Edge>();
    for (int i = 0; i < d1.length; i++)
      if (d1[min]['distance'] == d1[i]['distance'])
        edges.add(new Edge(d1[i]['class1'], d1[i]['class2']));

    return {
      'edges': edges,
      'entropy': e,
      'distance': 1.0 *d1[min]['distance'],
      'type': type
    };
  }

  Map<String, dynamic> entropyByClasses(List<Map<String, dynamic>> distances) {
    Map<int, List<Map<String, dynamic>>> group = new Map();
    for (Map<String, dynamic> item in distances) {
      if (!group.containsKey(item['class1']))
        group[item['class1']] = new List();
      group[item['class1']].add({
        "key": item['class1'],
        "class_subject": item["class2"],
        "distance": item["distance"]
      });
    }
    return entropy(group);
  }

  Map<String, dynamic> entropy(Map<int, List<Map<String, dynamic>>> dict) {
    double sum_value = 0.0;
    double sum_count = 0.0;

    for (List<Map<String, dynamic>> item in dict.values) {
      for (Map<String, dynamic> _dict in item) sum_value += _dict['distance'];
      sum_count += item.length;
    }
    double limit = 1.0 * sum_value / sum_count;

    //todo không cần làm trơn, 1 làm trơn than_count, -1 làm trơn less_count, 0 làm trơn cả 2
    SmoothType type = SmoothType.none;
    List<Map<String, dynamic>> data = new List();
    for (List<Map<String, dynamic>> item in dict.values) {
      int than_count = 0;
      int less_count = 0;
      for (Map<String, dynamic> _dict in item) {
        if (_dict['distance'] > limit)
          than_count += 1;
        else
          less_count += 1;
      }
      if (than_count == 0) {
        if (type == SmoothType.less_limit)
          type = SmoothType.both;
        else if (type == SmoothType.none) type = SmoothType.than_limit;
      }
      if (less_count == 0) {
        if (type == SmoothType.than_limit)
          type = SmoothType.both;
        else if (type == SmoothType.none) type = SmoothType.less_limit;
      }
      data.add({
        'items': item,
        'than_count': 1.0 * than_count,
        'less_count': 1.0 * less_count,
        'sum_count': 1.0 * item.length
      });
    }

    //todo làm trơn
    double total_than_count = 0.0;
    double total_less_count = 0.0;
    for (Map<String, dynamic> item in data) {
      item['than_count'] +=
          (type == SmoothType.than_limit || type == SmoothType.both ? 1 : 0);
      item['less_count'] +=
          (type == SmoothType.less_limit || type == SmoothType.both ? 1 : 0);
      item['sum_count'] +=
          (type == SmoothType.both ? 2 : (type == SmoothType.none ? 0 : 1));
      total_than_count += item['than_count'];
      total_less_count += item['less_count'];
    }

    //todo entropy càng nhỏ thì dữ liệu càng ổn định
    double total_e = 0.0;
    for (Map<String, dynamic> _dict in data) {
      double e1 = (_dict['than_count'] / _dict['sum_count']) *
          log2((_dict['than_count'] / _dict['sum_count']));
      double e2 = (_dict['less_count'] / _dict['sum_count']) *
          log2((_dict['less_count'] / _dict['sum_count']));
      double e = (_dict['sum_count'] / sum_count) * (e1 + e2);
      e = e < 0 ? -e : e;
      total_e += e;
    }
    return {
      "value": total_e / data.length,
      "type": total_than_count > total_less_count
          ? DistanceType.high_distance
          : DistanceType.low_distance
    };
  }

  double log2(double value) {
    return log(value) / log(2);
  }

  int distance(ClassSubject it1, ClassSubject it2) {
    int v11 = (it1.dayOfWeek - 1) * 18 + it1.startPeriod;
    int v12 = (it1.dayOfWeek - 1) * 18 + it1.endPeriod;
    int v21 = (it2.dayOfWeek - 1) * 18 + it2.startPeriod;
    int v22 = (it2.dayOfWeek - 1) * 18 + it2.endPeriod;
    if (v21 > v11)
      return (v21 - v12 < (v11 + 18 * 7) - v22
              ? v21 - v12
              : (v11 + 18 * 7) - v22) -
          1;
    else
      return (v11 - v22 < (v21 + 18 * 7) - v12
              ? v11 - v22
              : (v21 + 18 * 7) - v12) -
          1;
  }

  bool conflicts(ClassSubject it1, ClassSubject it2) {
    for (ClassSubject it in R)
      if (conflict(it, it1) || conflict(it, it2)) return true;
    return conflict(it1, it2);
  }

  bool conflict(ClassSubject it1, ClassSubject it2) {
    return it1.codeSubject == it2.codeSubject ||
        (it1.dayOfWeek == it2.dayOfWeek &&
            ((it1.endPeriod >= it2.startPeriod &&
                    it1.startPeriod <= it2.startPeriod) ||
                (it2.endPeriod >= it1.startPeriod &&
                    it2.startPeriod <= it1.startPeriod))) ||
        conflictByCondition(it1) ||
        conflictByCondition(it2);
  }

  bool conflictByCondition(ClassSubject it) {
    for (int i = it.startPeriod; i <= it.endPeriod; i++)
      if (!_condition.periods[i]) return true;
    return !_condition.dayOfWeeks[it.dayOfWeek];
  }

  bool containsClassSubject(List<ClassSubject> list, String codeSubject) {
    for (ClassSubject item in list)
      if (item.codeSubject == codeSubject) return true;
    return false;
  }

  List<Edge> prepareEdges(List<SubjectScheduling> subjects) {
    List<Edge> edges = new List<Edge>();
    for (int i = 0; i < subjects.length; i++) {
      int x = subjects[i].subject.index;
      for (int j = i + 1; j < subjects.length; j++) {
        int y = subjects[j].subject.index;

        if (containsClassSubject(
            subjects[i].adj, subjects[j].subject.codeSubject))
          edges.add(new Edge(x, y));
      }
    }
    return edges;
  }
}
