import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/utils/MapUtils.dart';
import 'package:student_portal/models/objects/Schedule.dart';
import 'package:student_portal/models/objects/Subject.dart';
import 'package:student_portal/models/objects/SummarySchedule.dart';

class ScheduleRequest {
  final ScheduleType type;
  final dynamic dataPrev;
  bool isRefresh = false;
  ScheduleRequest(this.type, this.dataPrev);
}

class ScheduleDateRequest extends ScheduleRequest {
  DateTime date;

  ScheduleDateRequest({DateTime date, dynamic dataPrev})
      : super(ScheduleType.day,
            dataPrev ?? new BaseScheduleList.fromJson(new List<Map>())) {
    if (date == null) this.date = DateTime.now();
  }
}

class ScheduleWeekRequest extends ScheduleRequest {
  DateTime startDate;
  DateTime endDate;
  DateTime date;

  ScheduleWeekRequest(
      {DateTime date, DateTime startDate, DateTime endDate, dynamic dataPrev})
      : super(ScheduleType.week,
            dataPrev ?? new BaseScheduleList.fromJson(new List<Map>())) {
    if (date == null) this.date = DateTime.now();
    Map<String, DateTime> map = MapUtils.getStartDateEndDateOfWeek(this.date);
    if (startDate == null) this.startDate = map['start'];
    if (endDate == null) this.endDate = map['end'];
  }
}

class ScheduleSubjectRequest extends ScheduleRequest {
  int indexSelected;
  List<BaseHeaderSchedule> listHeader;

  ScheduleSubjectRequest(
      {int indexSelected,
      List<BaseHeaderSchedule> listHeader,
      dynamic dataPrev})
      : super(ScheduleType.subject,
            dataPrev ?? new SubjectScheduleList.fromJson(new List<Map>())) {
    if (indexSelected == null) this.indexSelected = -1;
    if (listHeader == null) this.listHeader = new List<BaseHeaderSchedule>();
  }
}

class ScheduleSemesterRequest extends ScheduleRequest {
  int indexSelected;
  List<BaseHeaderSchedule> listHeader;

  ScheduleSemesterRequest(
      {int indexSelected,
      List<BaseHeaderSchedule> listHeader,
      dynamic dataPrev})
      : super(ScheduleType.semester,
            dataPrev ?? new SummaryScheduleList.fromJson(new List<Map>())) {
    if (indexSelected == null) this.indexSelected = -1;
    if (listHeader == null) this.listHeader = new List<BaseHeaderSchedule>();
  }
}
