import 'package:student_portal/global/Singleton.dart';

enum BaseSubjectType { oldSubject, newSubject, repalceSubject }

class BaseSubject {
  final dynamic credit;
  final String nameSubject;
  final int idSubject;
  final String codeSubject;
  final String namePlusSubject;
  final String nameFirstSubject;
  final BaseSubjectType type;
  final int replaceId;

  BaseSubject.byOld(Map map)
      : this.codeSubject = map["maMonHoc"],
        this.credit = map["soTinChi"],
        this.nameSubject = map["tenmh"],
        this.namePlusSubject = map["tenMonHocKem"],
        this.nameFirstSubject = map["tenMonTienQuyet"],
        this.idSubject = map["monHocId"],
        this.replaceId = null,
        this.type = BaseSubjectType.oldSubject;

  BaseSubject.byNew(Map map)
      : this.credit = map["soTinChiMH"],
        this.codeSubject = map["maMonHoc"],
        this.nameSubject = map["tenMonHoc"],
        this.namePlusSubject = map["tenMonHocKem"],
        this.nameFirstSubject = map["tenMonTienQuyet"],
        this.idSubject = map["monHocId"],
        this.replaceId = null,
        this.type = BaseSubjectType.newSubject;

  BaseSubject.byReplace(Map map)
      : this.credit = map["soTinChi"],
        this.codeSubject = map["maMonHoc"],
        this.idSubject = map["monHocId"],
        this.nameSubject = map["tenMonHoc"],
        this.replaceId = map["thayTheId"],
        this.namePlusSubject = null,
        this.nameFirstSubject = null,
        this.type = BaseSubjectType.repalceSubject;

  bool get isOldSubject => type == BaseSubjectType.oldSubject;
  bool get isNewSubject => type == BaseSubjectType.newSubject;
  bool get isReplaceSubject => type == BaseSubjectType.repalceSubject;
  bool get isUnknown => this.idSubject == null;
}

class SubjectOld extends BaseSubject {
  String codeSemester;
  int times;
  double totalScore;
  double reTest1;
  dynamic idStudent;
  dynamic idSemester;
  int idTraining; //chươn gtrinfh đào tạo
  String nameSemester;
  String typeSubject;
  int replace;

  SubjectOld.fromJson(Map map) : super.byOld(map) {
    this.idStudent = map["hocVienId"];
    this.codeSemester = map["mahk"];
    this.idSemester = map["hocKyId"];
    this.idTraining = map["ctdt"];
    this.nameSemester = map["tenhk"];
    this.typeSubject = map["loaimh"];
    this.replace = map["thayThe"];
    this.times = map["soLan"];
    this.totalScore = map["tongdiem"];
    this.reTest1 = map["thiLai1"];
  }
}

class SubjectRegisterList {
  final List<BaseSubject> list;

  SubjectRegisterList(this.list);

  factory SubjectRegisterList.init() {
    return new SubjectRegisterList(new List());
  }

  factory SubjectRegisterList.oldSubject(dynamic json) {
    List<BaseSubject> items = new List<BaseSubject>();
    for (dynamic map in json) items.add(new SubjectOld.fromJson(map));
    return new SubjectRegisterList(items);
  }

  factory SubjectRegisterList.newSubject(dynamic json) {
    List<BaseSubject> items = new List<BaseSubject>();
    for (dynamic map in json) items.add(new SubjectNew.fromJson(map));
    return new SubjectRegisterList(items);
  }

  factory SubjectRegisterList.fromItems(List<BaseSubject> items) {
    return new SubjectRegisterList(items);
  }
  factory SubjectRegisterList.replaceSubject(dynamic json) {
    List<SubjectReplace> items = new List<SubjectReplace>();
    for (dynamic map in json) items.add(new SubjectReplace.from(map));

    List<BaseSubject> list = new List<BaseSubject>();
    for (int i = 0; i < items.length; i++) {
      SubjectReplace child = items[i];
      for (BaseSubject _child in child.items)
        list.add(new BaseSubject.byReplace({
          "soTinChi": _child.credit,
          "maMonHoc": _child.codeSubject,
          "monHocId": _child.idSubject,
          "tenMonHoc": _child.nameSubject,
          "thayTheId": i
        }));
    }
    return new SubjectRegisterList(list);
  }

  List<SubjectReplace> get replaceSubjects {
    List<SubjectReplace> items = new List();

    try {
      if (list.length != 0 && list.length%4 == 0) {
        for (int i = 0; i < list.length; i += 4) {
          BaseSubject subject1 = list[i];
          BaseSubject subject2 = list[i + 1];
          BaseSubject subjectReplace1 = list[i + 2];
          BaseSubject subjectReplace2 = list[i + 3];
          items.add(new SubjectReplace.fromItems(subject1, subject2, subjectReplace1, subjectReplace2));
        }
      }
    } catch (E) {}

    return items;
  }
}

class SubjectNew extends BaseSubject {
  int countTime;
  dynamic idGroup;
  dynamic idPlusSubject;
  dynamic typeRoom;
  double creditTheory;
  double creditPractice;
  String typeSubject;

  SubjectNew.fromJson(Map map) : super.byNew(map) {
    this.typeSubject = map['loaiMonHoc'];
    this.countTime = map["soTietHoc"];
    this.idGroup = map["khoiNganhId"];
    this.idPlusSubject = map["monHocKemId"];
    this.typeRoom = map["loaiPhong"];
    this.creditTheory = map["soTinChiLT"];
    this.creditPractice = map["soTinChiTH"];
  }

  String get typeSubjectText => this.typeSubject.trim() == "TC"
      ? Singleton.setting.localization.elective_subject
      : Singleton.setting.localization.compulsory_subject;
}

class SubjectReplace {
  final List<BaseSubject> items;

  SubjectReplace(this.items);

  factory SubjectReplace.fromItems(BaseSubject subject1, BaseSubject subject2,
      BaseSubject subjectReplace1, BaseSubject subjectReplace2) {
    return new SubjectReplace(new List<BaseSubject>()
      ..add(subject1)
      ..add(subject2)
      ..add(subjectReplace1)
      ..add(subjectReplace2));
  }

  factory SubjectReplace.from(dynamic data) {
    List<BaseSubject> list = new List<BaseSubject>();
    list.add(new BaseSubject.byReplace({
      "soTinChi": data["soTinChi1"],
      "maMonHoc": data["maMonHoc1"],
      "monHocId": data["monHoc1Id"],
      "tenMonHoc": data["tenMonHoc1"],
    }));
    list.add(new BaseSubject.byReplace({
      "soTinChi": data["soTinChi2"],
      "maMonHoc": data["maMonHoc2"],
      "monHocId": data["monHoc2Id"],
      "tenMonHoc": data["tenMonHoc2"],
    }));
    list.add(new BaseSubject.byReplace({
      "soTinChi": data["soTinChiThayThe1"],
      "maMonHoc": data["maMonHocThayThe1"],
      "monHocId": data["monHocThayThe1Id"],
      "tenMonHoc": data["tenMonHocThayThe1"],
    }));
    list.add(new BaseSubject.byReplace({
      "soTinChi": data["soTinChiThayThe2"],
      "maMonHoc": data["maMonHocThayThe2"],
      "monHocId": data["monHocThayThe2Id"],
      "tenMonHoc": data["tenMonHocThayThe2"],
    }));
    return new SubjectReplace(list);
  }

  BaseSubject get subject1 => items[0];
  BaseSubject get subject2 => items[1];

  BaseSubject get subjectReplace1 => items[2];
  BaseSubject get subjectReplace2 => items[3];
}
