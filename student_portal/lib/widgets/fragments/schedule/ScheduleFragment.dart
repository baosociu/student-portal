import 'dart:async';
import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Semester.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/schedule/view/ScheduleContentView.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleDateMode.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleSubjectMode.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleSemesterMode.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleMode.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleWeekMode.dart';
import 'package:student_portal/widgets/components/alert/ChoiceOneAlert.dart';
import 'package:student_portal/global/service/schedule/ScheduleAPI.dart';
import 'package:student_portal/global/service/schedule/ScheduleStream.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';

// ignore: must_be_immutable
class ScheduleFragment extends BaseFragment {
  final ScheduleAPI api = new ScheduleAPI();
  ScheduleFragmentState myState;
  ScheduleFragment(mainPage)
      : super(mainPage, Singleton.SCHEDULE_FRAGMENT_ID,
            Singleton.setting.localization.schedule_fragment_title,"resources/icons/schedule.png");

  @override
  ScheduleFragmentState createState() {
    super.createState();
    myState = new ScheduleFragmentState();
    return myState;
  }

  void refresh(){
    myState.refresh();
  }
}

class ScheduleFragmentState extends BaseFragmentState {

  final ScheduleDateMode scheduleByDay = new ScheduleDateMode();
  final ScheduleWeekMode scheduleByWeek = new ScheduleWeekMode();
  final ScheduleSubjectMode scheduleBySubject = new ScheduleSubjectMode();
  final ScheduleSemesterMode scheduleBySemester = new ScheduleSemesterMode();

  ScheduleStream stream;
  ScheduleContentView content;

  ScheduleMode scheduleMode;
  SemesterScheduleList listSemester;
  BuildContext _context;

  @override
  void initState() {
    super.initState();
    this.scheduleMode = scheduleByDay;
    this.stream = new ScheduleStream((widget as ScheduleFragment).api);
    this.content = new ScheduleContentView(
        execute: stream.onExecute.add,
        state: stream.state,
        scheduleMode: this.scheduleMode);
    getHeaderSemestersOption(Singleton.student.id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
        body: new Builder(builder: (BuildContext context){
          _context = context;
          return new Center(
            child: listSemester == null
                ? new Column(
              children: <Widget>[
                ViewUtils.buildLoadingCardView()
              ],
            )
                : (listSemester.list.length == 0
                ? ViewUtils.buildNothingSemesterCardView(() =>
                getHeaderSemestersOption(Singleton.student.id.toString()))
                : new Column(
              children: <Widget>[
                buildOptionsSchedule(),
                new Flexible(
                  child: this.content,
                ),
              ],
            )),
          );
        }) );
  }

  //todo show dialog schedule options
  Future<Null> showOptionsDialog(BuildContext context) async {
    List<String> values = new List<String>();
    values.add(scheduleByDay.header);
    values.add(scheduleByWeek.header);
    values.add(scheduleBySubject.header);
    values.add(scheduleBySemester.header);
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new ChoiceOneAlert(
          values,
          textTitle: Singleton.setting.localization.select_schedule_mode,
          textMessage:
              Singleton.setting.localization.you_need_select_schedule_mode,
          textPositiveButton: Singleton.setting.localization.select,
          textNegativeButton: Singleton.setting.localization.cancel,
          color: Singleton.setting.primaryColor,
          iconHeader: Icons.access_time,
          indexSelected: ScheduleType.values.indexOf(scheduleMode.request.type),
          isAutoSelect: true,
          onPositiveTap: (indexSelected) {
            setState(() {
              this.scheduleMode =
                  getScheduleModeByMode(ScheduleType.values[indexSelected]);
              this.stream = new ScheduleStream(new ScheduleAPI());
            });
            this.onLoadData();
          },
        ));
  }

  void onLoadData() {
    if (this.content.myState != null && listSemester != null) {
      this.content.myState.scheduleMode = this.scheduleMode..request.isRefresh = false;
      this.content.myState.execute(this.content.myState.scheduleMode.request);
    }
  }

  void refresh(){
    if (this.content.myState != null && listSemester != null) {
      this.content.myState.scheduleMode = this.scheduleMode..request.isRefresh = true;
      this.content.myState.execute(this.content.myState.scheduleMode.request);
    }
  }

  //todo build option schedule
  Widget buildOptionsSchedule() {
    double radius = 8.0;

    return new InkWell(
      child: new Container(
        margin: new EdgeInsets.all(8.0),
        child: new Material(
          borderRadius: new BorderRadius.circular(radius),
          shadowColor: Singleton.setting.primaryColor,
          elevation: 5.0,
          child: new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Container(
                    padding: new EdgeInsets.only(
                        top: 4.0, bottom: 4.0, left: 12.0, right: 12.0),
                    child: new Row(
                      children: <Widget>[
                        new Icon(
                          this.scheduleMode.iconData,
                          color: Singleton.setting.primaryColor,
                        ),
                        new Container(
                          margin: new EdgeInsets.all(4.0),
                          child: new TextView.text(this.scheduleMode.header),
                        ),
                      ],
                    )),
                new Icon(
                  Icons.arrow_drop_down,
                  color: Singleton.setting.primaryColor,
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: () {
        showOptionsDialog(context);
      },
    );
  }

  //todo get schedule mode by index
  ScheduleMode getScheduleModeByMode(ScheduleType mode) {
    switch (mode) {
      case ScheduleType.day:
        return scheduleByDay;
      case ScheduleType.week:
        return scheduleByWeek;
      case ScheduleType.subject:
        return scheduleBySubject;
      default:
        return scheduleBySemester;
    }
  }

  //todo build header semester of student
  getHeaderSemestersOption(String idStudent) async {
    setState(() {
      this.listSemester = null;
    });

    final ServerResponse<SemesterScheduleList> response =
        await API.infoSemestersScheduleById<SemesterScheduleList>(idStudent, (data) {
      return new SemesterScheduleList.fromJson(data);
    }, () {
      return new SemesterScheduleList.fromJson(new List<Map>());
    });
    if(!this.mounted) return;

    if(response.type == ResultType.resultEmpty || response.type == ResultType.hasError){
      ToastUtils.showToast(_context, response.message);
    }

    if (!mounted) return;
    setState(() {
      listSemester = response.data;
      this.scheduleBySemester.setData(listSemester.list);
      this.scheduleBySubject.setData(listSemester.list);
    });

    this.onLoadData();
  }
}
