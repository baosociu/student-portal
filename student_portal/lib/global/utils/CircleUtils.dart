import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class CircleUtilsConstant{
  static const double SMALL_SIZE = 50.0;
}

class CircleUtils{

  static Widget buildPieScoreChart(double progressRate,double middleRate,double finalRate){
    final GlobalKey<AnimatedCircularChartState> _chartKey = new GlobalKey<AnimatedCircularChartState>();
    progressRate = progressRate == null ? 0.0: progressRate;
    middleRate = middleRate == null ? 0.0: middleRate;
    finalRate = finalRate == null ? 0.0: finalRate;

    List<CircularStackEntry> data = <CircularStackEntry>[
      new CircularStackEntry(
        <CircularSegmentEntry>[
          new CircularSegmentEntry(progressRate, Colors.green[200], rankKey: 'Progress'),
          new CircularSegmentEntry(middleRate, Colors.blue[200], rankKey: 'Middle'),
          new CircularSegmentEntry(finalRate, Colors.red[200], rankKey: 'Final'),
        ],
        rankKey: 'Quarterly Score',
      ),
    ];

    return new AnimatedCircularChart(
      key: _chartKey,
      size: const Size(100.0, 100.0),
      duration: new Duration(seconds: 1),
      initialChartData: data,
      chartType: CircularChartType.Pie,
    );
  }


  static Widget buildSmallCircleChart({String label, dynamic value, Color color}) {
    double progress = 0.0;
    try{
      progress = double.parse(value.toString());
    }catch(error){

    }

    if(color == null)
      color = Singleton.setting.primaryColor;

    final double remaining = 100.0 - progress;
    final textView = new TextView.text(label);

    final GlobalKey<AnimatedCircularChartState> _chartKey =
    new GlobalKey<AnimatedCircularChartState>();

    return new AnimatedCircularChart(
      key: _chartKey,
      size: const Size(CircleUtilsConstant.SMALL_SIZE, CircleUtilsConstant.SMALL_SIZE),
      initialChartData: <CircularStackEntry>[
        new CircularStackEntry(
          <CircularSegmentEntry>[
            new CircularSegmentEntry(
              progress,
              color,
              rankKey: 'completed',
            ),
            new CircularSegmentEntry(
              remaining,
              Colors.blueGrey[100],
              rankKey: 'remaining',
            ),
          ],
          rankKey: 'progress',
        ),
      ],
      chartType: CircularChartType.Radial,
      duration: new Duration(seconds: 1),
      percentageValues: true,
      holeLabel: label,
      labelStyle: textView.style,
    );
  }
}