import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/models/objects/Schedule.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleMode.dart';
import 'package:student_portal/widgets/components/alert/ChoiceOneFixedHeightAlert.dart';
import 'package:student_portal/global/service/schedule/ScheduleRequest.dart';

class ScheduleSubjectMode extends ScheduleMode {
  ScheduleSubjectMode()
      : super( new ScheduleSubjectRequest(),
           Singleton.setting.localization.schedule_by_subject, Icons.list, Singleton.setting.primaryColor);

  void setData(List<BaseHeaderSchedule> listHeader) {
    (request as ScheduleSubjectRequest).listHeader = listHeader;
    (request as ScheduleSubjectRequest).indexSelected =
        listHeader != null && listHeader.length != 0
            ? listHeader.length - 1
            : -1;
  }

  int getIdItem() {
    return (request as ScheduleSubjectRequest)
        .listHeader[(request as ScheduleSubjectRequest).indexSelected]
        .id;
  }

  @override
  String getTitle() {
    return (request as ScheduleSubjectRequest).indexSelected == -1
        ? ""
        : (request as ScheduleSubjectRequest)
            .listHeader[(request as ScheduleSubjectRequest).indexSelected]
            .name;
  }

  @override
  void onTapLeft() {
    if ((request as ScheduleSubjectRequest).indexSelected == -1) return;
    (request as ScheduleSubjectRequest).indexSelected =
        ((request as ScheduleSubjectRequest).indexSelected - 1) %
            (request as ScheduleSubjectRequest).listHeader.length;
  }

  @override
  void onTapRight() {
    if ((request as ScheduleSubjectRequest).indexSelected == -1) return;
    (request as ScheduleSubjectRequest).indexSelected =
        ((request as ScheduleSubjectRequest).indexSelected + 1) %
            (request as ScheduleSubjectRequest).listHeader.length;
  }

  Future<Null> showSemestersDialog(BuildContext context,
      dynamic Function(int valueSelected) onPositiveTap) async {
    List<TextView> values = new List<TextView>();
    (request as ScheduleSubjectRequest).listHeader.forEach((item) {
      values.add(new TextView.alertText(item.name));
    });

    return await showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new ChoiceOneAlertFixedHeight(values,
            textTitle: Singleton.setting.localization.select_semester,
            textMessage: Singleton.setting.localization.you_need_select_semester,
            textPositiveButton: Singleton.setting.localization.select,
            textNegativeButton: Singleton.setting.localization.cancel,
            iconHeader: iconData,
            isAutoSelect: true,
            indexSelected: (request as ScheduleSubjectRequest).indexSelected,
            onPositiveTap: onPositiveTap));
  }

  @override
  void onTapCenter(BuildContext context, onPositiveTap(int valueSelected)) {
    showSemestersDialog(context, onPositiveTap);
  }

}
