class OpacityLoading {
  double value = 0.0;
  bool addOpacity = true;
  OpacityLoading(double value, bool addOpacity) {
    this.value = value;
    this.addOpacity = addOpacity;
  }

  void updateOpacity() {
    if (this.value >= 0.9) {
      this.value = 0.9;
      addOpacity = false;
    }
    if (this.value <= 0.1) {
      this.value = 0.1;
      addOpacity = true;
    }
    this.value += 0.0005 * (addOpacity ? 1 : -1);
  }
}