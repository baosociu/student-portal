import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';

// ignore: must_be_immutable
class ChoiceMultiAlertFixedHeight extends StatefulWidget {
  ChoiceMultiAlertFixedHeight(this.values,this.valuesSelected,
      {Key key,
      this.height: 200.0,
      Color color,
      this.iconHeader,
      this.textTitle: '',
      this.textMessage: '',
      this.textPositiveButton: '',
      this.textNegativeButton: '',
      this.onPositiveTap,
      this.padding: 16.0,
      this.onWillPop})
      : this.color = color ?? Singleton.setting.primaryColor;

  final double height;
  final List<TextView> values;
  final Color color;
  final IconData iconHeader;
  final String textTitle;
  final String textMessage;
  final String textPositiveButton;
  final String textNegativeButton;
  final double padding;
  final ValueChanged<List<bool>> onPositiveTap;
  final Function onWillPop;
  final List<bool> valuesSelected;

  @override
  State<StatefulWidget> createState() {
    return new ChoiceMultiAlertFixedHeightState(onWillPop);
  }
}

class ChoiceMultiAlertFixedHeightState extends State<ChoiceMultiAlertFixedHeight> {
  static FlatButton positiveButton;
  static FlatButton negativeButton;
  final Function onWillPop;
  List<bool> valuesSelected;

  ChoiceMultiAlertFixedHeightState(this.onWillPop);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.valuesSelected = widget.valuesSelected;


    positiveButton = new FlatButton(
        child: new TextView.text(widget.textPositiveButton, color: widget.color,),
        onPressed: () {
          Navigator.of(context).pop();
          widget.onPositiveTap(valuesSelected);
        });

    negativeButton = new FlatButton(
      child: new TextView.text(widget.textNegativeButton),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: new AlertDialog(
          titlePadding: new EdgeInsets.all(0.0),
          contentPadding: new EdgeInsets.only(
              top: widget.padding, right: widget.padding, left: widget.padding),
          title: new Container(
            padding: new EdgeInsets.all(widget.padding / 2),
            decoration: new BoxDecoration(color: widget.color),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(
                  widget.iconHeader,
                  color: Singleton.textLightColor,
                  size: 30.0,
                )
              ],
            ),
          ),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new TextView.alertTitle(
                      widget.textTitle
                    ),
                    new SizedBox(height: 10.0),
                    new TextView.alertText(
                      widget.textMessage
                    ),
                    new SizedBox(height: 10.0),
                    new Container(
                      height: widget.height,
                      child: new ListView.builder(
                          shrinkWrap: true,
                          itemCount: widget.values.length,
                          itemBuilder: (context, index) {
                            return new Column(
                              children: <Widget>[
                                new InkWell(
                                  child: new Container(
                                    child: new Row(
                                      children: <Widget>[
                                        new Icon(
                                          this.valuesSelected[index]
                                                ? Icons.check_box
                                                : Icons.check_box_outline_blank,
                                            color: widget.color),
                                        new SizedBox(width: 10.0),

                                          widget.values[index]
                                      ],
                                    ),
                                    margin: new EdgeInsets.only(
                                        top: 2.0, bottom: 2.0),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      this.valuesSelected[index] = !this.valuesSelected[index];
                                    });
                                  },
                                ),
                              ],
                            );
                          }),
                    ),
                  ],
                ),
                new Container(
                  margin: new EdgeInsets.only(top: widget.padding),
                  padding: new EdgeInsets.only(
                      right: widget.padding, left: widget.padding),
                  decoration: new BoxDecoration(color: widget.color),
                  child: new Row(
                    children: <Widget>[
                      new SizedBox(
                        height: 0.5,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[negativeButton, positiveButton],
        ),
        onWillPop: () {
          Navigator.of(context).pop();
          this.onWillPop();
        });
  }
}
