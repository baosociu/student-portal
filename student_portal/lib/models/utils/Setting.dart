import 'package:flutter/material.dart';
import 'package:student_portal/global/service/Localization.dart';
import 'package:student_portal/models/utils/Theme.dart';

class Setting {
  static const String KEY_TITLE_FONT = "KEY_TITLE_FONT";
  static const String KEY_NAVIGATION_FONT = "KEY_NAVIGATION_FONT";
  static const String KEY_TEXT_FONT = "KEY_TEXT_FONT";
  static const String KEY_LOCALIZATION = "KEY_LOCALIZATION";
  static const String KEY_SHOW_HINT = "KEY_SHOW_HINT";
  static const String KEY_THEME = "KEY_THEME";

  String titleFont;
  String navigationFont;
  String textFont;
  Localization localization;
  bool isShowHint;
  ThemeColor theme;

  String get background => theme.background;
  Color get primaryColor => theme.primaryColor;
  Color get secondColor => theme.secondColor;
  Color get primary20OpacityColor => theme.primaryColor.withOpacity(0.2);
  Color get primary80OpacityColor =>  theme.primaryColor.withOpacity(0.8);
  Color get primary50OpacityColor =>  theme.primaryColor.withOpacity(0.5);

  String get nameBackground => BackgroundConstant.getNameBackground( theme.background);
  int get indexBackground => BackgroundConstant.getIndexBackground( theme.background);

  Setting(
      ThemeColor theme,
      String titleFont,
      String navigationFont,
      String textFont,
      Localization localization,
      bool isShowHint) {
    this.theme = theme;
    this.titleFont = titleFont;
    this.navigationFont = navigationFont;
    this.textFont = textFont;
    this.localization = localization;
    this.isShowHint = isShowHint;
  }

  Setting.byDefault({bool isShowHint = false}) {
    this.theme = new ThemeColor.red();
    this.titleFont = FontConstant.FONT_FAMILY_ARIMO;
    this.navigationFont = FontConstant.FONT_FAMILY_ASAP;
    this.textFont = FontConstant.FONT_FAMILY_CUPRUM;
    this.localization = new Localization.vi();
    this.isShowHint = isShowHint;
  }

  Setting.fromJson(Map<String, dynamic> json) {
    this.theme = new ThemeColor.fromJson(json[KEY_THEME]);
    this.titleFont = json[KEY_TITLE_FONT];
    this.navigationFont = json[KEY_NAVIGATION_FONT];
    this.textFont = json[KEY_TEXT_FONT];
    this.isShowHint = json[KEY_SHOW_HINT];
    this.localization = new Localization.fromJson(json[KEY_LOCALIZATION]);
  }

  Map<String, dynamic> toJson() {
    return {
      KEY_THEME: theme.toJson(),
      KEY_TITLE_FONT: titleFont,
      KEY_NAVIGATION_FONT: navigationFont,
      KEY_TEXT_FONT: textFont,
      KEY_SHOW_HINT: isShowHint,
      KEY_LOCALIZATION: localization.toJson(),
    };
  }
}

class FontConstant {
  static const String FONT_FAMILY_ARIMO = "Arimo";
  static const String FONT_FAMILY_ASAP = "Asap";
  static const String FONT_FAMILY_CUPRUM = "Cuprum";
  static const String FONT_FAMILY_INCONSOLATA = "Inconsolata";
  static const String FONT_FAMILY_MONTSERRAT = "Montserrat";
  static const String FONT_FAMILY_OSWALD = "Oswald";
  static const String FONT_FAMILY_PLAY = "Play";
  static const String FONT_FAMILY_ROBOTO = "Roboto";
  static const String FONT_FAMILY_SOURCESANSPRO = "SourceSansPro";
}
