import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/DateTimeUtils.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/models/objects/Register.dart';
import 'package:student_portal/models/objects/Schedule.dart';
import 'package:student_portal/models/objects/SummarySchedule.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class ToastUtils {
  static showToast(BuildContext context, String content) {
    Scaffold.of(context).showSnackBar(new SnackBar(
          content: new TextView.notification(content),
          duration: new Duration(seconds: 2),
        ));
  }

//  static showToastDetailRegisterNumberStudent(
//      BuildContext context, RegisterSubject courseRegister) {
//    String str = Singleton.setting.localization.name_subject +
//        ": " +
//        courseRegister.nameSubject.toString();
//    str += "\n" +
//        Singleton.setting.localization.min +
//        ": " +
//        courseRegister.minTotal.toString();
//    str += "\n" +
//        Singleton.setting.localization.number_of_student +
//        ": " +
//        courseRegister.numberStudent.toString();
//    str += "\n" +
//        Singleton.setting.localization.max +
//        ": " +
//        courseRegister.maxTotal.toString();
//
//    showToast(context, str);
//  }

  static showToastDetailRegister(
      BuildContext context, RegisterScheduleItem courseRegister) {
    String str = Singleton.setting.localization.name_subject +
        ": " +
        courseRegister.nameSubject.toString();
    str += "\n" +
        Singleton.setting.localization.code_subject +
        ": " +
        courseRegister.codeSubject.toString();
    str += "\n";
    courseRegister.schedules.forEach((item) {
      str += (StringUtils.getLabelOfWeekByInt(item.idDate) +
              " - " +
              Singleton.setting.localization.period +
              ": " +
              item.time.toString()) +
          "; ";
    });
    if (str.endsWith("; ")) str = str.substring(0, str.lastIndexOf("; "));
    str += "\n" +
        Singleton.setting.localization.teacher +
        ": " +
        courseRegister.nameTeacher.toString();
    str += "\n" +
        Singleton.setting.localization.time +
        ": " +
        courseRegister.dateStart.toString() +
        " - " +
        courseRegister.dateEnd.toString();

    showToast(context, str);
  }

  static showToastTestSchedule(
      BuildContext context, TestSchedule testSchedule) {
    bool isToday =
        DateTimeUtils.isEqualsDMY(testSchedule.date, new DateTime.now());

    String message =
        (isToday ? Singleton.setting.localization.to_day + " - " : "") +
            testSchedule.startTime.toString() +
            " " +
            StringUtils.convertDateTimeToString(testSchedule.date, "ddMMyyyy") +
            " " +
            Singleton.setting.localization.exam.toLowerCase() +
            " " +
            testSchedule.typeTest.toString().toLowerCase() +
            " " +
            testSchedule.nameSubject.toString();

    showToast(context, message);
  }

  static showToastNormalSchedule(
      BuildContext context, NormalSchedule normalSchedule) {
    bool isToday =
        DateTimeUtils.isEqualsDMY(normalSchedule.date, new DateTime.now());
    String message = (isToday
            ? Singleton.setting.localization.to_day + " - "
            : "") +
        Singleton.setting.localization.period +
        " " +
        normalSchedule.time.toString() +
        " " +
        StringUtils.convertDateTimeToString(normalSchedule.date, "ddMMyyyy") +
        ", " +
        normalSchedule.nameSubject.toString();
    showToast(context, message);
  }

  static showToastSummarySchedule(
      BuildContext context, SummarySchedule summarySchedule) {
    String message = Singleton.setting.localization.period +
        " " +
        summarySchedule.time.toString() +
        ", " +
        summarySchedule.labelDayOfWeek.toString() +
        ", " +
        Singleton.setting.localization.room +
        " " +
        summarySchedule.nameRoom.toString() +
        ", " +
        summarySchedule.nameBranch.toString() +
        ", " +
        summarySchedule.nameSubject.toString();
    showToast(context, message);
  }
}
