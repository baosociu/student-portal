import 'dart:convert';

import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/models/objects/Result.dart';

class Register {
  dynamic idGroupMajor;
  String nameGroupMajor;
  int countTime;
  dynamic idDate;
  int sumTime;
  dynamic idClassSubject;
  int maxTotal;
  dynamic idSemester;
  String dateEnd;
  String dateStart;
  bool isOpenClass;
  String codeSubject;
  String nameSubject;
  dynamic idSubject;
  dynamic id;
  dynamic idTeacher;
  int time;
  int minTotal;
  String nameTeacher;
  int numberStudent;
  dynamic idSubjectPlus;
  String room;

  Register.fromJson(Map json) {
    this.idGroupMajor = json['khoiNganhId'];
    this.nameGroupMajor = json['tenKhoiNganh'];
    this.countTime = json['soTiet'];
    this.idDate = json['ngayId'] == 1 ? 7 : (json['ngayId'] - 1);
    this.sumTime = json['tongSoTiet'];
    this.idClassSubject = json['lopMonHocId'];
    this.maxTotal = json['siSoMax'];
    this.idSemester = json['hocKyId'];
    this.dateEnd = json['ngayKetThuc'];
    this.dateStart = json['ngayBatDau'];
    this.isOpenClass = json['moLopYN'];
    this.codeSubject = json['maMonHoc'];
    this.nameSubject = json['tenMonHoc'];
    this.idSubject = json['monHocId'];
    this.id = json['id'];
    this.idTeacher = json['giaoVienId'];
    this.time = json['tietHoc'];
    this.minTotal = json['siSoMin'];
    this.nameTeacher = json['tenGV'];
    this.numberStudent = json['siSoThuc'];
    this.idSubjectPlus = json['monHocKemId'];
    this.room = json['tenPhong'];
  }
}

class RegisterList {
  List<Register> list = new List<Register>();

  RegisterList.fromJson(dynamic data) {
    list = new List<Register>();
    data.forEach((json) {
      Register value = new Register.fromJson(json);
      list.add(value);
    });
  }
}

class RegisterSchedule {
  dynamic idDate;
  int time;

  RegisterSchedule.from(Register item) {
    this.idDate = item.idDate;
    this.time = item.time;
  }

  RegisterSchedule.fromResult(Result item) {
    this.idDate = item.idDate;
    this.time = item.time;
  }

  bool equals(RegisterSchedule item) {
    return item.idDate == idDate && item.time == time;
  }
}

class RegisterScheduleItem {
  dynamic idGroupMajor;
  int countTime;
  int sumTime;
  dynamic idClassSubject;
  int maxTotal;
  dynamic idSemester;
  String dateEnd;
  String dateStart;
  bool isOpenClass;
  String codeSubject;
  String nameSubject;
  dynamic idSubject;
  dynamic id;
  dynamic idTeacher;
  int minTotal;
  String nameTeacher;
  int numberStudent;
  dynamic idSubjectPlus;
  String room;
  List<RegisterSchedule> schedules;
  List<String> groups;

  RegisterScheduleItem.from(Register item) {
    schedules = new List<RegisterSchedule>();
    groups = new List<String>();
    this.idGroupMajor = item.idGroupMajor;
    this.countTime = item.countTime;
    this.sumTime = item.sumTime;
    this.idClassSubject = item.idClassSubject;
    this.maxTotal = item.maxTotal;
    this.idSemester = item.idSemester;
    this.dateEnd = item.dateEnd;
    this.dateStart = item.dateStart;
    this.isOpenClass = item.isOpenClass;
    this.codeSubject = item.codeSubject;
    this.nameSubject = item.nameSubject;
    this.idSubject = item.idSubject;
    this.id = item.id;
    this.idTeacher = item.idTeacher;
    this.minTotal = item.minTotal;
    this.nameTeacher = item.nameTeacher;
    this.numberStudent = item.numberStudent;
    this.idSubjectPlus = item.idSubjectPlus;
    this.room = item.room;
  }
}

class RegisterScheduleList {
  List<RegisterScheduleItem> list;

  RegisterScheduleList.nothing(){
    this.list = new List<RegisterScheduleItem>();
  }

  RegisterScheduleList.fromJson(dynamic data) {
    list = new List<RegisterScheduleItem>();

    RegisterList courseRegisterList = new RegisterList.fromJson(data);

    courseRegisterList.list.forEach((courseRegister) {
      int i = 0;
      for (i = 0; i < list.length; i++)
        if (list[i].idClassSubject == courseRegister.idClassSubject)
          break;

      if (i == list.length)
        list.add(new RegisterScheduleItem.from(courseRegister));

      int j = 0;
      for (j = 0; j < list[i].schedules.length; j++)
        if (list[i]
            .schedules[j]
            .equals(new RegisterSchedule.from(courseRegister)))
          break;
      if (j == list[i].schedules.length)
        list[i].schedules.add(new RegisterSchedule.from(courseRegister));

      if (!list[i].groups.contains(courseRegister.nameGroupMajor))
        list[i].groups.add(courseRegister.nameGroupMajor);
    });
  }

  bool  get isNothing => list.length == 0;
}

//todo group theo tên môn, mã môn
class RegisterSubjectItem{
  final dynamic idGroupMajor;
  final int countTime;
  final int sumTime;
  final dynamic idClassSubject;
  final int maxTotal;
  final dynamic idSemester;
  final String dateEnd;
  final  String dateStart;
  final  bool isOpenClass;
  final dynamic id;
  final dynamic idTeacher;
  final int minTotal;
  final String nameTeacher;
  final int numberStudent;
  final String room;
  final List<RegisterSchedule> schedules;
  final List<String> groups;

  RegisterSubjectItem(this.idGroupMajor, this.countTime, this.sumTime, this.idClassSubject, this.maxTotal, this.idSemester, this.dateEnd, this.dateStart, this.isOpenClass, this.id, this.idTeacher, this.minTotal, this.nameTeacher, this.numberStudent,this.room, this.schedules, this.groups);

  factory RegisterSubjectItem.from(RegisterScheduleItem item){
    return new RegisterSubjectItem(item.idGroupMajor, item.countTime,item. sumTime,
        item.idClassSubject, item.maxTotal, item.idSemester, item.dateEnd, item.dateStart,
        item.isOpenClass, item.id, item.idTeacher,item. minTotal, item.nameTeacher,
        item.numberStudent, item.room, item.schedules,item.groups);
  }
}

class RegisterSubject{
  final dynamic idSubject;
  final String codeSubject;
  final String nameSubject;
  final dynamic idSubjectPlus;
  List<RegisterSubjectItem> list;
  int indexSelected = 0;
  RegisterSubjectItem get selected => list[indexSelected];

  RegisterSubject(this.idSubject, this.codeSubject, this.nameSubject, this.idSubjectPlus, this.list);

  factory RegisterSubject.nothing(){
    return new RegisterSubject(-1, "nothing", "nothing", -1, new List<RegisterSubjectItem>());
  }

  Map<String,dynamic> toJson(){
    return {
      "hocVienId": Singleton.student.id.toString(),
      "lopMonHocId": selected.idClassSubject,
      "hocKyId": selected.idSemester,
      "ngayDangKy": StringUtils.convertDateTimeToString(
          new DateTime.now(), "ddMMyyyy"),
    };
  }
}

class RegisterSubjectList{
  final List<RegisterSubject> list;
  int indexExpanded = -1;

  RegisterSubjectList(this.list);

  factory RegisterSubjectList.nothing(){
    return new RegisterSubjectList(new List<RegisterSubject>());
  }

  factory RegisterSubjectList.from(RegisterScheduleList list){
    if(list.isNothing)
      return new RegisterSubjectList.nothing();

    Map<dynamic, RegisterSubject> maps = new Map();
    for(RegisterScheduleItem schedule in list.list){
      if(!maps.containsKey(schedule.idSubject))
        maps[schedule.idSubject] = new RegisterSubject(schedule.idSubject, schedule.codeSubject, schedule.nameSubject, schedule.idSubjectPlus, new List<RegisterSubjectItem>());
      maps[schedule.idSubject].list.add(new RegisterSubjectItem.from(schedule));
    }

    List<RegisterSubject> _list = new List<RegisterSubject>();
    for(RegisterSubject subject in maps.values.toList())
      _list.add(subject);
    return new RegisterSubjectList(_list);
  }

  String toString(){
    String str = "[";
    list.forEach((i)=>str += (json.encode(i.toJson()) + ","));
    if(str.endsWith(","))
      str = str.substring(0,str.length-1);
    return str += "]";
  }

  List<String> get getIdSubjectList{
    List<String> ids = new List<String>();
    for(RegisterSubject item in list)
      ids.add(item.idSubject.toString());
    return ids;
  }
}
