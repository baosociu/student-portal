import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class CheckboxTextView extends StatefulWidget {
  CheckboxTextView({
    Key key,
    this.padding: 2.0,
    this.margin: 2.0,
    Color color,
    this.label,
    this.checked: false,
    this.onCheckChanged,
  }):  this.color = color ?? Singleton.setting.primaryColor;

  final double margin;
  final double padding;
  final String label;
  final Color color;
  final bool checked;
  final ValueChanged<bool> onCheckChanged;

  @override
  CheckboxTextViewState createState() => new CheckboxTextViewState();
}

class CheckboxTextViewState extends State<CheckboxTextView> {
  bool checked;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.checked = widget.checked;
  }

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      child: new Container(
        padding: new EdgeInsets.all(widget.padding),
        margin: new EdgeInsets.only(top: widget.margin, bottom: widget.margin),
        child: new Row(
          children: <Widget>[
            new Icon(
              this.checked ? Icons.check_box : Icons.check_box_outline_blank,
              color: widget.color,
            ),
            new Flexible(
              child: new TextView.text(
                widget.label,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      ),
      onTap: () {
        setState(() {
          this.checked = !this.checked;
          if (widget.onCheckChanged != null)
            widget.onCheckChanged(this.checked);
        });
      },
    );
  }
}
