import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class PrimaryAlert extends StatelessWidget {
  PrimaryAlert(
      {Key key,
      Color color,
      this.iconHeader,
      this.textTitle: '',
      this.textMessage: '',
      this.textPositiveButton: '',
      this.textNegativeButton: '',
      this.padding: 16.0,
      this.onPositiveTap,
      this.onNegativeTap,
      this.child,
      this.onWillPop})
      : this.color = color ?? Singleton.setting.primaryColor;

  final Color color;
  final IconData iconHeader;
  final String textTitle;
  final String textMessage;
  final String textPositiveButton;
  final String textNegativeButton;
  final double padding;
  final GestureTapCallback onPositiveTap;
  final GestureTapCallback onNegativeTap;
  final Widget child;
  final Function onWillPop;

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: new AlertDialog(
          titlePadding: new EdgeInsets.all(0.0),
          contentPadding: new EdgeInsets.only(
              top: this.padding, right: this.padding, left: this.padding),
          title: new Container(
            padding: new EdgeInsets.all(this.padding / 2),
            decoration: new BoxDecoration(color: color),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(
                  iconHeader,
                  color: Singleton.textLightColor,
                  size: 30.0,
                )
              ],
            ),
          ),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    //todo header
                    new TextView.alertTitle(
                      this.textTitle,
                    ),
                    new SizedBox(
                      height: 10.0,
                    ),

                    //todo message
                    new TextView.alertText(this.textMessage),

                    //todo child
                    new SizedBox(height: child == null ? 0.0 : 10.0),
                    child == null ? new SizedBox() : child
                  ],
                ),

                //todo line
                new Container(
                  margin: new EdgeInsets.only(top: this.padding),
                  padding: new EdgeInsets.only(
                      right: this.padding, left: this.padding),
                  decoration: new BoxDecoration(color: color),
                  child: new Row(
                    children: <Widget>[
                      new SizedBox(
                        height: 0.5,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            buildPositiveButton(context),
            buildNegativeButton(context),
          ],
        ),
        onWillPop: () {
          Navigator.of(context).pop();
          onWillPop == null ? (){} : this.onWillPop();
        });
  }

  Widget buildNegativeButton(BuildContext context) {
    return new Align(
      child: new FlatButton(
        child: this.textNegativeButton.isEmpty
            ? new SizedBox()
            : new TextView.text(this.textNegativeButton),
        onPressed: () {
          Navigator.of(context).pop();
          onNegativeTap == null ? () {} : onNegativeTap();
        },
      ),
      alignment: Alignment.bottomCenter,
    );
  }

  Widget buildPositiveButton(BuildContext context) {
    return new Align(
      child: this.textPositiveButton.isEmpty
          ? new SizedBox()
          : new FlatButton(
              child: new TextView.text(
                this.textPositiveButton,
                color: this.color,
              ),
              onPressed: () {
                Navigator.of(context).pop();
                onPositiveTap == null ? () {} : onPositiveTap();
              },
            ),
      alignment: Alignment.bottomCenter,
    );
  }
}
