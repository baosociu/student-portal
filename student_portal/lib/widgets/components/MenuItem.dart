import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class MenuItem extends StatefulWidget {
  final bool isSelected;
  final Color color;
  final Color colorSelected;
  final Color textColor;
  final String iconPath;
  final String text;
  final GestureTapCallback onTap;

  MenuItem(this.text,
      {Key key,
      this.isSelected: false,
      Color color,
      Color colorSelected,
      Color textColor,
      this.iconPath,
      this.onTap})
      : this.color = color ?? Singleton.setting.primaryColor,
        this.colorSelected = colorSelected ?? Singleton.textLightColor,
        this.textColor = textColor ?? Singleton.menuTextColor;

  @override
  State<StatefulWidget> createState() {
    return new MenuItemState(text,
        isSelected: this.isSelected,
        color: this.color,
        colorSelected: this.colorSelected,
        textColor: this.textColor,
        icon: this.iconPath == null ? null : new Image.asset(this.iconPath),
        onTap: this.onTap);
  }
}

class MenuItemState extends State<MenuItem> {
  final bool isSelected;
  final Color color;
  final Color colorSelected;
  final Color textColor;
  final Image icon;
  final String text;
  final GestureTapCallback onTap;

  MenuItemState(this.text,
      {Key key,
      this.isSelected: false,
      Color color,
      Color colorSelected,
      Color textColor,
      this.icon,
      this.onTap})
      : this.color = color ?? Singleton.setting.primaryColor,
        this.colorSelected = colorSelected ?? Singleton.textLightColor,
        this.textColor = textColor ?? Singleton.menuTextColor;

  @override
  Widget build(BuildContext context) {
    double sizeIcon = 28.0;
    return new Container(
      child: new ListTile(
          title: new TextView.menu(
            this.text,
            fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
            textSize: isSelected ? TextView.LAGRE_SIZE : TextView.MEDIUM_SIZE,
            color: isSelected ? colorSelected : Singleton.menuTextColor,
          ),
          trailing: new Stack(
            children: <Widget>[
              new Align(
                child: isSelected
                    ? buildCircleShape(radius: 44.0)
                    : new SizedBox.fromSize(size: new Size.fromRadius(22.0)),
                alignment: Alignment.center,
              ),
              new Padding(
                child: new Container(
                  child: new SizedBox(
                    height: sizeIcon,
                    width: sizeIcon,
                  ),
                  decoration: icon == null
                      ? null
                      : new BoxDecoration(
                          image: new DecorationImage(image: this.icon.image)),
                ),
                padding: new EdgeInsets.all(8.0),
              ),
            ],
          ),
          onTap: this.onTap),
      decoration: new BoxDecoration(
          gradient: new LinearGradient(colors: <Color>[
        isSelected ? color : colorSelected,
        colorSelected,
      ])),
    );
  }

  Widget buildCircleShape({double radius: 50.0}) {
    final GlobalKey<AnimatedCircularChartState> _chartKey =
        new GlobalKey<AnimatedCircularChartState>();

    List<CircularStackEntry> data = <CircularStackEntry>[
      new CircularStackEntry(
        <CircularSegmentEntry>[
          new CircularSegmentEntry(100.0, Singleton.setting.primaryColor,
              rankKey: 'Circle'),
        ],
        rankKey: 'Circle Shape',
      ),
    ];

    return new AnimatedCircularChart(
      key: _chartKey,
      size: new Size(radius, radius),
      initialChartData: data,
      chartType: CircularChartType.Pie,
      duration: new Duration(milliseconds: 800),
    );
  }
}
