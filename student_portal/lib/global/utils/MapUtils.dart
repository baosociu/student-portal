class MapUtils {


  static Map<String, DateTime> getStartDateEndDateOfWeek(DateTime date) {
    if(date == null)
      return {"start": null, "end": null};
    int dayOfWeek = date.weekday%7;
    DateTime start = date.subtract(new Duration(days: dayOfWeek));
    DateTime end = date.add(new Duration(days: 6 - dayOfWeek));
    return {"start": start, "end": end};
  }
}
