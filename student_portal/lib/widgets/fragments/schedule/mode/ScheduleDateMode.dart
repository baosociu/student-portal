import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleMode.dart';
import 'package:student_portal/global/service/schedule/ScheduleRequest.dart';
import 'package:student_portal/widgets/components/alert/CalendarDateAlert.dart';

class ScheduleDateMode extends ScheduleMode {
  ScheduleDateMode()
      : super(new ScheduleDateRequest(),
            Singleton.setting.localization.daily_schedule, Icons.access_time, Singleton.setting.primaryColor);

  @override
  String getTitle() {
    return StringUtils.convertDateTimeToString(
        (request as ScheduleDateRequest).date, "ddMMyyyy", "-");
  }

  @override
  void onTapLeft() {
    (request as ScheduleDateRequest).date =
        (request as ScheduleDateRequest).date.subtract(new Duration(days: 1));
  }

  @override
  void onTapRight() {
    (request as ScheduleDateRequest).date =
        (request as ScheduleDateRequest).date.add(new Duration(days: 1));
  }

  @override
  void onTapCenter(
      BuildContext context, onPositiveTap(DateTime valueSelected)) {
    showDayDialog(context, onPositiveTap);
  }

  Future<Null> showDayDialog(BuildContext context,
      dynamic Function(DateTime valueSelected) onPositiveTap) async {
    return await showCalendarDatePicker(
        context: context,
        firstYear: (request as ScheduleDateRequest).date.year - 200,
        lastYear: (request as ScheduleDateRequest).date.year + 200,
        initialDate:
            (request as ScheduleDateRequest).date ?? new DateTime.now(),
        textTitle: Singleton.setting.localization.select_date,
        textMessage: Singleton.setting.localization.you_need_select_date,
        textPositiveButton: Singleton.setting.localization.select,
        textNegativeButton: Singleton.setting.localization.cancel,
        iconHeader: iconData,
        onPositiveTap: onPositiveTap);
  }
}
