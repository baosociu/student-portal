import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/models/objects/Schedule.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleMode.dart';
import 'package:student_portal/widgets/components/alert/ChoiceOneFixedHeightAlert.dart';
import 'package:student_portal/global/service/schedule/ScheduleRequest.dart';

class ScheduleSemesterMode extends ScheduleMode {
  ScheduleSemesterMode()
      : super(
            new ScheduleSemesterRequest(),
            Singleton.setting.localization.schedule_by_semester,
            Icons.calendar_today,
            Singleton.setting.primaryColor);

  void setData(List<BaseHeaderSchedule> listHeader) {
    (request as ScheduleSemesterRequest).listHeader = listHeader;
    (request as ScheduleSemesterRequest).indexSelected =
        listHeader != null && listHeader.length != 0
            ? listHeader.length - 1
            : -1;
  }

  int getIdItem() {
    return (request as ScheduleSemesterRequest)
        .listHeader[(request as ScheduleSemesterRequest).indexSelected]
        .id;
  }

  @override
  String getTitle() {
    return (request as ScheduleSemesterRequest).indexSelected == -1
        ? ""
        : (request as ScheduleSemesterRequest)
            .listHeader[(request as ScheduleSemesterRequest).indexSelected]
            .name;
  }

  @override
  void onTapLeft() {
    if ((request as ScheduleSemesterRequest).indexSelected == -1) return;
    (request as ScheduleSemesterRequest).indexSelected =
        ((request as ScheduleSemesterRequest).indexSelected - 1) %
            (request as ScheduleSemesterRequest).listHeader.length;
  }

  @override
  void onTapRight() {
    if ((request as ScheduleSemesterRequest).indexSelected == -1) return;
    (request as ScheduleSemesterRequest).indexSelected =
        ((request as ScheduleSemesterRequest).indexSelected + 1) %
            (request as ScheduleSemesterRequest).listHeader.length;
  }

  void onTapAt(int index) {
    if (index >= 0 &&
        index < (request as ScheduleSemesterRequest).listHeader.length)
      (request as ScheduleSemesterRequest).indexSelected = index;
  }

  Future<Null> showSemestersDialog(BuildContext context,
      dynamic Function(int valueSelected) onPositiveTap) async {
    List<TextView> values = new List<TextView>();
    (request as ScheduleSemesterRequest).listHeader.forEach((item) {
      values.add(new TextView.alertText(item.name));
    });

    return await showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new ChoiceOneAlertFixedHeight(values,
            textTitle: Singleton.setting.localization.select_semester,
            textMessage: Singleton.setting.localization.you_need_select_semester,
            textPositiveButton: Singleton.setting.localization.select,
            textNegativeButton: Singleton.setting.localization.cancel,
            iconHeader: iconData,
            isAutoSelect: true,
            indexSelected: (request as ScheduleSemesterRequest).indexSelected,
            onPositiveTap: onPositiveTap));
  }

  @override
  void onTapCenter(BuildContext context, onPositiveTap(int valueSelected)) {
    showSemestersDialog(context, onPositiveTap);
  }

  onPositiveTap(int valueSelected) {
    (request as ScheduleSemesterRequest).indexSelected = valueSelected;
  }
}
