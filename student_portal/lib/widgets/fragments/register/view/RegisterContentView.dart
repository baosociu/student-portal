import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/service/register/BaseRegister.dart';
import 'package:student_portal/global/service/register/RegisterRequest.dart';
import 'package:student_portal/global/service/register/RegisterState.dart';
import 'package:student_portal/global/service/register/SubjectRegister.dart';
import 'package:student_portal/global/utils/AlertUtils.dart';
import 'package:student_portal/global/utils/SharedPrefrencesUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/widgets/components/CardView.dart';
import 'package:student_portal/widgets/components/LoadingView.dart';
import 'package:student_portal/widgets/components/RoundTextField.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/register/alert/OptionsRegisterAlert.dart';
import 'package:student_portal/widgets/fragments/register/view/RegisterContentListView.dart';
import 'package:student_portal/widgets/fragments/register/view/RegisterReplaceSubjectListView.dart';

// ignore: must_be_immutable
class RegisterContentView extends StatefulWidget {
  Stream<RegisterState> state;
  ValueChanged<RegisterRequest> execute;
  RegisterContentViewState myState;

  RegisterContentView(this.state, this.execute);

  @override
  RegisterContentViewState createState() {
    myState = new RegisterContentViewState(this.state, this.execute);
    return myState;
  }
}

class RegisterContentViewState extends State<RegisterContentView> {
  final String idStudent = Singleton.student.id.toString();
  final String idGroupMajor = Singleton.student.groupMajor.id.toString();

  Stream<RegisterState> state;
  ValueChanged<RegisterRequest> execute;

  RegisterContentViewState(this.state, this.execute);

  SemesterRegisterList listSemester;
  TypeRegisterList listType;
  BaseRegisterList listMethod;
  GroupRegisterList listGroup;

  int indexSelected;

  RegisterRequest request;
  RoundTextField searchField;
  String keySearch;

  //todo params

  List<BaseSubject> data = new List<BaseSubject>(); //todo temp save results

  @override
  void initState() {
    super.initState();
    this.indexSelected = -1;
    this.keySearch = "";

    searchField = new RoundTextField(
      typeShape: TypeShape.square,
      hintText: Singleton.setting.localization.search,
      prefixIcon: Icons.search,
      padding: 0.0,
      onTextChanged: (searchKey) {
        onSearch(searchKey);
      },
    );

    //todo get from local
    getOptions();
  }

  @override
  Widget build(BuildContext context) {
    return new StreamBuilder<RegisterState>(
        stream: state,
        initialData: new RegisterState.init(),
        builder: (BuildContext context, AsyncSnapshot<RegisterState> snapshot) {
          RegisterState model;
          model = snapshot.data ??
              new RegisterState.error(snapshot.error is SocketException
                  ? (snapshot.error as SocketException).message
                  : snapshot.error.toString());

          Widget resultView = buildResultView(model);
          return new Column(
            children: <Widget>[
              //todo search
              listSemester == null ||
                      listSemester.list == null ||
                      listSemester.list.length == 0
                  ? new SizedBox()
                  : new Column(
                      children: <Widget>[searchField, buildInfoOptions()],
                    ),

              listSemester == null
                  ? ViewUtils.buildLoadingCardView()
                  : (listSemester.list.length == 0
                      ? ViewUtils.buildNothingSemesterCardView(
                          () => getOptionsFromServer())
                      : new Flexible(
                          child: new CardView(
                          margin: 0.0,
                          isShowTitle: false,
                          child: new Flexible(
                              child: new Column(
                            children: <Widget>[
                              ViewUtils.buildRefreshView(model.loadingType),
                              new Flexible(child: resultView)
                            ],
                          )),
                        ))),
              ViewUtils.buildLoadMoreView(model.loadingType)
            ],
          );
        });
  }

  Future getOptions() async {
    dynamic data = await SharedPreferencesUtils.getRegisterOptions();
    getOptionsFromServer(data: data);
  }

  //todo build header semester of student
  getOptionsFromServer({Map<String, dynamic> data}) async {
    if (data == null) {
      setState(() {
        listSemester = null;
        listGroup = null;
        listMethod = null;
        listType = null;
      });

      final ServerResponse<dynamic> response =
          await API.infoOptions<dynamic>((data) {
        return data;
      }, () {
        return new Map<String, dynamic>();
      });
      if (!this.mounted) return;

      if (response.type == ResultType.resultEmpty ||
          response.type == ResultType.hasError) {
        ToastUtils.showToast(context, response.message);
      }

      if (!mounted) return;

      //todo save to local
      await SharedPreferencesUtils.saveRegisterOptions((response.data));

      setState(() {
        listSemester =
            new SemesterRegisterList.fromJson(response.data["hocKys"]);
        listGroup = new GroupRegisterList.fromJson(response.data["khoiNganhs"]);
        listMethod = new BaseRegisterList.method(response.data["heDaoTaos"]);
        listType = new TypeRegisterList.init();

        this.request = new RegisterRequest.from(listSemester.selected,
            listType.selected, listMethod.selected, listGroup.groupSelected,
            keySearch: keySearch, dataPrev: this.data);
      });
    } else
      setState(() {
        listSemester = new SemesterRegisterList.fromJson(data["hocKys"]);
        listGroup = new GroupRegisterList.fromJson(data["khoiNganhs"]);
        listMethod = new BaseRegisterList.method(data["heDaoTaos"]);
        listType = new TypeRegisterList.init();
      });

    setState(() {
      listGroup.list.insert(0, new GroupRegister.all());

      listSemester.index = 0;
      listSemester.semesterSelected.index = 0;
      listMethod.index = 0;
      listType.index = 0;

      listGroup.index = BaseRegisterList.indexById(
          listGroup.list, Singleton.student.groupMajor.course);
      listGroup.groupSelected.index = BaseRegisterList.indexById(
          listGroup.groupSelected.items,
          Singleton.student.groupMajor.mainMajor.id);
    });
    onExecute();
  }

  void onExecute({LoadingType type, List<int> idSubjectTemp}) {
    this.request = new RegisterRequest(listSemester.selected, listType.selected,
        listMethod.selected, listGroup.groupSelected,
        loadType: type ?? LoadingType.loading,
        dataPrev: data,
        keySearch: keySearch,
        idSubjectTemp: idSubjectTemp);
    execute(this.request);
  }

  void onSearch(String value) {
    print(value);
    setState(() {
      keySearch = value;
    });
    onExecute();
  }

  Widget buildResultView(RegisterState state) {
    if (state == null)
      return new SizedBox();
    else {
      if (state.result != null && state.result.result != null)
        this.data = state.result.result.list;

      if (state.result == null)
        return buildResultViewStateNull(state);
      else
        return buildResultViewStateNotNull(state);
    }
  }

  //todo state not null
  Widget buildResultViewStateNotNull(RegisterState state) {
    switch (state.result.resultType) {
      case ResultType.resultEmpty:
        return ViewUtils.buildNothingSubjectView(() {
          onExecute();
        });

      case ResultType.availableItems:
        return request.type.isReplaceRegister
            ? new RegisterReplaceSubjectListView(
                state.result.result.replaceSubjects, state, request)
            : new RegisterContentListView(state.result.result, state, request);

      case ResultType.loginAgain:
        return ViewUtils.buildErrorView(
          state.result.message,
          () {
            AlertUtils.showAlertRestart(context);
          },
        );
      default:
        return ViewUtils.buildErrorView(
          state.result.message,
          () {
            onExecute(type: LoadingType.refresh);
          },
        );
    }
  }

  //todo state null
  Widget buildResultViewStateNull(RegisterState state) {
    if (state.loadingType == LoadingType.loading)
      return new Column(
        children: <Widget>[
          new LoadingView(
            color: Singleton.setting.primaryColor,
          )
        ],
      );
    else //todo change, refresh, loadMore
      return request.type.isReplaceRegister ?
      new RegisterReplaceSubjectListView(
          new SubjectRegisterList.init().replaceSubjects, state, request):
       new RegisterContentListView(
          new SubjectRegisterList.init(), state, request);
  }

  Widget buildInfoOptions() {
    return new CardView(
      textLeft: listType.selected.name.toString(),
      header: "",
      iconLeft: Icons.gavel,
      iconRight: Icons.refresh,
      onTapRight: () => getOptionsFromServer(),
      child: new InkWell(
          onTap: showAlertOptions,
          child: new ListView.builder(
              shrinkWrap: true,
              itemCount: 2,
              itemBuilder: (context, index) {
                if (index == 0) {
                  return new Container(
                    margin: new EdgeInsets.only(
                        left: 4.0, right: 4.0, top: 2.0, bottom: 2.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Flexible(
                            child: new TextView.text(
                          Singleton.setting.localization.level +
                              ": " +
                              listMethod.selected.name.toString(),
                          overflow: TextOverflow.ellipsis,
                        )),
                        listSemester.isDisable ? new SizedBox() : new TextView.text(listSemester.selected.name.toString())
                      ],
                    ),
                  );
                } else
                  return new Container(
                    margin: new EdgeInsets.only(
                        left: 4.0, right: 4.0, top: 2.0, bottom: 2.0),
                    child: new Row(
                      children: <Widget>[
                        new Flexible(
                            child: listGroup.isDisable ? new SizedBox() : new TextView.text(
                              Singleton.setting.localization.major_group +
                                  ": " +
                                  listGroup.selected.name.toString(),
                              overflow: TextOverflow.ellipsis,
                            )),
                      ],
                    ),
                  );
              })),
    );
  }

  Future showAlertOptions() async {
    await showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new OptionsRegisterAlert(
            listType, listMethod, listSemester, listGroup,
            textTitle: Singleton.setting.localization.select_options_register,
            iconHeader: Icons.mode_edit, onPositiveTap: (Map<String, int> map) {
          print(map);
          setState(() {
            //todo index selected;
            listType.index = map[OptionsRegisterAlert.INDEX_TYPE];
            listMethod.index = map[OptionsRegisterAlert.INDEX_METHOD];
            listSemester.index = map[OptionsRegisterAlert.INDEX_YEAR_SEMESTER];
            listSemester.semesterSelected.index =
                map[OptionsRegisterAlert.INDEX_SEMESTER];
            listGroup.index = map[OptionsRegisterAlert.INDEX_COURSE_GROUP];
            listGroup.groupSelected.index =
                map[OptionsRegisterAlert.INDEX_GROUP];

            //todo disable by type;
            listSemester.isDisable = listType.selected.isReplaceRegister;
            listGroup.isDisable = listType.selected.isAgainRegister || listType.selected.isImproveRegister;

            data.clear();
            searchField.state.controller.clear();
            keySearch = "";
          });
          onExecute();
        }));
  }
}
