import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/widgets/components/CardView.dart';
import 'package:student_portal/widgets/components/LoadingView.dart';
import 'package:student_portal/widgets/components/RoundButton.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/components/alert/ChoiceMultiFixedHeightAlert.dart';
import 'package:student_portal/widgets/fragments/suggest/Condition.dart';
import 'package:student_portal/widgets/fragments/suggest/Scheduling.dart';
import 'package:student_portal/widgets/fragments/suggest/Subject.dart';

class SuggestFragment extends StatefulWidget {
  final String title = Singleton.setting.localization.suggest_fragment_title;

  SuggestFragment({Key key, this.idSemester});

  final String idSemester;

  @override
  SuggestFragmentState createState() => new SuggestFragmentState(this.title);
}

class SuggestFragmentState extends State<SuggestFragment> {
  final String title;
  String idStudent, idSemester;
  BuildContext _context;
  SubjectList subjectList;
  List<ClassSubject> listItems = new List();
  List<ClassSubject> suggestList = new List();
  Condition condition;
  RoundButton btnScheduling;

  SuggestFragmentState(this.title);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    idStudent = Singleton.student.id.toString();
    idSemester = widget.idSemester;
    condition = new Condition(true);
    btnScheduling = new RoundButton(Singleton.setting.localization.scheduling,
        color: Singleton.setting.primaryColor,
        colorText: Singleton.textLightColor,
        onTap: onTapSchedulingButton);

    getListItems();
    getListSubjects();
    //getInfoResultRegister(idStudent);
  }

  void getListSubjects() {
    subjectList = new SubjectList.fromListClassSubject(this.listItems);
  }

  void getListItems() {
    listItems = new List<ClassSubject>();
    listItems.add(new ClassSubject(1,'001716','001716','Giáo dục thể chất 3',1,1,4,'trung tâm thể thao tây thạnh'));
    listItems.add(new ClassSubject(2,'004502','004502','Thí nghiệm vật lý đại cương ',1,7,11,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(3,'007556','007556','Anh văn A2 ',2,4,6,'D302'));
    listItems.add(new ClassSubject(4,'000420','000420','Cấu trúc dữ liệu và giải thuật ',3,1,3,'B502'));
    listItems.add(new ClassSubject(5,'002289','002289','Kiến trúc máy tính ',3,4,6,'B504'));
    listItems.add(new ClassSubject(6,'004502','004502','Thí nghiệm vật lý đại cương ',3,8,12,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(7,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',4,8,12,'A103-B'));
    listItems.add(new ClassSubject(8,'001673','001673','Giáo dục quốc phòng - an ninh 3 AB ',5,1,5,'T2 (Sân tập GDQP 2)'));
    listItems.add(new ClassSubject(9,'003742','003742','Phương pháp tính ',5,7,8,'B506'));
    listItems.add(new ClassSubject(10,'003297','003297','Môi trường và con người ',5,9,10,'F303'));
    listItems.add(new ClassSubject(11,'006469','006469','Vật lý đại cương 2 ',6,1,2,'B304'));
    listItems.add(new ClassSubject(12,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',6,8,12,'A101-B'));
    listItems.add(new ClassSubject(13,'004502','004502','Thí nghiệm vật lý đại cương ',1,1,5,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(14,'003297','003297','Môi trường và con người ',1,3,4,'B304'));
    listItems.add(new ClassSubject(15,'007556','007556','Anh văn A2 ',1,7,9,'D302'));
    listItems.add(new ClassSubject(16,'004502','004502','Thí nghiệm vật lý đại cương ',2,1,5,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(17,'001716','001716','Giáo dục thể chất 3 ',3,1,5,'trung tâm thể thao tây thạnh'));
    listItems.add(new ClassSubject(18,'002289','002289','Kiến trúc máy tính ',4,1,3,'B501'));
    listItems.add(new ClassSubject(19,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',4,1,5,'A103-A'));
    listItems.add(new ClassSubject(20,'000420','000420','Cấu trúc dữ liệu và giải thuật ',4,4,6,'B501'));
    listItems.add(new ClassSubject(21,'000420','000420','Cấu trúc dữ liệu và giải thuật ',4,4,6,'B403'));
    listItems.add(new ClassSubject(22,'003297','003297','Môi trường và con người ',4,11,12,'B406'));
    listItems.add(new ClassSubject(23,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',5,1,5,'A108'));
    listItems.add(new ClassSubject(24,'003742','003742','Phương pháp tính ',5,7,8,'B504'));
    listItems.add(new ClassSubject(25,'006469','006469','Vật lý đại cương 2 ',5,9,10,'B506'));
    listItems.add(new ClassSubject(26,'001673','001673','Giáo dục quốc phòng - an ninh 3 AB ',6,8,12,'T3 (Sân tập GDQP 3)'));
    listItems.add(new ClassSubject(27,'001716','001716','Giáo dục thể chất 3 ',1,1,4,'Sân bóng đá Dạ phi cơ'));
    listItems.add(new ClassSubject(28,'004502','004502','Thí nghiệm vật lý đại cương ',1,7,11,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(29,'007556','007556','Anh văn A2 ',1,10,12,'D301'));
    listItems.add(new ClassSubject(30,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',2,1,5,'A208-C'));
    listItems.add(new ClassSubject(31,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',3,1,5,'A208-C'));
    listItems.add(new ClassSubject(32,'007556','007556','Anh văn A2 ',3,7,9,'D302'));
    listItems.add(new ClassSubject(33,'006469','006469','Vật lý đại cương 2 ',3,11,12,'B503'));
    listItems.add(new ClassSubject(34,'000420','000420','Cấu trúc dữ liệu và giải thuật ',4,1,3,'F603'));
    listItems.add(new ClassSubject(35,'002289','002289','Kiến trúc máy tính ',4,4,6,'F603'));
    listItems.add(new ClassSubject(36,'003297','003297','Môi trường và con người ',4,7,8,'B506'));
    listItems.add(new ClassSubject(37,'003742','003742','Phương pháp tính ',5,9,10,'B504'));
    listItems.add(new ClassSubject(38,'001673','001673','Giáo dục quốc phòng - an ninh 3 AB ',6,1,5,'T3 (Sân tập GDQP 3)'));
    listItems.add(new ClassSubject(39,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',1,1,5,'A208-C'));
    listItems.add(new ClassSubject(40,'007556','007556','Anh văn A2 ',1,7,9,'D301'));
    listItems.add(new ClassSubject(41,'004502','004502','Thí nghiệm vật lý đại cương ',2,1,5,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(42,'004502','004502','Thí nghiệm vật lý đại cương ',2,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(43,'001716','001716','Giáo dục thể chất 3 ',3,1,4,'Sân bóng đá Dạ phi cơ'));
    listItems.add(new ClassSubject(44,'003297','003297','Môi trường và con người ',3,9,10,'A507'));
    listItems.add(new ClassSubject(45,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',4,1,5,'A102-B'));
    listItems.add(new ClassSubject(46,'003742','003742','Phương pháp tính ',4,7,8,'B504'));
    listItems.add(new ClassSubject(47,'001673','001673','Giáo dục quốc phòng - an ninh 3 AB ',5,8,12,'T2 (Sân tập GDQP 2)'));
    listItems.add(new ClassSubject(48,'006469','006469','Vật lý đại cương 2 ',6,5,6,'B203'));
    listItems.add(new ClassSubject(49,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',6,8,12,'A102-B'));
    listItems.add(new ClassSubject(50,'002289','002289','Kiến trúc máy tính ',1,1,3,'B507'));
    listItems.add(new ClassSubject(51,'007556','007556','Anh văn A2 ',1,4,6,'D302'));
    listItems.add(new ClassSubject(52,'000420','000420','Cấu trúc dữ liệu và giải thuật ',1,7,9,'A406'));
    listItems.add(new ClassSubject(53,'001716','001716','Giáo dục thể chất 3 ',2,1,4,'trung tâm thể thao tây thạnh'));
    listItems.add(new ClassSubject(54,'003297','003297','Môi trường và con người ',3,7,8,'B504'));
    listItems.add(new ClassSubject(55,'006469','006469','Vật lý đại cương 2 ',3,9,10,'E.09'));
    listItems.add(new ClassSubject(56,'001673','001673','Giáo dục quốc phòng - an ninh 3 AB ',4,1,5,'T3 (Sân tập GDQP 3)'));
    listItems.add(new ClassSubject(57,'003742','003742','Phương pháp tính ',4,9,10,'B504'));
    listItems.add(new ClassSubject(58,'004502','004502','Thí nghiệm vật lý đại cương ',5,1,5,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(59,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',6,7,11,'A102-A'));
    listItems.add(new ClassSubject(60,'004794','004794','Thực hành cấu trúc dữ liệu và giải thuật ',6,7,11,'A103-A'));
    listItems.add(new ClassSubject(61,'007556','007556','Anh văn A2 ',1,10,12,'D302'));
    listItems.add(new ClassSubject(62,'007556','007556','Anh văn A2 ',3,1,3,'D301'));
    listItems.add(new ClassSubject(63,'007556','007556','Anh văn A2 ',4,4,6,'D303'));
    listItems.add(new ClassSubject(64,'007556','007556','Anh văn A2 ',6,4,6,'B305'));
    listItems.add(new ClassSubject(65,'001716','001716','Giáo dục thể chất 3 ',5,9,12,'Hồ bơi Tây thạnh'));
    listItems.add(new ClassSubject(66,'001716','001716','Giáo dục thể chất 3 ',3,9,12,'Hồ bơi Tây thạnh'));
    listItems.add(new ClassSubject(67,'003742','003742','Phương pháp tính ',1,5,6,'F603'));
    listItems.add(new ClassSubject(68,'007556','007556','Anh văn A2 ',2,1,3,'A404'));
    listItems.add(new ClassSubject(69,'007556','007556','Anh văn A2 ',4,10,12,'D303'));
    listItems.add(new ClassSubject(70,'007556','007556','Anh văn A2 ',5,1,3,'B202'));
    listItems.add(new ClassSubject(71,'007556','007556','Anh văn A2 ',1,4,6,'B107'));
    listItems.add(new ClassSubject(72,'001673','001673','Giáo dục quốc phòng - an ninh 3 AB ',2,1,5,'T1 (Sân tập GDQP 1)'));
    listItems.add(new ClassSubject(73,'001716','001716','Giáo dục thể chất 3 ',4,9,12,'Hồ bơi Tây thạnh'));
    listItems.add(new ClassSubject(74,'001716','001716','Giáo dục thể chất 3  ',4,9,12,'sân dạ phi cơ'));
    listItems.add(new ClassSubject(75,'007556','007556','Anh văn A2 ',2,1,3,'D303'));
    listItems.add(new ClassSubject(76,'001716','001716','Giáo dục thể chất 3 ',3,1,4,'Hồ bơi Tây thạnh'));
    listItems.add(new ClassSubject(77,'001716','001716','Giáo dục thể chất 3  ',3,1,4,'sân dạ phi cơ'));
    listItems.add(new ClassSubject(78,'001716','001716','Giáo dục thể chất 3 ',4,9,12,'Sân bóng chuyền dạ phi cơ'));
    listItems.add(new ClassSubject(79,'001673','001673','Giáo dục quốc phòng - an ninh 3 AB ',6,1,5,'T2 (Sân tập GDQP 2)'));
    listItems.add(new ClassSubject(80,'004502','004502','Thí nghiệm vật lý đại cương ',6,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(81,'004502','004502','Thí nghiệm vật lý đại cương ',1,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(82,'004502','004502','Thí nghiệm vật lý đại cương ',3,1,5,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(83,'004502','004502','Thí nghiệm vật lý đại cương ',5,7,11,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(84,'004502','004502','Thí nghiệm vật lý đại cương ',6,7,11,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(85,'003742','003742','Phương pháp tính ',2,7,8,'E.08'));
    listItems.add(new ClassSubject(86,'004502','004502','Thí nghiệm vật lý đại cương ',3,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(87,'004502','004502','Thí nghiệm vật lý đại cương ',4,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(88,'006469','006469','Vật lý đại cương 2 ',6,3,4,'B406'));
    listItems.add(new ClassSubject(89,'004502','004502','Thí nghiệm vật lý đại cương ',3,7,11,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(90,'004502','004502','Thí nghiệm vật lý đại cương ',4,7,11,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(91,'006469','006469','Vật lý đại cương 2 ',5,7,8,'B107'));
    listItems.add(new ClassSubject(92,'003742','003742','Phương pháp tính ',6,1,2,'B404'));
    listItems.add(new ClassSubject(93,'003297','003297','Môi trường và con người ',2,1,2,'B403'));
    listItems.add(new ClassSubject(94,'003297','003297','Môi trường và con người ',1,5,6,'B504'));
    listItems.add(new ClassSubject(95,'003297','003297','Môi trường và con người ',2,1,2,'E.08'));
    listItems.add(new ClassSubject(96,'003297','003297','Môi trường và con người ',3,1,2,'B406'));
    listItems.add(new ClassSubject(97,'003297','003297','Môi trường và con người ',3,3,4,'A403'));
    listItems.add(new ClassSubject(98,'003297','003297','Môi trường và con người ',3,5,6,'F401'));
    listItems.add(new ClassSubject(99,'003297','003297','Môi trường và con người ',3,3,4,'B201'));
    listItems.add(new ClassSubject(100,'004502','004502','Thí nghiệm vật lý đại cương ',5,1,5,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(101,'004502','004502','Thí nghiệm vật lý đại cương ',5,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(102,'003297','003297','Môi trường và con người ',5,11,12,'B105'));
    listItems.add(new ClassSubject(103,'003297','003297','Môi trường và con người ',1,7,8,'A303'));
    listItems.add(new ClassSubject(104,'004502','004502','Thí nghiệm vật lý đại cương ',4,7,11,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(105,'004502','004502','Thí nghiệm vật lý đại cương ',3,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(106,'004502','004502','Thí nghiệm vật lý đại cương ',3,7,11,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(107,'004502','004502','Thí nghiệm vật lý đại cương ',4,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(108,'004502','004502','Thí nghiệm vật lý đại cương ',4,7,11,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(109,'004502','004502','Thí nghiệm vật lý đại cương ',5,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(110,'004502','004502','Thí nghiệm vật lý đại cương ',5,7,11,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(111,'004502','004502','Thí nghiệm vật lý đại cương ',2,7,11,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(112,'004502','004502','Thí nghiệm vật lý đại cương ',2,7,11,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(113,'004502','004502','Thí nghiệm vật lý đại cương ',2,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(114,'004502','004502','Thí nghiệm vật lý đại cương ',4,1,5,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(115,'003297','003297','Môi trường và con người ',5,11,12,'B502'));
    listItems.add(new ClassSubject(116,'004502','004502','Thí nghiệm vật lý đại cương ',1,1,5,'P.TNVL1 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(117,'003297','003297','Môi trường và con người ',1,1,2,'F202'));
    listItems.add(new ClassSubject(118,'003297','003297','Môi trường và con người ',1,7,8,'B206'));
    listItems.add(new ClassSubject(119,'004502','004502','Thí nghiệm vật lý đại cương ',3,1,5,'P.TNVL2 (P.Thí nghiệm vật lý)'));
    listItems.add(new ClassSubject(120,'003297','003297','Môi trường và con người ',5,3,4,'B204'));

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: new TextView.appbar(this.title),
          backgroundColor: Singleton.setting.primaryColor,
        ),
        body: new Builder(builder: (BuildContext context) {
          this._context = context;
          return new Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                subjectList == null
                    ? ViewUtils.buildLoadingCardView()
                    : buildSuggestViews(),
                btnScheduling
              ],
            ),
          );
        }));
  }

// title: response.data.list[0].nameSemester.toString()
  Widget buildSuggestViews() {
    return new Flexible(
        child: new ListView.builder(
            shrinkWrap: true,
            itemCount: 3,
            itemBuilder: (context, index) {
              switch (index) {
                case 0:
                  return buildSubjectList(subjectList);
                case 1:
                  return buildConditionOption();
                default:
                  return buildScheduling(this.suggestList);
              }
            }));
  }

  Widget buildScheduling(List<ClassSubject> list) {
    return CardView(
        header: Singleton.setting.localization.scheduling_result,
        child: list == null
            ? new SizedBox.fromSize(
                size: new Size.fromHeight(200.0),
                child: new Column(
                  children: <Widget>[new LoadingView()],
                ))
            : (list.length == 0
                ? new SizedBox.fromSize(
                    size: new Size.fromHeight(200.0),
                    child: ViewUtils.buildNothingView(
                        Singleton.setting.localization.scheduling_nothing,
                        () => onTapSchedulingButton()),
                  )
                : new ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: list.length,
                    itemBuilder: (context, index) {
                      return new Column(
                        children: <Widget>[
                          buildItemClassSubject(list[index]),
                          index < list.length - 1
                              ? new Divider(color: Singleton.setting.primaryColor)
                              : new SizedBox()
                        ],
                      );
                    })));
  }

  Widget buildItemClassSubject(ClassSubject item) {
    return new ListTile(
      leading: new CircleAvatar(
        child: new TextView.leading(
            StringUtils.getLabelOfWeekShortestByInt(item.dayOfWeek)),
        backgroundColor: Singleton.setting.primaryColor,
      ),
      title: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Flexible(
              child: new TextView.title(
            item.nameSubject,
            textSize: 14.0,
            overflow: TextOverflow.ellipsis,
          )),
          new TextView.title(
            item.codeSubject,
            textSize: 14.0,
          )
        ],
      ),
      subtitle: new TableView([
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.code_class_subject,
            item.codeClassSubject),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.time,
            StringUtils.getLabelOfWeekByInt(item.dayOfWeek) +
                " (" +
                item.startPeriod.toString() +
                " - " +
                item.endPeriod.toString() +
                ")"),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.room, item.room),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.teacher, item.teacher),
      ], margin: 0.0),
    );
  }

  Widget buildConditionOption() {
    return new CardView(
        header: Singleton.setting.localization.conditions_option,
        child: new ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: 2,
            itemBuilder: (context, index) {
              switch (index) {
                case 0:
                  return buildDayOfWeekCondition();
                case 1:
                  return buildPeriodWeekCondition();
                default:
                  return new SizedBox();
              }
            }));
  }

  Widget buildDayOfWeekCondition() {
    List<TextView> valuesDialog = new List<TextView>();
    for (int i = 1; i <= 7; i++)
      valuesDialog
          .add(new TextView.alertText(StringUtils.getLabelOfWeekByInt(i)));

    return new InkWell(
      child: new Column(
        children: <Widget>[
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Padding(
                  padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                  child: new TextView.text(Singleton.setting.localization.day)),
              new Flexible(
                  child: new Padding(
                      padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                      child: new Align(
                        child: new TextView.text(
                          condition.getValueDayOfWeek(),
                          overflow: TextOverflow.ellipsis,
                        ),
                        alignment: Alignment.centerLeft,
                      ))),
              new Padding(
                  padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                  child: new Icon(
                    Icons.arrow_drop_down,
                    color: Singleton.setting.primaryColor,
                  ))
            ],
          ),
          new Divider(color: Singleton.setting.primaryColor)
        ],
      ),
      onTap: () async {
        await showDialog<Null>(
            context: context,
            barrierDismissible: false,
            child: new ChoiceMultiAlertFixedHeight(
                valuesDialog, condition.dayOfWeeks.values.toList(),
                height: 210.0,
                textTitle:
                    Singleton.setting.localization.select_day_of_week_condition,
                textMessage: Singleton
                    .setting.localization.you_need_select_day_of_week_condition,
                textPositiveButton: Singleton.setting.localization.confirm,
                textNegativeButton: Singleton.setting.localization.cancel,
                iconHeader: Icons.date_range,
                onPositiveTap: (List<bool> valuesSelected) {
              //todo save font here
              print(valuesSelected);
              setState(() {
                condition.updateDayOfWeek(valuesSelected);
              });
            }));
      },
    );
  }

  Widget buildPeriodWeekCondition() {
    List<TextView> valuesDialog = new List<TextView>();
    for (int i = 1; i <= 18; i++)
      valuesDialog.add(new TextView.alertText(
          Singleton.setting.localization.period + " " + i.toString()));

    return new InkWell(
      child: new Column(
        children: <Widget>[
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Padding(
                  padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                  child:
                      new TextView.text(Singleton.setting.localization.period)),
              new Flexible(
                  child: new Padding(
                      padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                      child: new Align(
                        child: new TextView.text(
                          condition.getValuePeriod(),
                          overflow: TextOverflow.ellipsis,
                        ),
                        alignment: Alignment.centerLeft,
                      ))),
              new Padding(
                  padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                  child: new Icon(
                    Icons.arrow_drop_down,
                    color: Singleton.setting.primaryColor,
                  ))
            ],
          )
        ],
      ),
      onTap: () async {
        await showDialog<Null>(
            context: context,
            barrierDismissible: false,
            child: new ChoiceMultiAlertFixedHeight(
                valuesDialog, condition.periods.values.toList(),
                height: 210.0,
                textTitle:
                    Singleton.setting.localization.select_period_condition,
                textMessage: Singleton
                    .setting.localization.you_need_select_period_condition,
                textPositiveButton: Singleton.setting.localization.confirm,
                textNegativeButton: Singleton.setting.localization.cancel,
                iconHeader: Icons.access_time,
                onPositiveTap: (List<bool> valuesSelected) {
              //todo save font here
              print(valuesSelected);
              setState(() {
                condition.updatePeriods(valuesSelected);
              });
            }));
      },
    );
  }

  Widget buildSubjectList(SubjectList list) {
    return new CardView(
        header: Singleton.setting.localization.subject_list,
        child: new ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: list.list.length,
            itemBuilder: (context, index) {
              Subject item = list.list[index];
              return new InkWell(
                child: new Column(
                  children: <Widget>[
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Padding(
                            padding: new EdgeInsets.only(right: 8.0, left: 8.0),
                            child: new Icon(
                              item.isSelected
                                  ? Icons.check_box
                                  : Icons.check_box_outline_blank,
                              color: Singleton.setting.primaryColor,
                            )),
                        new Flexible(
                            child: new ListView.builder(
                                itemCount: 2,
                                shrinkWrap: true,
                                primary: false,
                                itemBuilder: (context, index) {
                                  return index == 0
                                      ? buildHeaderSubject(index, item)
                                      : buildHeaderSubjectCode(item);
                                })),
                      ],
                    ),
                    index < list.list.length - 1
                        ? new Divider(color: Singleton.setting.primaryColor)
                        : new SizedBox()
                  ],
                ),
                onTap: () => setState(() {
                      item.isSelected = !item.isSelected;
                    }),
              );
            }));
  }

  Widget buildHeaderSubject(int index, Subject item) {
    return new Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: <Widget>[
      new Flexible(child: new TextView.title(
        item.nameSubject,
        fontWeight: FontWeight.bold,
        color: Singleton.setting.primaryColor,
        overflow: TextOverflow.ellipsis,
      ),)
    ],);
  }

  Widget buildHeaderSubjectCode(Subject item) {
    return new TextView.title(
      Singleton.setting.localization.code_subject + ": " + item.codeSubject,
      // StringUtils.convertDateTimeToString(item.date, "ddMMyyyy"),
      overflow: TextOverflow.ellipsis,
    );
  }

  void onTapSchedulingButton() {
    setState(() {
      btnScheduling.state.setLoading(true);
      suggestList = null;
    });

    (new Scheduling.from(listItems,subjectList, condition)).scheduling().then((result) {
      if (!mounted) return;

      setState(() {
        suggestList =
            result["success"] ? result["result"] : new List<ClassSubject>();
        suggestList.sort((ClassSubject it1, ClassSubject it2) =>
            it1.dayOfWeek > it2.dayOfWeek
                ? 1
                : (it1.dayOfWeek < it2.dayOfWeek
                    ? -1
                    : (it1.startPeriod > it2.startPeriod
                        ? 1
                        : (it1.startPeriod < it2.startPeriod ? -1 : 0))));
      });

      ToastUtils.showToast(_context, result["message"]);
      setState(() {
        btnScheduling.state.setLoading(false);
      });
    });
  }
}
