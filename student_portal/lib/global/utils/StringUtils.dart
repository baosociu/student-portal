import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/MapUtils.dart';

class StringUtils {
  static String toVND(int value) {
    bool isNegative = false;

    if (value < 0) {
      isNegative = true;
      value *= -1;
    }

    String str = value.toString();
    int countDot = str.length ~/ 3;
    if (str.length % 3 == 0) countDot--;

    for (int i = 0; i < countDot; i++)
      str = str.substring(0, str.length - (i + 1) * 3 - i) +
          "." +
          str.substring(str.length - (i + 1) * 3 - i);

    if (isNegative) str = "-" + str;

    return str + " VNĐ";
  }

  static String convertDateStringFormat(String source,
      [String sourceFormat = "yyyyMMdd",
      String desFormat = "ddMMyyyy",
      String pattern = "-"]) {
    if (sourceFormat == desFormat) return source;

    DateTime dt;
    try {
      int indexSourceYear = sourceFormat.indexOf("yyyy");
      int indexSourceMonth = sourceFormat.indexOf("MM");
      int indexSourceDay = sourceFormat.indexOf("dd");
      Map<String, int> indexSource =
          getIndexddMMyyyy(indexSourceYear, indexSourceMonth, indexSourceDay);
      indexSourceYear = indexSource["yyyy"];
      indexSourceMonth = indexSource["MM"];
      indexSourceDay = indexSource["dd"];

      int indexDesYear = desFormat.indexOf("yyyy");
      int indexDesMonth = desFormat.indexOf("MM");
      int indexDesDay = desFormat.indexOf("dd");
      Map<String, int> indexDes =
          getIndexddMMyyyy(indexDesYear, indexDesMonth, indexDesDay);
      indexDesYear = indexDes["yyyy"];
      indexDesMonth = indexDes["MM"];
      indexDesDay = indexDes["dd"];

      List<String> lstSource = new List<String>(3);
      lstSource[indexSourceYear] = "yyyy";
      lstSource[indexSourceMonth] = "MM";
      lstSource[indexSourceDay] = "dd";

      List<String> arr = source.split(pattern);
      if (indexSourceYear != indexDesYear) {
        swapString(arr, indexSourceYear, indexDesYear);
        swapString(lstSource, indexSourceYear, indexDesYear);
      } else if (indexSourceMonth != indexDesMonth) {
        swapString(arr, indexSourceMonth, indexDesMonth);
        swapString(lstSource, indexSourceMonth, indexDesMonth);
      } else if (indexSourceDay != indexDesDay) {
        swapString(arr, indexSourceDay, indexDesDay);
        swapString(lstSource, indexSourceDay, indexDesDay);
      }

      return convertDateStringFormat(
          arr.join(pattern), lstSource.join(), desFormat);

      // dt = new  DateTime(int.parse(arr[indexDesYear]), int.parse(arr[indexDesMonth]), int.parse(arr[indexDesDay]));
    } catch (e) {
      dt = null;
    }
    return dt == null ? null : dt.toIso8601String();
  }

  static swapString(List<String> lst, int i, int j) {
    String str = lst[i];
    lst[i] = lst[j];
    lst[j] = str;
  }

  static Map<String, int> getIndexddMMyyyy(
      int indexYear, int indexMonth, int indexDay) {
    if (indexMonth < indexYear) {
      //MMyyyy
      if (indexDay < indexMonth) {
        //ddMMyyyy
        indexYear = 2;
        indexMonth = 1;
        indexDay = 0;
      } else {
        if (indexDay < indexYear) {
          //MMddyyyy
          indexYear = 2;
          indexMonth = 0;
          indexDay = 1;
        } else {
          //MMyyyydd
          indexYear = 1;
          indexMonth = 0;
          indexDay = 2;
        }
      }
    } else {
      //yyyyMM
      if (indexDay < indexYear) {
        //ddyyyyMM
        indexYear = 1;
        indexMonth = 2;
        indexDay = 0;
      } else {
        if (indexDay < indexMonth) {
          //yyyyddMM
          indexYear = 0;
          indexMonth = 2;
          indexDay = 1;
        } else {
          //yyyyMMdd
          indexYear = 0;
          indexMonth = 1;
          indexDay = 2;
        }
      }
    }
    return {"yyyy": indexYear, "MM": indexMonth, "dd": indexDay};
  }

  static String getString(String str) {
    return str == null ? '' : str;
  }

  static String convertDateTimeToString(DateTime source, String desFormat,
      [String sourcePatten = "-"]) {
    if (source == null) return null;
    String sourceString = source.toIso8601String().split("T")[0];
    String des = convertDateStringFormat(
        sourceString, "yyyyMMdd", desFormat, sourcePatten);
    return des;
  }

  static String getStringStartWeekOfWeek(DateTime date) {
    Map<String, DateTime> startEnd = MapUtils.getStartDateEndDateOfWeek(date);

    String start =
        StringUtils.convertDateTimeToString(startEnd['start'], "ddMMyyyy", "-");
    String end =
        StringUtils.convertDateTimeToString(startEnd['end'], "ddMMyyyy", "-");

    return start.toString() + " - " + end.toString();
  }

  static String getLabelOfWeekByInt(int index) {
    switch (index) {
      case 1:
        return Singleton.setting.localization.monday;
      case 2:
        return Singleton.setting.localization.tuesday;
      case 3:
        return Singleton.setting.localization.wednesday;
      case 4:
        return Singleton.setting.localization.thursday;
      case 5:
        return Singleton.setting.localization.friday;
      case 6:
        return Singleton.setting.localization.saturday;
      default:
        return Singleton.setting.localization.sunday;
    }
  }

  static String getLabelOfWeekShortestByInt(int index) {
    switch (index) {
      case 1:
        return Singleton.setting.localization.monday_shortest;
      case 2:
        return Singleton.setting.localization.tuesday_shortest;
      case 3:
        return Singleton.setting.localization.wednesday_shortest;
      case 4:
        return Singleton.setting.localization.thursday_shortest;
      case 5:
        return Singleton.setting.localization.friday_shortest;
      case 6:
        return Singleton.setting.localization.saturday_shortest;
      default:
        return Singleton.setting.localization.sunday_shortest;
    }
  }

  static String getLabelOfWeek(DateTime date) {
    switch (date.weekday) {
      case 1:
        return Singleton.setting.localization.monday;
      case 2:
        return Singleton.setting.localization.tuesday;
      case 3:
        return Singleton.setting.localization.wednesday;
      case 4:
        return Singleton.setting.localization.thursday;
      case 5:
        return Singleton.setting.localization.friday;
      case 6:
        return Singleton.setting.localization.saturday;
      default:
        return Singleton.setting.localization.sunday;
    }
  }

  static String getLabelOfWeekShortest(DateTime date) {
    switch (date.weekday) {
      case 1:
        return Singleton.setting.localization.monday_shortest;
      case 2:
        return Singleton.setting.localization.tuesday_shortest;
      case 3:
        return Singleton.setting.localization.wednesday_shortest;
      case 4:
        return Singleton.setting.localization.thursday_shortest;
      case 5:
        return Singleton.setting.localization.friday_shortest;
      case 6:
        return Singleton.setting.localization.saturday_shortest;
      default:
        return Singleton.setting.localization.sunday_shortest;
    }
  }
}
