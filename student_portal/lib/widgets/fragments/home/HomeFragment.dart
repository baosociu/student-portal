import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/service/schedule/ScheduleAPI.dart';
import 'package:student_portal/global/service/schedule/ScheduleStream.dart';
import 'package:student_portal/global/utils/ImageProviderUtils.dart';
import 'package:student_portal/global/utils/SharedPrefrencesUtils.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/models/objects/Student.dart';
import 'package:student_portal/widgets/components/CardView.dart';
import 'package:student_portal/widgets/components/LineTextView.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/alert/ChangePasswordAlert.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleDateMode.dart';
import 'package:student_portal/widgets/fragments/schedule/view/ScheduleContentView.dart';

// ignore: must_be_immutable
class HomeFragment extends BaseFragment {
  HomeFragment(mainPage)
      : super(mainPage, Singleton.HOME_FRAGMENT_ID,
            Singleton.setting.localization.home_fragment_title,"resources/icons/home.png");

  final ScheduleAPI api = new ScheduleAPI();
  @override
  HomeFragmentState createState() {
    super.createState();
    return new HomeFragmentState();
  }
}

class HomeFragmentState extends BaseFragmentState {
  ScheduleStream stream;
  ScheduleContentView scheduleView;
  ChangePasswordAlert changePasswordAlert;

  @override
  void initState() {
    super.initState();
    stream = new ScheduleStream((widget as HomeFragment).api);
    scheduleView = new ScheduleContentView(
      scheduleMode: new ScheduleDateMode(),
      state: stream.state,
      execute: stream.onExecute.add,
    );
    changePasswordAlert = new ChangePasswordAlert(
        color: Singleton.setting.primaryColor,
        onPositiveTap: () {
          String passwordOld = changePasswordAlert.passwordOld;
          String passwordNew = changePasswordAlert.passwordNew;
          String passwordConfirm = changePasswordAlert.passwordConfirm;

          if (passwordNew == passwordConfirm)
            SharedPreferencesUtils
                .getUsernamePassword((String username, String password) {
              changePassword(username, passwordOld, passwordNew);
            });
          else
            ToastUtils.showToast(
                context, Singleton.setting.localization.password_not_match);
        });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            buildInfoStudentCardView(),
            new Flexible(child: scheduleView),
          ],
        ),
      ),
    );
  }

  //todo mode show information
  Widget buildAvatar() {
    ImageProvider avatar = ImageProviderUtils.getAvatarBase64(
        Singleton.student.avatar,
        Singleton.student.gender,
        Singleton.setting.primaryColor);

    return new Align(
      child: new CircleAvatar(
        radius: 36.0,
        backgroundImage: new FadeInImage(
          image: avatar,
          placeholder: new Image.asset("").image,
        ).image,
      ),
      alignment: Alignment.center,
    );
  }

  Widget buildInfoStudentTable() {
    Student student = Singleton.student;
    return new TableView(<Widget>[
      new LineTextView(
        label: Singleton.setting.localization.code_student,
        value: StringUtils.getString(student.code),
      ),
      new LineTextView(
        label: Singleton.setting.localization.full_name,
        value: StringUtils.getString(student.lastName) +
            " " +
            StringUtils.getString(student.firstName),
      ),
      new LineTextView(
        label: Singleton.setting.localization.class_student,
        value: StringUtils.getString(student.codeClass),
      ),
      new LineTextView(
        label: Singleton.setting.localization.major,
        value: StringUtils.getString(student.major.name),
      ),
    ]);
  }

  Widget buildInfoStudentAvatar() {
    return new Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Container(
          margin: new EdgeInsets.all(5.0),
          child: buildAvatar(),
        ),
        buildInfoStudentTable(),
      ],
    );
  }

  Widget buildInfoStudentCardView() {
    return new CardView(
      header: Singleton.setting.localization.profile,
      iconHeader: Icons.person,
      iconRight: Icons.lock_outline,
      onTapRight: () => showChangePasswordAlert(),
      child: buildInfoStudentAvatar(),
    );
  }

  Future changePassword(
      String username, String passwordOld, String passwordNew) async {
    Singleton.loadingPage.state.setLoading(true);

    final ServerResponse<dynamic> _response = await API
        .changePassword<dynamic>(username, passwordOld, passwordNew, (data) {
      return data;
    }, () {
      return null;
    });
    if (!this.mounted) return;

    if (_response.type == ResultType.availableItems) {
      Map<dynamic, String> data = new Map<dynamic,String>();
      _response.data.forEach((key, value) {
        data[key] = value;
      });

      if (data != null && data['result_flag'].toString() == 'true') {
        //todo update local
        SharedPreferencesUtils.clearUsernamePassword();
        SharedPreferencesUtils.setUsernamePassword(username, passwordNew);
      }
      ToastUtils.showToast(context, data['result_message'].toString());
    } else
      ToastUtils.showToast(context, _response.message);

    Singleton.loadingPage.state.setLoading(false);
  }

  void onTapSave() {
    print("save!!");
  }

  Future<Null> showChangePasswordAlert() {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: changePasswordAlert);
  }
}
