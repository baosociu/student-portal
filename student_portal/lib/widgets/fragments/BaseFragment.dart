import 'package:flutter/material.dart';
import 'package:student_portal/widgets/pages/MainPage.dart';


// ignore: must_be_immutable
class BaseFragment extends StatefulWidget{
  final int id;
  final String title;
  final String iconPath;
  BuildContext context;
  final MainPage mainPage;
  BaseFragmentState state_;

  BaseFragment(this.mainPage, this.id, this.title,this.iconPath, {Key key}) : super(key: key);
  @override
  BaseFragmentState createState() {
    state_ = new BaseFragmentState();
    return state_;
  }

  onRefresh(){
    print("refresh...");
  }
}

class BaseFragmentState extends State<BaseFragment>{


  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    widget.context = context;
  }

  @override
  Widget build(BuildContext context) {
    return null;
  }
}