import 'package:flutter/material.dart';

// ignore: must_be_immutable
class StudentPortalWidget extends StatefulWidget {
  final Widget child;
  StudentPortalWidgetState state;
  StudentPortalWidget({this.child});

  static restartApp(BuildContext context) {
    final StudentPortalWidgetState state =
    context.ancestorStateOfType(const TypeMatcher<StudentPortalWidgetState>());
    state.restartApp();
  }


  @override
  StudentPortalWidgetState createState() {
    this.state = new StudentPortalWidgetState();
    return state;
  }
}

class StudentPortalWidgetState extends State<StudentPortalWidget> {
  Key key = new UniqueKey();

  @override
  void initState(){
    super.initState();
  }

  void restartApp() {
    this.setState(() {
      key = new UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      key: key,
      child: widget.child,
    );
  }
}