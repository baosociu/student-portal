import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/utils/AlertUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Register.dart';
import 'package:student_portal/widgets/components/LoadingView.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/register/view/RegisterViewUtils.dart';
import 'package:student_portal/widgets/fragments/register_class_subject/ClassSubjectView.dart';

class RegisterClassSubjectFragment extends StatefulWidget {
  final String title;
  final String idStudent;
  final String idSemester;
  final String idSubject;

  RegisterClassSubjectFragment(
      this.title, this.idStudent, this.idSemester, this.idSubject,
      {Key key});

  @override
  RegisterClassSubjectFragmentState createState() =>
      new RegisterClassSubjectFragmentState(
          this.title, this.idStudent, this.idSemester, this.idSubject);
}

class RegisterClassSubjectFragmentState
    extends State<RegisterClassSubjectFragment> {
  final String title;
  final String idStudent;
  final String idSemester;
  final String idSubject;
  ServerResponse<RegisterSubjectList> response;
  bool enableLoading;
  BuildContext _context;

  RegisterClassSubjectFragmentState(
      this.title, this.idStudent, this.idSemester, this.idSubject);

  @override
  void initState() {
    super.initState();
    enableLoading = false;
    getClassSubject(idStudent, idSemester, idSubject);
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: enableLoading ? () {} : null,
        child: new Stack(
          children: <Widget>[
            buildMainPage(),
            enableLoading
                ? new Column(
                    children: <Widget>[
                      new LoadingView(
                        opacity: 0.6,
                        background: Singleton.darkColor,
                      ),
                    ],
                  )
                : new SizedBox()
          ],
        ));
  }

  Widget buildMainPage() {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new TextView.appbar(this.title),
        backgroundColor: Singleton.setting.primaryColor,
        actions: buildAppBarAction(),
      ),
      body: new Builder(builder: (context){
        _context = context;
        return new Center(
            child: new Column(
              children: <Widget>[
                response == null
                    ? ViewUtils.buildLoadingCardView()
                    : buildClassList(),
              ],
            ));
      }),
    );
  }

  List<Widget> buildAppBarAction() {
    return <Widget>[
      new InkWell(
        child: new Center(
          child: new Padding(
              padding: new EdgeInsets.only(right: 12.0, left: 12.0),
              child: new TextView.text(
                Singleton.setting.localization.register,
                theme: TypeTheme.light,
                textSize: TextView.MEDIUM_SIZE,
              )),
        ),
        onTap: () {
          if(response!= null){
            List<RegisterSubjectItem> _items = new List<RegisterSubjectItem>();
            for (RegisterSubject subject in response.data.list)
              _items.add(subject.selected);
            AlertUtils.showRegisterItemsAlert(context, response.data,
                funcPositive: (RegisterSubjectList items) {
                  onRegisterItem(items);
                });
          }
        },
      ),
    ];
  }

  Widget buildClassList() {
    return response.buildCardViewByResponse<RegisterSubject>(
        context: context,
        list: response.data.list,
        callback: () => getClassSubject(idStudent, idSemester, idSubject),
        emptyView: ViewUtils.buildNothingClassSubjectCardView,
        item: (index, item) {
          Color color = index == response.data.indexExpanded
              ? Singleton.setting.primaryColor
              : Singleton.textDarkColor;

          return new Column(
            children: <Widget>[
              new InkWell(
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new SizedBox(
                          height: 8.0,
                        ),
                        new Padding(
                            padding: new EdgeInsets.all(8.0),
                            child: new Column(
                              children: <Widget>[
                                new Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new Flexible(
                                      child: new TextView.text(
                                        item.nameSubject.toString(),
                                        textSize: TextView.LAGRE_SIZE,
                                        fontWeight: FontWeight.bold,
                                        color: color,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    new Icon(
                                      index == response.data.indexExpanded
                                          ? Icons.keyboard_arrow_down
                                          : Icons.chevron_right,
                                      color: color,
                                    )
                                  ],
                                ),
                                new Row(
                                  children: <Widget>[
                                    new Flexible(
                                        child: new TextView.text(
                                      Singleton.setting.localization
                                              .code_subject +
                                          ": " +
                                          item.codeSubject.toString(),
                                      textSize: TextView.MEDIUM_SIZE,
                                      fontWeight: FontWeight.bold,
                                      color: color,
                                      overflow: TextOverflow.ellipsis,
                                    ))
                                  ],
                                )
                              ],
                            )),
                        this.response.data.indexExpanded == index
                            ? new SizedBox()
                            : RegisterViewUtils
                                .buildClassSubjectItem(item.selected),
                        new Divider(),
                      ]),
                  onTap: () => setState(() {
                        this.response.data.indexExpanded =
                            this.response.data.indexExpanded == index
                                ? -1
                                : index;
                      })),
              new ClassSubjectView(
                item: item,
                isExpanded: response.data.indexExpanded == index,
                indexChanged: (int indexSelected) {
                  setState(() {
                    item.indexSelected = indexSelected;
                    this.response.data.indexExpanded = -1;
                  });
                },
              ),
            ],
          );
        });
  }

  //todo action
  void setLoading(bool enable) {
    setState(() {
      this.enableLoading = enable;
    });
  }

  getClassSubject(String idStudent, String idSemester, String idSubject) async {
    setState(() {
      this.response = null;
    });

    final ServerResponse<RegisterSubjectList> response = await API
        .infoClassSubjectBySubject<RegisterSubjectList>(
            idStudent, idSemester, idSubject, (data) {
      RegisterScheduleList _scheduleList =
          new RegisterScheduleList.fromJson(data);
      return new RegisterSubjectList.from(_scheduleList);
    }, () {
      return new RegisterSubjectList.nothing();
    });
    if (!this.mounted) return;

    setState(() {
      this.response = response;
    });
  }

  Future onRegisterItem(RegisterSubjectList items) async {
    this.setLoading(true);

    final ServerResponse<dynamic> _response =
        await API.registerClassSubject<dynamic>(items, (data) {
      return data;
    }, () {
      return null;
    });
    if (!this.mounted) return;

    if (_response.type == ResultType.availableItems) {
      Map data;
      _response.data.forEach((_map){
        data = _map;
      });

      if (data != null && data['result_flag'].toString() == 'true') {
        //todo register success
        data['result_id_subject'] = items.getIdSubjectList;
        Navigator.of(_context).pop(data);
      }
      ToastUtils.showToast(_context, data['result_message'].toString());
    } else
      ToastUtils.showToast(_context, _response.message);

    this.setLoading(false);
  }
}
