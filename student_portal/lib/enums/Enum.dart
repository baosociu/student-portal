enum ProfileMode  {
  SHOW_MODE, EDIT_MODE
}

enum LoadingType {loading, refresh, loadMore, change}
//loading ==> loadingView
//refresh ==> refresh, listView
//loadMore ==> listView, loadMore
//change ==> listView

enum ResultType{ resultEmpty, availableItems, hasError, loginAgain }

enum ScheduleType { day, week, subject, semester }

enum BackgroundType {simple, blank, custom}

enum ScrollingType {normal, top, bottom}

enum TextType {menu, appbar, title, text, leading, notification, alertTitle, alertText, primaryTitle, primaryText}

enum SmoothType {none, than_limit, less_limit, both }

enum DistanceType { high_distance, low_distance }

enum ScoreType {
  none, warning, passed
}
