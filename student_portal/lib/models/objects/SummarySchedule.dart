import 'package:student_portal/global/utils/StringUtils.dart';

//todo lịch học tổng quát
class SummarySchedule {


  int countTime; //số tiết
  dynamic idStudent;
  dynamic idClassSubject;
  dynamic idRoom;

  double weightPractice;
  double weight;
  String week;
  dynamic idSemester;

  String codeSubject;
  String nameSubject;
  dynamic idSubject;
  String nameRoom;

  int dayOfWeek;
  int time;
  double weightTheory;
  String nameBranch;

  String labelDayOfWeek;
  String labelDayOfWeekShortest;

  SummarySchedule.fromJson(Map json) {
    this.countTime = json['soTiet'];
    this.idStudent = json['hocVienId'];
    this.idClassSubject = json['lopMonHocID'];
    this.idRoom = json['phongId'];

    this.weightPractice = json['soTinChiTH'];
    this.weight = json['soTinChi'];
    this.week = json['tuanHoc'];
    this.idSemester = json['hocKyId'];

    this.codeSubject = json['maMonHoc'];
    this.nameSubject = json['tenMonHoc'];
    this.idSubject = json['monHocId'];
    this.nameRoom = json['tenPhong'];

    this.dayOfWeek = json['thuTrongTuan'] == 1 ? 7 : (json['thuTrongTuan'] - 1);
    this.time = json['tietHoc'];
    this.weightTheory = json['soTinChiLT'];
    this.nameBranch = json['tenCoSo'];

    this.labelDayOfWeek = StringUtils.getLabelOfWeekByInt(this.dayOfWeek);
    this.labelDayOfWeekShortest =
        StringUtils.getLabelOfWeekShortestByInt(this.dayOfWeek);
  }
}

class SummaryScheduleList {
  List<SummarySchedule> list = new List<SummarySchedule>();

  SummaryScheduleList.fromJson(dynamic data) {
    list = new List<SummarySchedule>();

    data.forEach((json) {
      SummarySchedule value = new SummarySchedule.fromJson(json);
      this.list.add(value);
    });
  }
}

class GroupSummaryScheduleByDay {
  final int dayOfWeek;
  final String labelDayOfWeek;
  final String labelDayOfWeekShortest;
  final List<SummarySchedule> items;

  GroupSummaryScheduleByDay(
      this.dayOfWeek, this.labelDayOfWeek, this.labelDayOfWeekShortest, this.items);

  factory GroupSummaryScheduleByDay.from(
      SummarySchedule item, List<SummarySchedule> items) {
    return new GroupSummaryScheduleByDay(
        item.dayOfWeek, item.labelDayOfWeek, item.labelDayOfWeekShortest, items);
  }
}

class GroupSummaryScheduleByDayList {
  final List<GroupSummaryScheduleByDay> items;

  GroupSummaryScheduleByDayList(this.items);

  factory GroupSummaryScheduleByDayList.from(List<SummarySchedule> items) {
    Map<dynamic, GroupSummaryScheduleByDay> maps = new Map();
    for (SummarySchedule item in items) {
      if (!maps.containsKey(item.dayOfWeek))
        maps[item.dayOfWeek] = new GroupSummaryScheduleByDay.from(
            item, new List<SummarySchedule>());
      maps[item.dayOfWeek].items.add(item);
    }

    return new GroupSummaryScheduleByDayList(maps.values.toList());
  }
}