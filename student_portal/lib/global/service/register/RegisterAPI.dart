import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/service/register/RegisterRequest.dart';
import 'package:student_portal/global/service/register/RegisterResult.dart';

class RegisterAPI {
  final Map<String, RegisterResult> cache;
  final HttpClient client;

  RegisterAPI({
    HttpClient client,
    Map<String, RegisterResult> cache,
  })  : this.client = client ?? new HttpClient(),
        this.cache = cache ?? <String, RegisterResult>{};

  RegisterRequest submittedSubject(RegisterRequest request) {
    cache.clear();
    //todo edit request
    return new RegisterRequest(
        request.semester, request.type, request.method, request.course,
        loadType: request.loadType,
        keySearch: request.keySearch,
        dataPrev: request.dataPrev,
        idSubjectTemp: new List<int>());
  }

  Future<RegisterResult> search(RegisterRequest request) async {
    if (request.idSubjectTemp.isNotEmpty) request = submittedSubject(request);

//    String key = getKey(request);
//
//    if (cache.containsKey(key) && request.loadType == LoadingType.loading) {
//      RegisterResult result = cache[key];
//      return result;
//    } else {

    final result = await _fetchResults(request);
    //todo tạm
    // final result = new RegisterResult.fromItems(new List());

//      if (key.isNotEmpty && result.isAvailable)
//        cache[key] = result;

    return result;
//    }
  }

  Future<RegisterResult> _fetchResults(RegisterRequest request) async {
    return await fetchByType(request).then((response) async {
      final dynamic data = await json
          .decode(await response.stream.transform(utf8.decoder).join());
      switch (response.statusCode) {
        case HttpStatus.OK:
          RegisterResult result =
              new RegisterResult.fromJson(data, request.type);
          return result;
        case HttpStatus.UNAUTHORIZED:
          return new RegisterResult.hasError(
              Singleton.setting.localization.require_try_login);
        default:
          return new RegisterResult.hasError(
              Singleton.setting.localization.error +
                  ": " +
                  response.reasonPhrase.toString());
      }
    });
  }

  Future<StreamedResponse> fetchByType(RegisterRequest request) {
    String codeStudent = Singleton.student.code.toString();
    String idStudent = Singleton.student.id.toString();
    String idSemester = request.semester.id.toString();
    String idGroup = request.course.selected.id.toString();
    String idCourse = request.course.id.toString();

    if (request.type.isAgainRegister)
      return API.infoSubjectAgain(idStudent, request.keySearch);
    else if (request.type.isImproveRegister)
      return API.infoSubjectImprove(idStudent, request.keySearch);
    else if (request.type.isNewRegister)
      return API.infoSubjectNew(
          idStudent, idSemester, idGroup, idCourse, request.keySearch);
    else
      return API.infoSubjectEquivalent(idCourse, idGroup, request.keySearch);
  }

  String getKey(RegisterRequest request) {
    return request.semester.name +
        request.type.name +
        request.method.name +
        request.course.selected.name +
        request.keySearch;
  }
}
