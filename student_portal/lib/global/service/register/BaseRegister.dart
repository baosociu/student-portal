import 'package:student_portal/global/Singleton.dart';

class BaseRegisterConst {
  static final int ID_BY_ALL = -1;
  static final int ID_NOTHING = -2000;
  static final int ID_NEW_REGISTER = 0;
  static final int ID_IMPROVE_REGISTER = 1;
  static final int ID_AGAIN_REGISTER = 2;
  static final int ID_REPLACE_REGISTER = 3;
}

class BaseRegister {
  final dynamic id;
  final String name;

  BaseRegister(this.id, this.name);

  //todo loai hinh dao tao
  factory BaseRegister.method(Map map) {
    return new BaseRegister(map["id"], map["ten"]);
  }

  //todo loai hinh dao tao
  factory BaseRegister.nothing() {
    return new BaseRegister(BaseRegisterConst.ID_NOTHING, "Nothing");
  }

  factory BaseRegister.all() {
    return new BaseRegister(
        BaseRegisterConst.ID_BY_ALL, Singleton.setting.localization.by_all);
  }
}

class BaseRegisterList extends BaseList {
  final List<BaseRegister> list;

  BaseRegister get selected => index >= 0 && index < list.length
      ? list[index]
      : new BaseRegister.nothing();

  BaseRegisterList(this.list);

  factory BaseRegisterList.method(dynamic list) {
    List<BaseRegister> _list = new List<BaseRegister>();
    for (Map map in list) _list.add(new BaseRegister.method(map));
    return new BaseRegisterList(_list);
  }

  factory BaseRegisterList.byIndex(List<BaseRegister> list, int index) {
    return new BaseRegisterList(list)..index = index;
  }

  static int indexById(List<BaseRegister> list, int id) {
    int index = 0;
    for (int i = 0; i < list.length; i++)
      if (list[i].id == id) {
        index = i;
        break;
      }
    return index;
  }
}

//todo học kỳ {năm học _ học kỳ}
class SemesterRegister extends BaseRegister {
  final List<BaseRegister> items;
  int index = 0;

  BaseRegister get selected => index >= 0 && index < items.length
      ? items[index]
      : new BaseRegister.nothing();
  SemesterRegister(dynamic id, String name, this.items) : super(id, name);

  factory SemesterRegister.nothing() {
    return new SemesterRegister(
        BaseRegisterConst.ID_NOTHING, "Nothing", new List<BaseRegister>());
  }
}

class SemesterRegisterList extends BaseList {
  final List<SemesterRegister> list;
  SemesterRegister get semesterSelected => index >= 0 && index < list.length
      ? list[index]
      : new SemesterRegister.nothing();
  BaseRegister get selected => semesterSelected.selected;

  SemesterRegisterList(this.list);

  factory SemesterRegisterList.fromJson(dynamic data) {
    Map<dynamic, List<BaseRegister>> maps = new Map();
    for (Map _map in data) {
      dynamic namhoc = _map["namHoc"];
      if (!maps.containsKey(namhoc)) maps[namhoc] = new List<BaseRegister>();

      dynamic id = _map["id"];
      String name = _map["ten"];
      maps[namhoc].add(new BaseRegister(id, name));
    }

    List<SemesterRegister> list = new List<SemesterRegister>();
    for (dynamic namhoc in maps.keys)
      list.add(new SemesterRegister(
          namhoc, "Năm " + namhoc.toString(), maps[namhoc]));

    return new SemesterRegisterList(list);
  }
}

//todo khối ngành {khóa _ ngành}
class GroupRegister extends BaseRegister {
  final List<BaseRegister> items;
  int index = 0;

  BaseRegister get selected => index >= 0 && index < items.length
      ? items[index]
      : new BaseRegister.nothing();
  GroupRegister(dynamic id, String name, this.items)
      : super(id, name); //khối ngành theo khối

  factory GroupRegister.nothing() {
    return new GroupRegister(
        BaseRegisterConst.ID_NOTHING, "Nothing", new List<BaseRegister>());
  }

  factory GroupRegister.all() {
    return new GroupRegister(
        BaseRegisterConst.ID_BY_ALL,
        Singleton.setting.localization.by_all,
        new List<BaseRegister>()..add(new BaseRegister.all()));
  }
}

class GroupRegisterList extends BaseList {
  final List<GroupRegister> list;
  GroupRegister get groupSelected => index >= 0 && index < list.length
      ? list[index]
      : new GroupRegister.nothing();
  BaseRegister get selected => groupSelected.selected;

  GroupRegisterList(this.list);

  factory GroupRegisterList.fromJson(dynamic data) {
    Map<dynamic, List<BaseRegister>> maps = new Map();
    for (Map _map in data) {
      dynamic khoahoc = _map["khoaHoc"];
      if (!maps.containsKey(khoahoc)) {
        maps[khoahoc] = new List<BaseRegister>();
        maps[khoahoc].add(new BaseRegister.all());
      }

      dynamic id = _map["id"];
      String name = _map["ten"];
      maps[khoahoc].add(new BaseRegister(id, name));
    }

    List<GroupRegister> list = new List<GroupRegister>();
    for (dynamic khoahoc in maps.keys)
      list.add(new GroupRegister(
          khoahoc, "Khóa " + khoahoc.toString(), maps[khoahoc]));

    return new GroupRegisterList(list);
  }
}

class TypeRegister extends BaseRegister {
  TypeRegister(dynamic id, String name)
      : super(id, name); //khối ngành theo khối

  factory TypeRegister.newRegister() {
    return new TypeRegister(BaseRegisterConst.ID_NEW_REGISTER, Singleton.setting.localization.register_new);
  }

  factory TypeRegister.improveRegister() {
    return new TypeRegister(BaseRegisterConst.ID_IMPROVE_REGISTER, Singleton.setting.localization.register_improve);
  }

  factory TypeRegister.againRegister() {
    return new TypeRegister(BaseRegisterConst.ID_AGAIN_REGISTER, Singleton.setting.localization.register_again);
  }

  factory TypeRegister.replaceRegister() {
    return new TypeRegister(BaseRegisterConst.ID_REPLACE_REGISTER, Singleton.setting.localization.register_equivalent);
  }

  factory TypeRegister.nothing() {
    return new TypeRegister(BaseRegisterConst.ID_NOTHING, "Nothing");
  }

  bool get isNewRegister => id == BaseRegisterConst.ID_NEW_REGISTER;
  bool get isImproveRegister => id == BaseRegisterConst.ID_IMPROVE_REGISTER;
  bool get isAgainRegister => id == BaseRegisterConst.ID_AGAIN_REGISTER;
  bool get isReplaceRegister => id == BaseRegisterConst.ID_REPLACE_REGISTER;

}

class TypeRegisterList extends BaseList {
  final List<TypeRegister> list;
  TypeRegister get selected => index >= 0 && index < list.length
      ? list[index]
      : new TypeRegister.nothing();

  TypeRegisterList(this.list);

  factory TypeRegisterList.init() {
    List<TypeRegister> _list = new List<TypeRegister>();
    _list.add(new TypeRegister.newRegister());
    _list.add(new TypeRegister.improveRegister());
    _list.add(new TypeRegister.againRegister());
    _list.add(new TypeRegister.replaceRegister());

    return new TypeRegisterList(_list);
  }
}

class BaseList {
  bool isDisable = false;
  int index = 0;

}
