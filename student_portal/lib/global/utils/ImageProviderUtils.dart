import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';

class ImageProviderUtils {
  static ImageProvider getAvatarBase64(String base64, [int gender= 1, Color color]){
    color = color ?? Singleton.setting.primaryColor;
    Image avatar = new Image.asset("resources/images/avatar_female.png",color: color,);
    if (gender == 1) {
      avatar = new Image.asset("resources/images/avatar_male.png",color: color);
    }

    try {
      Uint8List decode = base64Decode(Singleton.student.avatar);
      avatar = new Image.memory(decode,color: color,);
    } catch (e) {
      print(e.toString());
    }
    return avatar.image;
  }
}