//todo Faculty: Khoa
class Faculty {
  dynamic id;
  String name;
  String code;

  Faculty.fromJson(Map json) {
    this.id = json['id'];
    this.code = json['ma'];
    this.name = json['ten'];
  }

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'ma': this.code,
        'ten': this.name,
      };
}
