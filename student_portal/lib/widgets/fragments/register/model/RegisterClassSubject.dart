import 'dart:convert';

import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/models/objects/Register.dart';


class RegisterClassSubject {
  final String idStudent;
  final String idClassSubject;
  final String idSemester;
  final String registerDate;

  RegisterClassSubject (this.idStudent, this.idClassSubject, this.idSemester, this.registerDate);

  Map<String,dynamic> toJson(){
    return {
      "hocVienId": idStudent,
      "lopMonHocId": idClassSubject,
      "hocKyId": idSemester,
      "ngayDangKy": registerDate,
    };
  }
}

class RegisterClassSubjectList{
  final List<RegisterClassSubject> list;

  RegisterClassSubjectList(this.list);

  factory RegisterClassSubjectList.fromList(List<RegisterScheduleItem> list){
    List<RegisterClassSubject> _list = new List<RegisterClassSubject>();
    for(RegisterScheduleItem item in list)
      _list.add( new RegisterClassSubject( Singleton.student.id.toString(),
          item.idClassSubject.toString(),
          item.idSemester.toString(),
          StringUtils.convertDateTimeToString(
              new DateTime.now(), "ddMMyyyy")));
    return new RegisterClassSubjectList(_list);
  }

   List<String> get getIdSubjectList{
    List<String> ids = new List<String>();
    for(RegisterClassSubject item in list)
      ids.add(item.idSemester);
    return ids;
  }

  String toString(){
    String str = "[";
    list.forEach((i)=>str += (json.encode(i.toJson()) + ","));
    if(str.endsWith(","))
      str = str.substring(0,str.length-1);
    return str += "]";
  }
}
