import 'package:flutter/material.dart';
import 'package:material_pickers/material_pickers.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/fragments/setting/SettingsFragment.dart';

class ColorSettingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ColorPicker(
      type: MaterialType.transparency,
      onColor: (color) {
        //todo save color here
        (Singleton.currentFragment as SettingsFragment)
            .state
            .setting
        .theme
            .primaryColor = color;
        print(color);
      },
      currentColor: (Singleton.currentFragment as SettingsFragment)
          .state
          .setting
          .primaryColor,
    );
  }
}
