import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/DateTimeUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Schedule.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/TextView.dart';

// ignore: must_be_immutable
class ScheduleByDateListView extends StatelessWidget {
  final BaseScheduleList scheduleList;

  ScheduleByDateListView(this.scheduleList);
  BuildContext context;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return new Flexible(
        child: new ListView.builder(
            shrinkWrap: true,
            itemCount: this.scheduleList.list.length,
            itemBuilder: (context, index) {
              BaseSchedule baseSchedule = this.scheduleList.list[index];
              return new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    baseSchedule.runtimeType ==
                        new NormalSchedule().runtimeType
                        ? buildItemByNormalSchedule(
                        baseSchedule as NormalSchedule)
                        : buildItemByTestSchedule(
                        baseSchedule as TestSchedule),
                    new Divider(color: Singleton.setting.primaryColor),
                  ]);
            }));
  }

  //todo normal
  Widget buildItemByNormalSchedule(NormalSchedule normalSchedule) {
    bool isToday =
        DateTimeUtils.isEqualsDMY(normalSchedule.date, new DateTime.now());

    return new Container(
      padding: new EdgeInsets.all(4.0),
      child: new ListTile(
        onTap: () {
          ToastUtils.showToastNormalSchedule(context, normalSchedule);
        },
        title: buildHeaderNormalSchedule(isToday, normalSchedule),
        subtitle: buildDetailNormalSchedule(isToday, normalSchedule),
      ),
    );
  }

  Widget buildHeaderNormalSchedule(
      bool isToday, NormalSchedule normalSchedule) {
    return isToday
        ? new Row(
            children: <Widget>[
              ViewUtils.buildCheckIcon(Singleton.setting.primaryColor),
              new Flexible(
                  child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new TextView.title(
                    normalSchedule.nameSubject,
                    overflow: TextOverflow.ellipsis,
                  ),
                  new TextView.title(
                    Singleton.setting.localization.period +
                        ": " +
                        normalSchedule.time.toString() +
                        " -> " +
                        (normalSchedule.time + normalSchedule.countTime - 1)
                            .toString(),
                  ),
                ],
              )),
            ],
          )
        : new TextView.title(
            normalSchedule.nameSubject,
            overflow: TextOverflow.ellipsis,
          );
  }

  Widget buildDetailNormalSchedule(
      bool isToday, NormalSchedule normalSchedule) {
    return new TableView(
      <Widget>[
        isToday
            ? new SizedBox()
            : ViewUtils.buildItemTextView(
                Singleton.setting.localization.period + ": ",
                normalSchedule.time.toString() +
                    " -> " +
                    (normalSchedule.time + normalSchedule.countTime - 1)
                        .toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.room + ": ",
            normalSchedule.nameRoom.toString() +
                " - " +
                normalSchedule.nameBranch.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.teacher + ": ",
            normalSchedule.teacher.toString()),
      ],
      margin: 0.0,
    );
  }

  //todo test

  Widget buildItemByTestSchedule(TestSchedule testSchedule) {
    return new Container(
      padding: new EdgeInsets.all(4.0),
      decoration: new BoxDecoration(color: Singleton.yellow10OpacityColor),
      child: new ListTile(
        onTap: () {
          ToastUtils.showToastTestSchedule(context, testSchedule);
        },
        title: buildHeaderTestSchedule(testSchedule),
        subtitle: buildDetailTestSchedule(testSchedule),
      ),
    );
  }

  Widget buildHeaderTestSchedule(TestSchedule testSchedule) {
    return new Row(
      children: <Widget>[
        ViewUtils.buildStartIcon(),
        new Flexible(
            child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new TextView.title(
                Singleton.setting.localization.exam.toUpperCase() +
                    " " +
                    testSchedule.typeTest.toString().toUpperCase()),
            new TextView.title(
              testSchedule.nameSubject.toString().toUpperCase(),
              overflow: TextOverflow.ellipsis,
            ),
          ],
        )),
      ],
    );
  }

  Widget buildDetailTestSchedule(TestSchedule testSchedule) {
    return new TableView(
      <Widget>[
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.exam_type + ":",
            testSchedule.typeTest.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.room + ":",
            testSchedule.nameRoom.toString() +
                " - " +
                testSchedule.nameBranch.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.exam_time + ":",
            testSchedule.startTime.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.exam_period + ":",
            testSchedule.timeTest.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.supervisor_1 + ":",
            testSchedule.teacher1.toString()),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.supervisor_2 + ":",
            testSchedule.teacher2 == null
                ? ""
                : testSchedule.teacher2.toString()),
      ],
      margin: 0.0,
    );
  }
}
