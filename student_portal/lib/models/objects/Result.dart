import 'package:student_portal/models/objects/Register.dart';

class Result {
  String room;
  dynamic idStudent;
  int totalTime;
  dynamic idClassSubject;
  dynamic idSemester;
  String dateEnd;
  int numberOfStudent;
  String nameSemester;
  String status;
  String codeSubject;
  dynamic idSubjectPlus;
  dynamic idSubject;
  String dateRegister;
  dynamic id;
  String methodRegister;
  String nameTeacher;
  dynamic idBlockMajor;
  int countTime;
  bool isAccept;
  String dateUpdate;
  int maxTotal;
  String dateStart;
  bool isOpenClass;
  dynamic resultProcess;
  String nameSubject;
  dynamic accountUpdate;
  dynamic resultRegister;
  dynamic idTeacher;
  dynamic idDate;
  int time;
  int minTotal;

  Result.fromJson(Map map) {
    idStudent = map["hocVienId"];
    totalTime = map["tongSoTiet"];
    idClassSubject = map["lopMonHocId"];
    idSemester = map["hocKyId"];
    dateEnd = map["ngayKetThuc"];
    numberOfStudent = map["siSoThuc"];
    nameSemester = map["tenHocKy"];
    status = map["trangThai"];
    codeSubject = map["maMonHoc"];
    idSubjectPlus = map["monHocKemId"];
    idSubject = map["monHocId"];
    dateRegister = map["ngayDangKy"];
    id = map["id"];
    methodRegister = map["hinhThucDK"];
    nameTeacher = map["tenGV"];
    idBlockMajor = map["khoiNganhId"];
    countTime = map["soTiet"];
    isAccept = map["duyetYN"];
    idDate = map["ngayId"];
    dateUpdate = map["ngayCapNhat"];
    maxTotal = map["siSoMax"];
    dateStart = map["ngayBatDau"];
    isOpenClass = map["moLopYN"];
    resultProcess = map["ketQuaXuLy"];
    nameSubject = map["tenMonHoc"];
    accountUpdate = map["taiKhoanCapNhat"];
    resultRegister = map["ketQuaDk"];
    idTeacher = map["giaoVienId"];
    time = map["tietHoc"];
    minTotal = map["siSoMin"];
    room = map["tenPhong"];
  }
}

class ResultList {
  List<Result> list = new List<Result>();

  ResultList.fromJson(dynamic json) {
    for (Map map in json) {
      list.add(new Result.fromJson(map));
    }
  }
}

class ResultSemester {
  String room;
  dynamic idStudent;
  int totalTime;
  dynamic idClassSubject;
  String dateEnd;
  int numberOfStudent;
  String status;
  String codeSubject;
  dynamic idSubjectPlus;
  dynamic idSubject;
  String dateRegister;
  dynamic id;
  String methodRegister;
  String nameTeacher;
  dynamic idBlockMajor;
  int countTime;
  bool isAccept;
  String dateUpdate;
  int maxTotal;
  String dateStart;
  bool isOpenClass;
  dynamic resultProcess;
  String nameSubject;
  dynamic accountUpdate;
  dynamic resultRegister;
  dynamic idTeacher;
  int minTotal;
  List<RegisterSchedule> schedule;

  ResultSemester.fromItem(Result item) {
    schedule = new List<RegisterSchedule>();
    idStudent = item.idStudent;
    totalTime = item.totalTime;
    idClassSubject = item.idClassSubject;
    dateEnd = item.dateEnd;
    numberOfStudent = item.numberOfStudent;
    status = item.status;
    codeSubject = item.codeSubject;
    idSubjectPlus = item.idSubjectPlus;
    idSubject = item.idSubject;
    dateRegister = item.dateRegister;
    id = item.id;
    methodRegister = item.methodRegister;
    nameTeacher = item.nameTeacher;
    idBlockMajor = item.idBlockMajor;
    countTime = item.countTime;
    isAccept = item.isAccept;
    dateUpdate = item.dateUpdate;
    maxTotal = item.maxTotal;
    dateStart = item.dateStart;
    isOpenClass = item.isOpenClass;
    resultProcess = item.resultProcess;
    nameSubject = item.nameSubject;
    accountUpdate = item.accountUpdate;
    resultRegister = item.resultRegister;
    idTeacher = item.idTeacher;
    minTotal = item.minTotal;
    room = item.room;
  }
}

class ResultSemesterItem {
  dynamic idSemester;
  String nameSemester;
  List<ResultSemester> list;

  ResultSemesterItem.fromItem(Result item) {
    idSemester = item.idSemester;
    nameSemester = item.nameSemester;
    list = new List<ResultSemester>();
  }
}

class ResultSemesterList {
  List<ResultSemesterItem> list;

  ResultSemesterList.fromJson(dynamic json) {
    list = new List<ResultSemesterItem>();

    ResultList resultList = new ResultList.fromJson(json);

    resultList.list.forEach((item) {
      int i = 0;
      for (i = 0; i < list.length; i++)
        if (item.idSemester == list[i].idSemester) break;
      if (i == list.length) list.add(new ResultSemesterItem.fromItem(item));

      int j = 0;
      for (j = 0; j < list[i].list.length; j++)
        if (list[i].list[j].idClassSubject == item.idClassSubject) break;
      if (j == list[i].list.length)
        list[i].list.add(new ResultSemester.fromItem(item));
      list[i].list[j].schedule.add(new RegisterSchedule.fromResult(item));
    });
  }
}

//class ResultSchedule {
//  final dynamic idDate;
//  final int time;
//  ResultSchedule(this.idDate, this.time);
//}
