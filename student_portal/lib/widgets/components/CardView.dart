import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class CardView extends StatelessWidget {
  CardView({
    Key key,
    this.child,
    this.padding: 8.0,
    this.margin: 8.0,
    this.radius: 0.0,
    this.header: "header",
    this.isShowTitle: true,
    this.iconHeader,
    this.iconLeft,
    this.iconRight,
    this.textRight,
    this.textLeft,
    this.onTapRight,
    this.onTapLeft,
    this.onTapCenter,
    this.onTapBottomCenter,
    this.isShowBottom: false,
    this.iconBottom,
    this.bottomTitle: "bottom",
    Color colorBackground,
  }) : this.colorBackground = colorBackground ?? Singleton.setting.primaryColor;

  final Widget child;
  final double padding;
  final double margin;
  final double radius;
  final String header;
  final String textRight;
  final String textLeft;
  final bool isShowTitle;
  final Color colorBackground;
  final IconData iconHeader;
  final IconData iconLeft;
  final IconData iconRight;
  final GestureTapCallback onTapRight;
  final GestureTapCallback onTapLeft;
  final GestureTapCallback onTapCenter;
  final bool isShowBottom;
  final IconData iconBottom;

  final String bottomTitle;
  final GestureTapCallback onTapBottomCenter;

  @override
  Widget build(BuildContext context) {
    double paddingHeader = isShowTitle ? 0.0 : this.padding;
    double paddingContainer = isShowTitle ? this.padding : 0.0;

    return new Container(
      margin: new EdgeInsets.all(this.margin),
      padding: new EdgeInsets.only(
          bottom: this.padding,
          top: paddingHeader,
          right: paddingHeader,
          left: paddingHeader),
      child: new Material(
        borderRadius: new BorderRadius.circular(this.radius),
        shadowColor: this.colorBackground,
        color: Singleton.backgroundNothing,
        elevation: 5.0,
        child: new Column(
          children: <Widget>[
            //todo header
            buildHeader(),
            //todo body
            new SizedBox(
              height: paddingContainer,
            ),
            this.child,
            new SizedBox(
              height: paddingContainer,
            ),
            buildBottom(),
          ],
        ),
      ),
    );
  }

  //todo build bottom
  Widget buildBottom() {
    return isShowBottom
        ? new Container(
            padding: new EdgeInsets.all(6.0),
            decoration: new BoxDecoration(
              color: this.colorBackground,
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new SizedBox(),
                buildBottomCenter(),
                new SizedBox(),
              ],
            ))
        : new Row(
            children: <Widget>[
              new Flexible(child: new SizedBox()),
            ],
          );
  }

  //todo build bottomCenter
  Widget buildBottomCenter() {
    return new InkWell(
      child: new Center(
        child: new Row(
          children: <Widget>[
            buildIconBottomCenter(),
            new Align(
              alignment: Alignment.center,
              child: new TextView.text(this.bottomTitle, theme: TypeTheme.light,),
            ),
          ],
        ),
      ),
      onTap: this.onTapBottomCenter,
    );
  }

  //todo buildIconBottomCenter
  Widget buildIconBottomCenter() {
    return this.iconBottom == null
        ? new SizedBox()
        : new Container(
            padding: new EdgeInsets.all(2.0),
            child: new Icon(
              this.iconBottom,
              size: 18.0,
              color: Colors.white,
            ),
          );
  }

  //todo build header
  Widget buildHeader() {
    return isShowTitle
        ? new Container(
            padding: new EdgeInsets.all(6.0),
            decoration: new BoxDecoration(
              color: this.colorBackground,
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Flexible(
                  child: buildLeft(),
                ),
                buildCenter(),
                new Flexible(
                  child: buildRight(),
                ),
              ],
            ))
        : new Row(
            children: <Widget>[
              new Flexible(child: new SizedBox()),
            ],
          );
  }

  //todo build center
  Widget buildIconCenter() {
    return this.iconHeader == null
        ? new SizedBox()
        : new Container(
            padding: new EdgeInsets.all(2.0),
            child: new Icon(
              this.iconHeader,
              size: 18.0,
              color: Colors.white,
            ),
          );
  }

  Widget buildCenter() {
    return new InkWell(
      child: new Center(
        child: new Row(
          children: <Widget>[
            buildIconCenter(),
           new Align(alignment: Alignment.topCenter, child:  new TextView.title(this.header, theme: TypeTheme.light,),)
          ],
        ),
      ),
      onTap: this.onTapCenter,
    );
  }

  //todo build Left
  Widget buildLeft() {
    return new Center(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          this.iconLeft == null ? new SizedBox() : buildIconLeft(),
          this.textLeft == null ? new SizedBox() : buildTextLeft(),
        ],
      ),
    );
  }

  Widget buildTextLeft() {
    return new InkWell(
      onTap: this.onTapLeft,
      child: new TextView.text(this.textLeft, theme: TypeTheme.light,),
    );
  }

  Widget buildIconLeft() {
    return new InkWell(
      onTap: this.onTapLeft,
      child: new Container(
        padding: new EdgeInsets.all(2.0),
        child: new Icon(
          this.iconLeft,
          size: 18.0,
          color: Colors.white,
        ),
      ),
    );
  }

  //todo build Right
  Widget buildRight() {
    return new Center(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          this.iconRight == null ? new SizedBox() : buildIconRight(),
          this.textRight == null ? new SizedBox() : buildTextRight(),
        ],
      ),
    );
  }

  Widget buildTextRight() {
    return new InkWell(
      onTap: this.onTapRight,
      child: new TextView.text(this.textRight, theme: TypeTheme.light,),
    );
  }

  Widget buildIconRight() {
    return new InkWell(
      onTap: this.onTapRight,
      child: new Container(
        padding: new EdgeInsets.all(2.0),
        child: new Icon(
          this.iconRight,
          size: 18.0,
          color: Colors.white,
        ),
      ),
    );
  }
}
