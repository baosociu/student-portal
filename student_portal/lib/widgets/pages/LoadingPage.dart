import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/AlertUtils.dart';
import 'package:student_portal/global/utils/SharedPrefrencesUtils.dart';
import 'package:student_portal/widgets/components/LoadingView.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/pages/MainPage.dart';

// ignore: must_be_immutable
class LoadingPage extends StatefulWidget {
  LoadingPageState state;

  @override
  State<StatefulWidget> createState() {
    state = new LoadingPageState();
    return state;
  }
}

class LoadingPageState extends State<LoadingPage> {
  bool enableLoading;
  bool isShowHint;
  int selected = -1;
  List<String> hints = <String>[
    Singleton.setting.localization.hint_1,
    Singleton.setting.localization.hint_2,
    Singleton.setting.localization.hint_3
  ];

  MainPage main = new MainPage();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    enableLoading = false;
    Singleton.loadingPage = widget;
    selected = 0;
    isShowHint = Singleton.setting.isShowHint;
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: enableLoading ? () {} : _onWillPop,
        child: new Scaffold(
            body: new Stack(
          children: <Widget>[
            main,
            enableLoading
                ? new Column(
                    children: <Widget>[
                      new LoadingView(
                        opacity: 0.6,
                        background: Singleton.darkColor,
                      ),
                    ],
                  )
                : new SizedBox(),
            isShowHint ? buildBlurView() : new SizedBox(),
            isShowHint ? buildHintView() : new SizedBox(),
          ],
        )));
  }

  Widget buildHintView() {
    return new Stack(
      children: <Widget>[
        new Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new SizedBox(),
            new Container(
              child: new Align(
                  alignment: Alignment.center,
                  child: new CircleAvatar(
                    radius: 50.0,
                    backgroundImage:
                        new AssetImage('resources/images/logo-white.png'),
                    backgroundColor: Colors.transparent,
                  )),
            ),
            new SizedBox(),
            new SizedBox()
          ],
        ),
        new GestureDetector(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new Column(
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      new Flexible(
                          child: new Container(
                        decoration: new BoxDecoration(
                            borderRadius:
                                new BorderRadius.all(new Radius.circular(5.0)),
                            border: new Border.all(color: Colors.white)),
                        child: new TextView.text(
                          hints[selected],
                          textSize: TextView.LAGRE_SIZE,
                          color: Colors.white,
                          softWrap: true,
                        ),
                        margin: new EdgeInsets.all(20.0),
                        padding: new EdgeInsets.all(10.0),
                      ))
                    ],
                  ),
                  new Container(
                    margin: new EdgeInsets.all(20.0),
                    padding: new EdgeInsets.all(10.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        dot(0, selected == 0),
                        dot(1, selected == 1),
                        dot(2, selected == 2),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
          onHorizontalDragEnd: (DragEndDetails details) {
            print(details.primaryVelocity);
            double distance = details.primaryVelocity < 0
                ? -details.primaryVelocity
                : details.primaryVelocity;
            if (distance >= 400) {
              int index = selected;
              if (details.velocity.pixelsPerSecond.dx < 0)
                index++;
              else
                index--;

              if (index < 0) index = 2;
              if (index > 2) index = 0;

              setState(() {
                this.selected = index;
              });
            }
          },
        ),
        new Container(
            margin: new EdgeInsets.all(20.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Column(
                      children: <Widget>[
                        new Align(
                          alignment: Alignment.center,
                          child: new TextView.text(
                            "Trương Huỳnh Gia Bảo",
                            textSize: TextView.SMALL_SIZE,
                            color: Colors.white,
                          ),
                        ),
                        new Align(
                          alignment: Alignment.center,
                          child: new TextView.text(
                            "Nguyễn Văn Hiếu",
                            textSize: TextView.SMALL_SIZE,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    new InkWell(
                      child: new Container(
                        padding: new EdgeInsets.all(5.0),
                        decoration: new BoxDecoration(
                            borderRadius:
                                new BorderRadius.all(new Radius.circular(5.0)),
                            border: new Border.all(color: Colors.white)),
                        child: new TextView.text(
                          Singleton.setting.localization.gotIt,
                          color: Colors.white,
                        ),
                      ),
                      onTap: () async {
                        await SharedPreferencesUtils.saveSettings(
                            Singleton.setting..isShowHint = false);
                        setState(() {
                          this.isShowHint = false;
                        });
                      },
                    )
                  ],
                )
              ],
            ))
      ],
    );
  }

  Widget dot(int index, bool isSelected) {
    return new InkWell(
      child: new Container(
        margin: new EdgeInsets.only(left: 5.0, right: 5.0),
        child: new CircleAvatar(
          backgroundColor:
              isSelected ? Colors.white : Colors.white.withOpacity(0.4),
          radius: 5.0,
        ),
      ),
      onTap: () {
        setState(() {
          this.selected = index;
        });
      },
    );
  }

  Widget buildBlurView() {
    return new Column(
      children: <Widget>[
        new Flexible(
          child: new Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new ExactAssetImage(Singleton.setting.background),
                fit: BoxFit.cover,
              ),
            ),
            child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: new Container(
                decoration:
                    new BoxDecoration(color: Colors.white.withOpacity(0.0)),
              ),
            ),
          ),
        )
      ],
    );
  }

  void setLoading(bool enable) {
    setState(() {
      this.enableLoading = enable;
    });
  }

  //todo back
  Future<bool> _onWillPop() {
    return AlertUtils.showAlertLogout(context);
  }
}
