import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/schedule/view/ScheduleBySubjectDetailListView.dart';

class ScheduleBySubjectDetailFragment extends StatefulWidget {
  final String title;
  final int idClassSubject;

  ScheduleBySubjectDetailFragment(this.idClassSubject, this.title, {Key key});

  @override
  State<StatefulWidget> createState() =>
      new ScheduleBySubjectDetailFragmentState(this.idClassSubject, this.title);
}

class ScheduleBySubjectDetailFragmentState
    extends State<ScheduleBySubjectDetailFragment> {
  final String title;
  final int idClassSubject;

  ScheduleBySubjectDetailFragmentState(this.idClassSubject, this.title);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: new TextView.appbar(this.title),
          backgroundColor: Singleton.setting.primaryColor,
        ),
        body: new ScheduleBySubjectDetailListView(
            Singleton.student.id.toString(), this.idClassSubject.toString()));
  }
}
