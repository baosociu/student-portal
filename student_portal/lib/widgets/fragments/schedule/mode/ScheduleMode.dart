import 'package:flutter/material.dart';
import 'package:student_portal/global/service/schedule/ScheduleRequest.dart';


abstract class ScheduleMode {
  String header;
  IconData iconData;
  Color color;
  ScheduleRequest request;

  ScheduleMode(ScheduleRequest request, String header, IconData iconData, Color color) {
    this.header = header;
    this.iconData = iconData;
    this.color = color;
    this.request = request;
  }

  String getTitle();

  void onTapRight();

  void onTapLeft();

  void onTapCenter(BuildContext context,onPositiveTap(dynamic valueSelected));

}