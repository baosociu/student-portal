import 'package:student_portal/global/utils/DateTimeUtils.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/models/objects/GroupMajor.dart';
import 'package:student_portal/models/objects/MainMajor.dart';
import 'package:student_portal/models/objects/Major.dart';
import 'package:student_portal/models/objects/TrainingSystem.dart';

class Student {
  dynamic id;
  String code;
  String lastName; //ho
  String firstName;
  String avatar;
  String codeClass;
  TrainingSystem trainingSystem;
  Major major;
  MainMajor mainMajor;
  GroupMajor groupMajor;
  int yearOfAdmission; //năm nhập học
  int course;
  DateTime dateCreate;
  String codeStatus;
  DateTime dateOfBirth;
  String email;
  String phoneNumber;
  String identityNumber;
  int gender;

  Student.fromJson(Map json) {
    this.id = json['id'];
    this.code = json['mshv'];
    this.lastName = json['ho'];
    this.firstName = json['ten'];
    this.avatar = json['hinhAnh'];
    this.codeClass = json['maLop'];
    this.trainingSystem = new TrainingSystem.fromJson(json['heDaoTaoId']);
    this.major = new Major.fromJson(json['nganhId']);
    this.mainMajor = new MainMajor.fromJson(json['chuyenNganhId']);
    this.groupMajor = new GroupMajor.fromJson(json['khoiNganhId']);
    this.yearOfAdmission = json['namNhapHoc'];
    this.course = json['khoaHoc'];
    this.dateCreate = DateTimeUtils.convertStringToDateTime(json['ngayLap'], "ddMMyyyy", "-");
    this.codeStatus = json['maTrangThai'];
    this.dateOfBirth = DateTimeUtils.convertStringToDateTime(json['ngaySinh'], "ddMMyyyy", "-");
    this.email = json['email'];
    this.phoneNumber = json['sdt'];
    this.identityNumber = json['cmnd'];
    this.gender = json['gioiTinh'];
  }

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'mshv': this.code,
        'ho': this.lastName,
        'ten': this.firstName,
        'hinhAnh': this.avatar,
        'maLop': this.codeClass,
        'heDaoTaoId': this.trainingSystem.toJson(),
        'nganhId': this.major.toJson(),
        'chuyenNganhId': this.mainMajor.toJson(),
        'khoiNganhId': this.groupMajor.toJson(),
        'namNhapHoc': this.yearOfAdmission,
        'khoaHoc': this.course,
        'ngayLap': StringUtils.convertDateTimeToString(this.dateCreate, "ddMMyyyy"),
        'maTrangThai': this.codeStatus,
        'ngaySinh': StringUtils.convertDateTimeToString(this.dateOfBirth, "ddMMyyyy"),
        'email': this.email,
        'sdt': this.phoneNumber,
        'cmnd': this.identityNumber,
        'gioiTinh': this.gender,
      };
}
