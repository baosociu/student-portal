import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';

enum SizeFont { normal, small, large }

class LineTextView extends StatelessWidget {
  LineTextView(
      {Key key,
        this.fontWeight: FontWeight.normal,
      this.padding: 2.0,
      this.margin: 2.0,
      this.sizeFont: SizeFont.normal,
      Color colorLine,
      this.label,
      this.value: "",
      this.hiddenLine: true,
      this.leftFlex: 2,
      this.rightFlex: 5,
      this.editable: false,
      this.obscureText: false,
      this.isOverflow: false,
       Widget rightWidget,
      this.isValueNumber: false})
      : this.colorLine = colorLine ?? Singleton.textDarkColor, this.rightWidget = rightWidget ?? new SizedBox();

  final SizeFont sizeFont;
  final double margin;
  final double padding;
  final Color colorLine;
  final String label;
  final String value;
  final bool hiddenLine;
  final int leftFlex;
  final int rightFlex;
  final bool editable;
  final bool obscureText;
  final bool isOverflow;
  final bool isValueNumber;
  final FontWeight fontWeight;
  final Widget rightWidget;

  @override
  Widget build(BuildContext context) {
    double size = sizeFont == SizeFont.normal
        ? TextView.MEDIUM_SIZE
        : (sizeFont == SizeFont.small ?TextView.SMALL_SIZE : TextView.LAGRE_SIZE);

    TextView labelTextView = new TextView.text(
      this.label,
      textSize: size,
      fontWeight: this.fontWeight,
      overflow: isOverflow ? TextOverflow.ellipsis : TextOverflow.fade,
    );

    TextField textField = new TextField(
      style: labelTextView.style,
      obscureText: this.obscureText,
      controller:
          new TextEditingController(text: this.value == null ? '' : this.value),
      focusNode: new FocusNode(),
      decoration: new InputDecoration(
        contentPadding: new EdgeInsets.all(0.0),
        border: InputBorder.none,
      ),
    );

    return
        //  new Flexible(child:
        new Container(
      padding: new EdgeInsets.all(this.padding),
      margin: new EdgeInsets.only(top: this.margin, bottom: this.margin),
      child: new Column(
        children: <Widget>[
          new Row(
            children: <Widget>[
              new Flexible(
                  flex: this.leftFlex,
                  child: new Align(
                      alignment: Alignment.centerLeft, child: labelTextView)),
//              new SizedBox(
//                width: 10.0,
//              ),
              new Flexible(
                flex: this.rightFlex,
                child: editable
                    ? new Align(
                        alignment: Alignment.centerRight,
                        child: new Column(
                          children: <Widget>[
                            textField,
                            new Divider(
                              color: this.colorLine,
                              height: 0.5,
                            ),
                          ],
                        ),
                      )
                    : new TextView.text(
                        this.value,
                        fontWeight: this.fontWeight,
                        textSize: size,
                        textAlign:
                            isValueNumber ? TextAlign.right : TextAlign.left,
                        overflow: isOverflow
                            ? TextOverflow.ellipsis
                            : TextOverflow.fade,
                      ),
              ),
              rightWidget
            ],
          ),
          hiddenLine
              ? new SizedBox()
              : new Divider(
                  color: this.colorLine,
                  height: 0.5,
                ),
        ],
      ),
      //    ),
    );
  }
}
