import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/Singleton.dart';


enum TypeTheme {dark, light}
class TextView extends StatelessWidget {
  static final double SMALL_SIZE = 16.0;
  static final double MEDIUM_SIZE = 18.0;
  static final double LAGRE_SIZE = 20.0;
  static final double XLAGRE_SIZE = 22.0;

  /// Creates a text widget.
  ///
  /// If the [style] argument is null, the text will use the style from the
  /// closest enclosing [DefaultTextStyle].
  const TextView(
    this.data,
    this.type, {
    Key key,
    this.textAlign,
    this.style,
    this.textDirection,
    this.softWrap,
    this.overflow,
    this.textScaleFactor,
    this.maxLines,
  })  : assert(data != null),
        textSpan = null,
        super(key: key);

  final String data;
  final TextSpan textSpan;
  final TextAlign textAlign;
  final TextDirection textDirection;
  final bool softWrap;
  final TextOverflow overflow;
  final double textScaleFactor;
  final int maxLines;
  final TextStyle style;
  final TextType type;

  factory TextView.alertTitle(
      String data,{
        TextSpan textSpan,
        TextAlign textAlign,
        TextDirection textDirection,
        bool softWrap,
        TextOverflow overflow,
        double textScaleFactor,
        int maxLines,
      }) {
    TextStyle  style = new TextStyle(fontFamily: Singleton.setting.titleFont, fontSize: LAGRE_SIZE, fontWeight: FontWeight.w700);  //notification


    return TextView(data, TextType.alertTitle,
        textAlign: textAlign,
        textDirection: textDirection,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        style: style);
  }

  factory TextView.alertText(
      String data,{
        String fontFamily,
        TextSpan textSpan,
        TextAlign textAlign,
        TextDirection textDirection,
        bool softWrap,
        TextOverflow overflow,
        double textScaleFactor,
        int maxLines,
      }) {
    String _fontFamily = fontFamily ?? Singleton.setting.textFont;
    TextStyle  style = new TextStyle(fontFamily: _fontFamily, fontSize: MEDIUM_SIZE);  //notification


    return TextView(data, TextType.alertText,
        textAlign: textAlign,
        textDirection: textDirection,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        style: style);
  }

  factory TextView.notification(
    String data,{
        Color color,
        TypeTheme theme,
        FontWeight fontWeight,
    TextSpan textSpan,
    TextAlign textAlign,
    TextDirection textDirection,
    bool softWrap,
    TextOverflow overflow,
    double textScaleFactor,
    int maxLines,
  }) {
    FontWeight _fontWeight = fontWeight ?? FontWeight.normal;
    TypeTheme _theme = theme ?? TypeTheme.light;
    Color _color = color ?? (_theme == TypeTheme.dark ? Singleton.textDarkColor : Singleton.textLightColor );
    TextStyle  style = new TextStyle(fontFamily: Singleton.setting.textFont, fontSize: SMALL_SIZE, color: _color, fontWeight: _fontWeight);  //notification


    return TextView(data, TextType.notification,
        textAlign: textAlign,
        textDirection: textDirection,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        style: style);
  }

  factory TextView.text(
      String data, {
        String fontFamily,
        double textSize,
        Color color,
        TypeTheme theme,
        FontWeight fontWeight,
        TextSpan textSpan,
        TextAlign textAlign,
        TextDirection textDirection,
        bool softWrap,
        TextOverflow overflow,
        double textScaleFactor,
        int maxLines,
      }) {
    String _fontFamily = fontFamily ?? Singleton.setting.textFont;
    double _textSize = textSize ?? MEDIUM_SIZE;
    FontWeight _fontWeight = fontWeight ?? FontWeight.normal;
    TypeTheme _theme = theme ?? TypeTheme.dark;
    Color _color = color ?? (_theme == TypeTheme.dark ? Singleton.textDarkColor : Singleton.textLightColor );
    TextStyle  style = new TextStyle(fontFamily: _fontFamily, fontSize: _textSize,fontWeight: _fontWeight, color: _color );

    return TextView(data, TextType.text,
        textAlign: textAlign,
        textDirection: textDirection,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        style: style);
  }


  factory TextView.title(
      String data, {
        double textSize,
        Color color,
        TypeTheme theme,
        FontWeight fontWeight,
        TextSpan textSpan,
        TextAlign textAlign,
        TextDirection textDirection,
        bool softWrap,
        TextOverflow overflow,
        double textScaleFactor,
        int maxLines,
      }) {
    double _textSize = textSize ?? LAGRE_SIZE;
    FontWeight _fontWeight = fontWeight ?? FontWeight.normal;
    TypeTheme _theme = theme ?? TypeTheme.dark;
    Color _color = color ?? (_theme == TypeTheme.dark ? Singleton.textDarkColor : Singleton.textLightColor );
    TextStyle style = new TextStyle(fontFamily: Singleton.setting.titleFont, fontSize: _textSize, color: _color, fontWeight: _fontWeight); //title

    return TextView(data, TextType.title,
        textAlign: textAlign,
        textDirection: textDirection,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        style: style);
  }

  factory TextView.appbar(
      String data,{
        TextSpan textSpan,
        TextAlign textAlign,
        TextDirection textDirection,
        bool softWrap,
        TextOverflow overflow,
        double textScaleFactor,
        int maxLines,
      }) {
    TextStyle  style = new TextStyle(fontFamily: Singleton.setting.navigationFont, fontSize: XLAGRE_SIZE, color: Colors.white, fontWeight: FontWeight.w700); //appbar

    return TextView(data, TextType.appbar,
        textAlign: textAlign,
        textDirection: textDirection,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        style: style);
  }

  factory TextView.leading(
      String data,{
        double textSize,
        FontWeight fontWeight,
        Color color,
        TypeTheme theme,
        TextSpan textSpan,
        TextAlign textAlign,
        TextDirection textDirection,
        bool softWrap,
        TextOverflow overflow,
        double textScaleFactor,
        int maxLines,
      }) {
    double _textSize = textSize ?? MEDIUM_SIZE;
    FontWeight _fontWeight = fontWeight ?? FontWeight.w700;
    TypeTheme _theme = theme ?? TypeTheme.light;
    Color _color = color ?? (_theme == TypeTheme.dark ? Singleton.textDarkColor : Singleton.textLightColor );
    TextStyle style = new TextStyle(fontFamily: Singleton.setting.textFont, fontSize: _textSize, color:  _color, fontWeight: _fontWeight); //leading

    return TextView(data, TextType.leading,
        textAlign: textAlign,
        textDirection: textDirection,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        style: style);
  }

  factory TextView.menu(
      String data, {
        FontWeight fontWeight,  Color color,
        TypeTheme theme,
        TextSpan textSpan,
        TextAlign textAlign,
        TextDirection textDirection,
        bool softWrap,
        TextOverflow overflow,
        double textScaleFactor,
        int maxLines,
        String fontFamily,
        double textSize,
      }) {

    String _fontFamily = fontFamily ?? Singleton.setting.navigationFont;
    double _textSize = textSize ?? MEDIUM_SIZE;
    FontWeight _fontWeight = fontWeight ?? FontWeight.normal;
    TypeTheme _theme = theme ?? TypeTheme.dark;
    Color _color = color ?? (_theme == TypeTheme.dark ? Singleton.textDarkColor : Singleton.textLightColor );
    TextStyle style = new TextStyle(fontFamily: _fontFamily, fontSize: _textSize, color: _color, fontWeight: _fontWeight); //leading

    return TextView(data, TextType.menu,
        textAlign: textAlign,
        textDirection: textDirection,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        style: style);
  }

  @override
  Widget build(BuildContext context) {
    final DefaultTextStyle defaultTextStyle = DefaultTextStyle.of(context);
    TextStyle effectiveTextStyle = style;
    if (style == null || style.inherit)
      effectiveTextStyle = defaultTextStyle.style.merge(style);
    return new RichText(
      textAlign: textAlign ?? defaultTextStyle.textAlign ?? TextAlign.start,
      textDirection:
          textDirection, // RichText uses Directionality.of to obtain a default if this is null.
      softWrap: softWrap ?? defaultTextStyle.softWrap,
      overflow: overflow ?? defaultTextStyle.overflow,
      textScaleFactor: textScaleFactor ??
          MediaQuery.of(context, nullOk: true)?.textScaleFactor ??
          1.0,
      maxLines: maxLines ?? defaultTextStyle.maxLines,
      text: new TextSpan(
        style: effectiveTextStyle,
        text: data,
        children: textSpan != null ? <TextSpan>[textSpan] : null,
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder description) {
    super.debugFillProperties(description);
    description.add(new StringProperty('data', data, showName: false));
    if (textSpan != null) {
      description.add(textSpan.toDiagnosticsNode(
          name: 'textSpan', style: DiagnosticsTreeStyle.transition));
    }
    style?.debugFillProperties(description);
    description.add(new EnumProperty<TextAlign>('textAlign', textAlign,
        defaultValue: null));
    description.add(new EnumProperty<TextDirection>(
        'textDirection', textDirection,
        defaultValue: null));
    description.add(new FlagProperty('softWrap',
        value: softWrap,
        ifTrue: 'wrapping at box width',
        ifFalse: 'no wrapping except at line break characters',
        showName: true));
    description.add(new EnumProperty<TextOverflow>('overflow', overflow,
        defaultValue: null));
    description.add(new DoubleProperty('textScaleFactor', textScaleFactor,
        defaultValue: null));
    description.add(new IntProperty('maxLines', maxLines, defaultValue: null));
  }
}
