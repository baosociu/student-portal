import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Fee.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class DetailFeeFragment extends StatefulWidget {
  final int idSemester;
  final String title;

  DetailFeeFragment(this.idSemester, this.title);

  @override
  DetailFeeFragmentState createState() {
    return new DetailFeeFragmentState(this.idSemester, this.title);
  }
}

class DetailFeeFragmentState extends State<DetailFeeFragment> {
  final int _idSemester;
  final String title;
  String idStudent;
  String idSemester;

  ServerResponse<FeeSemesterList> response;

  bool isRefresh;

  DetailFeeFragmentState(this._idSemester, this.title);

  @override
  void initState() {
    super.initState();
    idStudent = Singleton.student.id.toString();
    idSemester = _idSemester.toString();

    isRefresh = false;
    getInfoFeeSemester(idStudent, idSemester);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new TextView.appbar(this.title),
        backgroundColor: Singleton.setting.primaryColor,
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            isRefresh
                ? ViewUtils.buildRefreshView(LoadingType.refresh)
                : new SizedBox(),
            response == null
                ? ViewUtils.buildLoadingCardView()
                : buildFeeSemesterList(context),
          ],
        ),
      ),
    );
  }

  Widget buildFeeSemesterList(BuildContext context) {
    return response.data.list.length == 0
        ? ViewUtils.buildNothingDetailCardView(
            () => getInfoFeeSemester(idStudent, idSemester))
        : response.buildCardViewByResponse<FeeSemesterItem>(
            context: context,
            list: response.data.list[0].list,
            callback: () => getInfoFeeSemester(idStudent, idSemester),
            emptyView: ViewUtils.buildNothingDetailCardView,
            item: (index, item) {
              return new Column(children: <Widget>[
                new ListTile(
                    title: buildTitle(item),
                    subtitle: buildSubtitle(item)),
                index < response.data.list[0].list.length - 1
                    ? new Divider(color: Singleton.setting.primaryColor)
                    : new SizedBox()
              ]);
            });
  }

  Widget buildTitle(FeeSemesterItem item) {
    Color color = Singleton.setting.primaryColor;
    Color color20 = Singleton.setting.primary20OpacityColor;

    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Flexible(
            child: new Row(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(4.0),
                  child: new CircleAvatar(
                    child: new Icon(Icons.library_books,
                        color: Singleton.backgroundNothing),
                    backgroundColor: color,
                  ),
                ),
                new Flexible(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new TextView.title(
                          item.nameSubject.toString(),
                          overflow: TextOverflow.ellipsis,
                        ),
                        new TextView.text(
                          Singleton.setting.localization.code_subject +
                              ": " +
                              item.codeSubject.toString(),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    )),
              ],
            ),
          ),
          new Icon(
            Icons.chevron_right,
            color: color,
          ),
        ],
      ),
      decoration: new BoxDecoration(
          gradient: new LinearGradient(
              tileMode: TileMode.clamp,
              colors: <Color>[
                Colors.transparent,
                color20,
                color20,
                Colors.transparent
              ])),
    );
  }

  Widget buildSubtitle(FeeSemesterItem item) {
    return new Container(
      margin: new EdgeInsets.only(left: 24.0),
      decoration: new BoxDecoration(
          gradient: new RadialGradient(
              radius: 1.0,
              center: Alignment.bottomLeft,
              colors: <Color>[
                Singleton.setting.secondColor,
                Singleton.backgroundNothing
              ])),
      child: new Container(
        margin: new EdgeInsets.only(left: 4.0, bottom: 4.0),
        color: Singleton.backgroundNothing,
        child: new Padding(
            padding: new EdgeInsets.only(left: 8.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ViewUtils.buildItemNumberTextView(
                    Singleton.setting.localization.theory + ":",
                    item.weightTheory.toString() +
                        " x " +
                        (item.priceTheory == null
                            ? "null"
                            : StringUtils.toVND(item.priceTheory ~/ 1))),
                ViewUtils.buildItemNumberTextView(
                    Singleton.setting.localization.practice + ":",
                    item.weightPractice.toString() +
                        " x " +
                        (item.pricePractice == null
                            ? "null"
                            : StringUtils.toVND(item.pricePractice ~/ 1))),
                ViewUtils.buildItemNumberTextView(
                    Singleton.setting.localization.total_price + ":",
                    (item.totalPrice == null
                        ? "null"
                        : StringUtils.toVND(item.totalPrice ~/ 1)), fontWeight: FontWeight.bold),
              ],
            )
        ),
      ),
    );
  }

  getInfoFeeSemester(String idStudent, String idSemester) async {
    if (!isRefresh)
      setState(() {
        this.response = null;
      });

    final ServerResponse<FeeSemesterList> response =
        await API.infoFee<FeeSemesterList>(idStudent, idSemester, (data) {
      return new FeeSemesterList.fromJson(data);
    }, () {
      return new FeeSemesterList.fromJson(new List<Map>());
    });

    if (!mounted) return;

    setState(() {
      this.response = response;
      this.isRefresh = false;
    });
  }
}
