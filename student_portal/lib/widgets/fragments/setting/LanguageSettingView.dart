import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/Localization.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/setting/SettingsFragment.dart';

// ignore: must_be_immutable
class LanguageSettingView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LanguageSettingViewState();
  }
}

class LanguageSettingViewState extends State<LanguageSettingView> {
  Localization localization;

  @override
  void initState() {
    super.initState();
    localization = (Singleton.currentFragment as SettingsFragment).state.setting.localization;
  }

  @override
  Widget build(BuildContext context) {
    final Map<String,String> subtitles = {
      Localization.CODE_VI: Singleton.setting.localization.vietnames,
      Localization.CODE_EN: Singleton.setting.localization.english
    };

    return new ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: subtitles.length,
      itemBuilder: (BuildContext context, int index) {
        return new InkWell(
            onTap: () {
              setState(() {
                this.localization = subtitles.keys.toList()[index] == Localization.CODE_VI ? new Localization.vi() :  new Localization.en() ;
                (Singleton.currentFragment as SettingsFragment).state.setting.localization = this.localization;
              });
            },
            child: new Column(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(8.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Flexible(
                        child: new Align(
                          child: new TextView.text(
                            subtitles.values.toList()[index],
                            overflow: TextOverflow.ellipsis,
                          ),
                          alignment: Alignment.centerLeft,
                        ),
                      ),
                      new Align(
                        child: new Icon(
                          subtitles.keys.toList()[index] ==localization.locale
                              ? Icons.radio_button_checked
                              : Icons.radio_button_unchecked,
                          color: Singleton.setting.primaryColor,
                        ),
                      )
                    ],
                  ),
                ),
                index < subtitles.length - 1 ? new Divider(height: 5.0,) : new SizedBox()
              ],
            ));
      },
    );
  }
}
