import 'package:flutter/material.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class TextFieldFocus extends StatefulWidget {
  final TextStyle hintStyle;
  final TextStyle textStyle;
  final String hint;
  final bool obscureText;
  final String warningText;
  final Color color;
  final Function(String text) onValueChanged;
  TextFieldFocusState myState;

  TextFieldFocus(
      {Key key,
      this.hintStyle,
      this.textStyle,
      this.hint,
      this.obscureText,
      this.warningText,
        this.onValueChanged,
      this.color})
      : super(key: key);

  factory TextFieldFocus.init({
    TextStyle hintStyle,
    TextStyle textStyle,
    String hint,
    String warningText,
    bool obscureText,
    Color color,
    Function(String text) onValueChanged,
  }) {
    hintStyle = hintStyle == null
        ? (new TextView.text(
            "",
            color: Colors.grey[500],
          ))
            .style
        : hintStyle;
    textStyle = textStyle == null ? (new TextView.text("")).style : textStyle;
    obscureText = obscureText == null ? false : obscureText;
    hint = hint == null ? "" : hint;
    warningText = warningText == null ? "This field is required" : warningText;
    color = color == null ? Colors.red : color;
    onValueChanged = onValueChanged == null ? (text){} : onValueChanged;
    return new TextFieldFocus(
      hintStyle: hintStyle,
      textStyle: textStyle,
      hint: hint,
      obscureText: obscureText,
      warningText: warningText,
      onValueChanged: onValueChanged,
      color: color,
    );
  }

  @override
  TextFieldFocusState createState() {
    myState = new TextFieldFocusState();
    return myState;
  }

  String get value  => myState.textField.controller.text;
  bool get isEmpty => myState.textField.controller.text.isEmpty;
}

class TextFieldFocusState extends State<TextFieldFocus> {
  bool isEmpty;
  bool isInit;
  TextField textField;
  TextEditingController controller = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isEmpty = true;
    isInit = true;
    textField = new TextField(
      controller: controller,
        onChanged: (String text) {
          setState(() {
            isInit = false;
            isEmpty = text.isEmpty;
          });
          widget.onValueChanged(text);
        },
        obscureText: widget.obscureText,
        style: widget.textStyle,
        decoration: new InputDecoration(
          contentPadding: new EdgeInsets.all(0.0),
          labelText: widget.hint,
          labelStyle: widget.textStyle,
          border: InputBorder.none,
        ));
  }

  @override
  Widget build(BuildContext context) {
    bool isRequire = (isEmpty && !isInit);
    return new Padding(padding: new EdgeInsets.only(bottom: 16.0), child: new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Row(
          children: <Widget>[
            new Flexible(
                child: textField),
            isRequire
                ? new InkWell(
              child: new Icon(Icons.warning),
            )
                : new SizedBox(),
          ],
        ),
        new Divider(height: 1.0, color: widget.color,),
        isRequire
            ? new TextView.text(
          widget.warningText,
          color: Colors.red,
          textSize: TextView.SMALL_SIZE,
        )
            : new SizedBox(),
      ],
    ),);
  }
}
