import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextFieldFocus.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class ChangePasswordAlert extends StatefulWidget {
  ChangePasswordAlert({
    Key key,
    Color color,
    this.onPositiveTap,
  }) : this.color = color ?? Singleton.setting.primaryColor;

  final Color color;
  final GestureTapCallback onPositiveTap;
  ChangePasswordAlertState myState;

  @override
  ChangePasswordAlertState createState() {
    myState =  new ChangePasswordAlertState();
    return myState;
  }

  String get passwordOld  => myState.tfOldPassword.value;
  String get passwordNew  => myState.tfNewPassword.value;
  String get passwordConfirm  => myState.tfNewPassword2.value;

}

class ChangePasswordAlertState extends State<ChangePasswordAlert> {
  final double padding = 16.0;
  TextFieldFocus tfOldPassword;
  TextFieldFocus tfNewPassword;
  TextFieldFocus tfNewPassword2;
  bool isDisable;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isDisable = true;

    tfOldPassword = new TextFieldFocus.init(
      warningText: Singleton.setting.localization.require_text,
      hint: Singleton.setting.localization.password_current,
      color: Singleton.setting.primaryColor,
      onValueChanged: onValueChanged,
      obscureText: true,
    );

    tfNewPassword = new TextFieldFocus.init(
      warningText: Singleton.setting.localization.require_text,
      hint: Singleton.setting.localization.password_new,
      color: Singleton.setting.primaryColor,
      onValueChanged: onValueChanged,
      obscureText: true,
    );

    tfNewPassword2 = new TextFieldFocus.init(
      warningText: Singleton.setting.localization.require_text,
      hint: Singleton.setting.localization.password_confirm,
      color: Singleton.setting.primaryColor,
      onValueChanged: onValueChanged,
      obscureText: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: new AlertDialog(
          titlePadding: new EdgeInsets.all(0.0),
          contentPadding:
              new EdgeInsets.only(top: padding, right: padding, left: padding),
          title: new Container(
            padding: new EdgeInsets.all(padding / 2),
            decoration: new BoxDecoration(color: widget.color),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(
                  Icons.lock_outline,
                  color: Singleton.textLightColor,
                  size: 30.0,
                )
              ],
            ),
          ),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[
                //todo header
                new TextView.alertTitle(
                  Singleton.setting.localization.change_password,
                ),
                new SizedBox(
                  height: 10.0,
                ),

                //todo message
                new TextView.alertText(
                    Singleton.setting.localization.you_need_change_password),

                //todo child
                new SizedBox(height: 10.0),
                buildTextForm(),

                //todo line
                new Container(
                  margin: new EdgeInsets.only(top: this.padding),
                  padding: new EdgeInsets.only(
                      right: this.padding, left: this.padding),
                  decoration: new BoxDecoration(color: widget.color),
                  child: new Row(
                    children: <Widget>[
                      new SizedBox(
                        height: 0.5,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            buildPositiveButton(context),
            buildNegativeButton(context),
          ],
        ),
        onWillPop: () {
          Navigator.of(context).pop();
        });
  }

  Widget buildTextForm() {
    return new Column(
      children: <TextFieldFocus>[tfOldPassword, tfNewPassword, tfNewPassword2],
    );
  }

  Widget buildNegativeButton(BuildContext context) {
    return new Align(
      child: new FlatButton(
        child: new TextView.text(Singleton.setting.localization.cancel),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      alignment: Alignment.bottomCenter,
    );
  }

  Widget buildPositiveButton(BuildContext context) {
    return new Align(
      child: new FlatButton(
        child: new TextView.text(
          Singleton.setting.localization.confirm,
          color: isDisable ? null : widget.color,
        ),
        onPressed: () {
          if(!isDisable){
            Navigator.of(context).pop();
            widget.onPositiveTap == null ? () {} : widget.onPositiveTap();
          }
        },
      ),
      alignment: Alignment.bottomCenter,
    );
  }

  void onValueChanged(String text) {
    setState(() {
      isDisable = tfOldPassword.isEmpty || tfNewPassword2.isEmpty || tfNewPassword.isEmpty;
    });
  }
}
