import 'package:student_portal/models/objects/Major.dart';

//todo Major: chuyên ngành đào tạo
class MainMajor {
  dynamic id;
  String code;
  String name;
  Major major;
  String nameEnglish;

  MainMajor.fromJson(Map json) {
    this.id = json['id'];
    this.code = json['ma'];
    this.name = json['ten'];
    this.major = new Major.fromJson(json['nganhId']);
    this.nameEnglish = json['tenTA'];
  }

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'ma': this.code,
        'ten': this.name,
        'nganhId': this.major.toJson(),
        'tenTA': this.nameEnglish,
      };
}
