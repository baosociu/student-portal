import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/MapUtils.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleMode.dart';
import 'package:student_portal/widgets/components/alert/CalendarWeekAlert.dart';
import 'package:student_portal/global/service/schedule/ScheduleRequest.dart';

class ScheduleWeekMode extends ScheduleMode {
  ScheduleWeekMode()
      : super(new ScheduleWeekRequest(),
      Singleton.setting.localization.weekly_schedule, Icons.date_range, Singleton.setting.primaryColor);

  @override
  String getTitle() {
    return StringUtils
        .getStringStartWeekOfWeek((request as ScheduleWeekRequest).date);
  }

  @override
  void onTapCenter(
      BuildContext context, onPositiveTap(DateTime valueSelected)) {
    showWeekDialog(context, onPositiveTap);
  }

  @override
  void onTapLeft() {
    (request as ScheduleWeekRequest).date =
        (request as ScheduleWeekRequest).date.subtract(new Duration(days: 7));
    onUpdateValue();
  }

  @override
  void onTapRight() {
    (request as ScheduleWeekRequest).date =
        (request as ScheduleWeekRequest).date.add(new Duration(days: 7));
    onUpdateValue();
  }

  Future<Null> showWeekDialog(BuildContext context,
      dynamic Function(DateTime valueSelected) onPositiveTap) async {
    return await showCalendarWeekPicker(
      context: context,
      firstYear: (request as ScheduleWeekRequest).date.year - 200,
      lastYear: (request as ScheduleWeekRequest).date.year + 200,
      initialDate: (request as ScheduleWeekRequest).date ?? new DateTime.now(),
      textTitle: Singleton.setting.localization.select_week,
      textMessage: Singleton.setting.localization.you_need_select_week,
      textPositiveButton: Singleton.setting.localization.select,
      textNegativeButton: Singleton.setting.localization.cancel,
      iconHeader: iconData,
      onPositiveTap: onPositiveTap,
    );
  }

  onUpdateValue() {
    Map<String, DateTime> map = MapUtils
        .getStartDateEndDateOfWeek((request as ScheduleWeekRequest).date);
    (request as ScheduleWeekRequest).startDate = map['start'];
    (request as ScheduleWeekRequest).endDate = map['end'];
  }
}
