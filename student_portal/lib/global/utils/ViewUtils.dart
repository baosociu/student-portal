import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/CardView.dart';
import 'package:student_portal/widgets/components/LineTextView.dart';
import 'package:student_portal/widgets/components/LoadingView.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class ViewUtils {
  //todo text
  static Widget buildItemTextView(String label, String value,
      {int leftFlex, int rightFlex, Widget rightWidget,FontWeight fontWeight= FontWeight.normal}) {
    return new LineTextView(
        margin: 0.0,
        isOverflow: true,
        label: label,
        leftFlex: leftFlex ?? 2,
        rightFlex: rightFlex ?? 5,
        rightWidget: rightWidget,
        fontWeight: fontWeight,
        value: value);
  }

  static Widget buildItemSmallTextView(String label, String value,
      {int leftFlex, int rightFlex}) {
    return new LineTextView(
        margin: 0.0,
        isOverflow: true,
        label: label,
        sizeFont: SizeFont.small,
        leftFlex: leftFlex ?? 2,
        rightFlex: rightFlex ?? 5,
        value: value);
  }

  static Widget buildItemNumberTextView(String label, String value, {FontWeight fontWeight= FontWeight.normal}) {
    return new LineTextView(
      margin: 0.0,
      isOverflow: true,
      rightFlex: 2,
      leftFlex: 1,
      label: label,
      value: value,
      isValueNumber: true,
      fontWeight: fontWeight
    );
  }

  //todo view
  static Widget buildNothingView(String text, dynamic Function() callback) {
    return new Container(
      decoration: new BoxDecoration(color: Singleton.backgroundNothing),
      child: new Center(
        child: new InkWell(
          child: new Column(
            children: <Widget>[
              new Flexible(
                child: new Container(
                  height: 42.0,
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                          image: new AssetImage("resources/images/emo.gif"),
                          fit: BoxFit.fitHeight)),
                ),
              ),
              new TextView.text(
                text,
                textAlign: TextAlign.center,
              ),
              new Flexible(
                child: new MaterialButton(
                  onPressed: null,
                  child:
                      new Icon(Icons.refresh, color: Singleton.textDarkColor),
                ),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          onTap: () {
            callback();
          },
        ),
      ),
    );
  }

  static Widget buildErrorView(String error, dynamic Function() callback) {
    return buildNothingView(error, callback);
  }

  static Widget buildNothingScheduleView(dynamic Function() callback) {
    return buildNothingView(
        Singleton.setting.localization.nothing_schedule, callback);
  }

  static Widget buildNothingDetailView(dynamic Function() callback) {
    return buildNothingView(
        Singleton.setting.localization.nothing_subject, callback);
  }

  static Widget buildNothingSubjectView(dynamic Function() callback) {
    return buildNothingView(
        Singleton.setting.localization.nothing_subject, callback);
  }

  static Widget buildNothingClassSubjectView(dynamic Function() callback) {
    return buildNothingView(
        Singleton.setting.localization.nothing_class_subject, callback);
  }

  static Widget buildNothingSemesterView(dynamic Function() callback) {
    return buildNothingView(
        Singleton.setting.localization.nothing_semester, callback);
  }

  static Widget buildRefreshView(LoadingType type) {
    return type == LoadingType.refresh
        ? new Container(
            padding: new EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                new Center(
                  child: new RefreshProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        Singleton.setting.primaryColor),
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.all(4.0),
                  child: new TextView.text(
                      Singleton.setting.localization.refresh + "..."),
                ),
              ],
            ),
          )
        : new SizedBox();
  }

  static Widget buildLoadMoreView(LoadingType type) {
    return type == LoadingType.loadMore
        ? new Container(
            padding: new EdgeInsets.all(8.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Center(
                  child: new Row(
                    children: <Widget>[
                      new Center(
                        child: new CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              Singleton.setting.primaryColor),
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.all(4.0),
                        child: new TextView.text(
                            Singleton.setting.localization.load_more + "..."),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        : new SizedBox();
  }

  //todo icon
  static Widget buildStartIcon() {
    var data = new Icon(
      Icons.notifications_active,
      color: Singleton.starColor,
    );
    var dataShadow = new Icon(
      Icons.notifications_active,
      color: Singleton.textDarkColor,
    );
    return new Container(
      margin: new EdgeInsets.only(right: 8.0),
      child: new ClipRect(
        child: new Stack(
          children: [
            new Positioned(
              top: 2.0,
              left: 2.0,
              child: dataShadow,
            ),
            new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
              child: data,
            ),
          ],
        ),
      ),
    );
  }

  static Widget buildCheckIcon(Color color) {
    var data = new Icon(
      Icons.check,
      color: color,
    );
    var dataShadow = new Icon(
      Icons.check,
      color: Singleton.textDarkColor,
    );
    return new Container(
      margin: new EdgeInsets.only(right: 8.0),
      child: new ClipRect(
        child: new Stack(
          children: [
            new Positioned(
              top: 1.0,
              left: 1.0,
              child: dataShadow,
            ),
            new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
              child: data,
            ),
          ],
        ),
      ),
    );
  }

  //todo card view
  static Widget buildLoadingCardView() {
    return new Flexible(
      child: new CardView(
        child: new LoadingView(),
        isShowTitle: false,
      ),
    );
  }

  static Widget buildErrorCardView(String error, dynamic Function() callback) {
    return new Flexible(
        child: new CardView(
      child: new Flexible(child: buildNothingView(error, callback)),
      isShowTitle: false,
    ));
  }

  static Widget buildNothingSubjectCardView(dynamic Function() callback) {
    return new Flexible(
        child: new CardView(
      child: new Flexible(child: buildNothingSubjectView(callback)),
      isShowTitle: false,
    ));
  }

  static Widget buildNothingClassSubjectCardView(dynamic Function() callback) {
    return new Flexible(
        child: new CardView(
          child: new Flexible(child: buildNothingClassSubjectView(callback)),
          isShowTitle: false,
        ));
  }

  static Widget buildNothingDetailCardView(dynamic Function() callback) {
    return new Flexible(
        child: new CardView(
      child: new Flexible(child: buildNothingDetailView(callback)),
      isShowTitle: false,
    ));
  }

  static Widget buildNothingSemesterCardView(dynamic Function() callback) {
    return new Flexible(
        child: new CardView(
      child: new Flexible(child: buildNothingSemesterView(callback)),
      isShowTitle: false,
    ));
  }

  static Widget buildCardView(
      {Widget child,
      String title}) {
    return new Flexible(
        child: new CardView(
            isShowTitle: title != null,
            header: title ?? "",
            child: new Flexible(
                child: new Column(
              children: <Widget>[
                new SizedBox(
                  height: 8.0,
                ),
                new Flexible(
                    child: child ?? SizedBox()),
                new SizedBox(
                  height: 8.0,
                ),
              ],
            ))));
  }
}
