import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/message/Item.dart';
import 'package:student_portal/models/message/ListItem.dart';
import 'package:student_portal/widgets/components/CardView.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: must_be_immutable
class NotificationFragment extends BaseFragment {
  NotificationFragment(mainPage)
      : super(mainPage, Singleton.NOTIFICATION_FRAGMENT_ID,
            Singleton.setting.localization.notification_fragment_title,"resources/icons/notification.png");
  NotificationFragmentState state;

  @override
  NotificationFragmentState createState() {
    super.createState();
    state = new NotificationFragmentState();
    return state;
  }

  @override
  onRefresh() {
    state.loadData();
  }
}

class NotificationFragmentState extends BaseFragmentState {
  ListItem items = new ListItem();
  bool isLoading;

  @override
  void initState() {
    super.initState();
    loadData();
    isLoading = true;

    new Timer(new Duration(seconds: 1), () {
      setState(() {
        this.isLoading = false;
      });
    });
  }


  loadData() {
    SharedPreferences.getInstance().then((prefs) {
      final key = "_notification";
      String json = prefs.getString(key);
      Map data = new Map();
      if (json != null) data = jsonDecode(json);
      ListItem items = new ListItem.fromJson(data);

      setState(() {
        this.items = items;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            this.isLoading
                ? ViewUtils.buildLoadingCardView()
                : new Flexible(
                    child: new CardView(
                      child: buildNotificationList(),
                      isShowTitle: false,
                    ),
                  )
          ],
        ),
      ),
    );
  }

  Widget buildNotificationList() {
    return new ListView.builder(
        shrinkWrap: true,
        itemCount: items.data.length,
        itemBuilder: (context, index) {
          Item it = items.data[index];
          DateTime dt = DateTime.parse(it.date);
          String strDt = dt.hour.toString() +
              ":" +
              dt.minute.toString() +
              " " +
              dt.day.toString() +
              "-" +
              dt.month.toString() +
              "-" +
              dt.year.toString();
          return new Column(
            children: <Widget>[
              new ListTile(
                onTap: () => openURL(it.link),
                leading: new CircleAvatar(
                  backgroundColor: Singleton.setting.primaryColor,
                  child: new TextView.leading((index + 1).toString()),
                ),
                title: new Flexible(
                    child: new TextView.title(
                  it.title,
                  overflow: TextOverflow.ellipsis,
                )),
                subtitle: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Flexible(
                        child: new TextView.text(
                      it.message,
                      overflow: TextOverflow.ellipsis,
                    )),
                    new TextView.text(strDt),
                  ],
                ),
              ),
            index < items.data.length-1 ? new Divider(color: Singleton.setting.primaryColor) : new SizedBox()
            ],
          );
        });
  }

  openURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
