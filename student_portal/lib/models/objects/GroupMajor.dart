import 'package:student_portal/models/objects/MainMajor.dart';

//todo  khối ngành
class GroupMajor {
  dynamic id;
  String code;
  String name;
  MainMajor mainMajor;
  int course;
  double weight;
  int countSemester;

  GroupMajor.fromJson(Map json) {
    this.id = json['id'];
    this.code = json['ma'];
    this.name = json['ten'];
    this.mainMajor = new MainMajor.fromJson(json['chuyenNganhId']);
    this.course = json['khoaHoc'];
    this.weight = json['soTinChi'];
    this.countSemester = json['soHocKy'];
  }

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'ma': this.code,
        'ten': this.name,
        'chuyenNganhId': this.mainMajor.toJson(),
        'khoaHoc': this.course,
        'soTinChi': this.weight,
        'soHocKy': this.countSemester,
      };
}
