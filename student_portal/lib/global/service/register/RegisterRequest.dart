import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/service/register/BaseRegister.dart';
import 'package:student_portal/global/service/register/SubjectRegister.dart';

class RegisterRequest {
  final LoadingType loadType;

  final BaseRegister semester;
  final TypeRegister type;
  final BaseRegister method;
  final GroupRegister course;

  final String keySearch;
  final List<dynamic> dataPrev;
  final List<int> idSubjectTemp;

  RegisterRequest(this.semester, this.type,  this.method, this.course,
      {LoadingType loadType,
      String keySearch,
      List<dynamic> dataPrev,
      List<int> idSubjectTemp})
      : this.dataPrev = dataPrev ?? new List<BaseSubject>(),
        this.loadType = loadType ?? LoadingType.loading,
        this.keySearch = keySearch ?? "",
        this.idSubjectTemp = idSubjectTemp ?? new List<int>();

  factory RegisterRequest.from(BaseRegister semester, BaseRegister type,
      BaseRegister method, GroupRegister group,
      {LoadingType loadType,
      String keySearch,
      List<dynamic> dataPrev,
      List<int> idSubjectTemp}) {
    return new RegisterRequest( semester, type, method, group,
        dataPrev: dataPrev,
        loadType: loadType,
        idSubjectTemp: idSubjectTemp,
        keySearch: keySearch);
  }
}