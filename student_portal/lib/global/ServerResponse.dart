import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/AlertUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';

class ServerResponse<T> {
  final String message;
  final ResultType type;
  final T data;

  ServerResponse(this.data, this.message, this.type) {
    print(message);
  }

  factory ServerResponse.fromData(
      dynamic data, String message, Function fromJson, Function _default) {
    //todo data: map, list map
    if (data == null || data.toString().isEmpty)
      return new ServerResponse(_default() as T,
          Singleton.setting.localization.error_empty, ResultType.resultEmpty);
    else
      return new ServerResponse(
          fromJson(data) as T, message, data.toString() == "[]" ? ResultType.resultEmpty : ResultType.availableItems);
  }

  factory ServerResponse.fromMessage(String message, Function _default) {
    return new ServerResponse(_default() as T, message, ResultType.hasError);
  }

  factory ServerResponse.loginAgain(String message, Function _default) {
    return new ServerResponse(_default() as T, message, ResultType.loginAgain);
  }

  //todo view by response
  Widget buildViewByResponse<ItemType>(
      {BuildContext context,
      List<ItemType> list,
      Function() callback,
      Function emptyView,
      Widget item(int index, ItemType item)}) {
    switch (type) {
      case ResultType.hasError:
        return ViewUtils.buildErrorCardView(message, callback);
      case ResultType.loginAgain:
        return ViewUtils.buildErrorCardView(
            message, () => AlertUtils.showAlertRestart(context));
      case ResultType.resultEmpty:
        return emptyView(callback);
      default:
        return new Flexible(child:
          new Column(
            children: <Widget>[
              new SizedBox(
                height: 8.0,
              ),
              new Flexible(
                child: new ListView.builder(
                    itemCount: list.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return item(index, list[index]);
                    }),
              ),
              new SizedBox(
                height: 8.0,
              ),
            ],
          ),);
    }
  }

  Widget buildCardViewByResponse<ItemType>(
      {BuildContext context,
      List<ItemType> list,
      Function() callback,
      Function emptyView,
      String title,
      Widget item(int index, ItemType item)}) {
    switch (type) {
      case ResultType.hasError:
        return ViewUtils.buildErrorCardView(message, callback);
      case ResultType.loginAgain:
        return ViewUtils.buildErrorCardView(
            message, () => AlertUtils.showAlertRestart(context));
      case ResultType.resultEmpty:
        return emptyView(callback);
      default:
        return ViewUtils.buildCardView(
          title: title,
          child: new ListView.builder(
              itemCount: list.length,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return item(index, list[index]);
              }),
        );
    }
  }
}
