import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/register/BaseRegister.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/components/alert/CustomDialog.dart';

// ignore: must_be_immutable
class OptionsRegisterAlert extends StatefulWidget {
  static final String INDEX_TYPE = "INDEX_TYPE";
  static final String INDEX_YEAR_SEMESTER = "INDEX_YEAR_SEMESTER";
  static final String INDEX_SEMESTER = "INDEX_SEMESTER";
  static final String INDEX_METHOD = "INDEX_METHOD";
  static final String INDEX_COURSE_GROUP = "INDEX_COURSE_GROUP";
  static final String INDEX_GROUP = "INDEX_GROUP";

  OptionsRegisterAlert(
      this.listType, this.listMethod, this.listSemester, this.listGroup,
      {Key key,
      Color color,
      this.iconHeader,
      this.textTitle: '',
      this.onPositiveTap,
      this.padding: 16.0,
      this.onWillPop})
      : this.color = color ?? Singleton.setting.primaryColor;

  final Color color;
  final IconData iconHeader;
  final String textTitle;
  final double padding;
  final Function(Map<String, int>) onPositiveTap;
  final Function onWillPop;

  final TypeRegisterList listType;
  final SemesterRegisterList listSemester;
  final BaseRegisterList listMethod;
  final GroupRegisterList listGroup;

  @override
  State<StatefulWidget> createState() {
    return new OptionsRegisterAlertState(onWillPop);
  }
}

class OptionsRegisterAlertState extends State<OptionsRegisterAlert> {
  static FlatButton positiveButton;
  static FlatButton negativeButton;
  static FlatButton backButton;
  static FlatButton defaultButton;

  final Function onWillPop;
  Map<String, OptionItem> items;
  double height = 0.0;
  int indexSelected;
  BuildContext _context;

  OptionsRegisterAlertState(this.onWillPop);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    indexSelected = -1;
    height = 260.0;

    items = new Map();
    items[OptionsRegisterAlert.INDEX_TYPE] = new OptionItem(
        OptionsRegisterAlert.INDEX_TYPE,
        new BaseRegisterList.byIndex(
            widget.listType.list, widget.listType.index),
        Singleton.setting.localization.type_register);
    items[OptionsRegisterAlert.INDEX_METHOD] = new OptionItem(
        OptionsRegisterAlert.INDEX_METHOD,
        new BaseRegisterList.byIndex(
            widget.listMethod.list, widget.listMethod.index),
        Singleton.setting.localization.level);
    items[OptionsRegisterAlert.INDEX_YEAR_SEMESTER] = new OptionItem(
        OptionsRegisterAlert.INDEX_YEAR_SEMESTER,
        new BaseRegisterList.byIndex(
            widget.listSemester.list, widget.listSemester.index),
        Singleton.setting.localization.year);
    items[OptionsRegisterAlert.INDEX_SEMESTER] = new OptionItem(
        OptionsRegisterAlert.INDEX_SEMESTER,
        new BaseRegisterList.byIndex(widget.listSemester.semesterSelected.items,
            widget.listSemester.semesterSelected.index),
        Singleton.setting.localization.semester);
    items[OptionsRegisterAlert.INDEX_COURSE_GROUP] = new OptionItem(
        OptionsRegisterAlert.INDEX_COURSE_GROUP,
        new BaseRegisterList.byIndex(
            widget.listGroup.list, widget.listGroup.index),
        Singleton.setting.localization.course);
    items[OptionsRegisterAlert.INDEX_GROUP] = new OptionItem(
        OptionsRegisterAlert.INDEX_GROUP,
        new BaseRegisterList.byIndex(widget.listGroup.groupSelected.items,
            widget.listGroup.groupSelected.index),
        Singleton.setting.localization.major_group);

    positiveButton = new FlatButton(
        child: new TextView.text(
          Singleton.setting.localization.confirm,
          color: widget.color,
        ),
        onPressed: onPressPositive);

    negativeButton = new FlatButton(
      child: new TextView.text(Singleton.setting.localization.cancel),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    backButton = new FlatButton(
      child: new TextView.text(Singleton.setting.localization.back),
      onPressed: () {
        setState(() {
          this.indexSelected = -1;
        });
      },
    );

    defaultButton = new FlatButton(
        onPressed: () {
          setState(() {
            int idCourseGroup = Singleton.student.groupMajor.course;
            int idGroup = Singleton.student.groupMajor.id;

            items[OptionsRegisterAlert.INDEX_TYPE].childs.index = 0;

            items[OptionsRegisterAlert.INDEX_YEAR_SEMESTER].childs.index = 0;
            items[OptionsRegisterAlert.INDEX_SEMESTER].childs =
                new BaseRegisterList(
                    (items[OptionsRegisterAlert.INDEX_YEAR_SEMESTER].selected
                            as SemesterRegister)
                        .items);

            items[OptionsRegisterAlert.INDEX_COURSE_GROUP].childs.index =
                BaseRegisterList.indexById(
                    items[OptionsRegisterAlert.INDEX_COURSE_GROUP].childs.list,
                    idCourseGroup);
            items[OptionsRegisterAlert.INDEX_GROUP].childs =
                new BaseRegisterList(
                    (items[OptionsRegisterAlert.INDEX_COURSE_GROUP].selected
                            as GroupRegister)
                        .items);
            items[OptionsRegisterAlert.INDEX_GROUP].childs.index =
                BaseRegisterList.indexById(
                    items[OptionsRegisterAlert.INDEX_GROUP].childs.list,
                    idGroup);
          });
          onPressPositive();
        },
        child: new TextView.text(
          Singleton.setting.localization.default_,
          color: widget.color,
        ));
  }

  void onPressPositive() {
    Navigator.of(context).pop();

    Map<String, int> map = new Map();
    map[OptionsRegisterAlert.INDEX_TYPE] =
        items[OptionsRegisterAlert.INDEX_TYPE].childs.index;
    map[OptionsRegisterAlert.INDEX_METHOD] =
        items[OptionsRegisterAlert.INDEX_METHOD].childs.index;
    map[OptionsRegisterAlert.INDEX_YEAR_SEMESTER] =
        items[OptionsRegisterAlert.INDEX_YEAR_SEMESTER].childs.index;
    map[OptionsRegisterAlert.INDEX_SEMESTER] =
        items[OptionsRegisterAlert.INDEX_SEMESTER].childs.index;
    map[OptionsRegisterAlert.INDEX_COURSE_GROUP] =
        items[OptionsRegisterAlert.INDEX_COURSE_GROUP].childs.index;
    map[OptionsRegisterAlert.INDEX_GROUP] =
        items[OptionsRegisterAlert.INDEX_GROUP].childs.index;

    widget.onPositiveTap(map);
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: new CustomAlertDialog(
          alignmentActions: MainAxisAlignment.spaceBetween,
          titlePadding: new EdgeInsets.all(0.0),
          contentPadding: new EdgeInsets.only(
              top: widget.padding, right: widget.padding, left: widget.padding),
          title: new Column(children: <Widget>[
            new Container(
              padding: new EdgeInsets.all(widget.padding / 2),
              decoration: new BoxDecoration(color: widget.color),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    widget.iconHeader,
                    color: Singleton.textLightColor,
                    size: 30.0,
                  )
                ],
              ),
            ),
            new Container(
              child: new Column(
                children: <Widget>[
                  new TextView.alertTitle(widget.textTitle),
                  new Padding(
                      padding: new EdgeInsets.only(
                          right: widget.padding, left: widget.padding),
                      child: new Divider(
                        color: Singleton.setting.primaryColor,
                      )),
                ],
              ),
              margin: new EdgeInsets.only(top: widget.padding),
            ),
          ]),
          content: new Container(
              height: height,
              child: new Scaffold(
                backgroundColor: Singleton.backgroundNothing,
                body: new Builder(builder: (context) {
                  this._context = context;
                  return buildDialog();
                }),
              )),
          actions: indexSelected == -1
              ? <Widget>[
                  defaultButton,
                  new Row(
                    children: <Widget>[negativeButton, positiveButton],
                  )
                ]
              : <Widget>[backButton],
        ),
        onWillPop: () {
          if (indexSelected == -1)
            Navigator.of(context).pop();
          else
            setState(() {
              indexSelected = -1;
            });
          this.onWillPop();
        });
  }

  Widget buildDialog() {
    return new SingleChildScrollView(
      child: new ListBody(
        children: <Widget>[
          indexSelected == -1
              ? buildMainContent(items)
              : buildChildContent(items.values.toList()[indexSelected]),
          new SizedBox(height: widget.padding),
          new Divider(
            color: Singleton.setting.primaryColor,
          )
        ],
      ),
    );
  }

  Widget buildChildContent(OptionItem option) {
    return new Container(
        child: new Column(
      children: <Widget>[
        new Padding(
          padding: new EdgeInsets.only(bottom: 6.0, top: 6.0),
          child: new Align(
              alignment: Alignment.centerLeft,
              child: new TextView.text(
                option.title + ":",
                fontWeight: FontWeight.bold,
              )),
        ),
        buildExpandedItem(option.key, option.childs)
      ],
    ));
  }

  Widget buildMainContent(Map<String, OptionItem> items) {
    return new Container(
      child: new ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: items.length,
          itemBuilder: (context, index) {
            OptionItem _item = items.values.toList()[index];
            TypeRegister type = (items[OptionsRegisterAlert.INDEX_TYPE].selected
                    as TypeRegister);

            bool isDisable = false;
            if(_item.isYearOfSemesterOption)
              isDisable = type.isReplaceRegister;
            else if(_item.isSemesterOption)
              isDisable = type.isReplaceRegister;
            else if(_item.isCourseGroupOption)
              isDisable = type.isAgainRegister || type.isImproveRegister;
            else if(_item.isGroupOption)
              isDisable = type.isAgainRegister || type.isImproveRegister;

            return new InkWell(
              child: new Opacity(
                opacity: isDisable ? 0.5 : 1.0,
                child: new Container(
                  child: new Column(
                    children: <Widget>[
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new TextView.text(_item.title.toString()),
                          new SizedBox(
                            width: 12.0,
                          ),
                          new Flexible(
                              child: new TextView.text(
                            _item.selected.name.toString(),
                            overflow: TextOverflow.ellipsis,
                          )),
                        ],
                      ),
                      index < items.length - 1 ? new Divider() : new SizedBox()
                    ],
                  ),
                ),
              ),
              onTap: () {
                if (!isDisable)
                  setState(() {
                    indexSelected = index;
                  });
                else
                  ToastUtils.showToast(
                      _context,
                      Singleton
                          .setting.localization.option_register_not_available);
              },
            );
          }),
    );
  }

  Widget buildExpandedItem(String key, BaseRegisterList list) {
    List<BaseRegister> items = list.list;
    int indexSelected = list.index;
    return new ListView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: items.length,
        itemBuilder: (context, index) {
          BaseRegister _register = items[index];

          return new InkWell(
            child: new Container(
              child: new Column(
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      new Container(
                        margin: new EdgeInsets.only(right: 6.0),
                        child: new Icon(
                          index == indexSelected
                              ? Icons.radio_button_checked
                              : Icons.radio_button_unchecked,
                          color: index == indexSelected
                              ? Singleton.setting.primaryColor
                              : Singleton.disableColor,
                        ),
                      ),
                      new Flexible(
                          child: new TextView.text(
                        _register.name.toString(),
                        overflow: TextOverflow.ellipsis,
                      ))
                    ],
                  ),
                  index < items.length - 1 ? new Divider() : new SizedBox()
                ],
              ),
            ),
            onTap: () => setState(() {
                  this.items[key].childs.index = index;
                  if (key == OptionsRegisterAlert.INDEX_YEAR_SEMESTER) {
                    this.items[OptionsRegisterAlert.INDEX_SEMESTER].childs =
                        new BaseRegisterList(
                            widget.listSemester.list[index].items);
                  }
                  if (key == OptionsRegisterAlert.INDEX_COURSE_GROUP) {
                    this.items[OptionsRegisterAlert.INDEX_GROUP].childs =
                        new BaseRegisterList(
                            widget.listGroup.list[index].items);
                  }
                  this.indexSelected = -1;
                }),
          );
        });
  }
}

class OptionItem {
  final String key;
  final String title;
  BaseRegisterList childs;

  OptionItem(this.key, this.childs, this.title);

  BaseRegister get selected => childs.selected;

  bool get isTypeOption => key == OptionsRegisterAlert.INDEX_TYPE;
  bool get isYearOfSemesterOption => key == OptionsRegisterAlert.INDEX_YEAR_SEMESTER;
  bool get isSemesterOption => key == OptionsRegisterAlert.INDEX_SEMESTER;
  bool get isMethodOption => key == OptionsRegisterAlert.INDEX_METHOD;
  bool get isCourseGroupOption => key == OptionsRegisterAlert.INDEX_COURSE_GROUP;
  bool get isGroupOption => key == OptionsRegisterAlert.INDEX_GROUP;
}
