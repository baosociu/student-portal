import 'package:student_portal/models/objects/TrainingForm.dart';

//todo TrainingSystem: Hệ đào tạo
class TrainingSystem {
  dynamic id;
  String code;
  String name;
  String nameEnglish;
  TrainingForm form;

  TrainingSystem.fromJson(Map json) {
    this.id = json['id'];
    this.code = json['ma'];
    this.name = json['ten'];
    this.nameEnglish = json['tenTA'];
    this.form = new TrainingForm.fromJson(json['hinhThucDaoTaoId']);
  }

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'ma': this.code,
        'ten': this.name,
        'tenTA': this.nameEnglish,
        'hinhThucDaoTaoId': this.form.toJson(),
      };
}
