import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/models/objects/Schedule.dart';
import 'package:student_portal/models/objects/Subject.dart';
import 'package:student_portal/models/objects/SummarySchedule.dart';

class ScheduleResult {
  final ResultType type;
  final dynamic result;
  final String message;

  ScheduleResult(this.type, this.result, {String message})
      : this.message = message ?? "";

  factory ScheduleResult.init() {
    return new ScheduleResult(ResultType.resultEmpty, null);
  }

  factory ScheduleResult.hasError(String message) {
    return new ScheduleResult(ResultType.hasError, null, message: message);
  }

  factory ScheduleResult.loginAgain(String message) {
    return new ScheduleResult(ResultType.loginAgain, null, message: message);
  }

  bool get isAvailable => type == ResultType.availableItems;

  bool get isEmpty => type == ResultType.resultEmpty;

  factory ScheduleResult.fromJsonSemester(dynamic json) {
    SummaryScheduleList list = SummaryScheduleList.fromJson(json);
    return new ScheduleResult(
        list.list.length == 0
            ? ResultType.resultEmpty
            : ResultType.availableItems,
        list);
  }

  factory ScheduleResult.fromJsonDate(dynamic json) {
    BaseScheduleList list = BaseScheduleList.fromJson(json);
    return new ScheduleResult(
        list.list.length == 0
            ? ResultType.resultEmpty
            : ResultType.availableItems,
        list);
  }

  factory ScheduleResult.fromJsonWeek(dynamic json) {
    BaseScheduleList list = BaseScheduleList.fromJson(json);
    return new ScheduleResult(
        list.list.length == 0
            ? ResultType.resultEmpty
            : ResultType.availableItems,
        list);
  }

  factory ScheduleResult.fromJsonSubject(dynamic json) {
    SubjectScheduleList list = SubjectScheduleList.fromJson(json);
    return new ScheduleResult(
        list.list.length == 0
            ? ResultType.resultEmpty
            : ResultType.availableItems,
        list);
  }
}
