
class CourseCategory{
  dynamic idBlockMajor;
  int countTime;
  String nameBlockMajor;
  String nameSemester;
  double weightPractice;
  dynamic idMainMajor;
  int countSemester;
  int indexCourse; //khóa học
  String typeSubject;
  double weight;
  String codeSubject;
  String nameSubject;
  dynamic idSubject;
  double weightTheory;

  CourseCategory.from(Map json){
    this.idBlockMajor = json['khoiNganhId'];
    this.countTime = json['soTietHoc'];
    this.nameBlockMajor = json['tenKhoiNganh'];
    this.nameSemester = json['hocKy'];
    this.weightPractice = json['soTinChiTH'];
    this.idMainMajor = json['chuyenNganhId'];
    this.countSemester = json['soHocKy'];
    this.indexCourse = json['khoaHoc'];
    this.typeSubject = json['loaiMH'];
    this.weight = json['soTinChiMH'];
    this.codeSubject = json['maMonHoc'];
    this.nameSubject = json['tenMonHoc'];
    this.idSubject = json['monHocId'];
    this.weightTheory = json['soTinChiLT'];
  }
}

class CourseCategoryList{
  List<CourseCategory> list = new List<CourseCategory>();

  CourseCategoryList.from(dynamic data){
    list = new List<CourseCategory>();
    data.forEach((json){
      CourseCategory value = new CourseCategory.from(json);
      list.add(value);
    });
  }
}

class CourseCategoryGroupSemesterItem{
  final dynamic idBlockMajor;
  final int countTime;
  final String nameBlockMajor;
  final  double weightPractice;
  final dynamic idMainMajor;
  final int indexCourse; //khóa học
  final  String typeSubject;
  final double weight;
  final String codeSubject;
  final String nameSubject;
  final dynamic idSubject;
  final double weightTheory;
  final int countSemester;

  CourseCategoryGroupSemesterItem(this.idBlockMajor, this.countTime, this.nameBlockMajor, this.weightPractice, this.idMainMajor, this.indexCourse, this.typeSubject, this.weight, this.codeSubject, this.nameSubject, this.idSubject, this.weightTheory, this.countSemester);
}

class CourseCategoryGroupSemester{
  final String nameSemester;
  List<CourseCategoryGroupSemesterItem> list = new List<CourseCategoryGroupSemesterItem>();

  CourseCategoryGroupSemester(this.nameSemester);
}

class CourseCategoryGroupSemesterList{
  List<CourseCategoryGroupSemester> list = new List<CourseCategoryGroupSemester>();

  CourseCategoryGroupSemesterList.fromJson(dynamic data){
    CourseCategoryList courseCategoryList = new CourseCategoryList.from(data);

    courseCategoryList.list.forEach((courseCategory){
      int i = 0;
      for(i = 0; i < list.length ; i++)
        if(list[i].nameSemester == courseCategory.nameSemester)
          break;

      if(i == list.length)
        list.add(new CourseCategoryGroupSemester(courseCategory.nameSemester));
      list[i].list.add(new CourseCategoryGroupSemesterItem(courseCategory.idBlockMajor, courseCategory.countTime, courseCategory.nameBlockMajor, courseCategory.weightPractice, courseCategory.idMainMajor, courseCategory.indexCourse, courseCategory.typeSubject, courseCategory.weight, courseCategory.codeSubject, courseCategory.nameSubject, courseCategory.idSubject, courseCategory.weightTheory, courseCategory.countSemester));
    });
  }
}