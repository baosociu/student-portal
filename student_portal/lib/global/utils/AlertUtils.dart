import 'dart:async';

import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/SharedPrefrencesUtils.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Register.dart';
import 'package:student_portal/models/objects/Result.dart';
import 'package:student_portal/widgets/StudentPortalWidget.dart';
import 'package:student_portal/widgets/components/TableView.dart';
import 'package:student_portal/widgets/components/alert/PrimaryAlert.dart';
import 'package:student_portal/widgets/fragments/register/view/RegisterViewUtils.dart';

class AlertUtils {
  static Future<Null> showAlertLogout(BuildContext context) async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new PrimaryAlert(
          textTitle: Singleton.setting.localization.logout,
          textMessage: Singleton.setting.localization.do_you_want_log_out,
          textPositiveButton: Singleton.setting.localization.yes,
          textNegativeButton: Singleton.setting.localization.cancel,
          color: Singleton.setting.primaryColor,
          onWillPop: () {},
          onNegativeTap: () {},
          iconHeader: Icons.exit_to_app,
          onPositiveTap: () async {
            await SharedPreferencesUtils.clearUsernamePassword();
            Navigator.of(context).pop(); //out
            Singleton.token = null;
            Singleton.student = null;
          },
        ));
  }

  static Future<Null> showAlertRestart(BuildContext context) async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new PrimaryAlert(
          textTitle: Singleton.setting.localization.restart,
          textMessage: Singleton.setting.localization.do_you_want_restart,
          textPositiveButton: Singleton.setting.localization.yes,
          textNegativeButton: Singleton.setting.localization.cancel,
          color: Singleton.setting.primaryColor,
          onWillPop: () {},
          onNegativeTap: () {},
          iconHeader: Icons.settings_backup_restore,
          onPositiveTap: () {
            StudentPortalWidget.restartApp(context);
          },
        ));
  }

  static Future<Null> showRegisterItemsAlert(
      BuildContext context, RegisterSubjectList items,
      {Function(RegisterSubjectList items) funcPositive,
      Function funcNegative}) {
    List<Widget> widgets = new List<Widget>();
    for (int i = 0; i < items.list.length; i++) {
      RegisterSubject child = items.list[i];
      widgets.add(
        new Row(children: <Widget>[buildInfoDetailRegister(child)]),
      );

      if (i < items.list.length - 1)
        widgets.add(new Padding(
            padding: new EdgeInsets.only(left: 8.0, right: 8.0),
            child: new Divider(color: Singleton.setting.primaryColor)));
    }

    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new PrimaryAlert(
          textTitle: Singleton.setting.localization.register_n_course,
          textMessage:
              Singleton.setting.localization.do_you_want_n_register_course,
          textPositiveButton: Singleton.setting.localization.yes,
          textNegativeButton: Singleton.setting.localization.no,
          color: Singleton.setting.primaryColor,
          iconHeader: Icons.gavel,
          onWillPop: () => funcNegative(),
          child: new Column(children: widgets),
          onPositiveTap: () =>
              funcPositive == null ? () {} : funcPositive(items),
          onNegativeTap: () => funcNegative == null ? () {} : funcNegative(),
        ));
  }

  static Future<Null> showDeleteResultItemAlert(
      BuildContext context, ResultSemester item,
      {Function funcPositive, Function funcNegative}) {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new PrimaryAlert(
          textTitle: Singleton.setting.localization.delete_course,
          textMessage: Singleton.setting.localization.do_you_want_delete_course,
          textPositiveButton: Singleton.setting.localization.yes,
          textNegativeButton: Singleton.setting.localization.no,
          child: new Row(
              children: <Widget>[RegisterViewUtils.buildInfoResult(item)]),
          color: Singleton.setting.primaryColor,
          iconHeader: Icons.clear,
          onWillPop: () => funcNegative(),
          onPositiveTap: () =>
              funcPositive == null ? () {} : funcPositive(item),
          onNegativeTap: () => funcNegative == null ? () {} : funcNegative(),
        ));
  }

  static Future<Null> showDeleteResultItemsAlert(
      BuildContext context, ResultSemester item1, ResultSemester item2,
      {Function funcPositive, Function funcNegative}) {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        child: new PrimaryAlert(
          textTitle: Singleton.setting.localization.delete_n_course,
          textMessage:
              Singleton.setting.localization.do_you_want_delete_n_course,
          textPositiveButton: Singleton.setting.localization.yes,
          textNegativeButton: Singleton.setting.localization.no,
          child: new Column(children: <Widget>[
            new Row(
                children: <Widget>[RegisterViewUtils.buildInfoResult(item1)]),
            new Padding(
                padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                child: new Divider(color: Singleton.setting.primaryColor)),
            new Row(
                children: <Widget>[RegisterViewUtils.buildInfoResult(item2)]),
          ]),
          color: Singleton.setting.primaryColor,
          iconHeader: Icons.clear,
          onWillPop: () => funcNegative(),
          onPositiveTap: () =>
              funcPositive == null ? () {} : funcPositive(item1, item2),
          onNegativeTap: () => funcNegative == null ? () {} : funcNegative(),
        ));
  }

  static Widget buildInfoDetailRegister(RegisterSubject courseRegister) {
    RegisterSubjectItem selected = courseRegister.selected;
    return new TableView(
      <Widget>[
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.name_subject + ": ",
            courseRegister.nameSubject.toString()),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.code_subject + ": ",
            courseRegister.codeSubject.toString()),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.teacher + ": ",
            selected.nameTeacher.toString()),
        buildScheduleInformation(selected.schedules),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.room + ": ",
            selected.room.toString()),
        ViewUtils.buildItemSmallTextView(
            Singleton.setting.localization.time + ": ",
            selected.dateStart.toString() +
                " - " +
                selected.dateEnd.toString()),
      ],
      margin: 0.0,
    );
  }

  static Column buildScheduleInformation(List<RegisterSchedule> schedules) {
    List<Widget> widgets = new List<Widget>();
    for (int i = 0; i < schedules.length; i++) {
      RegisterSchedule item = schedules[i];
      widgets.add(ViewUtils.buildItemSmallTextView(
          i == 0 ? Singleton.setting.localization.schedule : "",
          StringUtils.getLabelOfWeekByInt(item.idDate) +
              " - " +
              Singleton.setting.localization.period +
              ": " +
              item.time.toString()));
    }
    return new Column(children: widgets);
  }
}
