import 'dart:async';
import 'dart:io';

class FileUtils {
  static Future<File> getFileFromPath(String path) async {
    if(path == null)
      return null;
    File f = File(path);
    final exist =  await f.exists();
    return exist ? f : null;
  }

  static String getPathFromFile(File file) {
    print("File path: "+file.path);
    return file.path;
  }
}
