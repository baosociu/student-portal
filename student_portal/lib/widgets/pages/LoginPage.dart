import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/FadeRoute.dart';
import 'package:student_portal/global/utils/SharedPrefrencesUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/models/message/Item.dart';
import 'package:student_portal/models/message/ListItem.dart';
import 'package:student_portal/models/objects/Student.dart';
import 'package:student_portal/models/utils/Setting.dart';
import 'package:student_portal/widgets/components/RoundButton.dart';
import 'package:student_portal/widgets/components/RoundTextField.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';
import 'package:student_portal/widgets/fragments/notification/NotificationFragment.dart';
import 'package:student_portal/widgets/pages/LoadingPage.dart';

final Map<String, Item> _items = <String, Item>{};

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  LoginPageState createState() => new LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  RoundTextField textFieldUsername;
  RoundTextField textFieldPassword;
  RoundButton btnLogin;
  BuildContext _context;
  String background = Singleton.setting.background;
  String hintUsername = Singleton.setting.localization.user_name;
  String hintPassword = Singleton.setting.localization.password;

  void getSettings() async {
    final Setting setting = await SharedPreferencesUtils.getSettings();
    if (setting != null) Singleton.setting = setting;
    setState(() {
      this.background = Singleton.setting.background;
      this.hintUsername = Singleton.setting.localization.user_name;
      this.hintPassword = Singleton.setting.localization.password;
    });
  }

  //todo firebase
  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  @override
  void initState() {
    super.initState();
    getSettings();

    btnLogin = new RoundButton(Singleton.setting.localization.login,
        color: Singleton.textDarkColor,
        colorText: Singleton.textLightColor,
        onTap: onTapLoginButton);

    textFieldUsername = new RoundTextField(
      labelText: this.hintUsername,
      prefixIcon: Icons.person,
      textColor: Singleton.textLightColor,
      color: Singleton.textDarkColor,
      controller: new TextEditingController(),
      opacity: 0.7,
    );

    textFieldPassword = new RoundTextField(
      labelText: this.hintPassword,
      prefixIcon: Icons.vpn_key,
      textColor: Singleton.textLightColor,
      color: Singleton.textDarkColor,
      opacity: 0.7,
      obscureText: true,
      controller: new TextEditingController(),
    );

    textFieldUsername.controller.text = "1401020043";
    textFieldPassword.controller.text = "123456";

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print("onMessage: $message");
        _showItemDialog(message);
      },
      onLaunch: (Map<String, dynamic> message) {
        print("onLaunch: $message");
        _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) {
        print("onResume: $message");
        _navigateToItemDetail(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      print(token.toString());
    });

    SharedPreferencesUtils.getUsernamePassword((username, password) {
      if (username.isNotEmpty && password.isNotEmpty) login(username, password);
    });
  }

  Item _itemForMessage(String itemId, Map<String, dynamic> message) {
    final Item item = _items.putIfAbsent(itemId, () => new Item(itemId))
      ..message = message['message']
      ..date = itemId
      ..link = message['link']
      ..title = message['title'];
    SharedPreferences.getInstance().then((prefs) {
      final key = "_notification";

      String json = prefs.getString(key);

      Map data = new Map();
      if (json != null) data = jsonDecode(json);

      ListItem listItem = new ListItem.fromJson(data);
      listItem.data.insert(0, item);

      Map _data = listItem.toJson();
      String _json = jsonEncode(_data);
      prefs.setString(key, _json);
    });
    return item;
  }

  void _showItemDialog(Map<String, dynamic> message) {
    final Item item =
        _itemForMessage(new DateTime.now().toIso8601String(), message);
    showAlertMessage(item);
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item =
        _itemForMessage(new DateTime.now().toIso8601String(), message);
    new Timer(new Duration(milliseconds: 200), () {
      showAlertMessage(item);
    });
  }

  Future<Null> showAlertMessage(Item item) async {
    BaseFragment fragment = Singleton.currentFragment;
    if (fragment != null) {
      if (fragment is NotificationFragment && fragment.state.mounted)
        fragment.onRefresh();
      else {
        try {
          Scaffold.of(fragment.context).showSnackBar(new SnackBar(
                content: new InkWell(
                  child: new TextView.notification(
                      Singleton.setting.localization.you_have_new_notification +
                          ": " +
                          item.title),
                  onTap: () {
                    fragment.mainPage.state.switchFragments(
                        new NotificationFragment(fragment.mainPage));
                  },
                ),
                duration: new Duration(seconds: 2),
              ));
        } catch (e) {
          print(e.toString());
        }
      }
    }
  }

  //todo: build background Box decoration
  BoxDecoration buildBackgroundDecoration() {
    return new BoxDecoration(
        image: new DecorationImage(
            colorFilter: new ColorFilter.mode(
                Singleton.textDarkColor.withOpacity(1.0), BlendMode.dstATop),
            image: new Image.asset(this.background).image,
            fit: BoxFit.cover));
  }

  //todo: onTap login button
  void onTapLoginButton() {
    String username = textFieldUsername.controller.text;
    String password = textFieldPassword.controller.text;
    if (username.isEmpty || password.isEmpty)
      ToastUtils.showToast(_context,
          Singleton.setting.localization.user_name_and_password_not_notthing);
    else
      login(username, password);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        body: new Builder(builder: (BuildContext context) {
          _context = context;
          return new Stack(
            children: <Widget>[
              buildLoginForm(),
              new Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new SizedBox(),
                  new Container(
                    child: new Align(
                        alignment: Alignment.center,
                        child: new CircleAvatar(
                          radius: 50.0,
                          backgroundImage:
                              new AssetImage('resources/images/logo-white.png'),
                          backgroundColor: Colors.transparent,
                        )),
                  ),
                  new SizedBox(),
                  new SizedBox()
                ],
              ),
            ],
          );
        }));
  }

  Widget buildLoginForm() {
    return new Center(
      child: new Container(
          decoration: buildBackgroundDecoration(),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new Center(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    textFieldUsername,
                    textFieldPassword,
                    new SizedBox(
                      height: 30.0,
                    ),
                    btnLogin,
                  ],
                ),
              ),
            ],
          )),
    );
  }

  void getInfoStudent(String token, String username, String password) async {
    final Response response = await API.getInfoStudent(token);
    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");

    if (response.statusCode == HttpStatus.OK)
      try {
        Map map = jsonDecode(response.body);
        Map data = map["userAuthentication"]["principal"]["hocVienId"];

        Singleton.student = new Student.fromJson(data);

        if (Singleton.student != null) {
          setState(() {
            textFieldUsername.controller.text = "";
            textFieldPassword.controller.text = "";
          });

          //todo save inform login
          await SharedPreferencesUtils.setUsernamePassword(username, password);
          btnLogin.state.setLoading(false);
          Navigator.push(
            context,
            new FadeRoute(builder: (context) => new LoadingPage()),
          );
        }
      } catch (e) {
        btnLogin.state.setLoading(false);
        ToastUtils.showToast(
            _context, Singleton.setting.localization.inform_account_not_exist);
      }
    else {
      btnLogin.state.setLoading(false);
      ToastUtils.showToast(
          _context,
          Singleton.setting.localization.error +
              ": " +
              response.statusCode.toString());
    }
  }

  void login(String username, String password) async {
    btnLogin.state.setLoading(true);

    try {
      final Response response = await API.login(username, password);
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.body}");

      String token = jsonDecode(response.body)['access_token'];
      print("token: $token");

      if (token != null)
        getInfoStudent(token, username, password);
      else {
        btnLogin.state.setLoading(false);
        ToastUtils.showToast(
            _context, Singleton.setting.localization.inform_login_wrong);
      }
    } catch (e) {
      btnLogin.state.setLoading(false);
      ToastUtils.showToast(
          _context, Singleton.setting.localization.error_in_process);
    }
  }
}
