import 'dart:async';

class Item {
  Item(String itemId) {
    this.itemId = itemId;
    this._controller = new StreamController<Item>.broadcast();
    this._controller.add(this);
  }
  String itemId;
  // ignore: close_sinks
  StreamController<Item> _controller;

  Stream<Item> get onChanged => _controller.stream;

  //todo _title
  String title;

  //todo _message
  String message;

  //todo _date
  String date;

  //todo _link
  String link;

  Item.fromJson(Map json){
    this._controller = new StreamController<Item>.broadcast();
    this._controller.add(this);

    this.itemId = json['itemId'];
    this.title = json['title'];
    this.message = json['message'];
    this.date = json['date'];
    this.link = json['link'];

  }

  Map toJson(){
    return {
      'itemId': this.itemId,
      'title': this.title,
      'message': this.message,
      'date': this.date,
      'link': this.link
    };
  }

//  static final Map<String, Route<Null>> routes = <String, Route<Null>>{};
//  Route<Null> get route {
//    final String routeName = '/detail/$itemId';
//    return routes.putIfAbsent(
//      routeName,
//          () => new FadeRoute<Null>(
//        settings: new RouteSettings(name: routeName),
//        builder: (BuildContext context) => new PushMessagingExample(),
//      ),
//    );
//  }
}