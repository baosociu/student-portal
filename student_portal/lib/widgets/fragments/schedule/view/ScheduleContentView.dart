import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/schedule/ScheduleState.dart';
import 'package:student_portal/global/utils/AlertUtils.dart';
import 'package:student_portal/global/utils/MapUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Schedule.dart';
import 'package:student_portal/models/objects/SummarySchedule.dart';
import 'package:student_portal/widgets/components/CardView.dart';
import 'package:student_portal/widgets/components/LoadingView.dart';
import 'package:student_portal/widgets/fragments/schedule/mode/ScheduleMode.dart';
import 'package:student_portal/global/service/schedule/ScheduleRequest.dart';
import 'package:student_portal/widgets/fragments/schedule/view/ScheduleByDateListView.dart';
import 'package:student_portal/widgets/fragments/schedule/view/ScheduleBySubjectListView.dart';
import 'package:student_portal/widgets/fragments/schedule/view/ScheduleByWeekListView.dart';
import 'package:student_portal/widgets/fragments/schedule/view/SummaryScheduleListView.dart';

// ignore: must_be_immutable
class ScheduleContentView extends StatefulWidget {
  Stream<ScheduleState> state;
  ValueChanged<ScheduleRequest> execute;
  ScheduleMode scheduleMode;
  ScheduleContentViewState myState;

  ScheduleContentView({
    Key key,
    @required this.state,
    @required this.execute,
    @required this.scheduleMode,
  }) : super(key: key);

  @override
  ScheduleContentViewState createState() {
    myState = new ScheduleContentViewState(
        this.state, this.execute, this.scheduleMode);
    return myState;
  }
}

class ScheduleContentViewState extends State<ScheduleContentView> {
  Stream<ScheduleState> state;
  ValueChanged<ScheduleRequest> execute;
  ScheduleMode scheduleMode;


  dynamic data; //todo temp save results

  ScheduleContentViewState(this.state, this.execute, this.scheduleMode);

  @override
  void initState() {
    super.initState();
    execute(this.scheduleMode.request..isRefresh = false);
  }

  @override
  Widget build(BuildContext context) {
    return new StreamBuilder<ScheduleState>(
        stream: state,
        initialData: new ScheduleState.init(),
        builder: (BuildContext context, AsyncSnapshot<ScheduleState> snapshot) {
          ScheduleState model;
          model = snapshot.data ??
              new ScheduleState.error(snapshot.error is SocketException
                  ? (snapshot.error as SocketException).message
                  : snapshot.error.toString());

          return new Scaffold(
            resizeToAvoidBottomPadding: false,
              body: CardView(
            header: this.scheduleMode.getTitle(),
            iconHeader: this.scheduleMode.iconData,
            iconLeft: Icons.chevron_left,
            iconRight: Icons.chevron_right,
            onTapRight: () {
              this.scheduleMode.onTapRight();
              execute(this.scheduleMode.request..isRefresh = false);
            },
            onTapLeft: () {
              this.scheduleMode.onTapLeft();
              execute(this.scheduleMode.request..isRefresh = false);
            },
            onTapCenter: () {
              scheduleMode.onTapCenter(context, (valueSelected) {
                onTapCenter(valueSelected);
                execute(this.scheduleMode.request..isRefresh = false);
              });
            },
            child: buildWidgetByState(model),
          ));
        });
  }

  Widget buildWidgetByState(ScheduleState state) {
    if (state == null)
      return new SizedBox();
    else {
      if (state != null && state.result != null && state.result.result != null)
        this.data = state.result.result;

      if (state.loadingType == LoadingType.loading &&
          state.result == null) if (this.scheduleMode.request.isRefresh) {
        LoadingType type = LoadingType.refresh;
        return new Flexible(
            child: Column(
          children: <Widget>[
            ViewUtils.buildRefreshView(type),
            buildWidgetByResult(this.data)
          ],
        ));
      } else
        return new LoadingView(
          color: Singleton.setting.primaryColor,
        );
      else if (state.result != null) {
        switch (state.result.type) {
          case ResultType.resultEmpty:
            return new Flexible(
                child: ViewUtils.buildNothingScheduleView(() =>
                    execute(this.scheduleMode.request..isRefresh = true)));

          case ResultType.availableItems:
            return buildWidgetByResult(state.result.result);

          case ResultType.loginAgain:
            return new Flexible(
                child: ViewUtils.buildErrorView(state.result.message,
                    () => AlertUtils.showAlertRestart(context)));

          default:
            return new Flexible(
                child: ViewUtils.buildErrorView(
                    state.result.message,
                    () =>
                        execute(this.scheduleMode.request..isRefresh = true)));
        }
      } else
        return new SizedBox();
    }
  }

  Widget buildWidgetByResult(dynamic data) {
    if (data == null)
      return new SizedBox();
    else
      switch (scheduleMode.request.type) {
        case ScheduleType.day:
          return new ScheduleByDateListView(
              data);
        case ScheduleType.week:
          return new ScheduleByWeekListView(
              new GroupScheduleByDayList.from(data.list));
        case ScheduleType.subject:
          return new ScheduleBySubjectListView(
              data);
        default:
          return new SummaryScheduleListView(
              new GroupSummaryScheduleByDayList.from( data.list));
      }
  }

  onTapCenter(dynamic value) {
    switch (scheduleMode.request.type) {
      case ScheduleType.day:
        (scheduleMode.request as ScheduleDateRequest).date = value;
        break;
      case ScheduleType.week:
        (scheduleMode.request as ScheduleWeekRequest).date = value;
        Map<String, DateTime> map = MapUtils.getStartDateEndDateOfWeek(
            (scheduleMode.request as ScheduleWeekRequest).date);
        (scheduleMode.request as ScheduleWeekRequest).startDate = map['start'];
        (scheduleMode.request as ScheduleWeekRequest).endDate = map['end'];
        break;
      case ScheduleType.subject:
        (scheduleMode.request as ScheduleSubjectRequest).indexSelected = value;
        break;
      default:
        (scheduleMode.request as ScheduleSemesterRequest).indexSelected = value;
    }
  }
}
