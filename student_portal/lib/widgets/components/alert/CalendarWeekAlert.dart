import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class CalendarWeekAlert extends StatefulWidget {
  CalendarWeekAlert({
    Key key,
    @required this.initialDate,
    @required this.firstYear,
    @required this.lastYear,
    Color color,
    this.iconHeader,
    this.textTitle: '',
    this.textMessage: '',
    this.textPositiveButton: '',
    this.textNegativeButton: '',
    this.padding: 16.0,
    this.onPositiveTap,
  }):this.color= color ?? Singleton.setting.primaryColor;

  final DateTime initialDate;
  final int firstYear;
  final int lastYear;
  final Color color;
  final IconData iconHeader;
  final String textTitle;
  final String textMessage;
  final String textPositiveButton;
  final String textNegativeButton;
  final double padding;
  final ValueChanged<DateTime> onPositiveTap;

  @override
  CalendarWeekAlertState createState() => new CalendarWeekAlertState();
}

class CalendarWeekAlertState extends State<CalendarWeekAlert> {
  @override
  void initState() {
    super.initState();
    _selectedYear = widget.initialDate.year;
    _selectedMonth = widget.initialDate.month;
    _selectedDay = widget.initialDate.day;
    positiveButtonText = new TextView.text(widget.textPositiveButton,color: widget.color);
    positiveButton = new FlatButton(
      child: positiveButtonText,
      onPressed: _handleOk,
    );
    updateStringDate();
  }

  int _selectedYear;
  int _selectedMonth;
  int _selectedDay;
  String strDate;
  FlatButton positiveButton;
  TextView positiveButtonText;

  final String defaultStrDate = Singleton.setting.localization.invalid_date;

  void _vibrate() {
    switch (Theme.of(context).platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        HapticFeedback.vibrate();
        break;
      case TargetPlatform.iOS:
        break;
    }
  }

  void _handleYearChanged(num value) {
    if (value < widget.firstYear || value > widget.lastYear) return;
    _vibrate();
    setState(() {
      _selectedYear = value;
      updateStringDate();
    });
  }

  void _handleMonthChanged(num value) {
    if (value < 1 || value > 12) return;
    _vibrate();
    setState(() {
      _selectedMonth = value;
      updateStringDate();
    });
  }

  void _handleDayChanged(num value) {
    if (value < 1 || value > 31) return;
    _vibrate();
    setState(() {
      _selectedDay = value;
      updateStringDate();
    });
  }

  void updateStringDate() {
    if (isValidDate()) {
      strDate = StringUtils.getStringStartWeekOfWeek(
          new DateTime(_selectedYear, _selectedMonth, _selectedDay));
      enablePositiveButton();
    } else {
      strDate = defaultStrDate;
      disablePositiveButton();
    }
  }

  void enablePositiveButton() {
    setState(() {
      this.positiveButtonText = new TextView.text(widget.textPositiveButton, color: widget.color,);
      this.positiveButton =
          new FlatButton(onPressed: _handleOk, child: this.positiveButtonText);
    });
  }

  void disablePositiveButton() {
    setState(() {
      this.positiveButtonText = new TextView.text(widget.textPositiveButton, color: Singleton.disableColor);
      this.positiveButton =
          new FlatButton(onPressed: null, child: this.positiveButtonText);
    });
  }

  bool isValidDate() {
    try {
      DateTime dt = new DateTime(_selectedYear, _selectedMonth, _selectedDay);
      return (dt.year == _selectedYear &&
          dt.month == _selectedMonth &&
          dt.day == _selectedDay);
    } catch (e) {
      return false;
    }
  }

  void _handleCancel() {
    Navigator.pop(context);
  }

  void _handleOk() {
    Navigator.pop(
        context);
    widget.onPositiveTap(
        new DateTime(_selectedYear, _selectedMonth, _selectedDay));
  }

  @override
  Widget build(BuildContext context) {

    return new AlertDialog(
      titlePadding: new EdgeInsets.all(0.0),
      contentPadding:
          new EdgeInsets.only(top: widget.padding, bottom: widget.padding),
      title: new Container(
        padding: new EdgeInsets.all(widget.padding / 2),
        decoration: new BoxDecoration(color: widget.color),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Icon(
              widget.iconHeader,
              color: Singleton.textLightColor,
              size: 30.0,
            )
          ],
        ),
      ),
      actions: <Widget>[
        new FlatButton(
          child: new TextView.text(
            widget.textNegativeButton
          ),
          onPressed: _handleCancel,
        ),
        positiveButton,
      ],
      content: new SingleChildScrollView(
        child: new ListBody(
          children: <Widget>[
            new Container(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new TextView.alertTitle(
                    widget.textTitle
                  ),
                  new SizedBox(
                    height: 10.0,
                  ),
                  new TextView.alertText(
                    widget.textMessage
                  ),
                ],
              ),
              padding: new EdgeInsets.only(
                  bottom: widget.padding,
                  left: widget.padding,
                  right: widget.padding),
            ),
            new SizedBox(
              height: 10.0,
            ),

            //todo top line
            new Container(
              margin: new EdgeInsets.only(bottom: widget.padding),
              padding: new EdgeInsets.only(
                  right: widget.padding, left: widget.padding),
              decoration: new BoxDecoration(color: widget.color),
              child: new Row(
                children: <Widget>[
                  new SizedBox(
                    height: 0.5,
                  ),
                ],
              ),
            ),

            //todo box dialog selected date
            new Container(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new TextView.title(Singleton.setting.localization.year, color: widget.color,),
                      new NumberPicker.integer(
                        initialValue: _selectedYear,
                        minValue: widget.firstYear,
                        maxValue: widget.lastYear ?? new DateTime.now().year,
                        onChanged: _handleYearChanged,
                      ),
                    ],
                  ),
                  new Flexible(
                    child: new Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        new TextView.title(Singleton.setting.localization.month, color: widget.color,),
                        new NumberPicker.integer(
                          initialValue: _selectedMonth,
                          minValue: 1,
                          maxValue: 12,
                          onChanged: _handleMonthChanged,
                        ),
                      ],
                    ),
                  ),
                  new Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new TextView.title(Singleton.setting.localization.day, color: widget.color,),
                      new NumberPicker.integer(
                        initialValue: _selectedDay,
                        minValue: 1,
                        maxValue: 31,
                        onChanged: _handleDayChanged,
                      ),
                    ],
                  ),
                ],
              ),
              padding: new EdgeInsets.all(0.0),
            ),

            //todo bottom line
            new Container(
              margin: new EdgeInsets.only(
                  top: widget.padding, bottom: widget.padding),
              padding: new EdgeInsets.only(
                  right: widget.padding, left: widget.padding),
              decoration: new BoxDecoration(color: widget.color),
              child: new Row(
                children: <Widget>[
                  new SizedBox(
                    height: 0.5,
                  ),
                ],
              ),
            ),

            new Center(
              child: new TextView.title(
                strDate,color: widget.color,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// Shows a dialog containing a birth date picker.
///
/// The returned [Future] resolves to the date selected by the user when the
/// user closes the dialog. If the user cancels the dialog, null is returned.
///
/// See also:
///
///  * [showTimePicker]
///  * [showDatePicker]
///  * <https://material.google.com/components/pickers.html#pickers-date-pickers>
Future<DateTime> showCalendarWeekPicker({
  @required BuildContext context,
  @required DateTime initialDate,
  @required int firstYear,
  int lastYear,
  IconData iconHeader,
  String textTitle,
  String textMessage,
  String textPositiveButton,
  String textNegativeButton,
  double padding,
  ValueChanged<DateTime> onPositiveTap,
}) async {
  assert(lastYear == null || lastYear >= firstYear,
      'lastYear must be on or after firstYear');
  assert(initialDate.year >= firstYear, 'initialDate must be after firstYear');
  assert(lastYear == null || initialDate.year <= lastYear,
      'initialDate must be before lastYear');
  return await showDialog(
      context: context,
      child: new CalendarWeekAlert(
          initialDate: initialDate,
          firstYear: firstYear,
          lastYear: lastYear,
          iconHeader: iconHeader,
          textTitle: textTitle == null ? '' : textTitle,
          textMessage: textMessage == null ? '' : textMessage,
          textPositiveButton:
              textPositiveButton == null ? '' : textPositiveButton,
          textNegativeButton:
              textNegativeButton == null ? '' : textNegativeButton,
          padding: padding == null ? 16.0 : padding,
          onPositiveTap: onPositiveTap));
}
