import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/setting/SettingsFragment.dart';

// ignore: must_be_immutable
class HintSettingView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new HintSettingViewState();
  }
}

class HintSettingViewState extends State<HintSettingView> {
  bool isShowHint;

  @override
  void initState() {
    super.initState();
    isShowHint = (Singleton.currentFragment as SettingsFragment)
        .state
        .setting
        .isShowHint;
  }

  @override
  Widget build(BuildContext context) {
    List<String> subtitles = <String>[Singleton.setting.localization.on,  Singleton.setting.localization.off];
    return new ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: 2,
      itemBuilder: (BuildContext context, int index) {
        return new InkWell(
            onTap: () {
              setState(() {
                this.isShowHint = index == 0;
                (Singleton.currentFragment as SettingsFragment)
                    .state
                    .setting
                    .isShowHint = this.isShowHint;
              });
            },
            child: new Column(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(8.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Flexible(
                        child: new Align(
                          child: new TextView.text(
                            subtitles[index],
                            overflow: TextOverflow.ellipsis,
                          ),
                          alignment: Alignment.centerLeft,
                        ),
                      ),
                      new Align(
                        child: new Icon(
                          ((isShowHint && index == 0) ||
                                  (!isShowHint && index == 1))
                              ? Icons.radio_button_checked
                              : Icons.radio_button_unchecked,
                          color: Singleton.setting.primaryColor,
                        ),
                      )
                    ],
                  ),
                ),
                index < subtitles.length - 1
                    ? new Divider(
                        height: 5.0,
                      )
                    : new SizedBox()
              ],
            ));
      },
    );
  }
}
