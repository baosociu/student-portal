import 'package:student_portal/global/utils/DateTimeUtils.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/models/objects/Schedule.dart';

class SemesterScore {
  String name;
  dynamic idStudent;
  String codeSemester;
  double avgSemester;
  double weight;
  dynamic idSemester;
  double sumScore;

  SemesterScore.fromJson(Map json) {
    this.name = json['tenhk'];
    this.idStudent = json['hocVienId'];
    this.codeSemester = json['mahk'];
    this.avgSemester = json['diemTBHK'];
    this.weight = json['soTinChi'];
    this.idSemester = json['hocKyId'];
    this.sumScore = json['tongDIem'];
  }

  Map<String, dynamic> toJson() => {
        'tenhk': this.name,
        'hocVienId': this.idStudent,
        'mahk': this.codeSemester,
        'diemTBHK': this.avgSemester,
        'soTinChi': this.weight,
        'hocKyId': this.idSemester,
        'tongDIem': this.sumScore,
      };
}

class SemesterScoreList{
  List<SemesterScore> list = new List<SemesterScore>();

  SemesterScoreList.fromJson(dynamic listItems){
    list = new List<SemesterScore>();

    for(Map value in listItems){
      SemesterScore semesterScore = new SemesterScore.fromJson(value);
      this.list.add(semesterScore);
    }
  }
}

class SemesterSchedule extends BaseHeaderSchedule{
  DateTime start;
  String code;
  int countWeek;
  int year;
  DateTime expiredPayment;
  DateTime end;

  SemesterSchedule.fromJson(Map json){
    this.start = DateTimeUtils.convertStringToDateTime(json['ngayBatDau'], 'ddMMyyyy') ;
    this.end = DateTimeUtils.convertStringToDateTime(json['ngayKetThuc'], 'ddMMyyyy') ;
    this.code = json['ma'];
    this.id = json['id'];
    this.name = json['ten'];
    this.countWeek = json['tongTuanHoc'];
    this.year = json['namHoc'];
    this.expiredPayment = DateTimeUtils.convertStringToDateTime(json['ngayHetHanHocPhi'], 'ddMMyyyy') ;
  }

  Map<String,dynamic> toJson() => {
    'ngayBatDau': StringUtils.convertDateTimeToString(this.start, "ddMMyyyy") ,
    'ngayKetThuc': StringUtils.convertDateTimeToString(this.end, "ddMMyyyy") ,
    'ma': this.code,
    'id': this.id,
    'ten': this.name,
    'tongTuanHoc': this.countWeek,
    'namHoc': this.year,
    'ngayHetHanHocPhi': StringUtils.convertDateTimeToString(this.expiredPayment, "ddMMyyyy")
  };
}

class SemesterScheduleList{
  List<SemesterSchedule> list = new List<SemesterSchedule>();

  SemesterScheduleList.fromJson(dynamic listItems){
    list = new List<SemesterSchedule>();
    listItems.forEach((value){
      SemesterSchedule semesterSchedule = new SemesterSchedule.fromJson(value);
      this.list.add(semesterSchedule);
    });
  }
}