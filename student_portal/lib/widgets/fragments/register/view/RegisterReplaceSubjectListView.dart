import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/register/RegisterRequest.dart';
import 'package:student_portal/global/service/register/RegisterState.dart';
import 'package:student_portal/global/service/register/SubjectRegister.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class RegisterReplaceSubjectListView extends StatefulWidget {
  final List<SubjectReplace> subjectReplaces;
  final RegisterState registerState;
  final RegisterRequest registerRequest;

  const RegisterReplaceSubjectListView(
      this.subjectReplaces, this.registerState, this.registerRequest,
      {Key key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new RegisterReplaceSubjectListViewState();
  }
}

class RegisterReplaceSubjectListViewState
    extends State<RegisterReplaceSubjectListView> {
  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;

    return new ListView.builder(
        shrinkWrap: true,
        itemCount: widget.subjectReplaces.length,
        itemBuilder: (context, index) {
          SubjectReplace item = widget.subjectReplaces[index];

          return new InkWell(
              child: new Container(
                  child: new Column(
            children: <Widget>[
              buildReplaceSubjectItem(item),
              index < widget.subjectReplaces.length - 1
                  ? new Divider(color: Singleton.setting.primaryColor,)
                  : new SizedBox()
            ],
          )));
        });
  }

  Widget buildReplaceSubjectItem(SubjectReplace item) {
    return new Padding(
      padding: new EdgeInsets.all(8.0),
      child: new ListBody(
        children: <Widget>[
          item.subject1.isUnknown
              ? new SizedBox()
              : buildDetailReplaceSubjectItem(item.subject1),
          item.subject2.isUnknown
              ? new SizedBox()
              : buildDetailReplaceSubjectItem(item.subject2),
          new Padding(
            padding: new EdgeInsets.only(left: 72.0, right: 72.0),
            child: Divider(),
          ),
          new Align(
            child:
                new TextView.text(Singleton.setting.localization.replaced_by),
          ),
          new Padding(
            padding: new EdgeInsets.only(left: 72.0, right: 72.0),
            child: Divider(),
          ),
          item.subjectReplace1.isUnknown
              ? new SizedBox()
              : buildDetailReplaceSubjectItem(item.subjectReplace1,
                  isReplace: true),
          item.subjectReplace2.isUnknown
              ? new SizedBox()
              : buildDetailReplaceSubjectItem(item.subjectReplace2,
                  isReplace: true),
        ],
      ),
    );
  }

  Widget buildDetailReplaceSubjectItem(BaseSubject item,
      {bool isReplace = false}) {
    Alignment alignment =
        isReplace ? Alignment.centerLeft : Alignment.centerRight;
    CrossAxisAlignment crossAxisAlignment =
        isReplace ? CrossAxisAlignment.end : CrossAxisAlignment.start;
    MainAxisAlignment mainAxisAlignment =
        isReplace ? MainAxisAlignment.end : MainAxisAlignment.start;

    return new Align(
      child: new Padding(
        padding: new EdgeInsets.only(
            left: isReplace ? 16.0 : 0.0, right: isReplace ? 0.0 : 16.0),
        child: new Column(
          crossAxisAlignment: crossAxisAlignment,
          children: <Widget>[
            new Row(
              mainAxisAlignment: mainAxisAlignment,
              children: <Widget>[
                isReplace
                    ? new Icon(
                        Icons.check,
                        color: Singleton.checkColor,
                      )
                    : new SizedBox(),
                new Flexible(
                    child: new TextView.title(
                  item.nameSubject.toString(),
                  textSize: TextView.LAGRE_SIZE,
                  color: isReplace
                      ? Singleton.setting.primaryColor
                      : Singleton.hintColor,
                  fontWeight: FontWeight.bold,
                  overflow: TextOverflow.ellipsis,
                ))
              ],
            ),
            new TextView.text(Singleton.setting.localization.code_subject +
                ": " +
                item.codeSubject.toString())
          ],
        ),
      ),
      alignment: alignment,
    );
  }
}
