import 'package:student_portal/models/objects/Faculty.dart';
import 'package:student_portal/models/objects/TrainingSystem.dart';

//todo Major: ngành đào tạo
class Major {
  dynamic id;
  String code;
  String name;
  String combinationSubject; //tổ hợp thi
  Faculty faculty;
  TrainingSystem trainingSystem;
  String nameEnglish;

  Major.fromJson(Map json) {
    this.id = json['id'];
    this.code = json['ma'];
    this.name = json['ten'];
    this.combinationSubject = json['khoiThi'];
    this.faculty = new Faculty.fromJson(json['khoaId']);
    this.trainingSystem = new TrainingSystem.fromJson(json['heDaoTaoId']);
    this.nameEnglish = json['tenTA'];
  }

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'ma': this.code,
        'ten': this.name,
        'khoiThi': this.combinationSubject,
        'khoaId': this.faculty,
        'heDaoTaoId': this.trainingSystem,
        'tenTA': this.nameEnglish,
      };
}
