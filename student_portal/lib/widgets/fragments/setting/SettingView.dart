import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/fragments/setting/BackgroundSettingView.dart';
import 'package:student_portal/widgets/fragments/setting/ColorSettingView.dart';
import 'package:student_portal/widgets/fragments/setting/FontSettingView.dart';
import 'package:student_portal/widgets/fragments/setting/HintSettingView.dart';
import 'package:student_portal/widgets/fragments/setting/LanguageSettingView.dart';

enum SettingType { color, background, font, language, hint }

class SettingView extends StatelessWidget {
  final SettingType type;
  final bool isExpanded;
  final String title;
  final IconData icon;
  final Function onTap;

  const SettingView(
      this.type, this.isExpanded, this.title, this.onTap, this.icon,
      {Key key})
      : super(key: key);

  factory SettingView.color({bool isExpanded, Function onTap}) {
    return SettingView(SettingType.color, isExpanded ?? false,
        Singleton.setting.localization.color, onTap, Icons.format_color_fill);
  }

  factory SettingView.background({bool isExpanded, Function onTap}) {
    return SettingView(SettingType.background, isExpanded ?? false,
        Singleton.setting.localization.theme, onTap, Icons.image);
  }

  factory SettingView.font({bool isExpanded, Function onTap}) {
    return SettingView(SettingType.font, isExpanded ?? false,
        Singleton.setting.localization.font, onTap, Icons.font_download);
  }

  factory SettingView.language({bool isExpanded, Function onTap}) {
    return SettingView(SettingType.language, isExpanded ?? false,
        Singleton.setting.localization.language, onTap, Icons.language);
  }

  factory SettingView.hint({bool isExpanded, Function onTap}) {
    return SettingView(SettingType.hint, isExpanded ?? false,
        Singleton.setting.localization.hint, onTap, Icons.question_answer);
  }

  @override
  Widget build(BuildContext context) {
    Widget _widget;
    _widget = buildWidgetByType();
    return _widget;
  }

  Widget buildWidgetByType() {
    Widget widget = new InkWell(
      onTap: () => onTap(),
      child: new Padding(
        padding:
            new EdgeInsets.only(top: 8.0, bottom: 8.0, left: 12.0, right: 12.0),
        child: new Column(
          children: <Widget>[
            //todo title
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Flexible(
                  child: new Row(
                    children: <Widget>[
                      new Icon(
                        icon,
                        color:
                            isExpanded ? Singleton.setting.primaryColor : null,
                      ),
                      new SizedBox(
                        width: 10.0,
                      ),
                      new TextView.text(
                        title,
                        textSize: isExpanded ? TextView.MEDIUM_SIZE : null,
                        color:
                            isExpanded ? Singleton.setting.primaryColor : null,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                new Icon(
                  isExpanded ? Icons.keyboard_arrow_down : Icons.chevron_right,
                  color: isExpanded ? Singleton.setting.primaryColor : null,
                ),
              ],
            ),
            //todo sublist
            isExpanded
                ? new Padding(
                    padding: new EdgeInsets.only(left: 12.0),
                    child: buildChildWidget(type),
                  )
                : new SizedBox()
          ],
        ),
      ),
    );
    return widget;
  }

  Widget buildChildWidget(SettingType type) {
    switch (type) {
      case SettingType.background:
        return new BackgroundSettingView();
      case SettingType.color:
        return new ColorSettingView();
      case SettingType.language:
        return new LanguageSettingView();
      case SettingType.hint:
        return new HintSettingView();
      default:
        return new FontSettingView();
    }
  }
}
