import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/service/register/BaseRegister.dart';
import 'package:student_portal/global/service/register/RegisterResult.dart';
import 'package:student_portal/global/service/register/SubjectRegister.dart';

class RegisterState {
  final RegisterResult result;
  final LoadingType loadingType;

  RegisterState({this.result, this.loadingType});

  factory RegisterState.refresh(List<BaseSubject> items, TypeRegister type) =>
      new RegisterState(
          loadingType: LoadingType.refresh,
          result: new RegisterResult.fromItems(items,type));

  factory RegisterState.loadMore(List<BaseSubject> items, TypeRegister type) =>
      new RegisterState(
          loadingType: LoadingType.loadMore,
          result: new RegisterResult.fromItems(items,type));

  factory RegisterState.change(List<BaseSubject> items, TypeRegister type) =>
      new RegisterState(
          loadingType: LoadingType.change,
          result: new RegisterResult.fromItems(items,type));

  factory RegisterState.init() =>
      new RegisterState(result: new RegisterResult.init(), loadingType: LoadingType.loading);

  factory RegisterState.loading() =>
      new RegisterState(loadingType: LoadingType.loading);

  factory RegisterState.error(String message) =>
      new RegisterState(result: RegisterResult.hasError(message));
}
