import 'package:flutter/material.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/register/BaseRegister.dart';
import 'package:student_portal/global/utils/AlertUtils.dart';
import 'package:student_portal/global/utils/FadeRoute.dart';
import 'package:student_portal/global/utils/ImageProviderUtils.dart';
import 'package:student_portal/global/utils/StringUtils.dart';
import 'package:student_portal/models/objects/Student.dart';
import 'package:student_portal/widgets/components/MenuItem.dart';
import 'package:student_portal/widgets/components/TextView.dart';
import 'package:student_portal/widgets/components/UserAccountDrawerHeader.dart';
import 'package:student_portal/widgets/fragments/BaseFragment.dart';
import 'package:student_portal/widgets/fragments/coursecategory/CourseCategoryFragment.dart';
import 'package:student_portal/widgets/fragments/fee/FeeFragment.dart';
import 'package:student_portal/widgets/fragments/home/HomeFragment.dart';
import 'package:student_portal/widgets/fragments/notification/NotificationFragment.dart';
import 'package:student_portal/widgets/fragments/register/RegisterFragment.dart';
import 'package:student_portal/widgets/fragments/result/ResultRegisterFragment.dart';
import 'package:student_portal/widgets/fragments/schedule/ScheduleFragment.dart';
import 'package:student_portal/widgets/fragments/score/ScoreFragment.dart';
import 'package:student_portal/widgets/fragments/setting/SettingsFragment.dart';
import 'package:student_portal/widgets/fragments/suggest/SuggestFragment.dart';

// ignore: must_be_immutable
class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);
  MainPageState state;

  @override
  MainPageState createState() {
    state = new MainPageState();
    return state;
  }
}

class MainPageState extends State<MainPage> {
  BaseFragment fragment;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fragment = new HomeFragment(this.widget);
    Singleton.currentFragment = fragment;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: buildAppBar(),
      drawer: new Drawer(child: buildDrawer()),
      //todo fragments
      body: this.fragment,
    );
  }

  Widget buildAppBar() {
    double sizeIcon = 28.0;
    return new AppBar(
      titleSpacing: 0.0,
      title: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Padding(
            child: new Container(
                child: new SizedBox(
                  height: sizeIcon,
                  width: sizeIcon,
                ),
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                        image: new AssetImage(this.fragment.iconPath)))),
            padding: new EdgeInsets.only(right: 8.0),
          ),
          new Flexible(
              child: new TextView.appbar(
            this.fragment.title,
            overflow: TextOverflow.ellipsis,
          ))
        ],
      ),
      backgroundColor: Singleton.setting.primaryColor,
      actions: buildAppBarActions(this.fragment.id),
    );
  }

  List<Widget> buildAppBarActions(int id) {
    switch (id) {
      case Singleton.REGISTER_FRAGMENT_ID:
        return <Widget>[
          new IconButton(
              icon: new Icon(Icons.shopping_cart),
              onPressed: () async {
                if (Singleton.currentFragment is RegisterFragment) {
                  BaseRegister semester;
                  try {
                    semester = (Singleton.currentFragment as RegisterFragment)
                        .state
                        .content
                        .myState
                        .request
                          .semester;
                      } catch (e) {}
                          if (semester == null) return;

                  final bool requireReload =
                  await Navigator.of(context).push(new FadeRoute(
                          builder: (context) => new ResultRegisterFragment(
                                idSemester: semester.id.toString(),
                              )));

                  if (requireReload &&
                      Singleton.currentFragment is RegisterFragment) {
                    (Singleton.currentFragment as RegisterFragment).reload();
                  }
                }
              }),
          new IconButton(
              icon: new Icon(Icons.sort),
              onPressed: () async {
                if (Singleton.currentFragment is RegisterFragment) {
                  BaseRegister semester =
                      (Singleton.currentFragment as RegisterFragment)
                          .state
                          .content
                          .myState
                          .request
                          .semester;
                  if (semester == null) return;
                  Navigator.of(context).push(new FadeRoute(
                      builder: (context) => new SuggestFragment(
                            idSemester: semester.id.toString(),
                          )));
                }
              })
        ];
      case Singleton.SCHEDULE_FRAGMENT_ID:
        return <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              onPressed: () async {
                if (Singleton.currentFragment is ScheduleFragment) {
                  (Singleton.currentFragment as ScheduleFragment).refresh();
                }
              })
        ];
      case Singleton.COURSE_CATEGORY_FRAGMENT_ID:
        return <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              onPressed: () async {
                if (Singleton.currentFragment is CourseCategoryFragment) {
                  (Singleton.currentFragment as CourseCategoryFragment)
                      .refresh();
                }
              })
        ];
      case Singleton.FEE_FRAGMENT_ID:
        return <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              onPressed: () async {
                if (Singleton.currentFragment is FeeFragment) {
                  (Singleton.currentFragment as FeeFragment).refresh();
                }
              })
        ];
      case Singleton.SCORE_FRAGMENT_ID:
        return <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              onPressed: () async {
                if (Singleton.currentFragment is ScoreFragment) {
                  (Singleton.currentFragment as ScoreFragment).refresh();
                }
              })
        ];
      case Singleton.NOTIFICATION_FRAGMENT_ID:
        return <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              onPressed: () async {
                if (Singleton.currentFragment is NotificationFragment) {
                  (Singleton.currentFragment as NotificationFragment)
                      .onRefresh();
                }
              })
        ];
      case Singleton.SETTINGS_FRAGMENT_ID:
        return <Widget>[
          new InkWell(
            child: new Center(
              child: new Padding(
                  padding: new EdgeInsets.only(right: 12.0, left: 12.0),
                  child: new TextView.text(
                    Singleton.setting.localization.save,
                    theme: TypeTheme.light,
                    textSize: TextView.MEDIUM_SIZE,
                  )),
            ),
            onTap: () => (this.fragment as SettingsFragment).onSave(),
          ),
        ];
      default:
        return <Widget>[];
    }
  }

  //todo build appbar register

  switchFragments(StatefulWidget widget) {
    if (widget.runtimeType == this.fragment.runtimeType) return;

    if (!mounted) return;

    setState(() {
      this.fragment = widget;
      Singleton.currentFragment = fragment;
    });
  }

  Widget buildDrawer() {
    Student student;
    ImageProvider avatar;
    String email = '';
    String name = '';

    if (Singleton.student != null) {
      student = Singleton.student;
      avatar = ImageProviderUtils.getAvatarBase64(
          student.avatar, student.gender, Singleton.setting.primaryColor);
      email = StringUtils.getString(student.email);
      name = StringUtils.getString(student.lastName) +
          " " +
          StringUtils.getString(student.firstName);
    }

    return new ListView(
      children: <Widget>[
        new UserAccountsDrawerHeaderBlur(
            currentAccountPicture: new InkWell(
              onTap: () {
                Navigator.of(context).pop();
                switchFragments(new HomeFragment(fragment.mainPage));
              },
              child: new CircleAvatar(backgroundImage: avatar),
            ),
            decoration: new BoxDecoration(
                image: new DecorationImage(
              colorFilter: new ColorFilter.mode(
                  Singleton.dark80OpacityColor, BlendMode.dstATop),
              fit: BoxFit.cover,
              image: new Image.asset(Singleton.setting.background).image,
            )),
            accountName: new TextView.text(
              name,
              fontWeight: FontWeight.w700,
              color: Colors.white,
            ),
            accountEmail: new TextView.text(
              email,
              color: Colors.white,
            )),
        //todo home
        new MenuItem(
          Singleton.setting.localization.home_fragment_title,
          iconPath: "resources/icons/home.png",
          isSelected: this.fragment.id == Singleton.HOME_FRAGMENT_ID,
          onTap: () {
            Navigator.of(context).pop();
            switchFragments(new HomeFragment(fragment.mainPage));
          },
        ),

        //todo notification
        new MenuItem(
          Singleton.setting.localization.notification_fragment_title,
          iconPath: "resources/icons/notification.png",
          isSelected: this.fragment.id == Singleton.NOTIFICATION_FRAGMENT_ID,
          onTap: () {
            Navigator.of(context).pop();
            switchFragments(new NotificationFragment(fragment.mainPage));
          },
        ),

        //todo debt
        new MenuItem(
          Singleton.setting.localization.fee_fragment_title,
          iconPath: "resources/icons/fee.png",
          isSelected: this.fragment.id == Singleton.FEE_FRAGMENT_ID,
          onTap: () {
            Navigator.of(context).pop();
            switchFragments(new FeeFragment(fragment.mainPage));
          },
        ),

        //todo schedule
        new MenuItem(
          Singleton.setting.localization.schedule_fragment_title,
          iconPath: "resources/icons/schedule.png",
          isSelected: this.fragment.id == Singleton.SCHEDULE_FRAGMENT_ID,
          onTap: () {
            Navigator.of(context).pop();
            switchFragments(new ScheduleFragment(fragment.mainPage));
          },
        ),

        //todo score
        new MenuItem(
          Singleton.setting.localization.score_fragment_title,
          iconPath: "resources/icons/score.png",
          isSelected: this.fragment.id == Singleton.SCORE_FRAGMENT_ID,
          onTap: () {
            Navigator.of(context).pop();
            switchFragments(new ScoreFragment(fragment.mainPage));
          },
        ),

        //todo CCourse category
        new MenuItem(
          Singleton.setting.localization.course_category_fragment_title,
          iconPath: "resources/icons/course_category.png",
          isSelected: this.fragment.id == Singleton.COURSE_CATEGORY_FRAGMENT_ID,
          onTap: () {
            Navigator.of(context).pop();
            switchFragments(new CourseCategoryFragment(fragment.mainPage));
          },
        ),

        //todo Course registration
        new MenuItem(
          Singleton.setting.localization.register_fragment_title,
          iconPath: "resources/icons/register.png",
          isSelected: this.fragment.id == Singleton.REGISTER_FRAGMENT_ID,
          onTap: () {
            Navigator.of(context).pop();
            switchFragments(new RegisterFragment(fragment.mainPage));
          },
        ),

        new Divider(),

        new MenuItem(
          Singleton.setting.localization.settings_fragment_title,
          iconPath: "resources/icons/settings.png",
          isSelected: this.fragment.id == Singleton.SETTINGS_FRAGMENT_ID,
          onTap: () {
            Navigator.of(context).pop();
            switchFragments(new SettingsFragment(fragment.mainPage));
          },
        ),

        new MenuItem(
          Singleton.setting.localization.logout,
          onTap: () {
            Navigator.of(context).pop();
            AlertUtils.showAlertLogout(context);
          },
        ),
      ],
    );
  }
}
