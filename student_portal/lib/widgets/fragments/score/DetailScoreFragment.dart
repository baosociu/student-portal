import 'package:flutter/material.dart';
import 'package:student_portal/enums/Enum.dart';
import 'package:student_portal/global/ServerResponse.dart';
import 'package:student_portal/global/Singleton.dart';
import 'package:student_portal/global/service/API.dart';
import 'package:student_portal/global/utils/CircleUtils.dart';
import 'package:student_portal/global/utils/ToastUtils.dart';
import 'package:student_portal/global/utils/ViewUtils.dart';
import 'package:student_portal/models/objects/Score.dart';
import 'package:student_portal/widgets/components/TextView.dart';

class DetailScoreFragment extends StatefulWidget {
  final String title;
  final int idSemester;

  DetailScoreFragment(this.idSemester, this.title, {Key key});

  @override
  DetailScoreFragmentState createState() =>
      new DetailScoreFragmentState(this.idSemester, this.title);
}

class DetailScoreFragmentState extends State<DetailScoreFragment> {
  final String title;
  final int _idSemester;
  ServerResponse<ScoreList> response;
  BuildContext _context;
  String idStudent;
  String idSemester;

  bool isRefresh;

  DetailScoreFragmentState(this._idSemester, this.title);
  @override
  void initState() {
    super.initState();
    idStudent = Singleton.student.id.toString();
    idSemester = this._idSemester.toString();
    isRefresh = false;

    getScoreBySemester(idStudent, idSemester);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new TextView.appbar(this.title),
        backgroundColor: Singleton.setting.primaryColor,
      ),
      body: new Builder(builder: (context){
        _context = context;
        return new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              isRefresh
                  ? ViewUtils.buildRefreshView(LoadingType.refresh)
                  : new SizedBox(),
              response == null
                  ? ViewUtils.buildLoadingCardView()
                  : buildScoreList(),
            ],
          ),
        );
      })
    );
  }

  Widget buildScoreList() {
    return response.buildCardViewByResponse<Score>(
        context: context,
        list: response.data.list,
        callback: () => getScoreBySemester(idStudent, idSemester),
        emptyView: ViewUtils.buildNothingSemesterCardView,
        item: (index, item) {
          ScoreType type = ScoreType.none;
          if (item.totalScore >= 4.0) type = ScoreType.warning;
          if (item.totalScore >= 6.0) type = ScoreType.passed;

          return new Stack(
            children: <Widget>[
              new Column(children: <Widget>[
                new ListTile(
                    title: buildTitle(item), subtitle: buildSubtitle(item)),
                index < response.data.list.length - 1
                    ? new Divider(color: Singleton.setting.primaryColor)
                    : new SizedBox()
              ]),
              buildMark(type)
            ],
          );
        });
  }

  Widget buildMark(ScoreType type) {
    switch (type) {
      case ScoreType.passed:
        return buildPassedMark();
      case ScoreType.warning:
        return buildWarningMark();
      default:
        return new SizedBox();
    }
  }

  Widget buildTitle(Score item) {
    Color color = Singleton.setting.primaryColor;
    Color color20 = Singleton.setting.primary20OpacityColor;

    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Flexible(
            child: new Row(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(4.0),
                  child: new CircleAvatar(
                    child: new TextView.leading(
                      item.totalScore.toString(),
                      color: Singleton.textLightColor,
                      fontWeight: FontWeight.bold,
                    ),
                    backgroundColor: color,
                  ),
                ),
                new Flexible(
                    child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new TextView.title(
                      item.nameSubject.toString(),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )),
              ],
            ),
          ),
        ],
      ),
      decoration: new BoxDecoration(
          gradient: new LinearGradient(
              tileMode: TileMode.clamp,
              colors: <Color>[
            Colors.transparent,
            color20,
            color20,
            Colors.transparent
          ])),
    );
  }

  Widget buildSubtitle(Score item) {
    Widget chart = CircleUtils.buildPieScoreChart(
        item.processRate, item.middleRate, item.finalRate);

    return new Stack(
      children: <Widget>[
        new Container(
          margin: new EdgeInsets.only(left: 24.0),
          decoration: new BoxDecoration(
              gradient: new RadialGradient(
                  radius: 1.0,
                  center: Alignment.bottomLeft,
                  colors: <Color>[
                Singleton.setting.secondColor,
                Singleton.backgroundNothing
              ])),
          child: new Container(
            margin: new EdgeInsets.only(left: 4.0, bottom: 4.0),
            color: Singleton.backgroundNothing,
            child: new Padding(
                padding: new EdgeInsets.only(left: 8.0),
                child: buildDetailScore(item)),
          ),
        ),
        new Align(
          alignment: Alignment.centerRight,
          child: new InkWell(
            child: chart,
            onTap: () {
              String message = item.nameSubject+"\n";
              message += "  -"+Singleton.setting.localization.progress_rate +
                  ": " +
                  item.processRate.toString() +
                  "%\n";
              message += "  -"+Singleton.setting.localization.middle_rate +
                  ": " +
                  item.middleRate.toString() +
                  "%\n";
              message += "  -"+Singleton.setting.localization.final_rate +
                  ": " +
                  item.finalRate.toString()+"%";
              ToastUtils.showToast(_context, message);
            },
          ),
        )
      ],
    );
  }

  Widget buildDetailScore(Score item) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.credit + ": ",
            item.weight.toString(),
            rightFlex: 2,
            leftFlex: 2),
        ViewUtils.buildItemTextView(
          Singleton.setting.localization.attendance_score + ": ",
          item.processScore.toString(),
          rightFlex: 2,
          leftFlex: 2,
        ),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.middle_exam_score + ": ",
            item.middleScore.toString(),
            rightFlex: 2,
            leftFlex: 2),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.final_exam_score + ": ",
            item.finalScore.toString(),
            rightFlex: 2,
            leftFlex: 2),
        ViewUtils.buildItemTextView(
            Singleton.setting.localization.total_score + ": ",
            item.totalScore.toString(),
            fontWeight: FontWeight.bold,
            rightFlex: 2,
            leftFlex: 2),
      ],
    );
  }

  Widget buildPassedMark() {
    double width = MediaQuery.of(context).size.width;
    double widthImage = width;
    double heightImage = width / 5;
    return new Container(
      child: new SizedBox(height: heightImage, width: widthImage),
      decoration: new BoxDecoration(
        image: new DecorationImage(
            image: new AssetImage(Singleton.setting.localization.image_passed),
            fit: BoxFit.contain,
            alignment: Alignment.centerRight),
      ),
    );
  }

  Widget buildWarningMark() {
    double width = MediaQuery.of(context).size.width;
    double widthImage = width;
    double heightImage = width / 5;
    return new Container(
      child: new SizedBox(height: heightImage, width: widthImage),
      decoration: new BoxDecoration(
        image: new DecorationImage(
            image: new AssetImage(Singleton.setting.localization.image_warning),
            fit: BoxFit.contain,
            alignment: Alignment.centerRight),
      ),
    );
  }

  getScoreBySemester(String idStudent, String idSemester) async {
    if (!isRefresh)
      setState(() {
        this.response = null;
      });

    final ServerResponse<ScoreList> response =
        await API.infoScoreBySemester<ScoreList>(idStudent, idSemester, (data) {
      return new ScoreList.fromJson(data);
    }, () {
      return new ScoreList.fromJson(new List<Map>());
    });
    if (!this.mounted) return;

    setState(() {
      this.response = response;
      this.isRefresh = false;
    });
  }
}
