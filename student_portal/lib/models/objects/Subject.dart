class SubjectSchedule {
  int countTime;
  String notes;
  String typeRoom;
  double rateFinal;
  double weightPractice;
  double weight;
  double rateProgress;
  int countSession;
  String code;
  double rateMiddle;
  dynamic id;
  String name;
  double weightTheory;
  dynamic idClassSubject;

  SubjectSchedule.fromJson(Map json) {
    this.countTime = json['soTietHoc'];
    this.notes = json['ghiChu'];
    this.typeRoom = json['loaiPhong'];
    this.rateFinal = json['phanTramDanhGiaCK'];
    this.weightPractice = json['soTinChiTH'];
    this.weight = json['soTinChi'];
    this.rateProgress = json['phanTramChuyenCan'];
    this.countSession = json['soBuoi'];
    this.code = json['maMonHoc'];
    this.rateMiddle = json['phanTramDanhGiaGK'];
    this.id = json['id'];
    this.name = json['ten'];
    this.weightTheory = json['soTinChiLT'];
    this.idClassSubject = json['lopMonHocId'];
  }

  Map<String, dynamic> toJson() => {
        'soTietHoc': this.countTime,
        'ghiChu': this.notes,
        'loaiPhong': this.typeRoom,
        'phanTramDanhGiaCK': this.rateFinal,
        'soTinChiTH': this.weightPractice,
        'soTinChi': this.weight,
        'phanTramChuyenCan': this.rateProgress,
        'soBuoi': this.countSession,
        'maMonHoc': this.code,
        'phanTramDanhGiaGK': this.rateMiddle,
        'id': this.id,
        'ten': this.name,
        'soTinChiLT': this.weightTheory,
        'lopMonHocId': this.idClassSubject,
      };
}

class SubjectScheduleList {
  List<SubjectSchedule> list = new List<SubjectSchedule>();

  SubjectScheduleList.fromJson(dynamic data) {
    list = new List<SubjectSchedule>();
    data.forEach((json) {
      SubjectSchedule subjectSchedule = new SubjectSchedule.fromJson(json);
      list.add(subjectSchedule);
    });
  }
}
